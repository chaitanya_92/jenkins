package test_definition.flybe.lf_two.booking;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class RegBooking {




    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightFromItalyWithAdultsOneWay() {
        Hashtable testData = new DataUtility().getTestData("105");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightFromItalyWithAdultsTwoWay() {
        Hashtable testData = new DataUtility().getTestData("106");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        selectedOverallFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOverallFlightDetails, passengerNames, 4, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightToItalyWithAdultsOneWay() {
        Hashtable testData = new DataUtility().getTestData("107");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOutboundFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightToItalyWithAdultsTwoWay() {
        Hashtable testData = new DataUtility().getTestData("108");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        selectedOverallFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOverallFlightDetails, passengerNames, 4, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_3688_verifyBookingLoganAirDirectFlightUKToUkWithJustFlySelectingSeatAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("111");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);

    }

    @Test(groups = {"regression", "p1"})
    public void TST_3691_verifyBookingLoganAirDirectFlightUKToUkWithGetMoreSelectingSeatAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("112");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3686_verifyBookingStobartDirectFlightUKToUkWithJustFlySelectingSeatAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("113");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3457_verifyContactUSLinkNavigatesToSupportPage() {
        Hashtable testData = new DataUtility().getTestData("115");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.navigateToSupportPage();
        basePage.support.verifyPageTitle();

    }

    @Test(groups = {"regression", "p3"})
    public void TST_3702_verifyClickingAddApiLinkDisplaysTheAdvancedPassengerInformationPage() {
        Hashtable testData = new DataUtility().getTestData("129");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
        basePage.bookingConfirmation.ContinueToCheckInThroughAddAPILink();
        basePage.leapCheckIn.verifyAddApiInfoIsDisplayed(true);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3654_verifyBookingWithIncorrectMandatoryFieldsInPaymentDisplaysError() {
        Hashtable testData = new DataUtility().getTestData("130");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.payment.verifyValidationForIncorrectPaymentDetails();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3653_verifyBookingWithoutMandatoryFieldsInPaymentDisplaysError() {
        Hashtable testData = new DataUtility().getTestData("130");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.selectCardTypeOption(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.verifyValidationForBlankMandatoryFields();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_5523_verifyUserCanFillUpAddressFieldsUsingPostCodeLookUp() {
        Hashtable testData = new DataUtility().getTestData("131");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.verifyAddressIsPopulatedAfterPostCodeIsSelected();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_5500_verifySeatSelectionPaneIsDisplayedForUser() {
        Hashtable testData = new DataUtility().getTestData("132");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.verifySeatSelectionPaneIsDisplayed();

    }

    @Test(groups = {"regression", "p1",})
    public void TST_3679_verifyPassengerInformationInBookingConfirmationWithOneAdultOneTeenUKToNonUKJustFlySelectingSeatAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("134");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3680_verifyPassengerInformationInBookingConfirmationWithOneAdultOneInfantUKToUKJustFlySelectingSeatAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("133");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3681_verifyPassengerInformationInBookingConfirmationWithOneAdultUKToNonUKGetMoreSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("135");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3682_verifyPassengerInformationInBookingConfirmationWithThreeAdultUKToUKGetMoreSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("136");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3683_verifyPassengerInformationInBookingConfirmationWithTwoAdultNonUKToUKAllInSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("137");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3684_verifyPassengerInformationInBookingConfirmationWithOneAdultOneChildOneInfantUKToUKAllInSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("138");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3685_verifyPassengerInformationInBookingConfirmationAirFranceWithTwoAdultOneChildUKToNonUKJustFlySelectingLuggage() {
        Hashtable testData = new DataUtility().getTestData("139");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData, "outbound");
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3686_verifyPassengerInformationInBookingConfirmationStobartWithThreeAdultUKToNonUKJustFlySelectingLuggageAndSeat() {
        Hashtable testData = new DataUtility().getTestData("140");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData, "outbound");
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3688_verifyPassengerInformationInBookingConfirmationLoganArWithTwoAdultOneInfantUKToUKJustFlySelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("141");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3689_verifyPaxInfoInBookingConfirmationStobartAirWithOneAdultOneChildOneInfantUKToNonUKGetMoreWithSeats() {
        Hashtable testData = new DataUtility().getTestData("142");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3690_verifyPassengerInformationInBookingConfirmationWithOneAdultNonUKToUKGetMoreSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("143");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p1",})
    public void TST_3694_verifyPassengerInformationInBookingConfirmationUKToNonUKALLInSelectingSeat() {
        Hashtable testData = new DataUtility().getTestData("144");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3692_verifyAirFranceDirectFlightNonUKtoUKWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("145");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3699_verifyAirFranceDirectFlightNonUKtoUKWithJustFlySelectingLuggage() {
        Hashtable testData = new DataUtility().getTestData("146");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3698_verifyStobartAirDirectFlightNonUKtoUKWithJustFlySelectingSeatsAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("147");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3693_verifyStobartAirDirectFlightNonUKtoUKWithAllInSelectingSeatsAndLuggage() {
        Hashtable testData = new DataUtility().getTestData("148");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }


    // Added by Arun TST-3619
    @Test(groups = {"regression", "p2"})
    public void TST_3619_VerifyLastChangeSectionIsDisplayedIfSeatsAndBagsAreNotAddedForJustFly() {
        Hashtable testData = new DataUtility().getTestData("164");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.VerifyLastChanceIfSeatsAndBagsAreNotSelected();

    }

    // Added by Arun TST-3625
    @Test(groups = {"regression", "p2"})
    public void TST_3625_3657_3650_3649__VerifyTheCardTypeInputDisplaysListOfEligibleCardsAndMyAssociatedFees() {
        Hashtable testData = new DataUtility().getTestData("152");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable <String,Double> basketDetails = basePage.yourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.verifyListOfEligibleCardsAndAssociatedFee(testData,basketDetails);
    }


    // Added by Arun TST-3599 // need to add new method for basket and test data also
    @Test(groups = {"regression", "p1"})
//    this is covered in TST_3656 in RegSearch flights
    public void TST_3599_VerifyThePassengerDetailsPageAreRetainedInBasket() {
        Hashtable testData = new DataUtility().getTestData("153");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2, 2);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);

    }


    // Added by Arun TST-3617
    @Test(groups = {"regression", "p2"})
    public void TST_3617_VerifyTheChargesAndMinimumAgesLinkDisplaysYoungDriverSurchargesPopup() {
        Hashtable testData = new DataUtility().getTestData("153");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyCarHireBookingPopup();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3528
    public void TST_3528_VerifyChooseYourSeatSectionIsNotDisplayedForAirFranceFlight() {
        Hashtable testData = new DataUtility().getTestData("154");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifySeatSelectionPaneNotDisplayed();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3485
    public void TST_3485_VerifyFaresRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("156");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.openRulesTaxesFromBasket("fare rules");
        basePage.yourDetails.verifyTheRulesPopupInPassengerDetails("fare rules");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3478
    public void TST_3478_VerifyPhoneNumberIsEmptyInSMSSection() {
        Hashtable testData = new DataUtility().getTestData("156");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyTextSMSConfirmationAndTextbox();

    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3472
    public void TST_3472_VerifyByDefault20KGAddHoldLuggageIsSelectedForJustFly() {
        Hashtable testData = new DataUtility().getTestData("158");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyAddHoldLuggageDefaultSelection();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3473
    public void TST_3473_VerifyAddHoldLuggageMessageWhenGetMoreOptionIsSelected() {
        Hashtable testData = new DataUtility().getTestData("157");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyBaggageMessageForGetMore();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3471
    public void TST_3471_VerifyNeedChangeableTicketSectionDisplayedWithFlybeFlexInfo() {
        Hashtable testData = new DataUtility().getTestData("159");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyFlybeFlexTicketFlexibilityCheckBoxIsUnchecked();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3470
    public void TST_3470_VerifyFirstNameLastNameTitleIsDisplayedForPassenger() {
        Hashtable testData = new DataUtility().getTestData("160");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.verifyFirstNameLastNameAndTitleIsDisplayedForPassenger();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3462
    public void TST_3462_VerifyCardChargeInformationIsDIsplayedAboveFooter() {
        Hashtable testData = new DataUtility().getTestData("160");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.verifyTheCardChargeInformationAboveFooter();

    }

    //Added by Arun
    @Test(groups = {"regression", "p1"})// Added by Arun TST-3396
    public void TST_3396_3397_VerifyTheDefaultCurrencyTypeIsDisplayedAsGBP() {
        Hashtable testData = new DataUtility().getTestData("169");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        testData = new DataUtility().getTestData("168");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("CHF");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("EUR");
        basePage.fareSelect.changeCurrency("USD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        testData = new DataUtility().getTestData("169");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("USD");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3475
    public void TST_3475_VerifyChooseSeatsSectionDisplayedForFlightsOperatedByFlybe() {
        Hashtable testData = new DataUtility().getTestData("162");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifySeatSelectionPaneIsDisplayed();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-1850
    public void TST_1850_VerifyFlightBookingForAdultWithTravelInsurance() {
        Hashtable testData = new DataUtility().getTestData("163");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames;
        passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }


    // Added by Arun TST-4186
    @Test(groups = {"regression", "p2"})
    public void TST_4186_VerifyTaxesFaresRulesBaggageAndPriceBreakdownDisplayedInBasketFareSelectPage() {
        Hashtable testData = new DataUtility().getTestData("165");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyTaxesFaresRulesBaggageAndPriceBreakdownAreDisplayedInBasket();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3477
    public void TST_3477_VerifyTextSMSsectionWithMobileNumberPrepopulated() {
        Hashtable testData = new DataUtility().getTestData("166");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTextSMSSectionWithMobileNumberPrepopulated();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3486
    public void TST_3486_VerifyBaggageRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("156");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.openRulesTaxesFromBasket("baggage rules");
        basePage.yourDetails.verifyTheRulesPopupInPassengerDetails("baggage rules");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3487
    public void TST_3487_VerifyTaxesAndChargesRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("154");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.openRulesTaxesFromBasket("taxes and charges");
        basePage.yourDetails.verifyTheRulesPopupInPassengerDetails("taxes and charges");
    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3488
    public void TST_3488_VerifyTermsAndConditionsLinkAndPopupInFlybeFlex() {
        Hashtable testData = new DataUtility().getTestData("156");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyTheTermsAndConditionsLinkAndPopupInFlybeFlex();

    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3474
    public void TST_3474_VerifyAddHoldLuggageSectionInPassengerDetailsWhenAllInTicketIsSelected() {
        Hashtable testData = new DataUtility().getTestData("167");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTheAddHoldLuggageSectionForAllIn();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3490
    public void TST_3490_VerifyTermsAndConditionsLinkInBaggageRulePopup() {
        Hashtable testData = new DataUtility().getTestData("155");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.openRulesTaxesFromBasket("baggage rules");
        basePage.yourDetails.verifyTermsAndConditionsInBaggageRulesPopup();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-4554
    public void TST_4554_VerifyStaticContentsInHomePage() {
        Hashtable testData = new DataUtility().getTestData("155");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.verifyTheStaticContentsInHomePage();
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3035_3712_3612_verifyFlightBookingWithAvisCarHire() {
        Hashtable testData = new DataUtility().getTestData("213");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        String carHireAmount = basePage.carHire.addCarHire("avis");
        basePage.carHire.continueToPaymentPage();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyCarHire(true, "avis", "1", carHireAmount);
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3518
    public void TST_3518_VerifyAdultTitlesDisplayedInPassengerDetails() {
        Hashtable testData = new DataUtility().getTestData("168");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.verifyAdultTitleOptions();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3394
    public void TST_3394_VerifyTheDefaultCurrencyIsLastPaidCurrencyForLoggedInUser() {
        Hashtable testData = new DataUtility().getTestData("169");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        testData = new DataUtility().getTestData("168");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("EUR");
        basePage.fareSelect.changeCurrency("GBP");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-7928
    public void TST_7928_VerifyUserIsAbleToRetrieveTheBookingUsingLowerCaseBookingReference() {
        Hashtable testData = new DataUtility().getTestData("190");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber.toLowerCase(), passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound");

    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3644
    public void TST_3644_VerifyTheIConfirmCheckBoxIsNotSelectedByDefaultInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.verifyDefaultValueOfTermsAndConditionsCheckbox("i confirm");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3646
    public void TST_3646_verifyTheFareRulesLinkDisplaysFareRulesPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.openRulesLinkInTermsAndConditionSectionInPaymentPage("fare rules");
        basePage.payment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("fare rules");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3645
    public void TST_3645_3647_verifyTheGeneralConditionOfCarriageLinkAndPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.openRulesLinkInTermsAndConditionSectionInPaymentPage("general conditions of carriage");
        basePage.payment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("general conditions of carriage");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3642
    public void TST_3642_verifyDangerousGoodsAndBaggageLinkAndPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.openRulesLinkInTermsAndConditionSectionInPaymentPage("dangerous goods");
        basePage.payment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("dangerous goods");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3643
    public void TST_3643_VerifyPleaseKeepMeUpdatedBoxIsSelectedByDefaultInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.verifyDefaultValueOfTermsAndConditionsCheckbox("please keep me update");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3641
    public void TST_3641_VerifyTripPurposeOptionsInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.verifyTripPurposeOptions();
        basePage.payment.selectTripPurpose(testData);

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3635
    public void TST_3635_VerifyBookingByChoosingDiffernetBillingAddressOptionInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("191");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(false);
        basePage.payment.selectTheBillingAddressCountryDropDown(testData);
        basePage.payment.enterPassengerBillingAddressManually(testData);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();


    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3639
    public void TST_3639_VerifyByClickingOnPaypalRadioButtonHidesCardPaymentSection() {
        Hashtable testData = new DataUtility().getTestData("195");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.verifyTheCardSectionIsHiddenWhenPayPalIsSelected();
    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3632
    public void TST_3632_VerifyByEnteringValidPostalCodeInputInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("192");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(false);
        basePage.payment.verifyCountryAndPostalCodeInputsWhenDifferentBillingAddressOptionIsSelected();
    }

    //Added by Arun
    @Test(groups = {"regression", "p2"})//Added by arun TST-3600
    public void TST_3600_VerifyCarParkingOptionIsDisplayedInExtrasPageForEligibleReturnRoutes() {
        Hashtable testData = new DataUtility().getTestData("193");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyCarParkingIsDisplayedInExtrasPage();

    }

    //Added by Arun
    @Test(groups = {"regression", "p2"})//Added by arun TST-3609
    public void TST_3609_VerifyCarHireOptionIsDisplayedInExtrasPageForEligibleReturnRoutes() {
        Hashtable testData = new DataUtility().getTestData("193");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyCarHireIsDisplayedInExtrasPage();

    }




    // Added by Arun
    //Todo
    //@Test(groups = {"regression", "p2"})// Added by Arun TST-3648
    public void TST_3648_VerifyBasketWhenSelectingPayPalOption() {
        Hashtable testData = new DataUtility().getTestData("221");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.verifyBasket();


    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3695
    public void TST_3695_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("222");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3696
    public void TST_3696_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("223");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3697
    public void TST_3697_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("224");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, true, seatSelected);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_2566_verifyCarParking() {
        Hashtable testData = new DataUtility().getTestData("186");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.selectSeats(testData, "inbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.addCarParking(testData);
        Hashtable<String, Double> extrasPageBasket = basePage.carHire.verifyBasket();
        basePage.carHire.continueToPaymentPage();
        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasPageBasket);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double totalPrice = basePage.payment.verifyBasket();
        basePage.payment.continueToBooking();
        totalPrice = basePage.payment.handlePriceChange("continue", totalPrice);
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyCarParking(testData, 1);
        basePage.bookingConfirmation.verifyTotalAmount(totalPrice);
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-7929
    public void TST_7929_VerifyTheCurrencyOptionsDisplayedForBGO(){
        Hashtable testData = new DataUtility().getTestData("221");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3531
    public void TST_3531_VerifySeatLegendsIsDisplayedForUKFlights(){
        Hashtable testData = new DataUtility().getTestData("226");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);

        basePage.yourDetails.verifyTheSeatLegends();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3235
    public void TST_3235_verifyChangeSeatAndBaggageInBookingConfirmation(){
        Hashtable testData = new DataUtility().getTestData("227");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outBound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeSeat();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.selectPassengerBaggage(testData,"edit");
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound","edit");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.chooseExtras.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,editedSeats);
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4596
    public void TST_4596_VerifyAncillariesOptionsPageIsDisplayedForSelectedCurrencies(){
        Hashtable testData = new DataUtility().getTestData("228");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("NOK");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifySeatsSectionIsDisplayed();
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyCurrencyInBaggageType("NOK");
        basePage.yourDetails.verifyCurrencyInSeatType("NOK");
////        basePage.yourDetails.changeCurrency("CHF");
//        basePage.yourDetails.verifyCurrencyInBaggageType("CHF");
//        basePage.yourDetails.verifyCurrencyInSeatType("CHF");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3547
    public void TST_3547_VerifyFlybeRoutesHaveJustFlyTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("229");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyElementsInBaggageSectionIsDisplayed();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3547
    public void TST_3547_VerifyStobartAirRoutesHaveJustFlyTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("230");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyElementsInBaggageSectionIsDisplayed();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3547
    public void TST_3547_VerifyHopForAriFranceRoutesHaveJustFlyTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("242");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyElementsInBaggageSectionIsDisplayed();


    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3547
    public void TST_3547_VerifyAirFranceRoutesHaveJustFlyTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("241");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyElementsInBaggageSectionIsDisplayed();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3545
    public void TST_3545_VerifyFlybeRoutesHaveAllInTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("244");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.verifyTheAddHoldLuggageSectionForAllIn();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3545
    public void TST_3545_VerifyStobartAirRoutesHaveAllInTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("245");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.verifyTheAddHoldLuggageSectionForAllIn();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3545
    public void TST_3545_VerifyHopCitiJetRoutesHaveAllInTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("246");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.verifyTheAddHoldLuggageSectionForAllIn();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3545
    public void TST_3545_VerifyAirFranceRoutesHaveAllInTicketAndAllBagTypes(){
        Hashtable testData = new DataUtility().getTestData("247");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.verifyTheAddHoldLuggageSectionForAllIn();
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-4222
    public void TST_4222_VerifyUnaccompaniedTeenMessageIsDisplayedWhenOnlyTeenIsSelectedForAirFranceHopCitijet(){
        Hashtable testData = new DataUtility().getTestData("243");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4221
    public void TST_4221_VerifyUnAccompinedChildMessageInPassenger() {
        Hashtable testData = new DataUtility().getTestData("252");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectNoOfPassengers(testData);
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForChild();
    }

    @Test(groups = {"regression","p2"})
    public void TST_3651_verifyOptionToSaveACardIsDisplayedAfterLogin(){
        Hashtable testData = new DataUtility().getTestData("290");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.verifySaveCardIsDisplayed(true);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, true);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_10419_VerifyUserIsAbleToCompleteBookingFromRecentSearch(){
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("322");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.navigateToHomePage();
        basePage.cheapFlight.selectRecentFlight(testData,1);
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }



}