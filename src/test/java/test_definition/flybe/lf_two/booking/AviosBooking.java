package test_definition.flybe.lf_two.booking;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;


public class AviosBooking {

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6857
    public void TST_6857_VerifyUserCannotUseAviosPartPayForCarParking() {
        Hashtable testData = new DataUtility().getTestData("255");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.selectLowestPriceDate(22.00,"inbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 1);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.addCarParking(testData);
        basePage.carHire.verifyBasket(maxAviosPrice);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6696
    public void TST_6696_VerifyPartPayAviosForEuroCurrency() {
        Hashtable testData = new DataUtility().getTestData("256");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("low");
        basePage.yourDetails.verifyCurrencyInAvios("\u20ac");
        basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2, 0);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6708
    public void TST_6708_VerifyUserIsAbleToLoginWithAviosAccountAndViewBalance() {
        Hashtable testData = new DataUtility().getTestData("257");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTheAviosOptions();
        basePage.yourDetails.verifyAviosDisplayPasswordForgottenPasswordAndYourUserNameAreDisplayed();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();

    }

    //    @Test(groups = {"regression", "p2"})// Added by Arun TST-6812
//    cannot be automated as the ticketing amount can be verified only from shares and DB
    public void TST_6812_VerifyTicketingDoneSuccessfullyForFullAmountOfBookingUsingAviosPartPay() {
        Hashtable testData = new DataUtility().getTestData("258");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2,0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6817
    public void TST_6817_VerifyUserCanMakeBookingByAddingAviosAndPaypalOptions() {
        Hashtable testData = new DataUtility().getTestData("259");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.payment.enterPayPalCardDetails(testData);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyPaidUsingPaypalIsDisplayed();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-8316
    public void TST_8316_VerifyWhenPayableAmountIsZeroUserIsAbleToCompleteBookingSuccessfullyWithByAddingAvios() {
        Hashtable testData = new DataUtility().getTestData("281");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double expectedAmount = basePage.payment.verifyBasket(maxAviosPrice);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyTotalAmount(expectedAmount);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    //    TODO: need to implement the verifyTheToolTipMessageInAviosLogin
//    @Test(groups = {"regression", "p2"})// Added by Arun TST-6844
    public void TST_6844_VerifyATRPMemberIsTryingToLoginShouldDisplayToolTipMessage() {
        Hashtable testData = new DataUtility().getTestData("282");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTheToolTipMessageInAviosLogin();

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6845
    public void TST_6845_VerifyBookingConfirmationAviosPartPayIsAppliedWithDifferentCurrency() {
        Hashtable testData = new DataUtility().getTestData("283");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyAviosCurrency("CHF");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }



    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForBags() {
        Hashtable testData = new DataUtility().getTestData("284");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice =basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForSeats() {
        Hashtable testData = new DataUtility().getTestData("291");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("medium");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6746
    public void TST_6746_VerifyAviosDiscountValueIsShownSeparatelyInBasketInAllPages() {
        Hashtable testData = new DataUtility().getTestData("285");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 1);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
       // basePage.carHire.addCarParking(testData);
        Hashtable<String, Double> carHireBasket = basePage.carHire.verifyBasket();
        basePage.carHire.continueToPaymentPage();

        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(carHireBasket);
        basePage.payment.selectPaymentOption(testData);

        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,overAllFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    //    @Test(groups = {"regression", "p2"})// Added by Arun TST-6753
//    cannot be automated as it requires avios points awarded to be checked in DB
    public void TST_6753_VerifyAviosPointsIsRewardedWhenBookingMadeUsingCurrencyCAD() {
        Hashtable testData = new DataUtility().getTestData("286");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("CAD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("low");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyAviosCurrency("CAD");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }

    // TODO: 10/14/2016 : promocode method needs to be added in the test data and promocode verification method needs to be written
//    @Test(groups = {"regression", "p2"})// Added by Arun TST-6930
    public void TST_6930_VerifyWhileBookingAviosIsAppliedAlongWithPromoCode() {
        Hashtable testData = new DataUtility().getTestData("287");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.enterPromoCode(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true,seatSelected);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6912
    public void TST_6912_VerifyAviosCannotBeUsedForTravelInsurance() {
        Hashtable testData = new DataUtility().getTestData("288");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(yourDetailsBasket);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket(maxAviosPrice);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    // TODO: 10/14/2016 : create a non uk login
//    @Test(groups = {"regression", "p2"})// Added by Arun TST-6905
    public void TST_6905_verifyNonUKUserCanLoginAndBookTicketUsingAvios() {
        Hashtable testData = new DataUtility().getTestData("289");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
//        basePage.yourDetails.selectFlybeAccountOption(true);
//        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
//        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
    }

    @Test(groups = {"smoke","s1"})
    public void TST_verifyAviosBooking()
    {
        Hashtable testData = new DataUtility().getTestData("350");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable selectedFlightDetails=basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.verifyTheAviosOptions();
        basePage.yourDetails.verifyAviosDisplayPasswordForgottenPasswordAndYourUserNameAreDisplayed();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        //Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice =basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }



}
