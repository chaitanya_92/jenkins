package test_definition.flybe.lf_two.leap_manage_booking;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class RegManageBooking {



    @Test(groups = {"regression", "p2"})
    public void TST_7934_VerifyUserIsAbleToRetrievingBookingFromCheckInAndManageBooking(){
        Hashtable testData = new DataUtility().getTestData("200");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String secondBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForAmending(firstBookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(2);
//        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("outbound");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForAmending(secondBookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(2);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_7510_7508_verifyChangeFeeChargedForJustFlyNon_FlexAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("214");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.fareSelect.changeCurrency("GBP");
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("262");
        basePage.changeYourFlight.changeDateAndRoute(testData);
        basePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.verifyChangeFeeIsDisplayed(true);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7509_verifyChangeFeeNotChargedForJustFlyFlexAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("215");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.addFlexToBooking();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("263");
        basePage.changeYourFlight.changeDateAndRoute(testData);
        basePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7507_verifyChangeFeeNotChargedForAllInAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("216");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("264");
        basePage.changeYourFlight.changeDateAndRoute(testData);
        basePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7506_verifyChangeFeeNotChargedForGetMoreAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("217");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("265");
        basePage.changeYourFlight.changeDateAndRoute(testData);
        basePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    //// TODO: 9/14/2016
    @Test(groups = {"regression", "p2"})// Added by Arun TST-4573
    public void TST_4573_VerifyUserIsAbleToChangeCurrencyForItineraryChangesInBookingConfirmationPage(){
        Hashtable testData = new DataUtility().getTestData("225");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.toClickOnChangeFlightLinkInBookingConfirmationPage();
        Hashtable<String,String> itineraryTestData = new DataUtility().getTestData("253");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.changeCurrency("EUR");
//        basePage.changeYourFlight.continueToYourDetails();
        basePage.changeYourFlight.verifyCurrency("EUR");


//        basePage.changeYourFlight.selectChangedFlight();
//        basePage.changeYourFlight.continueToYourDetails();
//        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);

//        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = basePage.yourDetails.captureSeatsSelectedForPassengers();
//        basePage.yourDetails.acceptTermsAndConditionForEditing();
//        basePage.yourDetails.continueToPayment();
//        basePage.yourDetails.handleNagWindow("confirm");
//        basePage.payment.selectPaymentOption(testData);
//        basePage.payment.enterCardDetails(testData);
//        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
//        basePage.payment.acceptTermsAndCondition();
//        basePage.payment.continueToBooking();
//        basePage.bookingConfirmation.verifyBookingReferenceNumber();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_7062_verifyChangeItineraryIsNotEnabledForPassenger() {
        Hashtable testData = new DataUtility().getTestData("218");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber, passengerNames, 2);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.verifyChangeBookingIsEnabled(false);
//        basePage.manageBooking.continueToChangeBooking();
//        basePage.changeBooking.verifyChangeFlightIsEnabled(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7061_verifyChangeItineraryIsEnabledForBooker() {
        Hashtable testData = new DataUtility().getTestData("219");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber, passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.verifyChangeFlightIsEnabled(true);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7470_verifyChangeFeeNotChargedForFlybeJustFlyAmendFlightsForJERGCI(){
        Hashtable testData = new DataUtility().getTestData("220");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        basePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("266");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7470_verifyChangeFeeNotChargedForBlueIslandsJustFlyAmendFlightsForJERGCI(){
        Hashtable testData = new DataUtility().getTestData("235");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        basePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("267");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7529_verifyChangeFeeNotChargedForFlybeJustFlyAmendFlightsForGCIJER(){
        Hashtable testData = new DataUtility().getTestData("236");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("268");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7529_verifyChangeFeeNotChargedForBlueIslandsJustFlyAmendFlightsForGCIJER(){
        Hashtable testData = new DataUtility().getTestData("237");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        basePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("269");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7927_verifyUserAbleToChangeTicketFromGetMoreToAllIn(){
        Hashtable testData = new DataUtility().getTestData("238");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(firstBookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("270");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        String secondBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.compareBookingRefNo(firstBookingRefNumber, secondBookingRefNumber);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_6100_verifyCarHireInItineraryFlowIsDisplayedWhenBookingAlreadyDoneWithCarHire(){
        Hashtable testData = new DataUtility().getTestData("239");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.carHire.continueToPaymentPage();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("271");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"inbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyCarHireIsDisplayedInExtrasPage();
    }

}
