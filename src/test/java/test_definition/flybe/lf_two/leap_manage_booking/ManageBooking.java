package test_definition.flybe.lf_two.leap_manage_booking;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

//import leap_business_logics.ManageBooking;

public class ManageBooking {

    @Test(groups = {"smoke", "p1","s1"})
    public void TST_6409_verifyAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("104");
       LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData,"outBound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("261");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.continueToYourDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.selectSeats(itineraryTestData,"outbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.selectPassengerBaggage(itineraryTestData);
        basePage.yourDetails.acceptBags();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
         basePage.bookingConfirmation.verifyBookingReferenceNumber();
//      ----------  TO Do--------
//        chanaged flight details needs to be captured

//        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,seatSelected);
    }

    @Test(groups = {"smoke", "p1","s1"})
    public void TST_6408_verifyChangeSeatAndBaggageInBookingConfirmation(){
        Hashtable testData = new DataUtility().getTestData("103");
       LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outBound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeSeat();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.selectSeats(testData,"outbound","edit");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.selectPassengerBaggage(testData,"edit");
        basePage.yourDetails.acceptBags();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.chooseExtras.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,editedSeats);
    }

    @Test(groups = {"smoke", "p3","s1"})
     public void TST_6414_verifyBookingInMyAccountLiveFlightList() {
        Hashtable testData = new DataUtility().getTestData("101");
       LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.sortByBookingDate();
        basePage.myAccountHome.verifyBookingInMyAccount(bookingRefNumber);
    }

    @Test(groups = {"smoke", "p2","s1"})
    public void TST_6384_verifyItineraryChangesInBoardingPass(){
        Hashtable testData = new DataUtility().getTestData("102");
       LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData,"outBound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeSeat();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.selectSeats(testData,"outbound","edit");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.acceptTermsAndConditionForEditing();
        basePage.chooseExtras.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,editedSeats);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.checkIn.selectPassengerAndFlightForCheckIn(testData);
        Hashtable<String,Hashtable> checkInDetails=basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers,selectedFlightDetails,1,"outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",editedSeats);

    }
}
