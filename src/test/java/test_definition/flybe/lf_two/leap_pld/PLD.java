package test_definition.flybe.lf_two.leap_pld;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by Admin on 5/29/2017.
 */
public class PLD {

    @Test(groups = {"regression","p2","smoke"})
    public void R169_TST_10133_10118_verifyUserCanRebookTheBookingThatWasfareLocked()
    {
        Hashtable testData = new DataUtility().getTestData("292");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        Hashtable overallBasketPrice=basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.enterCardDetails(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
        basePage.leapPLDConfirmation.navigateToPLDYourDetails();
        basePage.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(overallBasketPrice);
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);

    }

    @Test(groups = {"regression","p2"})
    public void TST_10133_verifyUserCanRebookTheBookingThatWasfareLockedFromManageBooking(){

        Hashtable testData = new DataUtility().getTestData("293");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        Hashtable overallBasketPrice=basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        Hashtable accountHolderName = basePage.leapPLD.getFlybeAccountHolderName(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.enterCardDetails(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        String pldRefNum = basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
        basePage.cheapFlight.enterPLDBookingDetailsForAmending(pldRefNum,accountHolderName);
        basePage.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(overallBasketPrice);
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);

    }

    @Test(groups = {"regression","p2"})
    public void R170_TST_10144_verifyFareLockProcessCanBeMadeThroughPaypal()
    {
        Hashtable testData = new DataUtility().getTestData("294");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        basePage.leapPLD.enterPayPalCardDetails(testData);
        basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
    }



    @Test(groups = {"regression","p2"})
    public void TST_10143_verifyPricePayableForFareLockPerPaxPerSectorForGBP()
    {
        Hashtable testData = new DataUtility().getTestData("296");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.verifyPlDTotalInBasket(testData,noOfPassengers);

    }

    @Test(groups = {"regression","p2"})
    public void TST_10143_verifyPricePayableForFareLockPerPaxPerSectorForEUR()
    {
        Hashtable testData = new DataUtility().getTestData("297");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.verifyPlDTotalInBasket(testData,noOfPassengers);

    }

    @Test(groups = {"regression","p2"})
    public void TST_10143_verifyPricePayableForFareLockPerPaxPerSectorForCHF()
    {
        Hashtable testData = new DataUtility().getTestData("298");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.verifyPlDTotalInBasket(testData,noOfPassengers);


    }

    @Test(groups = {"regression","p2"})
    public void TST_10143_verifyPricePayableForFareLockPerPaxPerSectorForNOK()
    {
        Hashtable testData = new DataUtility().getTestData("299");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.verifyPlDTotalInBasket(testData,noOfPassengers);

    }

    @Test(groups = {"regression","p2"})
    public void TST_10129_verifyUserIsAbleToAccessHeldBookingByClickingOnBookingInMyAccount()
    {
        Hashtable testData = new DataUtility().getTestData("300");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        Hashtable overallBasketPrice=basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        System.out.println(overallBasketPrice);
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.enterCardDetails(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        String pldRefNum =  basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.login(testData);
        basePage.myAccountHome.verifyPLDBookingInMyAccountANDnavigateToYourDetailsPage(pldRefNum);
        basePage.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(overallBasketPrice);
    }
}
