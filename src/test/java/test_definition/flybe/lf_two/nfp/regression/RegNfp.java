package test_definition.flybe.lf_two.nfp.regression;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class RegNfp {

    @Test(groups = {"regression", "p2"})
    public void TST_6377_verifyOperatedBYBlueIslands() {
        Hashtable testData = new DataUtility().getTestData("116");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyOperatedByInFlightBreakDown(selectedFlightDetails);
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
//        basePage.yourDetails.selectPassengerBaggage(testData);
//        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);

        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_6377verifyOperatedBYVLM() {
        Hashtable testData = new DataUtility().getTestData("117");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyOperatedByInFlightBreakDown(selectedFlightDetails);
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_6377_verifyOperatedBYStobartAir() {
        Hashtable testData = new DataUtility().getTestData("118");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyOperatedByInFlightBreakDown(selectedFlightDetails);
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

}
