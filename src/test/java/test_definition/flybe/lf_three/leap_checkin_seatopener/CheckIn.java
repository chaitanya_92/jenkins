package test_definition.flybe.lf_three.leap_checkin_seatopener;

import PageBase.LFBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class CheckIn {

  @Test(groups = {"smoke"})
  public void TST_6380_verifyCheckInEnabledForTodayFlight() {
    Hashtable testData = new DataUtility().getTestData("7");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlight = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData,"outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlight,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.verifyCheckInValidationMessageForTodayFlight("outbound");
  }

  @Test(groups = {"smoke"})
  public void TST_6376_verifyCheckInTwoAdultsFourChildrenOneTeenOneInfantInDirectFlightNonUKToNonUKAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("65");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();

    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();

    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");

    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.continueToAddAPI(1);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
    basePage.leapCheckIn.continueToAddAPI(2);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
    basePage.leapCheckIn.continueToAddAPI(3);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
    basePage.leapCheckIn.continueToAddAPI(4);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
    basePage.leapCheckIn.continueToAddAPI(5);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 5, "child");
    basePage.leapCheckIn.continueToAddAPI(6);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 6, "child");
    basePage.leapCheckIn.continueToAddAPI(7);
    basePage.addAPI.populateAPIDetails(testData, passengerNames, 7, "child");
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
  }


  @Test(groups = {"smoke"})
  public void TST_6382_verifyCheckInOutboundAndInboundOneAdultUKToUKDirectFlightWithoutLogin() {
    Hashtable testData = new DataUtility().getTestData("73");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedOutboundFlightDetails;
    selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    Hashtable<String, String> selectedInboundFlightDetails;
    selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
    basePage.fareSelect.selectFlightOption(testData, "inbound");
    Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.selectSeats(testData,"inbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(false);
    basePage.lfPayment.enterPassengerContact(testData);
    basePage.lfPayment.selectPassengerContactAddress(testData);
    basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
    basePage.seatOpener.seatOpen(selectedOutboundFlightDetails, 1, "tomorrow");
    basePage.seatOpener.seatOpen(selectedInboundFlightDetails, 1, "tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
    //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
    Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
  }


  @Test(groups = {"smoke", "p2"})
    public void TST_6379_verifyCheckInValidationMessageForNonTodayFlight() {
      //6379 merged with 6355
        Hashtable testData = new DataUtility().getTestData("9");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlight = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlight,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("outbound");
        basePage.leapCheckIn.verifyCheckInLinkIsOpenOrNot(noOfPassengers);

    }

  @Test(groups = {"smoke"})
  public void TST_6370_verifyCheckInWithOneAdultOneChildOneTeenOneInfantInDirectFlightUkToUkWithoutLogin() {
    Hashtable testData = new DataUtility().getTestData("57");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(false);
    basePage.lfPayment.enterPassengerContact(testData);
    basePage.lfPayment.selectPassengerContactAddress(testData);
    basePage.lfPayment.enterPassengerContactNumbersEmails(testData);

    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");

    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

  }

  @Test(groups = {"smoke"})
  public void TST_6368_verifyCheckInWithOneTeenDirectFlightUkToUkAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("55");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
  }

 /* @Test(groups = {"smoke", "p1","c2"})
  public void TST_6366_verifyCheckInWithTwoAdultsTwoChildrenTwoInfantsTwoTeensDirectFlightUkToUkAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("56");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
  }*/

  @Test(groups = {"smoke", "p1","c1"})
  public void TST_6364_verifyCheckInWithOneAdultOneChildDirectFlightUkToUkAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


  @Test(groups = {"smoke"})
  public void TST_6366_verifyCheckInWithTwoAdultsTwoChildrenTwoInfantsTwoTeensDirectFlightUkToUkAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("56");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
  }

  @Test(groups = {"smoke", "p1","c1"})
  public void TST_6364_verifyCheckInWithOneAdultOneTeenDirectFlightUkToUkAfterLogin() {
    Hashtable testData = new DataUtility().getTestData("53");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.openLoginRegistrationPage();
    basePage.login.selectLoginRegistrationOption("login");
    basePage.login.login(testData);
    basePage.myAccountHome.verifyLogin();
    basePage.cheapFlight.navigateToHomepage();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(true);
    basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.deSelectAllPassengersForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
  }

    @Test(groups = {"smoke", "p1","c1"})
    public void TST_6356_verifyCheckInWithOneAdultDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("50");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2","c1"})
    public void TST_6357_verifyCheckInWithFourAdultDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("51");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound");
    }

    @Test(groups = {"smoke", "p1","c1"})
    public void TST_6359_verifyCheckInWithOneAdultOneInfantDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("52");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        System.out.println(checkInDetails);
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p1","c1"})
    public void TST_6362_verifyCheckInWithOneAdultOneChildDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("54");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();




        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2","c2"})
    public void TST_6377_verifyCheckInWithOneTeenDirectFlightUkToUkWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("58");

      LFBase basePage = new LFBase();

        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);

        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
//        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2","c2"})
    public void TST_6410_verifyCheckInOneAdultOneTeenOneChildOneInfantUKToNonUKIndirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("68");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2","c2"})
    public void TST_6411_6413_6387_verifyCheckInTwoAdultsOneTeenOneChildNonUKToUKIndirectFlightWithoutLogin() {
      //merged 6413 and 6387 with 6411
        Hashtable testData = new DataUtility().getTestData("69");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
//        basePage.lfCarHire.addInsurance(testData);
//        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        String[] genders = {"male"};
        basePage.addAPI.verifyGenderForPassenger(genders);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "adult");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

    }

//    @Test(groups = {"smoke", "p3"})
//because space is not handled in the business logic
    public void TST_6412_verifyCheckInWithSpaceInNameUKToUKDirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("70");
       LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
//        basePage.yourDetails.selectFlybeAccountOption(false);
//        basePage.yourDetails.enterPassengerContact(testData);
//        basePage.yourDetails.selectPassengerContactAddress(testData);
//        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
//        basePage.yourDetails.enterPassengerDetails(testData);
//        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
//        basePage.yourDetails.selectPassengerBaggage(testData);
//        basePage.yourDetails.acceptBags();
//        basePage.yourDetails.selectSeats(testData, "outbound");
//        basePage.yourDetails.acceptSeats();
//        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
//        basePage.yourDetails.continueToPayment();
//        basePage.yourDetails.handleNagWindow("confirm");
//        basePage.payment.selectPaymentOption(testData);
//        basePage.payment.enterCardDetails(testData);
//        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
//        basePage.payment.acceptTermsAndCondition();
//        basePage.payment.continueToBooking();
//        basePage.payment.handlePriceChange("continue");
//        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
//        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
//        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
//        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
//        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
//        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
//        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
//        basePage.selectPassengerForCheckIn.checkIn();
//
//        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

//   @Test(groups = {"smoke", "p3"})
    //because Name1 is Name and cannot verify
    public void TST_6378_verifyCheckInWithNonAlphabeticCharactersInNameUKToUKDirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("71");
       LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
//        basePage.yourDetails.selectFlybeAccountOption(false);
//        basePage.yourDetails.enterPassengerContact(testData);
//        basePage.yourDetails.selectPassengerContactAddress(testData);
//        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
//        basePage.yourDetails.enterPassengerDetails(testData);
//        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
//        basePage.yourDetails.selectPassengerBaggage(testData);
//        basePage.yourDetails.acceptBags();
//        basePage.yourDetails.selectSeats(testData, "outbound");
//        basePage.yourDetails.acceptSeats();
//        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
//        basePage.yourDetails.continueToPayment();
//        basePage.yourDetails.handleNagWindow("confirm");
//        basePage.payment.selectPaymentOption(testData);
//        basePage.payment.enterCardDetails(testData);
//        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
//        basePage.payment.acceptTermsAndCondition();
//        basePage.payment.continueToBooking();
//        basePage.payment.handlePriceChange("continue");
//        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
//        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
//        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
//        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
//        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
//        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
//        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
//        basePage.selectPassengerForCheckIn.checkIn();
//        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2","c2"})
    public void TST_6394_verifyGateBoardingTimeUKToUKDirectFlightAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("72");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

//
    @Test(groups = {"smoke", "p2","c3"})
    public void TST_6399_verifyCheckInTwoWayOneAdultIndirectFlightUKToUKWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("75");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 4, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails,2,"tomorrow");
        basePage.seatOpener.seatOpen(selectedInboundFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 2, "outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 2, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 2, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 2, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 2, "inbound", seatSelected);


    }

//    @Test(groups = {"smoke", "p3"})
//  duplicate of 6377
    public void verifyCheckInOneWayOneTeenDirectFlightUKToUKWithoutLogin() {
    Hashtable testData = new DataUtility().getTestData("80");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedFlightDetails;
    selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(false);
    basePage.lfPayment.enterPassengerContact(testData);
    basePage.lfPayment.selectPassengerContactAddress(testData);
    basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames,1, noOfPassengers, false, seatSelected);
    basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(4);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
    //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound", seatSelected);
  }

    @Test(groups = {"smoke", "p3","c3"})
    public void TST_6401_verifyCheckInValidationMessageForReturnCheckInForOutboundForOneAdultDirectFlight() {
        Hashtable testData = new DataUtility().getTestData("77");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("inbound");
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
    }

//    @Test(groups = {"smoke", "p3"})
    public void TST_6361_verifyCheckInEightAdultsEightInfantsDirectFlightUKToUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("78");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound", seatSelected);
        }

    @Test(groups = {"smoke", "p3","c3"})
    public void TST_6402_verifyCheckTwoWayOneAdultOneChildOneInfantDirectFlightUKToUKWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("79");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails,1,"tomorrow");
        basePage.seatOpener.seatOpen(selectedInboundFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
    }


//      @Test(groups = {"smoke", "p3"})
//    This method verifies the gender in API page when Mstr is selected
    public void TST_6413_verifyCheckInGenderForMasterDirectFlightUKToNonUKWithoutLogin() {
      //merged 6413 and 6387 with 6411
        Hashtable testData = new DataUtility().getTestData("81");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        String[] genders = {"male"};
        basePage.addAPI.verifyGenderForPassenger(genders);
    }

//    @Test(groups = {"smoke", "p2"})
//    This method verifies the gender in Checkin page when DR is selected
    public void TST_6387_verifyCheckInGenderForDRDirectFlightUKToUKWithoutLogin() {
      //merged 6413 and 6387 with 6411
        Hashtable testData = new DataUtility().getTestData("82");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        String[] genders = {"male"};
        basePage.leapCheckIn.verifyGenderForPassenger(genders);
    }


//    @Test(groups = {"p5","demo"})
    public void TST_6300_verifyCheckInWithOneAdultDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("249");
      LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

  @Test(groups = {"smoke", "p3"})
  public void S46_TST_6397_verifyCheckInOutboundAndInboundOneAdultUKToUKDirectFlightWithoutLogin() {
    Hashtable testData = new DataUtility().getTestData("358");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
    basePage.cheapFlight.findFlights();
    Hashtable<String, String> selectedOutboundFlightDetails;
    selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "twoChange", "outbound");
    basePage.fareSelect.selectFlightOption(testData, "outbound");
    Hashtable<String, String> selectedInboundFlightDetails;
    selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
    basePage.fareSelect.selectFlightOption(testData, "inbound");
    Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
    basePage.fareSelect.verifyContinueEnabled();
    basePage.fareSelect.continueToPassengerDetails();
    basePage.lfYourDetails.enterPassengerDetails(testData);
    Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
    basePage.lfYourDetails.selectPassengerBaggage(testData);
    basePage.lfYourDetails.acceptBags();
    basePage.lfYourDetails.selectSeats(testData,"outbound");
    basePage.lfYourDetails.selectSeats(testData,"inbound");
    basePage.lfYourDetails.acceptSeats();
    Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
    basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
    basePage.lfCarHire.addInsurance(testData);
    basePage.lfCarHire.continueToPayment();
    basePage.lfPayment.selectFlybeAccountOption(false);
    basePage.lfPayment.enterPassengerContact(testData);
    basePage.lfPayment.selectPassengerContactAddress(testData);
    basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
    basePage.lfPayment.selectPaymentOption(testData);
    basePage.lfPayment.enterCardDetails(testData);
    basePage.lfPayment.selectBillingAddressOption(true);
    basePage.lfPayment.saveCardDetails();
    basePage.lfPayment.selectTripPurpose(testData);
    basePage.lfPayment.acceptTermsAndCondition();
    basePage.lfPayment.continueToBooking();
    basePage.lfPayment.handlePriceChange("continue");
    String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
    basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
    //basePage.seatOpener.seatOpen(selectedOutboundFlightDetails, 1, "tomorrow");
    //basePage.seatOpener.seatOpen(selectedInboundFlightDetails, 1, "tomorrow");
    basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
    basePage.leapCheckIn.waitAndCheckForCheckIn(3);
    basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
    //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
    //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
    Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
    Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
    basePage.selectPassengerForCheckIn.checkIn();
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
    basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
  }




//        @Test(groups = {"smoke", "p2"})
//    This method is invalid as we cannot verify the edited details
//    public void verifyUserAbleToEditAPISWithoutLogin() {
//        Hashtable testData = new DataUtility().getTestData("84");
//       LFBase basePage = new LFBase();
//        basePage.cheapFlight.enterSourceAndDestination(testData);
//        basePage.cheapFlight.enterTravelDates(testData);
//        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
//        basePage.cheapFlight.findFlights();
//        Hashtable<String, String> selectedFlightDetails;
//        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.verifyContinueEnabled();
//        basePage.fareSelect.continueToPassengerDetails();
//        basePage.yourDetails.selectFlybeAccountOption(false);
//        basePage.yourDetails.enterPassengerContact(testData);
//        basePage.yourDetails.selectPassengerContactAddress(testData);
//        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
//        basePage.yourDetails.enterPassengerDetails(testData);
//        basePage.yourDetails.selectPassengerBaggage(testData);
//        basePage.yourDetails.acceptBags();
//        basePage.yourDetails.selectSeats(testData, "outbound");
//        basePage.yourDetails.acceptSeats();
//        basePage.yourDetails.continueToPayment();
//        basePage.yourDetails.handleNagWindow("confirm");
//        basePage.payment.selectPaymentOption(testData);
//        basePage.payment.enterCardDetails(testData);
//        basePage.payment.selectBillingAddressOption(true);
//        basePage.payment.selectTripPurpose(testData);
//        basePage.payment.acceptTermsAndCondition();
//        basePage.payment.continueToBooking();
//        basePage.payment.handlePriceChange("continue");
//        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
//        basePage.cheapFlight.enterBookingDetailsForCheckIn(testData, bookingRefNumber);
//        basePage.miniConfirmation.waitAndCheckForTicketing(3);
//        basePage.miniConfirmation.addAPI();
//        basePage.addAPI.addAPIDetails(testData);
//        basePage.miniConfirmation.editAPI();
//        basePage.addAPI.editAPIDetails(testData);
//        basePage.miniConfirmation.editAPI();
//        basePage.addAPI.verifyEditedPassportNumber(testData);
//    }



}
