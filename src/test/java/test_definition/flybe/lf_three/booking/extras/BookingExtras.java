package test_definition.flybe.lf_three.booking.extras;


import PageBase.LFBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class BookingExtras {



    @Test(groups = {"regression","p3"})
    public void TST_11770_verifyValidationMessageInCheckoutPage(){
        Hashtable testData = new DataUtility().getTestData("91");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.verifyValidationMessageForeName();
    }

    @Test(groups = {"regression","p3"})
    public void TST_9582_verifyUserIsAbleToSeeForEachDepartureTimeTheShortestMultiSectorByJourneyTimeIsShownInFareSelectionPage() {
        Hashtable testData = new DataUtility().getTestData("92");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.clickToShowAllConnectingFlights();
        basePage.fareSelect.verifyShortestMSCbyJourneyTime();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-1850
    public void R9_TST_1850_VerifyFlightBookingForAdultWithTravelInsurance() {
        Hashtable testData = new DataUtility().getTestData("163");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        Hashtable <String,Double> basketDetails = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,1);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(basketDetails);
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }


    @Test(groups = {"regression", "p2","smoke"})
    public void S36_TST_3035_3712_3612_6405_verifyFlightBookingWithAvisCarHire() {
        Hashtable testData = new DataUtility().getTestData("213");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        String carHireAmount = basePage.lfCarHire.addCarHire("avis");
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyCarHire(true, "avis", "1", carHireAmount);
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    //Added by Arun
    @Test(groups = {"regression", "p2"})//Added by arun TST-3600
    public void R51_TST_3600_VerifyCarParkingOptionIsDisplayedInExtrasPageForEligibleReturnRoutes() {
        Hashtable testData = new DataUtility().getTestData("193");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyCarParkingIsDisplayedInExtrasPage();


    }


    //Added by Arun
    @Test(groups = {"regression", "p2"})//Added by arun TST-3609
    public void R52_TST_3609_VerifyCarHireOptionIsDisplayedInExtrasPageForEligibleReturnRoutes() {
        Hashtable testData = new DataUtility().getTestData("193");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyCarHireIsDisplayedInExtrasPage();

    }


    @Test(groups = {"regression", "p2","smoke"})
    public void S37_TST_2566_6406_verifyCarParking() {
        Hashtable testData = new DataUtility().getTestData("186");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        Hashtable<String, Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,1);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.lfCarHire.addCarParking(testData);
        Hashtable<String, Double> extrasBasket = basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double totalPrice = basePage.lfPayment.verifyBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue",totalPrice);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.bookingConfirmation.verifyCarParking(testData, 1);
        basePage.bookingConfirmation.verifyTotalAmount(totalPrice);
    }

    @Test(groups={"regression","p2"})
    public void R166_TST_9756_VerifyUserAbleToBookWithDefaultSavedCardPayment()
    {
        Hashtable testData = new DataUtility().getTestData("351");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.navigateToMyPaymentCardsPage();
        basePage.accountMyPaymentCards.checkAndAddSaveCard(testData);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.sortByBookingDate();
        basePage.myAccountHome.verifyBookingInMyAccount(bookingRefNumber);
    }


    @Test(groups = {"regression", "p2"})
    public void R173_TST_10833_verifyCurrencyDisplayedInCardDropDownWhenUserSelectsNOKCurrencyforUKtoUKRoute(){

        Hashtable testData = new DataUtility().getTestData("10");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.changeCurrency("NOK");
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.selectCardTypeOption(testData);
        basePage.lfPayment.verifyWhetherStringIsPresentInCardTypeDropDown("NOK");

    }



//    ---------smoke cases------------

    @Test(groups = {"smoke", "s1"})
    public void S3_TST_6414_6416_6351_verifyBookingWithInsuranceListedInMyAccountLiveFlightList() {
        Hashtable testData = new DataUtility().getTestData("101");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails =basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
//        Hashtable <String, Hashtable<String, Hashtable>> seatsSelectedForPassengers = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.sortByBookingDate();
        basePage.myAccountHome.verifyBookingInMyAccount(bookingRefNumber);
    }


}
