package test_definition.flybe.lf_three.booking.promocode;

import PageBase.LFBase;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by Admin on 6/22/2017.
 */
public class BookingWithPromoCode {

        @Test(groups = {"smoke", "p1"})
        public void S20_TST_6373_verifyBookingUsingPromoCodeDIR() {
                Hashtable testData = new DataUtility().getTestData("369");
                LFBase basePage1 = new LFBase();
                LegacyBase basePage = new LegacyBase();
                basePage1.cheapFlight.enterSourceAndDestination(testData);
                basePage1.cheapFlight.enterTravelDates(testData);
                int noOfPassengers = basePage1.cheapFlight.selectNoOfPassengers(testData);
                basePage1.cheapFlight.enterPromoCode(testData);
                basePage1.cheapFlight.findFlights();
                Hashtable<String,String> selectedFlightDetails;
                selectedFlightDetails = basePage1.fareSelect.selectFlight(testData, "nonStop", "outbound");
                basePage1.fareSelect.selectFlightOption(testData, "outbound");
                basePage1.fareSelect.verifyContinueEnabled();
                basePage1.fareSelect.continueToPassengerDetails();
                basePage.yourDetails.selectFlybeAccountOption(true);
                basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
                basePage.yourDetails.enterPassengerDetails(testData);
                Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
                basePage.yourDetails.selectPassengerBaggage(testData);
                basePage.yourDetails.acceptBags();
                basePage.yourDetails.selectSeats(testData, "outbound");
                basePage.yourDetails.acceptSeats();
                Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
                basePage.yourDetails.continueToPayment();
                basePage.yourDetails.handleNagWindow("confirm");
                basePage.payment.selectPaymentOption(testData);
                basePage.payment.enterCardDetails(testData);
                basePage.payment.selectBillingAddressOption(true);
                basePage.payment.selectTripPurpose(testData);
                basePage.payment.acceptTermsAndCondition();
                basePage.payment.continueToBooking();
                basePage.payment.handlePriceChange("continue");
                basePage.bookingConfirmation.verifyBookingReferenceNumber();
                basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        }
}
