package test_definition.flybe.lf_three.booking.search_flight;


import PageBase.LFBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class SearchFlights {

    @Test(groups = {"regression", "p2"})
    public void R202_TST_9511_verifyCurrencyIsSelectedBasedOnDepartureCurrencyNotSetViaURLCookie()
    {
        Hashtable<String, String> testData = new DataUtility().getTestData("321");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("CurrencyType"));
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyCurrency(testData.get("CurrencyType"));
    }


    @Test(groups = {"regression", "p1"})
    public void R203_TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyOneWay() {
        LFBase basePage = new LFBase();
        Hashtable testData = new DataUtility().getTestData("46");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p1"})
    public void R204_TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyTwoWay() {
        LFBase basePage = new LFBase();
        Hashtable testData = new DataUtility().getTestData("45");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void R1_TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyOneWay() {
        LFBase basePage = new LFBase();
        Hashtable testData = new DataUtility().getTestData("48");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void R2_TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyTwoWay() {
        LFBase basePage = new LFBase();
        Hashtable testData = new DataUtility().getTestData("49");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void R7_TST_1623_verifyDirectAirFranceDisabledForUnaccompaniedTeens() {
        Hashtable testData = new DataUtility().getTestData("109");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "nonstop");

    }

    @Test(groups = {"regression", "p1"})
    public void R8_TST_1623_verifyIndirectAirFranceDisabledForUnaccompaniedTeens() {
        Hashtable testData = new DataUtility().getTestData("110");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "onechange");
    }

    @Test(groups = {"regression", "p2"})
    public void R205_TST_1506_VerifyMessageForBookingWithMoreThanEightTeens() {
        LFBase basePage = new LFBase();
        Hashtable testData = new DataUtility().getTestData("115");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageForMoreThanEightPassengers();
    }

    @Test(groups = {"regression", "p2"})
    public void R122_TST_4825_verifyBaggageAllowanceTextForFlightLessThanFortyFiveMinFlightTime() {
        Hashtable testData = new DataUtility().getTestData("119");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void R121_TST_4824_verifyBaggageAllowanceTextForLoganAirTwinOtterFlights() {
        Hashtable testData = new DataUtility().getTestData("120");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "20kg");
    }

    @Test(groups = {"regression", "p2"})
    public void R120_TST_4823_verifyBaggageAllowanceTextForLoganAirFlights() {
        Hashtable testData = new DataUtility().getTestData("121");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void R119_TST_4719_verifyChangeRouteToBHXToMXPForTeenOnlyInSearchWidget() {
        Hashtable testData = new DataUtility().getTestData("122");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.changeDestinationToItaly();
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForTeensForItaly();
    }

    @Test(groups = {"regression", "p2"})
    public void R108_TST_4188_verifyClickingBaggageRulesLinkDisplaysBaggageRulesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("fare rules");
        basePage.fareSelect.verifyRulesPopupOpened("fare rules");
    }

    @Test(groups = {"regression", "p2"})
    public void R107_TST_4187_verifyClickingFareRulesLinkDisplaysFareRulesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("baggage rules");
        basePage.fareSelect.verifyRulesPopupOpened("baggage rules");
    }

    @Test(groups = {"regression", "p2"})
    public void R104_TST_4183_verifyClickingTaxesLinkDisplaysTaxesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
    }

    @Test(groups = {"regression", "p2"})
    public void R105_TST_4184_verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected() {
        Hashtable testData = new DataUtility().getTestData("124");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
        basePage.fareSelect.verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected(testData);

    }

    @Test(groups = {"regression", "p2"})
    public void R102_TST_4157_3412_verifySelectingOutboundGetMoreFareMakesInboundAllInAndJustFlyUnSelectable() {
        Hashtable testData = new DataUtility().getTestData("125");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.verifyJustFlyAllInOptionsAreDisableWhenGetMoreOptionIsSelectedInOutbound();

    }

    @Test(groups = {"regression", "p2"})
    public void R103_TST_4160_verifyDisplayingOfFlightStopDetailsOnClickingStopNumberLink() {
        Hashtable testData = new DataUtility().getTestData("126");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange", "outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(true);
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange", "outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R93_TST_4129_verifyAirFranceFaresDisplayedTeenMessaging() {
        Hashtable testData = new DataUtility().getTestData("127");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "nonstop");

    }

    @Test(groups = {"regression", "p2"})
    public void R100_TST_4136_verifyCitiJetHopRegionalFaresDisplayInformativeMessageIfMoreThanFourPassengersAreSelected() {
        Hashtable testData = new DataUtility().getTestData("128");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageForCitiJetHopRegionalWhenMoreThanFourPassengerIsSelected("outbound", "nonstop");
    }

    @Test(groups = {"regression", "p2"})
    public void R148_TST_7519_verifyACIIsNotDisplayedForBlueIslandDirectFlightUKToUK() {
        Hashtable testData = new DataUtility().getTestData("151");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyACIOptionDisplayedForBlueIsland();
    }

    @Test(groups = {"regression", "p2"})
    public void R24_TST_3398_verifyAdditionalAirportChargeIsDisplayedForNorwichAirport() {
        Hashtable testData = new DataUtility().getTestData("171");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageAdditionalAirportCharges(testData);
    }


    @Test(groups = {"regression", "p2"})
    public void R21_TST_3381_verifyLowestFareDisplayedIsMatchingWithFareSlider() {
        Hashtable testData = new DataUtility().getTestData("173");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyLowestFareIsDisplayedInFareSliderOutbound();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_BlueIslands() {
        Hashtable testData = new DataUtility().getTestData("174");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }



    @Test(groups = {"regression", "p2"})
    public void R150_TST_7828_verify23KgBaggageFreeForJER_GCIBlueIslands() {
        Hashtable testData = new DataUtility().getTestData("177");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void R206_TST_7828_verify23KgBaggageFreeForGCI_JERFlybe() {
        Hashtable testData = new DataUtility().getTestData("178");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void R207_TST_7828_verify23KgBaggageFreeForGCI_JERBlueIslands() {
        Hashtable testData = new DataUtility().getTestData("179");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }


    @Test(groups = {"regression", "p2"})
    public void R49_TST_3554_verifyIncreaseBaggageAllowanceForFlybeWithGetMore() {
        Hashtable testData = new DataUtility().getTestData("184");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceForGetMoreDisplayedAtReducedCost();
    }

    @Test(groups = {"regression", "p2"})
    public void R109_TST_4189_clickingPriceBreakdownLinkDisplaysBreakdownOfTheFareTotal() {
        Hashtable testData = new DataUtility().getTestData("201");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void R69_TST_3656_TST_3599_verifyBasketRetainsSelectionsFromPassengerDetailsPageAndExtrasPage() {
        Hashtable testData = new DataUtility().getTestData("202");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> outboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> inboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overallFlightDetails = basePage.fareSelect.getOverallFlightDetails(outboundFlightDetails, inboundFlightDetails);
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        Hashtable<String, Double> previousBasket;
        previousBasket = basePage.fareSelect.verifyBasket(testData);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);

        Hashtable<String, String> passsengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(previousBasket);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData, "outbound");
        basePage.lfYourDetails.selectSeats(testData, "inbound");


        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatsSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        previousBasket = basePage.lfYourDetails.verifyBasket(testData, noOFPassengers, 1, 1);

        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(previousBasket);
        basePage.lfCarHire.addCarHire("avis");
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable<String, Double> extrasPageBasket = basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        double totalAmount = basePage.lfPayment.verifyBasket();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue", totalAmount);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overallFlightDetails, passsengerNames, noOFPassengers, 2, false, seatsSelected);
        basePage.bookingConfirmation.verifyTotalAmount(totalAmount);

    }


    @Test(groups = {"regression", "p2"})
    public void R101_TST_4156_verifyFaresDisplayedMatchesWithPriceBreakDownForAllPax() {
        Hashtable testData = new DataUtility().getTestData("272");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void R118_TST_4636_verifyChildTaxesNotDisplayedWhenChildIsNotSelected() {
        Hashtable<String, String> testData = new DataUtility().getTestData("273");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyPaxIsNotDisplayedInPriceBreakDown("child");
    }


    @Test(groups = {"regression", "p2"})
    public void R208_TST_4115_3412_verifySelectingAnOutboundGetMoreGreysOutJustFlyAndAllInOptionsForInbound(){
        Hashtable<String, String> testData = new DataUtility().getTestData("307");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyPriceDifferenceBtwnGetMoreAndJustFly();
        basePage.fareSelect.verifyFlightBookingStepsAreDisplayed();
    }

    @Test(groups = {"regression", "p2"})
    public void R97_TST_7488_VerifyUserIsAbleToSeePleaseSeeSouthendAirportWebsiteForDetailsLink(){
        Hashtable<String, String> testData = new DataUtility().getTestData("312");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.navigateToArrivalAndDeparture();
        basePage.flightArrivalAndDeparture.searchByFlightNumber(testData);
        basePage.flightArrivalAndDeparture.verifySouthEndAirportLink();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4155_VerifyTicketTypeDescriptionsAreCorrect(){
        Hashtable<String, String> testData = new DataUtility().getTestData("314");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyAllInTicketTypeDescription();
        basePage.fareSelect.verifyGetMoreTicketTypeDescription();
        basePage.fareSelect.verifyJustFlyTicketTypeDescription();

    }


    @Test(groups = {"regression", "p2"})
    public void R92_TST_4126_VerifyEachSelectableNonStopFareDisplaysDefaultInformation(){
        Hashtable<String, String> testData = new DataUtility().getTestData("315");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyDirectFlightDetails(selectedFlightDetails);

    }

    @Test(groups = {"regression", "p2"})
    public void R171_TST_10217_VerifyNOKIsSetAsBaseCurrencyForFlightsDepartingFromBergen(){
        Hashtable<String, String> testData = new DataUtility().getTestData("318");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("NOK");
    }


    @Test(groups = {"regression", "p2"})
    public void R25_TST_3457_verifyContactUSLinkNavigatesToSupportPage() {
        Hashtable testData = new DataUtility().getTestData("115");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.navigateToSupportPage();
        basePage.support.verifyPageTitle();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3462
    public void R26_TST_3462_VerifyCardChargeInformationIsDIsplayedAboveFooter() {
        Hashtable testData = new DataUtility().getTestData("160");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.verifyTheCardChargeInformationAboveFooter();

    }



    // Added by Arun TST-4186
    @Test(groups = {"regression", "p2"})
    public void R106_TST_4186_VerifyTaxesFaresRulesBaggageAndPriceBreakdownDisplayedInBasketFareSelectPage() {
        Hashtable testData = new DataUtility().getTestData("165");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyTaxesFaresRulesBaggageAndPriceBreakdownAreDisplayedInBasket();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-4554
    public void R113_TST_4554_VerifyStaticContentsInHomePage() {
        Hashtable testData = new DataUtility().getTestData("155");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.verifyTheStaticContentsInHomePage();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4222
    public void R111_TST_4222_VerifyUnaccompaniedTeenMessageIsDisplayedWhenOnlyTeenIsSelectedForAirFranceHopCitijet(){
        Hashtable testData = new DataUtility().getTestData("243");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4221
    public void R110_TST_4221_VerifyUnAccompinedChildMessageInPassenger(){
        Hashtable testData = new DataUtility().getTestData("252");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectNoOfPassengers(testData);
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForChild();
    }


    @Test(groups = {"regression", "p2"})
    public void R151_TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_Flybe() {
        Hashtable testData = new DataUtility().getTestData("175");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }

    @Test(groups = {"regression", "p2"})
    public void R209_TST_7828_verify23KgBaggageFreeForJER_GCIFlybe() {
        Hashtable testData = new DataUtility().getTestData("176");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression","p2"})
    public void R210_TST_9512_verifyCurrencyIsSelectedBasedOnDepartureCurrencyNotSetViaURLCookie()
    {
        Hashtable<String, String> testData = new DataUtility().getTestData("352");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("CurrencyType"));
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyCurrency(testData.get("CurrencyType"));

    }

    @Test(groups = {"regression", "p2"})
    public void R163_TST_9514_verifyCurrencyIsSelectedBasedOnCurrencyParameterInURLWhenCurrencyCodeIsNotProvidedOrSelectedCurrencyIsNotInListofSupportedCurrency()
    {
        Hashtable testData = new DataUtility().getTestData("353");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.fareSelect.navigateToURL(testData.get("url").toString());
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency")+"");
    }

    @Test(groups = {"regression", "p2"})
    public void R164_TST_9515_verifyCurrencyIsSelectedBasedOnCountryParameterInURLWhenCurrencyCodeIsNotProvidedOrSelectedCurrencyIsNotInListofSupportedCurrency()
    {
        Hashtable testData = new DataUtility().getTestData("354");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.fareSelect.navigateToURL(testData.get("url")+"");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency")+"");
    }

    @Test(groups={"regression","p2"})
    public void R176_TST_11385_verifyFlexChargeDepartingFromNonUK(){
        Hashtable testData = new DataUtility().getTestData("323");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
    }

    @Test(groups={"regression","p2"})
    public void R213_TST_10205_verifyCurrencyIsTakenFromCurrencyCookieWhenCurrencyURLIsNotPassed(){
        Hashtable testData = new DataUtility().getTestData("324");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.cheapFlight.addCurrencyCookie(testData);
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency").toString());
    }

    @Test(groups={"regression","p2"})
    public void R214_TST_10207_verifyCurrencyIsTakenFromCountryCookieWhenCurrencyURLAndCurrencyCookieAreNotPresent(){
        Hashtable testData = new DataUtility().getTestData("325");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.cheapFlight.addCountryCookie(testData);
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency").toString());
    }

    @Test(groups={"regression","p2"})
    public void R177_TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorForLessThan14Days(){
        Hashtable testData = new DataUtility().getTestData("330");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }

    @Test(groups={"regression","p2"})
    public void R211_TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorFor15DaysTo30Days(){
        Hashtable testData = new DataUtility().getTestData("331");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }

    @Test(groups={"regression","p2"})
    public void R212_TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorForMoreThan30Days(){
        Hashtable testData = new DataUtility().getTestData("332");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }


    @Test(groups={"regression","p2"})
    public void R162_TST_8361_verifyFareDifferenceBetweenGetMoreAndJustFlyForEXTMANRouteIs28GBP(){
        Hashtable testData = new DataUtility().getTestData("370");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyDifferenceForJustFlyAndGetMoreIsGBP28ForEXT_MANRoute();
    }


    //    ------------smoke cases----------------
    @Test(groups = {"smoke", "s1"})
    public void S32_TST_6396_verifyMessageNoFlightsAvailable() {
    Hashtable testData = new DataUtility().getTestData("47");
    LFBase basePage = new LFBase();
    basePage.cheapFlight.enterSourceAndDestination(testData);
    basePage.cheapFlight.enterTravelDates(testData);
    basePage.cheapFlight.findFlights();
    basePage.fareSelect.verifyNoFlightsMessageDisplayed();
}




//    --------------Not valid in LF3--------------



//    44cases


    //    @Test(groups = {"regression", "p2"})
    //    test case is obsolete
    public void TST_3399_verifyAdditionalAirportChargeIsDisplayedForNewquayAirport() {
        Hashtable testData = new DataUtility().getTestData("172");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageAdditionalAirportCharges(testData);
    }




    //    @Test(groups = {"regression", "p2"})
    //    obsolete test case
    public void TST_3553_verify23KgBaggageFreeForEXT_MAN() {
        Hashtable testData = new DataUtility().getTestData("180");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    //    @Test(groups = {"regression", "p2"})
    // the baggage text will not be displayed in LF3
    public void TST_3548_verify20kgIsIncludedForLoganairWithJustFly() {
        Hashtable testData = new DataUtility().getTestData("181");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.lfYourDetails.verifyBaggageTextForLoganair("just fly");
    }

    //    @Test(groups = {"regression", "p2"})
    // the baggage text will not be displayed in LF3
    public void TST_3550_verify20kgIsIncludedForLoganairWithGetMore() {
        Hashtable testData = new DataUtility().getTestData("182");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyBaggageTextForLoganair("get more");
    }

    //    @Test(groups = {"regression", "p2"})
    // the baggage text will not be displayed in LF3
    public void TST_3551_verify30kgIsIncludedForLoganairWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("183");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyBaggageTextForLoganair("all in");
    }

    //    @Test(groups = {"regression", "p2"})
    // the increase baggage allowance message  will not be displayed in LF3
    public void TST_3549_verifyIncreaseBaggageAllowanceForFlybeWithGetMore() {
        Hashtable testData = new DataUtility().getTestData("184");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyIncreaseBaggageAllowanceForGetMore();
    }

    //    @Test(groups = {"regression", "p2"})
    // the increase baggage allowance message  will not be displayed in LF3
    public void TST_3549_verifyIncreaseBaggageAllowanceForStobartWithGetMore() {
        Hashtable testData = new DataUtility().getTestData("185");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyIncreaseBaggageAllowanceForGetMore();
    }


    //    @Test(groups = {"regression", "p2"})
    public void TST_3622_verifyClickingAddBagsNowAddsStandardBagCostToBasketAndChangesButtonToRemoveBags() {
        Hashtable testData = new DataUtility().getTestData("231");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.verifyBasket(testData, noOFPassengers, 1, 0);
        //
        //        basePage.yourDetails.selectFlybeAccountOption(true);
        //        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        //        basePage.yourDetails.enterPassengerDetails(testData);
        //        basePage.yourDetails.continueToPayment();
        //        basePage.yourDetails.handleNagWindow("confirm");
        //        basePage.carHire.isExtrasPageLoaded();
        //        basePage.payment.selectPaymentOption(testData);
        //        basePage.payment.addBaggageFromLastChance();
        //        basePage.payment.enterCardDetails(testData);
        //        basePage.payment.verifyBasket();
        //        basePage.payment.verifyRemoveBagsButtonIsDisplayed();
    }


    //    @Test(groups = {"regression", "p2"})
    //    All in hold baggage message is not displayed in LF3
    public void TST_3552_verifyHoldBaggageAllowanceForHopRegionalWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("187");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }


    //    @Test(groups = {"regression", "p2"})
    //    All in hold baggage message is not displayed in LF3
    public void TST_3552_verifyHoldBaggageAllowanceForAirFranceWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("188");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    //    @Test(groups = {"regression", "p2"})
    //    //    All in hold baggage message is not displayed in LF3
    public void TST_3552_verifyHoldBaggageAllowanceForStobartAirWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("189");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    //    @Test(groups = {"regression", "p2"})
    //    All in hold baggage message is not displayed in LF3
    public void TST_3552_verifyHoldBaggageAllowanceForFlybeWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("211");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        //        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }




    //    @Test(groups = {"regression", "p2"})
    public void R172_TST_10568_VerifyVerifyOnlyThreeRoutesAredisplayed (){
        Hashtable<String, String> testData = new DataUtility().getTestData("318");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.lfYourDetails.verifyBaggageSectionIsDisplayed(false);

    }


    //    @Test(groups = {"regression", "p2"})
    public void TST_8317_VerifyBaggagePriceForMan(){
        Hashtable<String, String> testData = new DataUtility().getTestData("318");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.lfYourDetails.verifyBaggageSectionIsDisplayed(false);

    }









}