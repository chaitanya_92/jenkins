package test_definition.flybe.lf_three.leap_manage_booking;

import PageBase.LFBase;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class RegManageBooking {



    @Test(groups = {"regression", "p2"})
    public void R159_TST_7934_VerifyUserIsAbleToRetrievingBookingFromCheckInAndManageBooking(){
        Hashtable testData = new DataUtility().getTestData("200");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);

        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>secondPassengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String ,String> secondPassengerForCheckin =basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String secondBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,secondPassengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(firstBookingRefNumber,passengerNames);
        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("outbound");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForAmending(secondBookingRefNumber,secondPassengerForCheckin);
        basePage.manageBooking.waitAndCheckForTicketing(2);
    }


    @Test(groups = {"regression", "p2"})
    public void R147_TST_7510_7508_verifyChangeFeeChargedForJustFlyNon_FlexAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("214");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("262");
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        legacyBasePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(true);
    }

    @Test(groups = {"regression", "p2"})
    public void R148_TST_7509_verifyChangeFeeNotChargedForJustFlyFlexAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("215");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("263");
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        legacyBasePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R99_TST_7507_verifyChangeFeeNotChargedForAllInAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("216");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("264");
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        legacyBasePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R98_TST_7506_verifyChangeFeeNotChargedForGetMoreAmendFlights(){
        Hashtable testData = new DataUtility().getTestData("217");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        testData = new DataUtility().getTestData("265");
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        legacyBasePage.changeYourFlight.selectChangedFlight(testData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    //// TODO: 9/14/2016
    @Test(groups = {"regression", "p2"})// Added by Arun TST-4573
    public void R114_TST_4573_VerifyUserIsAbleToChangeCurrencyForItineraryChanges(){
        Hashtable testData = new DataUtility().getTestData("225");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();

        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        basePage.bookingConfirmation.toClickOnChangeFlightLinkInBookingConfirmationPage();
        Hashtable<String,String> itineraryTestData = new DataUtility().getTestData("253");
        basePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        basePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        basePage.changeYourFlight.changeCurrency("EUR");
        basePage.changeYourFlight.verifyCurrency("EUR");

    }

    @Test(groups = {"regression", "p2"})
    public void R146_TST_7062_verifyChangeItineraryIsNotEnabledForPassenger() {
        Hashtable testData = new DataUtility().getTestData("218");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber, passengerNames, 2);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.verifyChangeBookingIsEnabled(false);
//        legacyBasePage.manageBooking.continueToChangeBooking();
//        legacyBasePage.changeBooking.verifyChangeFlightIsEnabled(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R145_TST_7061_verifyChangeItineraryIsEnabledForBooker() {

        Hashtable testData = new DataUtility().getTestData("219");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber, passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.verifyChangeFlightIsEnabled(true);
    }

    @Test(groups = {"regression", "p2"})
    public void R96_TST_7470_verifyChangeFeeNotChargedForFlybeJustFlyAmendFlightsForJERGCI(){
        Hashtable testData = new DataUtility().getTestData("220");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("266");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R215_TST_7470_verifyChangeFeeNotChargedForBlueIslandsJustFlyAmendFlightsForJERGCI(){
        Hashtable testData = new DataUtility().getTestData("235");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("267");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R149_TST_7529_verifyChangeFeeNotChargedForFlybeJustFlyAmendFlightsForGCIJER(){
        Hashtable testData = new DataUtility().getTestData("236");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("268");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R216_TST_7529_verifyChangeFeeNotChargedForBlueIslandsJustFlyAmendFlightsForGCIJER(){
        Hashtable testData = new DataUtility().getTestData("237");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String>passengerNamesForRetrive  = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForRetrive);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        legacyBasePage.changeYourFlight.changeDateAndRoute(testData);
        Hashtable itineraryTestData = new DataUtility().getTestData("269");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.verifyChangeFeeIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void R156_TST_7927_verifyUserAbleToChangeTicketFromGetMoreToAllIn(){
        Hashtable testData = new DataUtility().getTestData("238");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String,String> passengerNamesForAmending = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(firstBookingRefNumber,passengerNamesForAmending);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("270");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.yourDetails.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.payment.selectPaymentOption(testData);
        legacyBasePage.payment.enterCardDetails(testData);
        legacyBasePage.payment.selectBillingAddressOption(true);
        legacyBasePage.payment.selectTripPurpose(testData);
        legacyBasePage.payment.acceptTermsAndCondition();
        legacyBasePage.payment.continueToBooking();
        String secondBookingRefNumber = legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
        legacyBasePage.bookingConfirmation.compareBookingRefNo(firstBookingRefNumber, secondBookingRefNumber);
    }

    @Test(groups = {"regression", "p2"})
    public void R154_TST_7925_verifyUserAbleToChangeTicketFromJustFlyToGetMore(){
        Hashtable testData = new DataUtility().getTestData("310");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String,String> passengerNamesForAmending = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(firstBookingRefNumber,passengerNamesForAmending);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("311");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.yourDetails.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.payment.selectPaymentOption(testData);
        legacyBasePage.payment.enterCardDetails(testData);
        legacyBasePage.payment.selectBillingAddressOption(true);
        legacyBasePage.payment.selectTripPurpose(testData);
        legacyBasePage.payment.acceptTermsAndCondition();
        legacyBasePage.payment.continueToBooking();
        String secondBookingRefNumber = legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
        legacyBasePage.bookingConfirmation.compareBookingRefNo(firstBookingRefNumber, secondBookingRefNumber);
    }

    @Test(groups = {"regression", "p2"})
    public void R155_TST_7926_verifyUserAbleToChangeTicketFromJustFlyToAllIn(){

           Hashtable testData = new DataUtility().getTestData("316");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String,String> passengerNamesForAmending = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String firstBookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(firstBookingRefNumber,passengerNamesForAmending);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("317");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.yourDetails.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.payment.selectPaymentOption(testData);
        legacyBasePage.payment.enterCardDetails(testData);
        legacyBasePage.payment.selectBillingAddressOption(true);
        legacyBasePage.payment.selectTripPurpose(testData);
        legacyBasePage.payment.acceptTermsAndCondition();
        legacyBasePage.payment.continueToBooking();
        String secondBookingRefNumber = legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
        legacyBasePage.bookingConfirmation.compareBookingRefNo(firstBookingRefNumber, secondBookingRefNumber);
    }

    @Test(groups = {"regression", "p2"})
    public void R123_TST_6100_verifyCarHireInItineraryFlowIsDisplayedWhenBookingAlreadyDoneWithCarHire(){
        Hashtable testData = new DataUtility().getTestData("239");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        Hashtable<String, String> passengerNameForAmending = basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails, passengerNames, 2, noOfPassengers, false);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNameForAmending);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("271");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"inbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(false);
        legacyBasePage.yourDetails.enterPassengerContact(testData);
        Hashtable passengerContactAddress = new DataUtility().getTestData("240");
        legacyBasePage.yourDetails.selectPassengerContactAddress(passengerContactAddress);
        legacyBasePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.yourDetails.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.carHire.verifyCarHireIsDisplayedInExtrasPage();
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3235
    public void R20_TST_3235_verifyChangeSeatAndBaggageInBookingConfirmation(){
        Hashtable testData = new DataUtility().getTestData("227");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        Hashtable<String, String> passengerNameForCheckin = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNameForCheckin);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToChangeBooking();
        basePage.changeBooking.continueToChangeSeat();
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.selectPassengerBaggage(testData,"edit");
        legacyBasePage.yourDetails.acceptBags();
        legacyBasePage.yourDetails.selectSeats(testData,"outbound","edit");
        legacyBasePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = legacyBasePage.yourDetails.captureSeatsSelectedForPassengers();
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.chooseExtras.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.payment.selectPaymentOption(testData);
        legacyBasePage.payment.enterCardDetails(testData);
        legacyBasePage.payment.selectBillingAddressOption(true);
        legacyBasePage.payment.selectTripPurpose(testData);
        legacyBasePage.payment.acceptTermsAndCondition();
        legacyBasePage.payment.continueToBooking();
        legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,editedSeats);
    }

    @Test(groups = {"regression", "p2"})
    public void R153_TST_7924_VerifyAnGuestUserIsAbleToAmendTheFlightAfterBooking(){
        Hashtable testData = new DataUtility().getTestData("321");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        Hashtable<String, String> passengerNameForAmending = basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        LegacyBase legacyBasePage = new LegacyBase();
        legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNameForAmending);
        legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
        legacyBasePage.manageBooking.continueToChangeBooking();
        legacyBasePage.changeBooking.continueToChangeFlights();
        Hashtable itineraryTestData = new DataUtility().getTestData("261");
        legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
        legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
        legacyBasePage.changeYourFlight.continueToYourDetails();
        legacyBasePage.yourDetails.selectFlybeAccountOption(true);
        legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        legacyBasePage.yourDetails.selectSeats(itineraryTestData,"outbound");
        legacyBasePage.yourDetails.acceptSeats();
        legacyBasePage.yourDetails.selectPassengerBaggage(itineraryTestData);
        legacyBasePage.yourDetails.acceptBags();
        Hashtable<String, Hashtable<String, Hashtable>> editedSeats = legacyBasePage.yourDetails.captureSeatsSelectedForPassengers();
        legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
        legacyBasePage.yourDetails.continueToPayment();
        legacyBasePage.yourDetails.handleNagWindow("confirm");
        legacyBasePage.payment.selectPaymentOption(testData);
        legacyBasePage.payment.enterCardDetails(testData);
        legacyBasePage.payment.selectBillingAddressOption(true);
//   legacyBasePage.payment.selectTripPurpose(testData);
        legacyBasePage.payment.acceptTermsAndCondition();
        legacyBasePage.payment.continueToBooking();
        legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();

    }

    @Test(groups = {"regression", "p1"})
    public void R217_TST_10915_verifyCarParkingOptionIsNotDisplayedForRouteThatHasNoCarParkingOption(){
        Hashtable testData = new DataUtility().getTestData("86");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingReferenceNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingReferenceNumber,passengerNames);
        basePage.manageBooking.verifyCarParkingIsNotDisplayed();
    }

    @Test(groups = {"regression", "p1"})
    public void R174_TST_10901_verifyManageMyBookingPageSecondTimeOfBookingRetrievalWithBagSeatCarParkingInsurance() {
        Hashtable testData = new DataUtility().getTestData("87");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingReferenceNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingReferenceNumber, passengerNames);
        basePage.manageBooking.verifyBoxeverForBagAndSeat();
    }

    @Test(groups = {"regression", "p1"})
    public void R175_TST_10892_10903_verifyManageMyBookingPageSecondTimeOfBookingRetrievalWithBagSeatCarParkingInsurance() {
        Hashtable testData = new DataUtility().getTestData("88");
        LFBase basePage = new LFBase();
        LegacyBase basePage1 = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.addCarHire("avis");
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingReferenceNumber1 = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingReferenceNumber1, passengerNames);
        basePage.manageBooking.verifyBoxeverForBagAndSeat();
        basePage.manageBooking.selectElementFromBoxever("bag");
        basePage1.yourDetails.selectFlybeAccountOption(true);
        basePage1.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage1.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage1.yourDetails.selectPassengerBaggage(testData,"edit");
        basePage1.yourDetails.acceptBags();
        basePage1.yourDetails.selectCheckInOption(testData,passengerNames);
        basePage1.yourDetails.continueToPaymentOnManageBooking();
        basePage1.yourDetails.handleNagWindow("confirm");
        basePage1.payment.selectPaymentOption(testData);
        basePage1.payment.enterCardDetails(testData);
        basePage1.payment.selectBillingAddressOption(true);
        basePage1.payment.acceptTermsAndCondition();
        basePage1.payment.continueToBooking();
        String bookingReferenceNumber = basePage1.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingReferenceNumber, passengerNames);
        basePage.manageBooking.verifyBoxeverForBagAndSeat();

    }

}
