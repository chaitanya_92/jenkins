package test_definition.flybe.lf_three.lf3_checkin.international;

import PageBase.LFBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by varunvm on 3/20/2017.
 */
public class International {
    @Test(groups = {"smoke", "s1"})
    public void S43_TST_6410_verifyCheckInOneAdultOneTeenOneChildOneInfantUKToNonUKIndirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("68");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "s1"})
    public void S29_TST_6411_6413_6387_verifyCheckInTwoAdultsOneTeenOneChildNonUKToUKIndirectFlightWithoutLogin() {
        //merged 6413 and 6387 with 6411
        Hashtable testData = new DataUtility().getTestData("69");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
//        basePage.lfCarHire.addInsurance(testData);
//        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        String[] genders = {"male"};
        basePage.addAPI.verifyGenderForPassenger(genders);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "adult");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

    }

//-----------regression------------------

    @Test(groups = {"regression", "p2"})
    public void R95_TST_7378_verifyCheckInForOneAdultForFlybeAndBlueIslandsFlightForNonUKToUK(){
        Hashtable testData = new DataUtility().getTestData("212");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "adult");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound");
    }


    //    _________moved from smoke to regression---------


    @Test(groups = {"regression", "p2"})
    public void R135_TST_6404_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightNonUKToUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("67");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfPayment.selectFlybeAccountOption(true);

        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

    }

    @Test(groups = {"regression", "p1"})
    public void R134_TST_6403_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("66");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        basePage.selectPassengerForCheckIn.deSelectAllPassengersForCheckIn("outbound");
        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }

    @Test(groups = {"regression", "p3"})
    public void S24_TST_6376_verifyCheckInTwoAdultsFourChildrenOneTeenOneInfantInDirectFlightNonUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("65");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.continueToAddAPI(5);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 5, "child");
        basePage.leapCheckIn.continueToAddAPI(6);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 6, "child");
        basePage.leapCheckIn.continueToAddAPI(7);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 7, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }



    //      @Test(groups = {"smoke", "p3"})
//    This method verifies the gender in API page when Mstr is selected
    public void TST_6413_verifyCheckInGenderForMasterDirectFlightUKToNonUKWithoutLogin() {
        //merged 6413 and 6387 with 6411
        Hashtable testData = new DataUtility().getTestData("81");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false, seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        String[] genders = {"male"};
        basePage.addAPI.verifyGenderForPassenger(genders);
    }


    @Test(groups = {"regression", "p2"})
    public void R227_TST_3108_verifyCheckInTwoAdultOneTeenOneChildOneInfantUKToNonUKDirectStobartFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("320");
        LFBase basePage = new LFBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,overAllFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails,1,"tomorrow");
        basePage.seatOpener.seatOpen(selectedInboundFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("inbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
    }

}
