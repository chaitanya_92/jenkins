package test_definition.cms.lf_two.booking;

import PageBase.CMSObjects;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSAncillaries {

    @Test(groups = {"regression", "p2","smoke"})
    public void TST_10044_10045_verify0kg15kg23kg46kgBaggagesinFlightOptionPageFlybe()
    {
        Hashtable testData = new DataUtility().getTestData("352");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggageSectionIsDisplayed();
        basePage.yourDetails.verifyBaggagesDisplayedInHoldLuggage(noOfPassengers);
        basePage.yourDetails.verify23KgIsSelected(noOfPassengers);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_10045_verify0kg15kg23kg46kgBaggagesinFlightOptionPageAirFrance() {
        Hashtable testData = new DataUtility().getTestData("353");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verify23KgIsSelected(noOfPassengers);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_10045_verify0kg15kg23kg46kgBaggagesinFlightOptionPageStobartAir() {
        Hashtable testData = new DataUtility().getTestData("354");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verify23KgIsSelected(noOfPassengers);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_10045_verify0kg15kg23kg46kgBaggagesinFlightOptionPageBlueIsland() {
        Hashtable testData = new DataUtility().getTestData("355");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verify23KgIsSelected(noOfPassengers);
    }

}
