package test_definition.cms.lf_two.booking;

import PageBase.CMSObjects;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSAviosBooking {

    @Test(groups = {"smoke","s1"})
    public void TST_verifyAviosBooking()
    {
        Hashtable testData = new DataUtility().getTestData("350");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable selectedFlightDetails=basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.verifyTheAviosOptions();
        basePage.yourDetails.verifyAviosDisplayPasswordForgottenPasswordAndYourUserNameAreDisplayed();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        //Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice =basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6912
    public void TST_6912_VerifyAviosCannotBeUsedForTravelInsurance() {
        Hashtable testData = new DataUtility().getTestData("288");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(17.50,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(yourDetailsBasket);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket(maxAviosPrice);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6857
    public void TST_6857_VerifyUserCannotUseAviosPartPayForCarParking() {
        Hashtable testData = new DataUtility().getTestData("255");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(20.50,"outbound");
        basePage.fareSelect.selectLowestPriceDate(20.99,"inbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 1);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.addCarParking(testData);
        basePage.carHire.verifyBasket(maxAviosPrice);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6696
    public void TST_6696_VerifyPartPayAviosForEuroCurrency() {
        Hashtable testData = new DataUtility().getTestData("256");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("low");
        basePage.yourDetails.verifyCurrencyInAvios("\u20ac");
        basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2, 0);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6708
    public void TST_6708_VerifyUserIsAbleToLoginWithAviosAccountAndViewBalance() {
        Hashtable testData = new DataUtility().getTestData("257");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTheAviosOptions();
        basePage.yourDetails.verifyAviosDisplayPasswordForgottenPasswordAndYourUserNameAreDisplayed();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();

    }

    //    @Test(groups = {"regression", "p2"})// Added by Arun TST-6812
//    cannot be automated as the ticketing amount can be verified only from shares and DB
    public void TST_6812_VerifyTicketingDoneSuccessfullyForFullAmountOfBookingUsingAviosPartPay() {
        Hashtable testData = new DataUtility().getTestData("258");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2,0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6817
    public void TST_6817_VerifyUserCanMakeBookingByAddingAviosAndPaypalOptions() {
        Hashtable testData = new DataUtility().getTestData("259");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 2, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.payment.enterPayPalCardDetails(testData);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyPaidUsingPaypalIsDisplayed();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-8316
    public void TST_8316_VerifyWhenPayableAmountIsZeroUserIsAbleToCompleteBookingSuccessfullyWithByAddingAvios() {
        Hashtable testData = new DataUtility().getTestData("281");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(17.50,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        double maxAviosPrice = basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double expectedAmount = basePage.payment.verifyBasket(maxAviosPrice);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyTotalAmount(expectedAmount);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

    //    TODO: need to implement the verifyTheToolTipMessageInAviosLogin
//    @Test(groups = {"regression", "p2"})// Added by Arun TST-6844
    public void TST_6844_VerifyATRPMemberIsTryingToLoginShouldDisplayToolTipMessage() {
        Hashtable testData = new DataUtility().getTestData("282");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.verifyTheToolTipMessageInAviosLogin();

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6845
    public void TST_6845_VerifyBookingConfirmationAviosPartPayIsAppliedWithDifferentCurrency() {
        Hashtable testData = new DataUtility().getTestData("283");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyAviosCurrency("CHF");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }



    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForBags() {
        Hashtable testData = new DataUtility().getTestData("284");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(17.50,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        double aviosPrice =basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForSeats() {
        Hashtable testData = new DataUtility().getTestData("291");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(17.50,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("medium");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 0);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6746
    public void TST_6746_VerifyAviosDiscountValueIsShownSeparatelyInBasketInAllPages() {
        Hashtable testData = new DataUtility().getTestData("285");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.yourDetails.verifyAviosBalanceIsDisplayed();
        basePage.yourDetails.selectAviosType("high");
        Hashtable<String, Double> yourDetailsBasket = basePage.yourDetails.verifyBasket(testData, noOfPassengers, 1, 1);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.carHire.addCarParking(testData);
        Hashtable<String, Double> carHireBasket = basePage.carHire.verifyBasket();
        basePage.carHire.continueToPaymentPage();

        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(carHireBasket);
        basePage.payment.selectPaymentOption(testData);

        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.verifyBasket();
        double aviosPrice = basePage.payment.captureAviosPriceFromBasket();
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,overAllFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

}
