package test_definition.cms.lf_two.booking;

import PageBase.CMSObjects;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/1/2017.
 */
public class CMSBooking {

   // @Test(groups = "smoke")
    public void verifyBookingInCamFlow(){

        Hashtable testData = new DataUtility().getTestData("1");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.selectFlight(testData,"nonStop","inbound");
        basePage.fareSelect.selectFlightOption(testData,"inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
       // basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.selectSeats(testData,"inbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
    }

    @Test(groups = { "smoke","p1" })
    public void TST_6351_verifySearchFlights() {
        Hashtable testData = new DataUtility().getTestData("3");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.enterPromoCode(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyFlightListDisplayed();
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6396_verifyMessageNoFlightsAvailable() {
        Hashtable testData = new DataUtility().getTestData("47");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyNoFlightsMessageDisplayed();
    }

    @Test(groups = {"smoke", "p1",})
    public void TST_6353_verifyBookingSeatsWithoutSelectingSeatsAndLuggageAsLoggedinUser(){
        Hashtable testData = new DataUtility().getTestData("5");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_6352_verifyBookingSeatsWithoutSelectingSeatsAndLuggageAsGuestUser() {
        Hashtable testData = new DataUtility().getTestData("4");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "p1", "p5"})
    public void TST_6354_verifyBookingSeatsWithSelectingSeatsAndLuggageAsGuestUser() {
        Hashtable testData = new DataUtility().getTestData("6");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_6355_verifyBookingSeatsWithSelectingSeatsAndLuggageAsLoggedinUser() {
        Hashtable testData = new DataUtility().getTestData("8");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_6389_verifyAddAPIPresentForNonUKFlights(){
        Hashtable testData = new DataUtility().getTestData("41");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"smoke", "p2"})
    public void TST_6388_verifyAddAPINotPresentForUKFlights(){
        Hashtable testData = new DataUtility().getTestData("40");

        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_6416_verifyBookingWithInsurance(){
        Hashtable testData = new DataUtility().getTestData("24");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6395_verifyBookingWithIncorrectCardDetails(){
        Hashtable testData = new DataUtility().getTestData("25");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.verifySecurityNumberValidation();
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6414_verifyBookingInMyAccountLiveFlightList() {
        Hashtable testData = new DataUtility().getTestData("101");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.sortByBookingDate();
        basePage.myAccountHome.verifyBookingInMyAccount(bookingRefNumber);


    }

    @Test(groups = {"smoke", "p3"})
    public void TST_3132_3479_FooterTest(){
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.verifyHomePageFooter();
        Hashtable testData = new DataUtility().getTestData("58");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyFareSelectPageFooter();
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.verifyPassengerDetailsFooter();
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.verifyPaymentPageFooter();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.verifyCheckInPageFooter();
    }
}
