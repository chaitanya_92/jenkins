package test_definition.cms.lf_two.booking;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/6/2017.
 */
public class CMSRegressionSearchFlights {

    @Test(groups = {"regression", "p1"})
    public void TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyOneWay(){
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("46");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p1"})
    public void TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyTwoWay(){
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("45");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyOneWay() {
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("48");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyTwoWay() {
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("49");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1623_verifyDirectAirFranceDisabledForUnaccompaniedTeens(){
        Hashtable testData = new DataUtility().getTestData("109");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound","nonstop");

    }

    @Test(groups = {"regression", "p1"})
    public void TST_1623_verifyIndirectAirFranceDisabledForUnaccompaniedTeens(){
        Hashtable testData = new DataUtility().getTestData("110");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound","onechange");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_1506_VerifyMessageForBookingWithMoreThanEightTeens(){
        LegacyBase basePage = new LegacyBase();
        Hashtable testData = new DataUtility().getTestData("115");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageForMoreThanEightPassengers();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4825_verifyBaggageAllowanceTextForFlightLessThanFortyFiveMinFlightTime() {
        Hashtable testData = new DataUtility().getTestData("119");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly","20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more","20kg");
        basePage.fareSelect.verifyHoldLuggage("All in","30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4824_verifyBaggageAllowanceTextForLoganAirTwinOtterFlights() {
        Hashtable testData = new DataUtility().getTestData("120");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly","20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more","20kg");
        basePage.fareSelect.verifyHoldLuggage("All in","20kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4823_verifyBaggageAllowanceTextForLoganAirFlights() {
        Hashtable testData = new DataUtility().getTestData("121");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly","20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more","20kg");
        basePage.fareSelect.verifyHoldLuggage("All in","30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4719_verifyChangeRouteToBHXToMXPForTeenOnlyInSearchWidget() {
        Hashtable testData = new DataUtility().getTestData("122");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.changeDestinationToItaly();
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForTeensForItaly();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4188_verifyClickingBaggageRulesLinkDisplaysBaggageRulesPopup(){
        Hashtable testData = new DataUtility().getTestData("123");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("fare rules");
        basePage.fareSelect.verifyRulesPopupOpened("fare rules");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4187_verifyClickingFareRulesLinkDisplaysFareRulesPopup(){
        Hashtable testData = new DataUtility().getTestData("123");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("baggage rules");
        basePage.fareSelect.verifyRulesPopupOpened("baggage rules");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4183_verifyClickingTaxesLinkDisplaysTaxesPopup(){
        Hashtable testData = new DataUtility().getTestData("123");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4184_verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected(){
        Hashtable testData = new DataUtility().getTestData("124");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
        basePage.fareSelect.verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected(testData);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4157_verifySelectingOutboundGetMoreFareMakesInboundAllInAndJustFlyUnSelectable(){
        Hashtable testData = new DataUtility().getTestData("125");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.selectFlight(testData,"nonStop","inbound");
        basePage.fareSelect.verifyJustFlyAllInOptionsAreDisableWhenGetMoreOptionIsSelectedInOutbound();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4160_verifyDisplayingOfFlightStopDetailsOnClickingStopNumberLink(){
        Hashtable testData = new DataUtility().getTestData("126");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange","outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(true);
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange","outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4129_verifyAirFranceFaresDisplayedTeenMessaging(){
        Hashtable testData = new DataUtility().getTestData("127");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound","nonstop");

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4136_verifyCitiJetHopRegionalFaresDisplayInformativeMessageIfMoreThanFourPassengersAreSelected(){
        Hashtable testData = new DataUtility().getTestData("128");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageForCitiJetHopRegionalWhenMoreThanFourPassengerIsSelected("outbound","nonstop");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7519_verifyACIIsNotDisplayedForBlueIslandDirectFlightUKToUK(){
        Hashtable testData = new DataUtility().getTestData("151");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyACIOptionDisplayedForBlueIsland();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3398_verifyAdditionalAirportChargeIsDisplayedForNorwichAirport(){
        Hashtable testData = new DataUtility().getTestData("171");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageAdditionalAirportCharges(testData);
    }

    //    @Test(groups = {"regression", "p2"})
    //    test case is obsolete
    public void TST_3399_verifyAdditionalAirportChargeIsDisplayedForNewquayAirport(){
        Hashtable testData = new DataUtility().getTestData("172");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageAdditionalAirportCharges(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3381_verifyLowestFareDisplayedIsMatchingWithFareSlider(){
        Hashtable testData = new DataUtility().getTestData("173");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyLowestFareIsDisplayedInFareSliderOutbound();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_BlueIslands(){
        Hashtable testData = new DataUtility().getTestData("174");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }

    //    @Test(groups = {"regression", "p2"})
//    Flybe flights are no longer available in this route
    public void TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_Flybe(){
        Hashtable testData = new DataUtility().getTestData("175");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }

    //    @Test(groups = {"regression", "p2"})
//    Flybe flights are no longer available in this route
    public void TST_7828_verify23KgBaggageFreeForJER_GCIFlybe(){
        Hashtable testData = new DataUtility().getTestData("176");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForJER_GCIBlueIslands(){
        Hashtable testData = new DataUtility().getTestData("177");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForGCI_JERFlybe(){
        Hashtable testData = new DataUtility().getTestData("178");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForGCI_JERBlueIslands(){
        Hashtable testData = new DataUtility().getTestData("179");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    //    @Test(groups = {"regression", "p2"})
//    obsolete test case
    public void TST_3553_verify23KgBaggageFreeForEXT_MAN(){
        Hashtable testData = new DataUtility().getTestData("180");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3548_verify20kgIsIncludedForLoganairWithJustFly(){
        Hashtable testData = new DataUtility().getTestData("181");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggageTextForLoganair("just fly");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3550_verify20kgIsIncludedForLoganairWithGetMore(){
        Hashtable testData = new DataUtility().getTestData("182");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggageTextForLoganair("get more");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3551_verify30kgIsIncludedForLoganairWithAllIn(){
        Hashtable testData = new DataUtility().getTestData("183");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyBaggageTextForLoganair("all in");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3549_verifyIncreaseBaggageAllowanceForFlybeWithGetMore(){
        Hashtable testData = new DataUtility().getTestData("184");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyIncreaseBaggageAllowanceForGetMore();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3549_verifyIncreaseBaggageAllowanceForStobartWithGetMore(){
        Hashtable testData = new DataUtility().getTestData("185");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyIncreaseBaggageAllowanceForGetMore();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3554_verifyIncreaseBaggageAllowanceForFlybeWithGetMore(){
        Hashtable testData = new DataUtility().getTestData("184");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.fareSelect.changeCurrency("GBP");
        basePage.yourDetails.verifyIncreaseBaggageAllowanceForGetMore();
        basePage.yourDetails.verifyBaggagePriceForGetMoreDisplayedAtReducedCost();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4189_clickingPriceBreakdownLinkDisplaysBreakdownOfTheFareTotal(){
        Hashtable testData = new DataUtility().getTestData("201");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3656_TST_3599_verifyBasketRetainsSelectionsFromPassengerDetailsPageAndExtrasPage(){
        Hashtable testData = new DataUtility().getTestData("202");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.selectFlight(testData,"nonStop","inbound");
        basePage.fareSelect.selectFlightOption(testData,"inbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        Hashtable<String,Double> previousBasket;
        previousBasket = basePage.fareSelect.verifyBasket(testData);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(previousBasket);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData,"outbound");
        basePage.yourDetails.selectSeats(testData,"inbound");
        basePage.yourDetails.acceptSeats();
        previousBasket = basePage.yourDetails.verifyBasket(testData,noOFPassengers,1,1);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(previousBasket);
        basePage.carHire.addCarHire("avis");
        basePage.carHire.addCarParking(testData);
        Hashtable<String,Double> extrasPageBasket = basePage.carHire.verifyBasket();
        basePage.carHire.continueToPaymentPage();
        basePage.payment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasPageBasket);
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.verifyBasket();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3622_verifyClickingAddBagsNowAddsStandardBagCostToBasketAndChangesButtonToRemoveBags(){
        Hashtable testData = new DataUtility().getTestData("231");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.addBaggageFromLastChance();
        basePage.payment.enterCardDetails(testData);
        basePage.payment.verifyBasket();
        basePage.payment.verifyRemoveBagsButtonIsDisplayed();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3585_verifyOnCheckBoxCheckedFlexPriceIsAddedToBasket(){
        Hashtable testData = new DataUtility().getTestData("232");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        basePage.yourDetails.addFlexToBooking();
        basePage.yourDetails.verifyBasket(testData,noOFPassengers,1,0);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3552_verifyHoldBaggageAllowanceForHopRegionalWithAllIn(){
        Hashtable testData = new DataUtility().getTestData("187");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3552_verifyHoldBaggageAllowanceForAirFranceWithAllIn(){
        Hashtable testData = new DataUtility().getTestData("188");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3552_verifyHoldBaggageAllowanceForStobartAirWithAllIn(){
        Hashtable testData = new DataUtility().getTestData("189");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3552_verifyHoldBaggageAllowanceForFlybeWithAllIn(){
        Hashtable testData = new DataUtility().getTestData("211");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyAllinHoldBaggageMessage();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4156_verifyFaresDisplayedMatchesWithPriceBreakDownForAllPax(){
        Hashtable testData = new DataUtility().getTestData("272");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.selectFlight(testData,"nonStop","inbound");
        basePage.fareSelect.selectFlightOption(testData,"inbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4636_verifyChildTaxesNotDisplayedWhenChildIsNotSelected(){
        Hashtable<String, String> testData = new DataUtility().getTestData("273");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyPaxIsNotDisplayedInPriceBreakDown("child");
    }

    @Test(groups = {"regression","p2"})
    public void TST_9511_verifyCurrencyIsSelectedBasedOnDepartureCurrencyNotSetViaURLCookie()
    {
        Hashtable<String, String> testData = new DataUtility().getTestData("321");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("CurrencyType"));
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.verifyCurrency(testData.get("CurrencyType"));

    }

}
