package test_definition.cms.lf_two.login_register;

import PageBase.CMSObjects;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/5/2017.
 */
public class LoginAndRegister {

    @Test(groups = { "smoke","p1" })
    public void TST_6349_verifyLogin() {
        Hashtable testData = new DataUtility().getTestData("2");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
    }

    @Test(groups = { "smoke","p2" })
    public void TST_6350_verifyCustomerRegistration() {
        Hashtable testData = new DataUtility().getTestData("23");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("registration");
        basePage.login.enterRegistrationEmailDetails(testData);
        basePage.login.enterRegistrationPassengerDetails(testData);
        basePage.login.enterRegistrationPassengerAddress(testData);
        basePage.login.enterRegistrationPassengerPhoneNumber(testData);
        basePage.login.selectRegistrationAviosOptions(testData);
        basePage.login.continueToRegistration();
        basePage.login.verifyRegistration();
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_4552_TST_6415_verifyUserAbleToModifyUserDetailsAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("83");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.myAccountHome.continueToMyAccountDetails();
        String editedDetails[] = basePage.myAccountHome.editAccountDetails();
        basePage.myAccountHome.continueToMyAccountDetails();
        basePage.myAccountHome.verifyEditingAccountDetails(editedDetails);
        String revertedDetails[] = basePage.myAccountHome.editAccountDetails();
        basePage.myAccountHome.continueToMyAccountDetails();
        basePage.myAccountHome.verifyEditingAccountDetails(revertedDetails);

    }
}
