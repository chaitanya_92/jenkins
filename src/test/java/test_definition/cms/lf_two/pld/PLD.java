package test_definition.cms.lf_two.pld;

import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/5/2017.
 */
public class PLD {

    @Test(groups = {"regression","p2","smoke"})
    public void TST_10133_verifyUserCanRebookTheBookingThatWasfareLocked()
    {
        Hashtable testData = new DataUtility().getTestData("292");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        Hashtable overallBasketPrice=basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.enterCardDetails(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
        basePage.leapPLDConfirmation.navigateToPLDYourDetails();
        basePage.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(overallBasketPrice);
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);

    }
}
