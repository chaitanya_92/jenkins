package test_definition.cms.lf_two.checkin;

import PageBase.CMSObjects;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/6/2017.
 */
public class RegressionCheckin {
    @Test(groups = {"regression", "p2"})
    public void TST_4330_verifyCheckInWithOneAdultDirectFlightUkToUkWithoutLogin() {
        //needs test data to be created

        Hashtable testData = new DataUtility().getTestData("58");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_7378_verifyCheckInForOneAdultForFlybeAndBlueIslandsFlightForNonUKToUK(){
        Hashtable testData = new DataUtility().getTestData("212");
        CMSObjects basePage = new CMSObjects();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "adult");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7119_VerifyCheckInPageURLHasBookerUserName(){
        Hashtable testData = new DataUtility().getTestData("234");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRef = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRef,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToCheckIn();
        basePage.manageBooking.verifyURLForPassengerName(passengerNames);


    }

    //Added by Arun
    @Test(groups = {})//Added by arun TST-3725
    public void TST_3725_VerifyVouchersAreNotDisplayedLoganAirFlight() {
        Hashtable testData = new DataUtility().getTestData("194");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(true);
        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound");
        basePage.leapCheckIn.verifyTheVoucherSectionForLoganAirFlight();
    }

    @Test(groups = {"regression", "p1"})// Added by Arun TST-3096
    public void TST_3096_VerifyAirFranceCheckInAtTheAirport(){
        Hashtable testData = new DataUtility().getTestData("248");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData,passengerNames,noOfPassengers,"adult");
        basePage.leapCheckIn.verifyTheCheckInLinkForAirFrance();
    }


    @Test(groups = {"smoke", "p2"})
    public void TST_6404_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightNonUKToUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("67");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_6403_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("66");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }


    @Test(groups = {"smoke", "p2"})
    public void TST_6400_verifyCheckInValidationMessageForNonTodayFlightForOneAdultTwoWay() {
        Hashtable testData = new DataUtility().getTestData("76");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.selectSeats(testData, "inbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("outbound");
        basePage.leapCheckIn.verifyCheckInValidationMessageForNonTodayFlight("inbound");
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6398_verifyCheckInTwoWayTwoAdultsOneChildOneInfantDirectFlightUKToUKWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("74");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.selectSeats(testData, "inbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,overAllFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedOutboundFlightDetails,passengerNames,bookingRefNumber,checkInDetailsOutBound,false,noOfPassengers,1,"outbound",seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedInboundFlightDetails,passengerNames,bookingRefNumber,checkInDetailsInBound,false,noOfPassengers,1,"inbound",seatSelected);

    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6358_verifyCheckInWithEightAdultsDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("59");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"smoke", "p3"})
    public void TST_6367_verifyCheckInEightTeensDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("63");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    //    @Test(groups = {"smoke", "p3"})
    //Test data to be added
    public void TST_6382_verifyCheckInOutboundAndInboundOneAdultUKToUKDirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("85");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.selectSeats(testData, "inbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.carHire.isExtrasPageLoaded();
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6376_verifyCheckInTwoAdultsFourChildrenOneTeenOneInfantInDirectFlightNonUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("65");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.continueToAddAPI(5);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 5, "child");
        basePage.leapCheckIn.continueToAddAPI(6);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 6, "child");
        basePage.leapCheckIn.continueToAddAPI(7);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 7, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p2"})
    public void TST_6369_verifyCheckInThreeAdultsThreeChildrenTwoTeensTwoInfantsDirectFlightUkToUkWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("64");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.selectFlybeAccountOption(false);
        basePage.yourDetails.enterPassengerContact(testData);
        basePage.yourDetails.selectPassengerContactAddress(testData);
        basePage.yourDetails.enterPassengerContactNumbersEmails(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6365_verifyCheckInFourAdultsFourTeensDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("61");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"smoke", "p3"})
    public void TST_6363_verifyCheckInWithFourAdultsFourChildrenDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("62");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"smoke", "p3"})
    public void TST_6360_verifyCheckInWithFourAdultsFourInfantsDirectFlightUkToUkAfterLogin() {

        Hashtable testData = new DataUtility().getTestData("60");
        LegacyBase basePage = new LegacyBase();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage.yourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.yourDetails.capturePassengerNames(testData);
        basePage.yourDetails.selectPassengerBaggage(testData);
        basePage.yourDetails.acceptBags();
        basePage.yourDetails.selectSeats(testData, "outbound");
        basePage.yourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.yourDetails.captureSeatsSelectedForPassengers();
        basePage.yourDetails.continueToPayment();
        basePage.yourDetails.handleNagWindow("confirm");
        basePage.payment.selectPaymentOption(testData);
        basePage.payment.enterCardDetails(testData);
        basePage.payment.selectBillingAddressOption(true);
        basePage.payment.selectTripPurpose(testData);
        basePage.payment.acceptTermsAndCondition();
        basePage.payment.continueToBooking();
        basePage.payment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

}
