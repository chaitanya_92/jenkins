package test_definition.cms.lf_three.pld;

import PageBase.CMSObjects;
import PageBase.CMSObjectsLF;
import PageBase.LFBase;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/5/2017.
 */
public class PLD {

    @Test(groups = {"regression", "p2", "smoke"})
    public void TST_10133_verifyUserCanRebookTheBookingThatWasfareLocked() {
        Hashtable testData = new DataUtility().getTestData("356");
        CMSObjectsLF basePage = new CMSObjectsLF();
        CMSObjects basePage1 = new CMSObjects();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable overallBasketPrice = basePage.fareSelect.verifyTotalBasketCostWithOutboundAndInboundCost();
        basePage.fareSelect.navigateToPLDPayment();
        basePage.leapPLD.selectFlybeAccountOption(true);
        basePage.leapPLD.retrievePassengerContactDetailsByLogin(testData);
        basePage.leapPLD.selectPaymentOption(testData);
        basePage.leapPLD.enterCardDetails(testData);
        basePage.leapPLD.acceptTermsAndCondition();
        basePage.leapPLD.continueToPLDBooking();
        basePage.leapPLDConfirmation.verifyPLDReferenceNumber();
        basePage.leapPLDConfirmation.navigateToPLDYourDetails();
        basePage1.yourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(overallBasketPrice);
        basePage1.yourDetails.populateTelephoneNoManuallyWhenLoggedIn(testData);
        basePage1.yourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage1.yourDetails.capturePassengerNames(testData);
        basePage1.yourDetails.continueToPayment();
        basePage1.yourDetails.handleNagWindow("confirm");
        basePage1.payment.selectPaymentOption(testData);
        basePage1.payment.enterCardDetails(testData);
        basePage1.payment.selectBillingAddressOption(true);
        basePage1.payment.selectTripPurpose(testData);
        basePage1.payment.acceptTermsAndCondition();
        basePage1.payment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }
}

