package test_definition.cms.lf_three.checkin_seatopener;

import PageBase.CMSObjectsLF;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class RegCheckIn {


    @Test(groups = {"regression", "p2"})
    public void TST_4330_verifyCheckInWithOneAdultDirectFlightUkToUkWithoutLogin() {

        //needs test data to be created

        Hashtable testData = new DataUtility().getTestData("58");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_7378_verifyCheckInForOneAdultForFlybeAndBlueIslandsFlightForNonUKToUK(){
        Hashtable testData = new DataUtility().getTestData("212");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "adult");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7119_VerifyCheckInPageURLHasBookerUserName(){
        Hashtable testData = new DataUtility().getTestData("234");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRef = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.cheapFlight.enterBookingDetailsForAmending(bookingRef,passengerNames);
        basePage.manageBooking.waitAndCheckForTicketing(3);
        basePage.manageBooking.continueToCheckIn();
        basePage.manageBooking.verifyURLForPassengerName(passengerNames);
    }

    //Added by Arun
    @Test(groups = {"regression", "p2"})//Added by arun TST-3725
    public void TST_3725_VerifyVouchersAreNotDisplayedLoganAirFlight() {
        Hashtable testData = new DataUtility().getTestData("194");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound");
        basePage.leapCheckIn.verifyTheVoucherSectionForLoganAirFlight();

    }

    @Test(groups = {"regression", "p1"})// Added by Arun TST-3096
    public void TST_3096_VerifyAirFranceCheckInAtTheAirport(){
        Hashtable testData = new DataUtility().getTestData("248");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails,passengerNames, 1, noOfPassengers, false);
//        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData,passengerNames,noOfPassengers,"adult");
        basePage.leapCheckIn.verifyTheCheckInLinkForAirFrance();
    }

    @Test(groups = {"smoke", "p1"})
    public void TST_4582_verifyCheckInOneAdultsOneChildOneTeenOneInfantUKToUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("302");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_7376_VerifyCheckInOptionIsNotPossibleForInternationalFlightOperatedByBlueIslandsWithoutAPIDataEntered(){
        Hashtable testData = new DataUtility().getTestData("304");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false);
//        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);

    }


//    _________moved from smoke---------



    @Test(groups = {"regression", "p2"})
    public void TST_6404_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightNonUKToUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("67");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfPayment.selectFlybeAccountOption(true);

        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        ////basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

    }

    @Test(groups = {"regression", "p1"})
    public void TST_6403_verifyCheckInOneAdultOneTeenOneChildOneInfantIndirectFlightUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("66");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "teen");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        basePage.selectPassengerForCheckIn.deSelectAllPassengersForCheckIn("outbound");
        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_6398_verifyCheckInTwoWayTwoAdultsOneChildOneInfantDirectFlightUKToUKWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("74");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails,1,"tomorrow");
        basePage.seatOpener.seatOpen(selectedInboundFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);


    }


    @Test(groups = {"regression", "p3"})
    public void TST_6358_verifyCheckInWithEightAdultsDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("59");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);

    }


    @Test(groups = {"regression", "p3"})
    public void TST_6367_verifyCheckInEightTeensDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("63");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }



    @Test(groups = {"regression", "p3"})
    public void TST_6382_verifyCheckInOutboundAndInboundOneAdultUKToUKDirectFlightWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("73");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overAllFlightDetails,passengerNames, 2, noOfPassengers, false, seatSelected);
        basePage.seatOpener.seatOpen(selectedOutboundFlightDetails, 1, "tomorrow");
        basePage.seatOpener.seatOpen(selectedInboundFlightDetails, 1, "tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber, passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedOutboundFlightDetails, noOfPassengers, 1, "outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedInboundFlightDetails, noOfPassengers, 1, "inbound");
        Hashtable<String, Hashtable> checkInDetailsOutBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedOutboundFlightDetails, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetailsInBound = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedInboundFlightDetails, 1, "inbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedOutboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsOutBound, false, noOfPassengers, 1, "outbound", seatSelected);
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedInboundFlightDetails, passengerNames, bookingRefNumber, checkInDetailsInBound, false, noOfPassengers, 1, "inbound", seatSelected);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_6376_verifyCheckInTwoAdultsFourChildrenOneTeenOneInfantInDirectFlightNonUKToNonUKAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("65");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.continueToAddAPI(1);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 1, "infant");
        basePage.leapCheckIn.continueToAddAPI(2);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 2, "adult");
        basePage.leapCheckIn.continueToAddAPI(3);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 3, "teen");
        basePage.leapCheckIn.continueToAddAPI(4);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 4, "child");
        basePage.leapCheckIn.continueToAddAPI(5);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 5, "child");
        basePage.leapCheckIn.continueToAddAPI(6);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 6, "child");
        basePage.leapCheckIn.continueToAddAPI(7);
        basePage.addAPI.populateAPIDetails(testData, passengerNames, 7, "child");
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//      basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_6369_verifyCheckInThreeAdultsThreeChildrenTwoTeensTwoInfantsDirectFlightUkToUkWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("64");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_6365_verifyCheckInFourAdultsFourTeensDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("61");
        CMSObjectsLF basePage = new CMSObjectsLF();

        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();



        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_6363_verifyCheckInWithFourAdultsFourChildrenDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("62");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_6360_verifyCheckInWithFourAdultsFourInfantsDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("60");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();

        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }


    @Test(groups = {"regression", "p3","c1"})
    public void TST_6380_verifyCheckInEnabledForTodayFlight() {
        Hashtable testData = new DataUtility().getTestData("7");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlight = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlight,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.verifyCheckInValidationMessageForTodayFlight("outbound");
    }

    @Test(groups = {"regression", "p1","c1"})
    public void TST_6364_verifyCheckInWithOneAdultOneTeenDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("53");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.deSelectAllPassengersForCheckIn("outbound");
//        basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"regression", "p1","c2"})
    public void TST_6366_verifyCheckInWithTwoAdultsTwoChildrenTwoInfantsTwoTeensDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("56");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }

    @Test(groups = {"regression", "p3","c2"})
    public void TST_6370_verifyCheckInWithOneAdultOneChildOneTeenOneInfantInDirectFlightUkToUkWithoutLogin() {
        Hashtable testData = new DataUtility().getTestData("57");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);

        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,2,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 2, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 2, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,2,"outbound",seatSelected);

    }

    @Test(groups = {"regression", "p1","c2"})
    public void TST_6368_verifyCheckInWithOneTeenDirectFlightUkToUkAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("55");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.seatOpener.seatOpen(selectedFlightDetails,1,"tomorrow");
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(4);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
//        //basePage.selectPassengerForCheckIn.selectPassengerAndFlightForCheckIn(testData, selectedFlightDetails, noOfPassengers, 1, "outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",seatSelected);
    }







}
