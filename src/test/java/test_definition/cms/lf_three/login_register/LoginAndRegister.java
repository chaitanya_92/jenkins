package test_definition.cms.lf_three.login_register;

import PageBase.CMSObjectsLF;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class LoginAndRegister {

    @Test(groups = { "smoked","s1" })
    public void TST_6349_verifyLogin() {
        Hashtable testData = new DataUtility().getTestData("2");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
    }

    @Test(groups = { "smoked","s1" })
    public void TST_6350_verifyCustomerRegistration() {
        Hashtable testData = new DataUtility().getTestData("23");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("registration");
        basePage.login.enterRegistrationEmailDetails(testData);
        basePage.login.enterRegistrationPassengerDetails(testData);
        basePage.login.enterRegistrationPassengerAddress(testData);
        basePage.login.enterRegistrationPassengerPhoneNumber(testData);
        basePage.login.selectRegistrationAviosOptions(testData);
        basePage.login.continueToRegistration();
        basePage.login.verifyRegistration();
    }

    @Test(groups = {"smoked", "s1"})
    public void TST_4552_TST_6415_verifyUserAbleToModifyUserDetailsAfterLogin() {
        Hashtable testData = new DataUtility().getTestData("83");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.myAccountHome.continueToMyAccountDetails();
        String editedDetails[] = basePage.myAccountHome.editAccountDetails();
        basePage.myAccountHome.continueToMyAccountDetails();
        basePage.myAccountHome.verifyEditingAccountDetails(editedDetails);
        String revertedDetails[] = basePage.myAccountHome.editAccountDetails();
        basePage.myAccountHome.continueToMyAccountDetails();
        basePage.myAccountHome.verifyEditingAccountDetails(revertedDetails);

    }

}
