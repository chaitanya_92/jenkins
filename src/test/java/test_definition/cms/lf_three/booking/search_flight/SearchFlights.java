package test_definition.cms.lf_three.booking.search_flight;

import PageBase.CMSObjectsLF;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/6/2017.
 */
public class SearchFlights {

    @Test(groups = {"regression","p2"})
    public void TST_9511_verifyCurrencyIsSelectedBasedOnDepartureCurrencyNotSetViaURLCookie()
    {
        Hashtable<String, String> testData = new DataUtility().getTestData("321");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("CurrencyType"));
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyCurrency(testData.get("CurrencyType"));

    }


    @Test(groups = {"regression", "p1"})
    public void TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyOneWay() {
        CMSObjectsLF basePage = new CMSObjectsLF();
        Hashtable testData = new DataUtility().getTestData("46");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p1"})
    public void TST_1615_verifyMessageUnaccompaniedMinorForTeensFlyFromItalyTwoWay() {
        CMSObjectsLF basePage = new CMSObjectsLF();
        Hashtable testData = new DataUtility().getTestData("45");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyOneWay() {
        CMSObjectsLF basePage = new CMSObjectsLF();
        Hashtable testData = new DataUtility().getTestData("48");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1616_verifyMessageUnaccompaniedMinorForTeensFlyToItalyTwoWay() {
        CMSObjectsLF basePage = new CMSObjectsLF();
        Hashtable testData = new DataUtility().getTestData("49");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1623_verifyDirectAirFranceDisabledForUnaccompaniedTeens() {
        Hashtable testData = new DataUtility().getTestData("109");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "nonstop");

    }

    @Test(groups = {"regression", "p1"})
    public void TST_1623_verifyIndirectAirFranceDisabledForUnaccompaniedTeens() {
        Hashtable testData = new DataUtility().getTestData("110");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "onechange");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_1506_VerifyMessageForBookingWithMoreThanEightTeens() {
        CMSObjectsLF basePage = new CMSObjectsLF();
        Hashtable testData = new DataUtility().getTestData("115");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageForMoreThanEightPassengers();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4825_verifyBaggageAllowanceTextForFlightLessThanFortyFiveMinFlightTime() {
        Hashtable testData = new DataUtility().getTestData("119");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4824_verifyBaggageAllowanceTextForLoganAirTwinOtterFlights() {
        Hashtable testData = new DataUtility().getTestData("120");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "20kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4823_verifyBaggageAllowanceTextForLoganAirFlights() {
        Hashtable testData = new DataUtility().getTestData("121");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyHoldLuggage("Just fly", "20kg");
        basePage.fareSelect.verifyHoldLuggage("Get more", "20kg");
        basePage.fareSelect.verifyHoldLuggage("All in", "30kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4719_verifyChangeRouteToBHXToMXPForTeenOnlyInSearchWidget() {
        Hashtable testData = new DataUtility().getTestData("122");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.changeDestinationToItaly();
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForTeensForItaly();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4188_verifyClickingBaggageRulesLinkDisplaysBaggageRulesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("fare rules");
        basePage.fareSelect.verifyRulesPopupOpened("fare rules");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4187_verifyClickingFareRulesLinkDisplaysFareRulesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("baggage rules");
        basePage.fareSelect.verifyRulesPopupOpened("baggage rules");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4183_verifyClickingTaxesLinkDisplaysTaxesPopup() {
        Hashtable testData = new DataUtility().getTestData("123");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4184_verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected() {
        Hashtable testData = new DataUtility().getTestData("124");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("taxes");
        basePage.fareSelect.verifyRulesPopupOpened("taxes and charges");
        basePage.fareSelect.verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected(testData);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4157_3412_verifySelectingOutboundGetMoreFareMakesInboundAllInAndJustFlyUnSelectable() {
        Hashtable testData = new DataUtility().getTestData("125");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.verifyJustFlyAllInOptionsAreDisableWhenGetMoreOptionIsSelectedInOutbound();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4160_verifyDisplayingOfFlightStopDetailsOnClickingStopNumberLink() {
        Hashtable testData = new DataUtility().getTestData("126");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange", "outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(true);
        basePage.fareSelect.openFlightStopDetailsForMultiSectorFlight("oneChange", "outbound");
        basePage.fareSelect.verifyFlightStopDetailIsDisplayed(false);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4129_verifyAirFranceFaresDisplayedTeenMessaging() {
        Hashtable testData = new DataUtility().getTestData("127");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyAirFranceIsDisabled("outbound", "nonstop");

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4136_verifyCitiJetHopRegionalFaresDisplayInformativeMessageIfMoreThanFourPassengersAreSelected() {
        Hashtable testData = new DataUtility().getTestData("128");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageForCitiJetHopRegionalWhenMoreThanFourPassengerIsSelected("outbound", "nonstop");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7519_verifyACIIsNotDisplayedForBlueIslandDirectFlightUKToUK() {
        Hashtable testData = new DataUtility().getTestData("151");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyACIOptionDisplayedForBlueIsland();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3398_verifyAdditionalAirportChargeIsDisplayedForNorwichAirport() {
        Hashtable testData = new DataUtility().getTestData("171");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyMessageAdditionalAirportCharges(testData);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3381_verifyLowestFareDisplayedIsMatchingWithFareSlider() {
        Hashtable testData = new DataUtility().getTestData("173");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyLowestFareIsDisplayedInFareSliderOutbound();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_BlueIslands() {
        Hashtable testData = new DataUtility().getTestData("174");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }



    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForJER_GCIBlueIslands() {
        Hashtable testData = new DataUtility().getTestData("177");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForGCI_JERFlybe() {
        Hashtable testData = new DataUtility().getTestData("178");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7828_verify23KgBaggageFreeForGCI_JERBlueIslands() {
        Hashtable testData = new DataUtility().getTestData("179");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3554_verifyIncreaseBaggageAllowanceForFlybeWithGetMore() {
        Hashtable testData = new DataUtility().getTestData("184");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceForGetMoreDisplayedAtReducedCost();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4189_clickingPriceBreakdownLinkDisplaysBreakdownOfTheFareTotal() {
        Hashtable testData = new DataUtility().getTestData("201");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3656_TST_3599_verifyBasketRetainsSelectionsFromPassengerDetailsPageAndExtrasPage() {
        Hashtable testData = new DataUtility().getTestData("202");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> outboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        Hashtable<String, String> inboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        Hashtable<String, String> overallFlightDetails = basePage.fareSelect.getOverallFlightDetails(outboundFlightDetails, inboundFlightDetails);
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyRulesPopupOpened("price breakdown");
        Hashtable<String, Double> previousBasket;
        previousBasket = basePage.fareSelect.verifyBasket(testData);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);

        Hashtable<String, String> passsengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(previousBasket);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData, "outbound");
        basePage.lfYourDetails.selectSeats(testData, "inbound");


        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatsSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        previousBasket = basePage.lfYourDetails.verifyBasket(testData, noOFPassengers, 1, 1);

        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(previousBasket);
        basePage.lfCarHire.addCarHire("avis");
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable<String, Double> extrasPageBasket = basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        double totalAmount = basePage.lfPayment.verifyBasket();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue", totalAmount);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, overallFlightDetails, passsengerNames, noOFPassengers, 2, false, seatsSelected);
        basePage.bookingConfirmation.verifyTotalAmount(totalAmount);

    }


    @Test(groups = {"regression", "p2"})
    public void TST_4156_verifyFaresDisplayedMatchesWithPriceBreakDownForAllPax() {
        Hashtable testData = new DataUtility().getTestData("272");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        basePage.fareSelect.selectFlightOption(testData, "inbound");
        basePage.fareSelect.openRulesTaxesFromBasket("price breakdown");
        basePage.fareSelect.verifyBasket(testData);
    }

    @Test(groups = {"regression", "p2"})
    public void TST_4636_verifyChildTaxesNotDisplayedWhenChildIsNotSelected() {
        Hashtable<String, String> testData = new DataUtility().getTestData("273");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyPaxIsNotDisplayedInPriceBreakDown("child");
    }


    @Test(groups = {"regression", "p2"})
    public void TST_4115_3412_verifySelectingAnOutboundGetMoreGreysOutJustFlyAndAllInOptionsForInbound(){
        Hashtable<String, String> testData = new DataUtility().getTestData("307");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyPriceDifferenceBtwnGetMoreAndJustFly();
        basePage.fareSelect.verifyFlightBookingStepsAreDisplayed();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_7488_VerifyUserIsAbleToSeePleaseSeeSouthendAirportWebsiteForDetailsLink(){
        Hashtable<String, String> testData = new DataUtility().getTestData("312");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.navigateToArrivalAndDeparture();
        basePage.flightArrivalAndDeparture.searchByFlightNumber(testData);
        basePage.flightArrivalAndDeparture.verifySouthEndAirportLink();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_4155_VerifyTicketTypeDescriptionsAreCorrect(){
        Hashtable<String, String> testData = new DataUtility().getTestData("314");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyAllInTicketTypeDescription();
        basePage.fareSelect.verifyGetMoreTicketTypeDescription();
        basePage.fareSelect.verifyJustFlyTicketTypeDescription();

    }


    @Test(groups = {"regression", "p2"})
    public void TST_4126_VerifyEachSelectableNonStopFareDisplaysDefaultInformation(){
        Hashtable<String, String> testData = new DataUtility().getTestData("315");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyDirectFlightDetails(selectedFlightDetails);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_10217_VerifyNOKIsSetAsBaseCurrencyForFlightsDepartingFromBergen(){
        Hashtable<String, String> testData = new DataUtility().getTestData("318");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("NOK");
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3457_verifyContactUSLinkNavigatesToSupportPage() {
        Hashtable testData = new DataUtility().getTestData("115");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.navigateToSupportPage();
        basePage.support.verifyPageTitle();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3462
    public void TST_3462_VerifyCardChargeInformationIsDIsplayedAboveFooter() {
        Hashtable testData = new DataUtility().getTestData("160");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.verifyTheCardChargeInformationAboveFooter();

    }



    // Added by Arun TST-4186
    @Test(groups = {"regression", "p2"})
    public void TST_4186_VerifyTaxesFaresRulesBaggageAndPriceBreakdownDisplayedInBasketFareSelectPage() {
        Hashtable testData = new DataUtility().getTestData("165");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyTaxesFaresRulesBaggageAndPriceBreakdownAreDisplayedInBasket();

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-4554
    public void TST_4554_VerifyStaticContentsInHomePage() {
        Hashtable testData = new DataUtility().getTestData("155");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.verifyTheStaticContentsInHomePage();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4222
    public void TST_4222_VerifyUnaccompaniedTeenMessageIsDisplayedWhenOnlyTeenIsSelectedForAirFranceHopCitijet(){
        Hashtable testData = new DataUtility().getTestData("243");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.verifyMessageUnaccompaniedMinorForTeens();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-4221
    public void TST_4221_VerifyUnAccompinedChildMessageInPassenger(){
        Hashtable testData = new DataUtility().getTestData("252");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectNoOfPassengers(testData);
        basePage.fareSelect.verifyMessageUnaccompaniedMinorForChild();
    }


    @Test(groups = {"regression", "p2"})
    public void TST_7870_verifyGetMoreBundleCostForRouteJER_GCI_Flybe() {
        Hashtable testData = new DataUtility().getTestData("175");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyGetMoreBundleCostForRouteJER_GCI();
    }

    @Test(groups = {"regression", "p2"})

    public void TST_7828_verify23KgBaggageFreeForJER_GCIFlybe() {
        Hashtable testData = new DataUtility().getTestData("176");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggagePriceFreeFor23Kg();
    }

    @Test(groups = {"regression","p2"})
    public void TST_9512_verifyCurrencyIsSelectedBasedOnDepartureCurrencyNotSetViaURLCookie()
    {
        Hashtable<String, String> testData = new DataUtility().getTestData("352");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("CurrencyType"));
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyCurrency(testData.get("CurrencyType"));

    }

    @Test(groups = {"regression", "p2"})
    public void TST_9514_verifyCurrencyIsSelectedBasedOnCurrencyParameterInURLWhenCurrencyCodeIsNotProvidedOrSelectedCurrencyIsNotInListofSupportedCurrency()
    {
        Hashtable testData = new DataUtility().getTestData("353");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.fareSelect.navigateToURL(testData.get("url").toString());
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency")+"");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_9515_verifyCurrencyIsSelectedBasedOnCountryParameterInURLWhenCurrencyCodeIsNotProvidedOrSelectedCurrencyIsNotInListofSupportedCurrency()
    {
        Hashtable testData = new DataUtility().getTestData("354");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.fareSelect.navigateToURL(testData.get("url")+"");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency")+"");
    }

    @Test(groups={"regression","p2"})
    public void TST_11385_verifyFlexChargeDepartingFromNonUK(){
        Hashtable testData = new DataUtility().getTestData("323");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
    }

    @Test(groups={"regression","p2"})
    public void verifyCurrencyIsTakenFromCurrencyCookieWhenCurrencyURLIsNotPassed(){
        Hashtable testData = new DataUtility().getTestData("324");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.cheapFlight.addCurrencyCookie(testData);
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency").toString());
    }

    @Test(groups={"regression","p2"})
    public void verifyCurrencyIsTakenFromCountryCookieWhenCurrencyURLAndCurrencyCookieAreNotPresent(){
        Hashtable testData = new DataUtility().getTestData("325");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.handleHomeScreenPopUp();
        basePage.cheapFlight.addCountryCookie(testData);
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType(testData.get("currency").toString());
    }

    @Test(groups={"regression","p2"})
    public void TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorForLessThan14Days(){
        Hashtable testData = new DataUtility().getTestData("330");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }

    @Test(groups={"regression","p2"})
    public void TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorFor15DaysTo30Days(){
        Hashtable testData = new DataUtility().getTestData("331");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }

    @Test(groups={"regression","p2"})
    public void TST_11386_verifyFlexChargeCalaculatedPerPaxPerSectorForMoreThan30Days(){
        Hashtable testData = new DataUtility().getTestData("332");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
    }




    //    ------------smoke cases----------------
    @Test(groups = {"smoke", "s1"})
    public void TST_6396_verifyMessageNoFlightsAvailable() {
        Hashtable testData = new DataUtility().getTestData("47");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.verifyNoFlightsMessageDisplayed();
    }

}

