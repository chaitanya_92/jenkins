package test_definition.cms.lf_three.booking.avios;

import PageBase.CMSObjectsLF;
import PageBase.LFBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/6/2017.
 */
public class AviosBooking {
    @Test(groups = {"regression", "p2"})// Added by Arun TST-6857
    public void TST_6857_VerifyUserCannotUseAviosPartPayForCarParking() {
        Hashtable testData = new DataUtility().getTestData("255");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.selectLowestPriceDate(22.00,"inbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("inbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        Double maxAvios = basePage.lfYourDetails.selectAviosType("high");
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,1);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.verifyBasket(testData,maxAvios);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6696
    public void TST_6696_VerifyPartPayAviosForEuroCurrency() {
        Hashtable testData = new DataUtility().getTestData("256");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("low");
        basePage.lfYourDetails.verifyCurrencyInAvios("\u20ac");
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);


        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6708
    public void TST_6708_VerifyUserIsAbleToLoginWithAviosAccountAndViewBalance() {
        Hashtable testData = new DataUtility().getTestData("257");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.verifyTheAviosOptions();
        basePage.lfYourDetails.verifyAviosBalanceIsDisplayed();

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6817
    public void TST_6817_VerifyUserCanMakeBookingByAddingAviosAndPaypalOptions() {
        Hashtable testData = new DataUtility().getTestData("259");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");

        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,2,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.enterPayPalCardDetails(testData);
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyPaidUsingPaypalIsDisplayed();

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-8316
    public void TST_8316_VerifyWhenPayableAmountIsZeroUserIsAbleToCompleteBookingSuccessfullyWithByAddingAvios() {
        Hashtable testData = new DataUtility().getTestData("281");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();




        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }

//
////    @Test(groups = {"regression", "p2"})// Added by Arun TST-6844.
////    tool tip is not visisble in lf3
//    public void TST_6844_VerifyATRPMemberIsTryingToLoginShouldDisplayToolTipMessage() {
//        Hashtable testData = new DataUtility().getTestData("282");
//        CMSObjectsLF basePage = new CMSObjectsLF();
//        basePage.cheapFlight.enterSourceAndDestination(testData);
//        basePage.cheapFlight.enterTravelDates(testData);
//        basePage.cheapFlight.selectNoOfPassengers(testData);
//        basePage.cheapFlight.findFlights();
//        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.verifyContinueEnabled();
//        basePage.fareSelect.continueToPassengerDetails();
//
//
////        basePage.lfYourDetails.selectFlybeAccountOption(true);
////        basePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
////        basePage.yourDetails.enterPassengerDetails(testData);
////        basePage.lfYourDetails.verifyTheToolTipMessageInAviosLogin();
//
//    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6845
    public void TST_6845_VerifyBookingConfirmationAviosPartPayIsAppliedWithDifferentCurrency() {
        Hashtable testData = new DataUtility().getTestData("283");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.bookingConfirmation.verifyAviosCurrency("CHF");
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForBags() {
        Hashtable testData = new DataUtility().getTestData("284");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);

        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();

        basePage.lfPayment.handlePriceChange("continue");




        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-8315
    public void TST_8315_VerifyAviosPartPayIsApplicableForSeats() {
        Hashtable testData = new DataUtility().getTestData("291");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.handlePriceChange("continue");


        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6746
    public void TST_6746_VerifyAviosDiscountValueIsShownSeparatelyInBasketInAllPages() {
        Hashtable testData = new DataUtility().getTestData("285");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        Hashtable<String, String> selectedInboundFlightDetails;
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "inbound");
        Hashtable<String, String> overAllFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,1);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.lfCarHire.addCarParking(testData);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,overAllFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6912
    public void TST_6912_VerifyAviosCannotBeUsedForTravelInsurance() {
        Hashtable testData = new DataUtility().getTestData("288");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectLowestPriceDate(22.00,"outbound");
        basePage.fareSelect.toSelectLowestFlightAvailableForDaySelected("outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();

        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.verifyAviosBalanceIsDisplayed();
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6753
    public void TST_6743_VerifyAviosDiscountIsDeductedFromTotalPayableAndBalanceCanBePaidByDebitCard() {
        Hashtable testData = new DataUtility().getTestData("313");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.verifyAviosBalanceIsDisplayed();
        basePage.lfYourDetails.selectAviosType("low");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-6905
    public void TST_6905_verifyNonUKUserCanLoginAndBookTicketUsingAvios() {
        Hashtable testData = new DataUtility().getTestData("289");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.verifyAviosBalanceIsDisplayed();
        basePage.lfYourDetails.selectAviosType("high");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfYourDetails.verifyBasketPriceWithBasketPriceOfFareSelectionPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();

        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);

        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-6753
    public void TST_6753_VerifyAviosPointsIsRewardedWhenBookingMadeUsingCurrencyCAD() {
        Hashtable testData = new DataUtility().getTestData("286");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("CAD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        basePage.lfYourDetails.verifyAviosBalanceIsDisplayed();
        basePage.lfYourDetails.selectAviosType("low");
        Hashtable <String,Double> yourDetailsBasket = basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(yourDetailsBasket);
        basePage.lfCarHire.addInsurance(testData);
        Hashtable <String,Double> extrasBasket =  basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.verifyBasketPriceWithBasketPriceOfPreviousPage(extrasBasket);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.verifyBasket(yourDetailsBasket.get("maxAviosPrice"));
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        double aviosPrice = basePage.lfPayment.captureAviosPriceFromBasket();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
    }

    //    @Test(groups = {"regression", "p1"})
    public void TST_6353_verifyBookingSeatsWithoutSelectingSeatsAndLuggageAsLoggedInUser(){
        Hashtable testData = new DataUtility().getTestData("5");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    //    @Test(groups = {"regression", "p3"})
    public void TST_6354_verifyBookingSeatsWithSelectingSeatsAndLuggageAsLoggedInUser() {
        Hashtable testData = new DataUtility().getTestData("6");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outBound");
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    }

    @Test(groups = {"smoke","s1"})
    public void TST_6837_verifyAviosBooking()
    {
        Hashtable testData = new DataUtility().getTestData("42");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable selectedFlightDetails=basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outBound");
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.retrieveAviosDetailsByLogin(testData);
        Double aviosPrice = basePage.lfYourDetails.selectAviosType("low");
        basePage.lfYourDetails.verifyBasket(testData,noOfPassengers,1,0);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyAviosSectionIsDisplayedInBookingConfirmation();
        basePage.bookingConfirmation.verifyAviosPricesInBookingConfirmation(aviosPrice);

    }
}
