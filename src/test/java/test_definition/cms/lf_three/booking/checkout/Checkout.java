package test_definition.cms.lf_three.booking;

import PageBase.CMSObjectsLF;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

/**
 * Created by STejas on 6/6/2017.
 */
public class Checkout {
    //    ------------------smoke cases------------------------------

    @Test(groups = {"smoke", "s1"})
    public void TST_6352_6374_4595_6388_verifyBookingSeatsWithoutSelectingSeatsAndLuggageAsGuestUserWithCurrencyGBPWithVisaDebit() {
        //merged 6352 with 6374 4595 6388
        Hashtable testData = new DataUtility().getTestData("4");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("GBP");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("GBP");
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6389_6371_2556_verifyAddAPIPresentForNonUKFlightsWithVisaCredit(){
        Hashtable testData = new DataUtility().getTestData("41");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_3132_3479_6374_4595_VerifyFooterWithEUR(){
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.verifyHomePageFooter();
        Hashtable testData = new DataUtility().getTestData("58");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyFareSelectPageFooter();
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("EUR");
        basePage.lfYourDetails.verifyPassengerDetailsFooter();
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable <String, Hashtable<String, Hashtable>> seatsSelectedForPassengers = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.verifyExtrasPageFooter();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.verifyPaymentDetailsFooter();
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatsSelectedForPassengers);
        basePage.bookingConfirmation.verifyCurrency("EUR");

        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);

        basePage.leapCheckIn.verifyCheckInPageFooter();
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6371_2556_verifyBookingFlightWithPayPal(){
        Hashtable testData = new DataUtility().getTestData("44");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.lfPayment.enterPayPalCardDetails(testData);
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyPaidUsingPaypalIsDisplayed();
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6375_verifyBookingFlightWithOptionJustFly() {
        Hashtable testData = new DataUtility().getTestData("11");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6375_verifyBookingFlightWithOptionGetMore() {
        Hashtable testData = new DataUtility().getTestData("13");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6375_verifyBookingFlightWithOptionAllIn() {
        Hashtable testData = new DataUtility().getTestData("12");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }



    @Test(groups = {"smoke", "s1"})
    public void TST_6407_3476_verifyBookingWithCheckingOptionAsEmail_6407_3476() {
        Hashtable testData = new DataUtility().getTestData("20");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6407_3476_verifyBookingWithCheckingOptionAsMobile() {
        Hashtable testData = new DataUtility().getTestData("21");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"smoke", "s1"})
    public void TST_6407_3476_verifyBookingWithCheckingOptionAsManual() {
        Hashtable testData = new DataUtility().getTestData("22");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_10419_VerifyUserIsAbleToCompleteBookingFromRecentSearch() {
        Hashtable testData = new DataUtility().getTestData("322");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.navigateToHomePage();
        basePage.cheapFlight.selectRecentFlight(testData,1);
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData, "outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
    }

    // test cases are moved from smoke cases

    @Test(groups = {"regression", "p1","r1"})
    public void TST_6353_verifyBookingSeatsWithoutSelectingSeatsAndLuggageAsLoggedInUser(){
        Hashtable testData = new DataUtility().getTestData("5");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
    }

    @Test(groups = {"regression", "p1", "p5","r1"})
    public void TST_6354_verifyBookingSeatsWithSelectingSeatsAndLuggageAsGuestUser() {
        Hashtable testData = new DataUtility().getTestData("6");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outBound");
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
    }

    @Test(groups = {"regression", "p2", "r1"})
    public void TST_6388_verifyAddAPINotPresentForUKFlights(){
        Hashtable testData = new DataUtility().getTestData("40");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers =  basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.selectFlightOption(testData,"outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames =  basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        Hashtable<String,Hashtable<String,Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(false);
    }

    @Test(groups = {"regression", "p1", "r1"})
    public void TST_6371_2556_verifyBookingFlightWithMasterDebit() {
        Hashtable testData = new DataUtility().getTestData("14");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyLastFourDigitsOfPaidCard(testData);
    }

    @Test(groups = {"regression", "p1","r1"})
    public void TST_6371_2556_verifyBookingFlightWithAmericanExpress() {
        Hashtable testData = new DataUtility().getTestData("15");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyLastFourDigitsOfPaidCard(testData);
    }

    @Test(groups = {"regression", "p1","r1"})
    public void TST_6371_2556_verifyBookingFlightWithDiners() {
        Hashtable testData = new DataUtility().getTestData("16");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData, "outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.bookingConfirmation.verifyLastFourDigitsOfPaidCard(testData);
    }

    @Test(groups = {"regression", "p1","r1"})
    public void TST_6371_2556_verifyBookingFlightWithElectron_6371() {
        Hashtable testData = new DataUtility().getTestData("19");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyLastFourDigitsOfPaidCard(testData);
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithCAD() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("CAD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("CAD");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("CAD");
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithHKD() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("HKD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("HKD");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("HKD");
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithJPY() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("JPY");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("JPY");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("JPY");
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithNOK() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("NOK");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("NOK");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("NOK");
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithCHF() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("CHF");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("CHF");
    }

    @Test(groups = {"regression", "p2","r1"})
    public void TST_6374_4595_verifyBookingWithUSD() {
        Hashtable testData = new DataUtility().getTestData("100");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
//        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("USD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.verifyCurrency("USD");
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyCurrency("USD");
    }




//    ----------added from RegBooking---------

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightFromItalyWithAdultsOneWay() {
        Hashtable testData = new DataUtility().getTestData("105");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightFromItalyWithAdultsTwoWay() {
        Hashtable testData = new DataUtility().getTestData("106");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        selectedOverallFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOverallFlightDetails, passengerNames, 4, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightToItalyWithAdultsOneWay() {
        Hashtable testData = new DataUtility().getTestData("107");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOutboundFlightDetails, passengerNames, 2, noOfPassengers, false);
    }

    @Test(groups = {"regression", "p1"})
    public void TST_1617_verifyTeenCanBookFlightToItalyWithAdultsTwoWay() {
        Hashtable testData = new DataUtility().getTestData("108");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        selectedOverallFlightDetails = basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedOverallFlightDetails, passengerNames, 4, noOfPassengers, false);
    }


    @Test(groups = {"regression", "p3"})
    public void TST_3702_verifyClickingAddApiLinkDisplaysTheAdvancedPassengerInformationPage() {
        Hashtable testData = new DataUtility().getTestData("129");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
        basePage.bookingConfirmation.ContinueToCheckInThroughAddAPILink();
        basePage.leapCheckIn.verifyAddApiInfoIsDisplayed(true);

    }

    @Test(groups = {"regression", "p2"})
    public void TST_3654_verifyBookingWithIncorrectMandatoryFieldsInPaymentDisplaysError() {
        Hashtable testData = new DataUtility().getTestData("130");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.lfPayment.verifyValidationForIncorrectPaymentDetails();

    }


    @Test(groups = {"regression", "p2"})
    public void TST_5523_verifyUserCanFillUpAddressFieldsUsingPostCodeLookUp() {
        Hashtable testData = new DataUtility().getTestData("131");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.verifyAddressIsPopulatedAfterPostCodeIsSelected();

    }

    @Test(groups = {"regression", "p2"})
    public void TST_5500_verifySeatSelectionPaneIsDisplayedForUser() {
        Hashtable testData = new DataUtility().getTestData("132");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.verifySeatSelectionPaneIsDisplayed();
    }

    @Test(groups = {"regression", "p2"})
    public void TST_3692_verifyAirFranceDirectFlightNonUKtoUKWithAllIn() {
        Hashtable testData = new DataUtility().getTestData("145");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String>  passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(false);
        basePage.lfPayment.enterPassengerContact(testData);
        basePage.lfPayment.selectPassengerContactAddress(testData);
        basePage.lfPayment.enterPassengerContactNumbersEmails(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.bookingConfirmation.verifyAddApiInfoIsDisplayed(true);
    }


    // Added by Arun TST-3625
    @Test(groups = {"regression", "p2"})
    public void TST_3625_3657_3650_3649__VerifyTheCardTypeInputDisplaysListOfEligibleCardsAndMyAssociatedFees() {
        Hashtable testData = new DataUtility().getTestData("152");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        Hashtable<String,Double> basketDetails = basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.verifyListOfEligibleCardsAndAssociatedFee(testData,basketDetails);

    }



    @Test(groups = {"regression", "p1"})
    public void TST_2557_3648_verifyPayPalChargesAddedToBasket(){
        Hashtable testData = new DataUtility().getTestData("305");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String,String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String,String>passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        Hashtable<String,Double> basketDetails = basePage.lfCarHire.verifyBasket(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.verifyCalculationOFPayPal(basketDetails.get("overallBasketPrice"));
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.lfPayment.enterPayPalCardDetails(testData);

        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
        basePage.bookingConfirmation.verifyPaidUsingPaypalIsDisplayed();
    }





    // Added by Arun TST-3617
    @Test(groups = {"regression", "p2"})
    public void TST_3617_VerifyTheChargesAndMinimumAgesLinkDisplaysYoungDriverSurchargesPopup() {
        Hashtable testData = new DataUtility().getTestData("153");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedOutboundFlightDetails;
        Hashtable<String, String> selectedInboundFlightDetails;
        Hashtable<String, String> selectedOverallFlightDetails;
        selectedOutboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        selectedInboundFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "inbound");
        basePage.fareSelect.getOverallFlightDetails(selectedOutboundFlightDetails, selectedInboundFlightDetails);
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.selectSeats(testData,"inbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.verifyCarHireBookingPopup();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3528
    public void TST_3528_VerifyChooseYourSeatSectionIsNotDisplayedForAirFranceFlight() {
        Hashtable testData = new DataUtility().getTestData("154");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.verifySeatSelectionPaneNotDisplayed();

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3485
    public void TST_3485_VerifyFaresRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("156");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.openRulesTaxesFromBasket("fare rules");
        basePage.lfYourDetails.verifyRulesPopupOpened("fare rules");

    }





    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3470
    public void TST_3470_VerifyFirstNameLastNameTitleIsDisplayedForPassenger() {
        Hashtable testData = new DataUtility().getTestData("160");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers  = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyFirstNameLastNameAndTitleIsDisplayedForPassenger(noOfPassengers);
    }


    //Added by Arun
    @Test(groups = {"regression", "p1"})// Added by Arun TST-3396
    public void TST_3396_3397_VerifyTheDefaultCurrencyTypeIsDisplayedAsGBP() {

        Hashtable testData = new DataUtility().getTestData("169");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("conitnue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        testData = new DataUtility().getTestData("168");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("CHF");
        basePage.fareSelect.changeCurrency("EUR");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("conitnue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("EUR");
        basePage.fareSelect.changeCurrency("USD");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("conitnue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.cheapFlight.navigateToHomepage();
        testData = new DataUtility().getTestData("169");
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.toVerifyTheDefaultCurrencyType("USD");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();


    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3475
    public void TST_3475_VerifyChooseSeatsSectionDisplayedForFlightsOperatedByFlybe() {
        Hashtable testData = new DataUtility().getTestData("162");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.verifySeatSelectionPaneIsDisplayed();

    }




    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3486
    public void TST_3486_VerifyBaggageRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("156");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.openRulesTaxesFromBasket("baggage rules");

        basePage.lfYourDetails.verifyRulesPopupOpened("baggage rules");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3487
    public void TST_3487_VerifyTaxesAndChargesRulePopupInPassengerDetailsPage() {
        Hashtable testData = new DataUtility().getTestData("154");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.openRulesTaxesFromBasket("taxes and charges");
        basePage.lfYourDetails.verifyRulesPopupOpened("taxes and charges");

    }






    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3490
    public void TST_3490_VerifyTermsAndConditionsLinkInBaggageRulePopup() {
        Hashtable testData = new DataUtility().getTestData("155");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.openRulesTaxesFromBasket("baggage rules");
        basePage.lfYourDetails.verifyTermsAndConditionsInBaggageRulesPopup();

    }




    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3518
    public void TST_3518_VerifyAdultTitlesDisplayedInPassengerDetails() {
        Hashtable testData = new DataUtility().getTestData("168");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.verifyAdultTitleOptions();

    }





    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-7928
    public void TST_7928_VerifyUserIsAbleToRetrieveTheBookingUsingLowerCaseBookingReference() {
        Hashtable testData = new DataUtility().getTestData("190");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.openLoginRegistrationPage();
        basePage.login.selectLoginRegistrationOption("login");
        basePage.login.login(testData);
        basePage.myAccountHome.verifyLogin();
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails;
        selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();

        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("conitnue");
        String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false);
        basePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber.toLowerCase(), passengerNames);
        basePage.leapCheckIn.waitAndCheckForCheckIn(3);
        basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
        Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
        basePage.selectPassengerForCheckIn.checkIn();
        basePage.leapCheckIn.printAndVerifyBoardingPass(testData, selectedFlightDetails, passengerNames, bookingRefNumber, checkInDetails, false, noOfPassengers, 1, "outbound");

    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3644
    public void TST_3644_VerifyTheIConfirmCheckBoxIsNotSelectedByDefaultInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.verifyDefaultValueOfTermsAndConditionsCheckbox("i confirm");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3646
    public void TST_3646_verifyTheFareRulesLinkDisplaysFareRulesPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.openRulesLinkInTermsAndConditionSectionInPaymentPage("fare rules");
        basePage.lfPayment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("fare rules");


    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3645
    public void TST_3645_3647_verifyTheGeneralConditionOfCarriageLinkAndPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.openRulesLinkInTermsAndConditionSectionInPaymentPage("general conditions of carriage");
        basePage.lfPayment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("general conditions of carriage");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3642
    public void TST_3642_verifyDangerousGoodsAndBaggageLinkAndPopupInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("152");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.openRulesLinkInTermsAndConditionSectionInPaymentPage("dangerous goods");
        basePage.lfPayment.verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage("dangerous goods");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3643
    public void TST_3643_VerifyPleaseKeepMeUpdatedBoxIsSelectedByDefaultInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);

        basePage.lfPayment.verifyDefaultValueOfKeepMeUpdatedCheckBox("please keep me update");

    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3641
    public void TST_3641_VerifyTripPurposeOptionsInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("169");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();

        basePage.lfPayment.verifyTripPurposeOptions();
        basePage.lfPayment.selectTripPurpose(testData);
    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3635
    public void TST_3635_VerifyBookingByChoosingDifferentBillingAddressOptionInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("191");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(false);
        basePage.lfPayment.selectTheBillingAddressCountryDropDown(testData);
        basePage.lfPayment.enterPassengerBillingAddressManually(testData);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.bookingConfirmation.verifyBookingReferenceNumber();


    }


    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3639
    public void TST_3639_VerifyByClickingOnPaypalRadioButtonHidesCardPaymentSection() {
        Hashtable testData = new DataUtility().getTestData("195");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.verifyTheCardSectionIsHiddenWhenPayPalIsSelected();
    }

    // Added by Arun
    @Test(groups = {"regression", "p2"})// Added by Arun TST-3632
    public void TST_3632_VerifyByEnteringValidPostalCodeInputInPaymentPage() {
        Hashtable testData = new DataUtility().getTestData("192");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(false);
        basePage.lfPayment.verifyCountryAndPostalCodeInputsWhenDifferentBillingAddressOptionIsSelected();
    }



    @Test(groups = {"regression", "p2"})// Added by Arun TST-3695
    public void TST_3695_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("222");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
//        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, false, seatSelected);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3696
    public void TST_3696_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("223");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");


        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, true, seatSelected);

    }

    @Test(groups = {"regression", "p2"})// Added by Arun TST-3697
    public void TST_3697_VerifyPassengerInformationInBookingConfirmationPage() {
        Hashtable testData = new DataUtility().getTestData("224");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "oneChange", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);

        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();

        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");



        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, true, seatSelected);

    }


    @Test(groups = {"regression", "p2"})// Added by Arun TST-7929
    public void TST_7929_VerifyTheCurrencyOptionsDisplayedForBGO(){
        Hashtable testData = new DataUtility().getTestData("221");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails  = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.changeCurrency("CHF");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames = basePage.lfYourDetails.capturePassengerNames(testData);

        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
//        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,true);
        basePage.cheapFlight.navigateToHomepage();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

    }



    @Test(groups = {"regression","p2"})
    public void TST_3651_verifyOptionToSaveACardIsDisplayedAfterLogin(){
        Hashtable testData = new DataUtility().getTestData("290");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();

        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.verifySaveCardIsDisplayed(true);
        basePage.lfPayment.selectBillingAddressOption(true);
        basePage.lfPayment.saveCardDetails();
        basePage.lfPayment.selectTripPurpose(testData);
        basePage.lfPayment.acceptTermsAndCondition();
        basePage.lfPayment.continueToBooking();
        basePage.lfPayment.handlePriceChange("continue");
        basePage.bookingConfirmation.verifyBookingReferenceNumber();
        basePage.bookingConfirmation.verifyBookingDetails(testData, selectedFlightDetails, passengerNames, 1, noOfPassengers, true);
    }

    @Test(groups = {"regression","p2"})
    public void TST_3652_verifyOptionToUseSavedACardIsDisplayedAfterLogin(){
        Hashtable testData = new DataUtility().getTestData("303");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        Hashtable<String, String> selectedFlightDetails = basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        Hashtable<String, String> passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData,"outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.continueToCarHire();
        basePage.lfCarHire.addInsurance(testData);
        basePage.lfCarHire.continueToPayment();
        basePage.lfPayment.selectFlybeAccountOption(true);
        basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
        basePage.lfPayment.selectPaymentOption(testData);
        basePage.lfPayment.enterCardDetails(testData);
        basePage.lfPayment.verifySavedCardIsDisplayed(true);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3585_verifyOnCheckBoxCheckedFlexPriceIsAddedToBasket() {
        Hashtable testData = new DataUtility().getTestData("232");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        int noOFPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.changeCurrency("GBP");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.selectPassengerBaggage(testData);
        basePage.lfYourDetails.acceptBags();
        basePage.lfYourDetails.selectSeats(testData, "outbound");
        basePage.lfYourDetails.acceptSeats();
        basePage.lfYourDetails.selectCheckInOption(testData);
        basePage.lfYourDetails.addFlexToBooking();
        basePage.lfYourDetails.verifyBasket(testData, noOFPassengers, 1, 0);
    }


    @Test(groups = {"regression", "p2"})
    public void TST_3652_verifyOptionToUseSavedCardIsDisplayedIfSignedInToAccountWithSavedCard() {
        Hashtable<String, String> testData = new DataUtility().getTestData("300");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData, "nonStop", "outbound");
        basePage.fareSelect.selectFlightOption(testData, "outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.fareSelect.continueToPassengerDetails();
        basePage.lfYourDetails.enterPassengerDetails(testData);
        basePage.lfYourDetails.verifyBaggageIsIncluded("40kg");
    }

    @Test(groups = {"regression", "p2"})
    public void TST_9669_VerifyUserIsDisplayedOnlyThePassengerInformationSectionWhenUserNavigatesToFlightOption(){
        Hashtable<String, String> testData = new DataUtility().getTestData("318");
        CMSObjectsLF basePage = new CMSObjectsLF();
        basePage.cheapFlight.enterSourceAndDestination(testData);
        basePage.cheapFlight.enterTravelDates(testData);
        basePage.cheapFlight.selectNoOfPassengers(testData);
        basePage.cheapFlight.findFlights();
        basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
        basePage.fareSelect.verifyContinueEnabled();
        basePage.lfYourDetails.verifyFirstNameLastNameAndTitleIsDisplayedForPassenger(1);
        basePage.lfYourDetails.verifyBaggageSectionIsDisplayed(false);

    }
}
