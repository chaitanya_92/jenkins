package test_definition.cms.lf_three.manage_booking;

import PageBase.CMSObjectsLF;
import PageBase.LegacyBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.Hashtable;

public class ManageBooking {

  @Test(groups = {"smoke","regression", "s1"})
  public void TST_6409_7920_verifyAmendFlights(){
   Hashtable testData = new DataUtility().getTestData("104");
   CMSObjectsLF basePage = new CMSObjectsLF();
   basePage.cheapFlight.enterSourceAndDestination(testData);
   basePage.cheapFlight.enterTravelDates(testData);
   int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
   basePage.cheapFlight.findFlights();
   Hashtable<String,String> selectedFlightDetails;
   selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
   basePage.fareSelect.verifyContinueEnabled();
   basePage.fareSelect.continueToPassengerDetails();
   basePage.lfYourDetails.enterPassengerDetails(testData);
   Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
   basePage.lfYourDetails.selectPassengerBaggage(testData);
   basePage.lfYourDetails.acceptBags();
   basePage.lfYourDetails.selectSeats(testData,"outbound");
   basePage.lfYourDetails.acceptSeats();
   Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
   basePage.lfYourDetails.selectCheckInOption(testData);
   basePage.lfYourDetails.continueToCarHire();
   basePage.lfCarHire.addInsurance(testData);
   basePage.lfCarHire.continueToPayment();
   basePage.lfPayment.selectFlybeAccountOption(true);
   Hashtable<String, String>passengerNamesForCheckin = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
   basePage.lfPayment.selectPaymentOption(testData);
   basePage.lfPayment.enterCardDetails(testData);
   basePage.lfPayment.selectBillingAddressOption(true);
   basePage.lfPayment.saveCardDetails();
   basePage.lfPayment.selectTripPurpose(testData);
   basePage.lfPayment.acceptTermsAndCondition();
   basePage.lfPayment.continueToBooking();
   basePage.lfPayment.handlePriceChange("continue");
   String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
   basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
   LegacyBase legacyBasePage = new LegacyBase();
   legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerNamesForCheckin);
   legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
   legacyBasePage.manageBooking.continueToChangeBooking();
   legacyBasePage.changeBooking.continueToChangeFlights();
   Hashtable itineraryTestData = new DataUtility().getTestData("261");
   legacyBasePage.changeYourFlight.changeDateAndRoute(itineraryTestData);
   legacyBasePage.changeYourFlight.selectChangedFlight(itineraryTestData,"outbound");
   legacyBasePage.changeYourFlight.continueToYourDetails();
   legacyBasePage.yourDetails.selectFlybeAccountOption(true);
   legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
   legacyBasePage.yourDetails.selectSeats(itineraryTestData,"outbound");
   legacyBasePage.yourDetails.acceptSeats();
   legacyBasePage.yourDetails.selectPassengerBaggage(itineraryTestData);
   legacyBasePage.yourDetails.acceptBags();
   Hashtable<String, Hashtable<String, Hashtable>> editedSeats = legacyBasePage.yourDetails.captureSeatsSelectedForPassengers();
   legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
   legacyBasePage.yourDetails.continueToPayment();
   legacyBasePage.yourDetails.handleNagWindow("confirm");
   legacyBasePage.payment.selectPaymentOption(testData);
   legacyBasePage.payment.enterCardDetails(testData);
   legacyBasePage.payment.selectBillingAddressOption(true);
//   legacyBasePage.payment.selectTripPurpose(testData);
   legacyBasePage.payment.acceptTermsAndCondition();
   legacyBasePage.payment.continueToBooking();
   legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
   //      ----------  TO Do--------
   //        chanaged flight details needs to be captured

   //        basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,seatSelected);


  }

  @Test(groups = {"smoke", "s1"})
  public void TST_6408_verifyChangeSeatAndBaggageInBookingConfirmation(){
   Hashtable testData = new DataUtility().getTestData("103");
   CMSObjectsLF basePage = new CMSObjectsLF();
   basePage.cheapFlight.enterSourceAndDestination(testData);
   basePage.cheapFlight.enterTravelDates(testData);
   int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
   basePage.cheapFlight.findFlights();
   Hashtable<String,String> selectedFlightDetails;
   selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"oneChange","outbound");
   basePage.fareSelect.verifyContinueEnabled();
   basePage.fareSelect.continueToPassengerDetails();
   basePage.lfYourDetails.enterPassengerDetails(testData);
   Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
   basePage.lfYourDetails.selectPassengerBaggage(testData);
   basePage.lfYourDetails.acceptBags();
   basePage.lfYourDetails.selectSeats(testData,"outbound");
   basePage.lfYourDetails.acceptSeats();
   Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
   basePage.lfYourDetails.selectCheckInOption(testData);
   basePage.lfYourDetails.continueToCarHire();
   basePage.lfCarHire.addInsurance(testData);
   basePage.lfCarHire.continueToPayment();
   basePage.lfPayment.selectFlybeAccountOption(true);
   Hashtable<String ,String> passengerForAmending = basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
   basePage.lfPayment.selectPaymentOption(testData);
   basePage.lfPayment.enterCardDetails(testData);
   basePage.lfPayment.selectBillingAddressOption(true);
   basePage.lfPayment.saveCardDetails();
   basePage.lfPayment.selectTripPurpose(testData);
   basePage.lfPayment.acceptTermsAndCondition();
   basePage.lfPayment.continueToBooking();
   basePage.lfPayment.handlePriceChange("continue");
   String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
   basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,false,seatSelected);
   LegacyBase legacyBasePage = new LegacyBase();
   legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerForAmending);
   legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
   legacyBasePage.manageBooking.continueToChangeBooking();
   legacyBasePage.changeBooking.continueToChangeSeat();
   legacyBasePage.yourDetails.selectFlybeAccountOption(true);
   legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
   legacyBasePage.yourDetails.selectSeats(testData,"outbound","edit");
   legacyBasePage.yourDetails.acceptSeats();
   legacyBasePage.yourDetails.selectPassengerBaggage(testData,"edit");
   legacyBasePage.yourDetails.acceptBags();
   Hashtable<String, Hashtable<String, Hashtable>> editedSeats = legacyBasePage.yourDetails.captureSeatsSelectedForPassengers();
   legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
   legacyBasePage.chooseExtras.continueToPayment();
   legacyBasePage.yourDetails.handleNagWindow("confirm");
   legacyBasePage.payment.selectPaymentOption(testData);
   legacyBasePage.payment.enterCardDetails(testData);
   legacyBasePage.payment.selectBillingAddressOption(true);
//   legacyBasePage.payment.selectTripPurpose(testData);
   legacyBasePage.payment.acceptTermsAndCondition();
   legacyBasePage.payment.continueToBooking();
   legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
   basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,2,noOfPassengers,true,editedSeats);
  }

  @Test(groups = {"smoke", "s1"})
  public void TST_6414_verifyBookingInMyAccountLiveFlightList() {
   Hashtable testData = new DataUtility().getTestData("101");
   CMSObjectsLF basePage = new CMSObjectsLF();
   basePage.cheapFlight.enterSourceAndDestination(testData);
   basePage.cheapFlight.enterTravelDates(testData);
   int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
   basePage.cheapFlight.findFlights();
   Hashtable<String,String> selectedFlightDetails;
   selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
   basePage.fareSelect.selectFlightOption(testData,"outbound");
   basePage.fareSelect.verifyContinueEnabled();
   basePage.fareSelect.continueToPassengerDetails();
   basePage.lfYourDetails.enterPassengerDetails(testData);
   Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
   basePage.lfYourDetails.selectPassengerBaggage(testData);
   basePage.lfYourDetails.acceptBags();
   basePage.lfYourDetails.selectSeats(testData,"outbound");
   basePage.lfYourDetails.acceptSeats();
   basePage.lfYourDetails.selectCheckInOption(testData);
    basePage.lfYourDetails.continueToCarHire();
   basePage.lfCarHire.addInsurance(testData);
   basePage.lfCarHire.continueToPayment();
   basePage.lfPayment.selectFlybeAccountOption(true);
   basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
   basePage.lfPayment.selectPaymentOption(testData);
   basePage.lfPayment.enterCardDetails(testData);
   basePage.lfPayment.selectBillingAddressOption(true);
   basePage.lfPayment.saveCardDetails();
//   basePage.lfPayment.selectTripPurpose(testData);
   basePage.lfPayment.acceptTermsAndCondition();
   basePage.lfPayment.continueToBooking();
   basePage.lfPayment.handlePriceChange("continue");
   String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
   basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false);
   basePage.cheapFlight.navigateToHomepage();
   basePage.cheapFlight.openLoginRegistrationPage();
   basePage.login.selectLoginRegistrationOption("login");
   basePage.login.login(testData);
   basePage.myAccountHome.sortByBookingDate();
   basePage.myAccountHome.verifyBookingInMyAccount(bookingRefNumber);
  }

  @Test(groups = {"smoke", "s1"})
  public void TST_6384_verifyItineraryChangesInBoardingPass(){
   Hashtable testData = new DataUtility().getTestData("102");
   CMSObjectsLF basePage = new CMSObjectsLF();
   basePage.cheapFlight.enterSourceAndDestination(testData);
   basePage.cheapFlight.enterTravelDates(testData);
   int noOfPassengers = basePage.cheapFlight.selectNoOfPassengers(testData);
   basePage.cheapFlight.findFlights();
   Hashtable<String,String> selectedFlightDetails;
   selectedFlightDetails = basePage.fareSelect.selectFlight(testData,"nonStop","outbound");
   basePage.fareSelect.verifyContinueEnabled();
   basePage.fareSelect.continueToPassengerDetails();
   basePage.lfYourDetails.enterPassengerDetails(testData);
   Hashtable<String, String>passengerNames  = basePage.lfYourDetails.capturePassengerNames(testData);
   basePage.lfYourDetails.selectPassengerBaggage(testData);
   basePage.lfYourDetails.acceptBags();
   basePage.lfYourDetails.selectSeats(testData,"outbound");
   basePage.lfYourDetails.acceptSeats();
   Hashtable<String, Hashtable<String, Hashtable>> seatSelected = basePage.lfYourDetails.captureSeatsSelectedForPassengers();
   basePage.lfYourDetails.selectCheckInOption(testData);
   basePage.lfYourDetails.continueToCarHire();
   basePage.lfCarHire.addInsurance(testData);
   basePage.lfCarHire.continueToPayment();
   basePage.lfPayment.selectFlybeAccountOption(true);
   Hashtable<String ,String> passengerForAmending =basePage.lfPayment.retrievePassengerContactDetailsByLogin(testData);
   basePage.lfPayment.selectPaymentOption(testData);
   basePage.lfPayment.enterCardDetails(testData);
   basePage.lfPayment.selectBillingAddressOption(true);
   basePage.lfPayment.saveCardDetails();
   basePage.lfPayment.selectTripPurpose(testData);
   basePage.lfPayment.acceptTermsAndCondition();
   basePage.lfPayment.continueToBooking();
   basePage.lfPayment.handlePriceChange("continue");
   String bookingRefNumber = basePage.bookingConfirmation.verifyBookingReferenceNumber();
   basePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,seatSelected);
   LegacyBase legacyBasePage = new LegacyBase();
   legacyBasePage.cheapFlight.enterBookingDetailsForAmending(bookingRefNumber,passengerForAmending);
   legacyBasePage.manageBooking.waitAndCheckForTicketing(3);
   legacyBasePage.manageBooking.continueToChangeBooking();
   legacyBasePage.changeBooking.continueToChangeSeat();
   legacyBasePage.yourDetails.selectFlybeAccountOption(true);
   legacyBasePage.yourDetails.retrievePassengerContactDetailsByLogin(testData);
   legacyBasePage.yourDetails.selectSeats(testData,"outbound","edit");
   legacyBasePage.yourDetails.acceptSeats();
   Hashtable<String, Hashtable<String, Hashtable>> editedSeats = legacyBasePage.yourDetails.captureSeatsSelectedForPassengers();
   legacyBasePage.yourDetails.acceptTermsAndConditionForEditing();
   legacyBasePage.chooseExtras.continueToPayment();
   legacyBasePage.yourDetails.handleNagWindow("confirm");
   legacyBasePage.payment.selectPaymentOption(testData);
   legacyBasePage.payment.enterCardDetails(testData);
   legacyBasePage.payment.selectBillingAddressOption(true);
//   legacyBasePage.payment.selectTripPurpose(testData);
   legacyBasePage.payment.acceptTermsAndCondition();
   legacyBasePage.payment.continueToBooking();
   legacyBasePage.bookingConfirmation.verifyBookingReferenceNumber();
   legacyBasePage.bookingConfirmation.verifyBookingDetails(testData,selectedFlightDetails,passengerNames,1,noOfPassengers,false,editedSeats);
   legacyBasePage.cheapFlight.enterBookingDetailsForCheckIn(bookingRefNumber,passengerNames);
   legacyBasePage.leapCheckIn.waitAndCheckForCheckIn(3);
   legacyBasePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
   Hashtable<String, Hashtable> checkInDetails = basePage.selectPassengerForCheckIn.captureCheckInDetails(noOfPassengers, selectedFlightDetails, 1, "outbound");
   basePage.selectPassengerForCheckIn.checkIn();
   basePage.leapCheckIn.printAndVerifyBoardingPass(testData,selectedFlightDetails,passengerNames,bookingRefNumber,checkInDetails,false,noOfPassengers,1,"outbound",editedSeats);

  }
 }


