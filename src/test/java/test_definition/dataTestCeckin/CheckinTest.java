package test_definition.dataTestCeckin;

import PageBase.LFBase;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * Created by varunvm on 2/3/2017.
 */
public class CheckinTest {

    @Test(dataProvider = "pnr")
    public void testCheckIn(String pnr){
        try {
            openUrl(pnr);
            LFBase basePage = new LFBase();
            basePage.leapCheckIn.waitAndCheckForCheckIn(4);
            basePage.leapCheckIn.continueToSelectPassengerForCheckIn("outbound");
            basePage.selectPassengerForCheckIn.checkIn();
            basePage.leapCheckIn.verifyCheckIn("outbound");

            TestManager.getReportLogger().log(LogStatus.INFO, pnr+" ---successful ");
            System.out.println(pnr+" ---successful ");
        }catch (Exception e){
            TestManager.getReportLogger().log(LogStatus.WARNING, pnr+" --- NOT successful ");
            System.out.println(pnr+" ---NOT successful ");
            throw e;
        }








    }

    private void openUrl(String pnr){
        TestManager.getDriver().get("https://uatwww.flybe.com/web-app/check-in.php?__formTokenKey=&isRetrieved=true&target=o&action=&pnrLocator="+pnr+"&forename=adultone&surname=testing");
        new ActionUtility().waitForPageLoad(15);
    }

    @DataProvider(name = "pnr")
    public Object[][]checkInData(){
        Object[][] checkIn = new Object [6][1];
//        for (int i=0; i<5;i++){
//            Hashtable<String,String> testData = new DataUtility().getTestData(Integer.toString(i));
//            String pnr = testData.get("pnr");
//            checkIn[i][0]=pnr;
//        }



        checkIn[0][0]="GTRDS6";
        checkIn[1][0]="GTRDS3";
        checkIn[2][0]="GTRD9H";
        checkIn[3][0]="GTRDWN";
        checkIn[4][0]="GTRDYW";
        checkIn[4][0]="GTRD0K";



        return checkIn;
    }




}
