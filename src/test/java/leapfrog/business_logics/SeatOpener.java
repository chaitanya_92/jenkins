package leapfrog.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;

import static utilities.CommonUtility.convertStringToDouble;

public class SeatOpener {
    ActionUtility actionUtility;
    ExtentTest reportLogger;
    public SeatOpener(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
        }

    @FindBy (id = "endpointListTogger_seats")
    private WebElement showOrHideOption;

    @FindBy (linkText = "/openseats/today/{flightNumber}")
    private WebElement openForToday;

    @FindBy (linkText = "/openseats/tomorrow/{flightNumber}")
    private WebElement openForTomorrow;

    @FindBy (css = "div#seats_openForToday_content input[name='flightNumber']")
    private WebElement flightNumberToday;

    @FindBy (css = "div#seats_openForTomorrow_content input[name='flightNumber']")
    private WebElement flightNumberTomorrow;

    @FindBy (css = "div#seats_openForToday_content input.submit")
    private WebElement seatOpenerForToday;

    @FindBy (css = "div#seats_openForTomorrow_content input.submit")
    private WebElement seatOpenerForTomorrow;

    @FindBy (css = "div#seats_openForToday_content div.response_code pre")
    private WebElement responseToday;

    @FindBy (css = "div#seats_openForTomorrow_content div.response_code pre")
    private WebElement responseTomorrow;

    String todayResponseLoaderLocator = "//div[@class='seats_openForToday_content']/descendant::span[@class='response_throbber']";
    String tomorrowResponseLoaderLocator = "//div[@class='seats_openForTomorrow_content']/descendant::span[@class='response_throbber']";



    public void seatOpen(Hashtable<String,String> selectedFlightDetails, int noOfFlights, String openDate){
//        This method opens the check in for the flight numbers specified
        WebElement seatOpener = null, flightNumber = null, response = null;
        String responseLoader=null;
        try {
            String checkInUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("seatOpenerURL").getAsString();
            TestManager.getDriver().navigate().to(checkInUrl);
            actionUtility.waitForPageLoad(10);
            int count = 0;
            while(count<3){
                try{
                    actionUtility.waitForElementVisible(showOrHideOption,10);
                    actionUtility.click(showOrHideOption);
                    actionUtility.waitForElementClickable(openForTomorrow,5);
                    break;
                }catch (StaleElementReferenceException e){
                    actionUtility.hardSleep(2000);
                    count++;
                }catch (TimeoutException e){
                    actionUtility.hardSleep(2000);
                    count++;
                }
            }
            if(openDate.toLowerCase().equals("today")){
                actionUtility.click(openForToday);
                seatOpener = seatOpenerForToday;
                flightNumber = flightNumberToday;
                response = responseToday;
                responseLoader = todayResponseLoaderLocator;
            }
            else if(openDate.toLowerCase().equals("tomorrow")){
                actionUtility.click(openForTomorrow);
                seatOpener = seatOpenerForTomorrow;
                flightNumber = flightNumberTomorrow;
                response = responseTomorrow;
                responseLoader = tomorrowResponseLoaderLocator;
            }
            for(int i=0; i<noOfFlights; i++){
                actionUtility.waitForElementVisible(flightNumber,5);
                flightNumber.clear();
                Integer flightNumb = (int)CommonUtility.convertStringToDouble(selectedFlightDetails.get("flightNo"+(i+1)));
                flightNumber.sendKeys(flightNumb.toString());
                actionUtility.click(seatOpener);
                actionUtility.waitForElementNotPresent(responseLoader,10);
                actionUtility.waitForElementVisible(response,10);
                verifyResponse(response, selectedFlightDetails.get("flightNo"+(i+1)));
            }
            reportLogger.log(LogStatus.INFO,"successfully opened check-in for all the flights");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to open check-in for the flights");
            throw e;
        }

    }

    private void verifyResponse(WebElement response, String flightNo){
//        This method verifies the responses for the flight to be opened for check in
        if(response.getText().equals("200")){
            reportLogger.log(LogStatus.INFO,"successfully opened the flight for check-in "+flightNo);
        }
        else if (response.getText().equals("403")){
            reportLogger.log(LogStatus.WARNING,"unable to open because of Forbidden Error:403 "+flightNo);
            throw new RuntimeException("Forbidden error");
        }
        else if(response.getText().equals("401")){
            reportLogger.log(LogStatus.WARNING,"unable to open because of Unauthorized Error:401 "+flightNo);
            throw new RuntimeException("Unauthorized error");
        }
        else if(response.getText().equals("404")){
            reportLogger.log(LogStatus.WARNING,"unable to open because of Not Found Error:404 "+flightNo);
            throw new RuntimeException("Not Found");
        }
        else{
            reportLogger.log(LogStatus.WARNING,"unknown error has occurred error:"+response.getText()+" for the flight number "+flightNo);
            throw new RuntimeException("Unknown Error");
        }

    }
}