package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class LFYourDetails {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LFYourDetails() {

        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    ///------------------login---------------------------
    @FindBy(xpath = "//button[text()='Log in']")
    WebElement logInButton;

    @FindBy(xpath = "//form/h1[text()='Log in to your account']")
    private WebElement contactPassengerLoginMenu;

    @FindBy(css=".modal-body input[id^='frc-email']")
    private WebElement contactPassengerEmailForLogin;

    @FindBy(css=".modal-body input[id^='frc-password']")
    private WebElement contactPassengerPassword;

    @FindBy(css=".modal-body button")
    private WebElement contactPassengerLogin;

    @FindBy(xpath = "//div[@class='vertically-centered']/span")
    private List<WebElement> loggedInMessage;

    // ----------Passenger Names section--------------
    @FindBy(css = ".passengers form")
    private WebElement passengerDetailsSection;

    @FindBy(className = "form-group-title")
    private List<WebElement> passengerTypes;

    @FindBy(xpath = "//select[contains(@id,'title')]")
    private List<WebElement> passengerTitles;

    @FindBy(xpath = "//input[contains(@id,'forename')]")
    private List<WebElement> passengerFirstNames;

    @FindBy(xpath = "//input[contains(@id,'last-name')]")
    private List<WebElement> passengerLastNames;

    @FindBy(name = "day")
    private List<WebElement> infantBirthDates;

    @FindBy(name = "month")
    private List<WebElement> infantBirthMonths;

    @FindBy(name = "year")
    private List<WebElement> infantBirthYears;

    @FindBy(css = ".avios-button")
    private List<WebElement> aviosButtons;

    @FindBy(xpath = "//input[contains(@id,'avios-number')]")
    private List<WebElement> aviosNumbers;

    @FindBy(id = "btnPassengerDetails")
    private WebElement savePassengerNames;


    // ----------Passenger Bags section--------------

    @FindBy(css = ".luggage form")
    private WebElement passengerBaggageSection;

    @FindBy(xpath = "//div[contains(@class,'luggage ')]/descendant::div[@class='tbody']//div[@class='passenger-name']")
    private List<WebElement> passengerWithBags;

    @FindBy(xpath = "//label[contains(., '46kg')]/input")
    private List<WebElement> fortySixBags;

    @FindBy(xpath = "//label[contains(., '30kg')]/input")
    private List<WebElement> thirtyBags;

    @FindBy(xpath = "//label[contains(., '23kg')]/input")
    private List<WebElement> twentyThreeBags;

    @FindBy(xpath = "//label[contains(., '20kg')]/input")
    private List<WebElement> twentyBags;

    @FindBy(xpath = "//label[contains(., '15kg')]/input")
    private List<WebElement> fifteenBags;

    @FindBy(xpath = "//label[contains(., 'None')]/input")
    private List<WebElement> zeroBags;


    @FindBy(css = ".tbody .luggage-row:nth-child(1) label>span:not(.hidden-md)")
    private List<WebElement> availableBaggage;


    @FindBy(css = ".submitLuggage>button")
    private WebElement savePassengerBags;



    @FindBy(css = "div.luggage table tr>th:nth-child(6)>span:nth-child(1)")
    private WebElement baggageNone;

    @FindBy(css = "div.luggage table tr>th:nth-child(5)>span:nth-child(1)")
    private WebElement baggageSmall;

    @FindBy(xpath = "div.luggage table tr>th:nth-child(4)>span:nth-child(1)")
    private WebElement standardBaggage;

    @FindBy(xpath = "div.luggage table tr>th:nth-child(3)>span:nth-child(1)")
    private WebElement baggageLarge;

    @FindBy(xpath = "div.luggage table tr>th:nth-child(2)>span:nth-child(1)")
    private WebElement baggageTwoBags;





//    --------------baggagePrice------------------

    @FindBy(xpath = "//input[@value='extra-large']/ancestor::div[@class='tbody']/preceding-sibling::div[@class='thead']//span[contains(@class,'extra-large')]/following-sibling::span")
    private WebElement fortySixBagPrice;

    @FindBy(xpath = "//input[@value='medium']/ancestor::div[@class='tbody']/preceding-sibling::div[@class='thead']//span[text()='Standard']/following-sibling::span")
    private WebElement twentyThreeBagPrice;

//    @FindBy(xpath = "//input[@value='medium']/ancestor::div[@class='tbody']/preceding-sibling::div[@class='thead']//span[text()='Standard']/following-sibling::span")
//    private WebElement twentyBagPrice;

    @FindBy(xpath = "//input[@value='small']/ancestor::div[@class='tbody']/preceding-sibling::div[@class='thead']//span[text()='Small']/following-sibling::span")
    private WebElement fifteenBagPrice;

    @FindBy(css = ".luggage-price>span.formatted-currency")
    private List<WebElement> baggageCurrencies;




    //    ---------------passenger seat section --------------------------
    @FindBy(css = "div.seats")
    private WebElement passengerSeatSection;

        @FindBy(css = ".flight-direction.outbound .flight-stop .table")
    private List<WebElement> outboundSeatOptionTables;

    @FindBy(css = ".flight-direction.inbound .flight-stop .table")
    private List<WebElement> returnSeatOptionTables;

    @FindBy(xpath = "//div[contains(@class,'outbound')]/descendant::div[@class='flight-stop']/descendant::table//td[2]//input")
    private List<WebElement> outBoundSelectSeatOptions;

    @FindBy(xpath = "//div[contains(@class,'inbound')]/descendant::div[@class='flight-stop']/descendant::table//td[2]//input")
    private List<WebElement> inBoundSelectSeatOptions;

    @FindBy(xpath = "//div[contains(@class,'outbound')]/descendant::div[contains(@class,'flight-stop')][1]//div[@class='passenger-name']")
    private List<WebElement> passengersForSeatSelection;

    @FindBy(xpath = "//div[contains(@class,'outbound')]/div[contains(@class,'flight-stop')]")
    private List<WebElement> outboundFlightListForSeatSelection;

    @FindBy(css = ".outbound span.selected-seat .formatted-currency")
    private List<WebElement> outboundSelectedSeatPrice;

    @FindBy(css = ".inbound span.selected-seat .formatted-currency")
    private List<WebElement> inboundSelectedSeatPrice;


    @FindBy(css=".submitSeats>button")
    private WebElement saveSeats;


    @FindBy(xpath="//p[text()='Seat selection unavailable.']")
    private List <WebElement> seatSectionUnavailable;


    @FindBy(css = ".selected-seat  .formatted-currency")
    private List<WebElement> overallSeatPrices;


    @FindBy(xpath = "//div[contains(@class,'outbound')]/div[contains(@class,'flight-stop')]/descendant::div[contains(@class,'seats-row')]/div[3]//input")
    private List<WebElement> outboundRandomSeats;

    @FindBy(xpath = "//div[contains(@class,'inbound')]/div[contains(@class,'flight-stop')]/descendant::div[contains(@class,'seats-row')]/div[3]//input")
    private List<WebElement> inboundRandomSeats;

//    ------------ seat panel---------------------

    @FindBy(css = "#seatMapModal .modal-dialog .modal-body")
    private WebElement seatSelectionPanel;

    @FindBy(css = ".passenger.pointer")
    private List<WebElement> seatForPassengers;

    @FindBy(css = "div.display-seat.infant.selectable:not(.selected)")
    private List<WebElement> infantSeats;

    @FindBy(xpath = "//div[@class='shaft']//div[contains(@class,'selectable')and not(contains(@class,'selected'))and not(contains(@class,'extra-leg-room'))and not(descendant::span[contains(text(),'E')])and not(descendant::span[contains(text(),'I')])]")
    private List<WebElement> standardSeats;

    @FindBy(css = "div.display-seat.extra-leg-room.emergency:not(.selected)")
    private List<WebElement> extraLegRoomSeats;


    @FindBy(xpath = "//div[span[normalize-space(text()) = 'E']]")
    private List<WebElement> emergencySeats;


    @FindBy(xpath = "//div[@class='seatmap-container']//span[text()='Standard']/following-sibling::span/span")
    private WebElement standardSeatPrice;

    @FindBy(xpath = "//span[text()='Extra leg room']/following-sibling::span/span")
    private WebElement extraLegRoomSeatPrice;





    @FindBy(id = "btnFlex")
    private WebElement emergencySeatNotification;




    @FindBy(css = ".modal-footer .return-seats")
    private WebElement selectSeat;

    @FindBy(className = "front-container")
    private WebElement seatSelectionPaneFront;

    @FindBy(className = "shaft")
    private WebElement seatSelectionPaneMid;

    @FindBy(className = "back-container")
    private WebElement seatSelectionPaneBack;

    @FindBy(xpath = "//div[div[span[text()='Standard']]][@class='key']")
    private WebElement standardSeatLegend;

    @FindBy(xpath = "//div[div[span[text()='Extra leg room']]]")
    private WebElement extraLegRoomSeatLegend;

    @FindBy(xpath = "//div[div[span[text()='Not available']]]")
    private WebElement notAvailableSeatLegend;

    @FindBy(xpath = "//div[div[span[text()='Emergency exit']]]")
    private WebElement emergencyExitSeatLegend;

    @FindBy(xpath = "//div[div[span[text()='Selected']]]")
    private WebElement selectedSeatLegend;

    @FindBy(xpath = "//div[div[span[text()='Infant seats']]]")
    private WebElement infantSeatLegend;

    @FindBy(css=".seatmap-container span.formatted-currency")
    private List <WebElement> seatCurrencies;


//    ---------Automated Checkin--------------------

    @FindBy(css=".aci h3")
    private WebElement aciHeading;

    @FindBy(id="aciOptionEmail")
    private WebElement checkInByEmail;

    @FindBy(id="aciOptionMobile")
    private WebElement checkInByMobile;

    @FindBy(id="aciOptionLater")
    private WebElement checkInLater;

    @FindBy(xpath = "//select[contains(@id,'aci-gender')]")
    private List<WebElement> aciGender;

    @FindBy(xpath = "//input[contains(@id,'aci-email-address')]")
    private List<WebElement> aciEmailAddress;

    @FindBy(id = "aci-email-send-all-here")
    private WebElement sendAllBoardingPassToEmail;

    @FindBy(xpath = "//select[contains(@id,'diallingCode')]")
    private List<WebElement> aciMobileDiallingCodes;

    @FindBy(xpath = "//input[contains(@id,'aci-mobile-number')]")
    private List<WebElement> aciMobileNumbers;

    @FindBy(id="btnAci")
    private WebElement saveCheckInOption;

//    ---------flex-----------------

    @FindBy(name = "flex-selected")
    private WebElement flexTicket;

    @FindBy(xpath="//span[text()='Select Avios']/ancestor::button[@id='btnFlex']")
    private WebElement saveFlex;



    //    ----basket outbound & inbound total ------
    @FindBy(css = ".outbound  .price.basket-preoffer span.formatted-currency")
    private WebElement outboundTotal;

    @FindBy(css = ".inbound .price.basket-preoffer span.formatted-currency")
    private WebElement returnTotal;

    @FindBy(css = ".basket-inner .inbound .name")
    private WebElement flightBreakDownReturnFlightOption;

    @FindBy(css = ".basket-inner .outbound .name")
    private WebElement flightBreakDownOutboundFlightOption;

    //    -----basket  price----------
    @FindBy(css = ".total .amount span.formatted-currency")
    private WebElement overallBasketTotal;

    @FindBy(css = ".total .baggageAmount span.formatted-currency")
    private WebElement basketBaggagePrice;

    @FindBy(css = ".total .flexAmount span.formatted-currency")
    private WebElement basketFlexPrice;

    @FindBy(css = ".total .seatsAmount span.formatted-currency")
    private WebElement basketSeatPrice;

    @FindBy(css = ".currency-selected")
    private WebElement currencySelected;

    @FindBy(css = ".total .discountAviosAmount span.formatted-currency")
    private WebElement aviosBasketPrice;

    //    ---------------------basket - itinearary change-------------

    @FindBy(id = "basket.itinChangeFee.value")
    private WebElement changeFeePrice;

//    ----basket links---------

    @FindBy(css=".js-baggage-rules")
    private WebElement baggageRules;

    @FindBy(css=".js-fare-rules")
    private WebElement fareRules;

    @FindBy(css=".js-tax-rules")
    private WebElement taxes;

    @FindBy(css=".breakdown-btn")
    private WebElement priceBreakDown;

    @FindBy(linkText = "Baggage terms & conditions")
    private WebElement baggageTermsAndConditionsLink;

    @FindBy(css = "#pop h1")
    private  WebElement baggageTermsAndConditions;


    //    ---------rules popup---------
    @FindBy(css = "#rulesModal>.modal-dialog")
    private WebElement rulesPopup;

    @FindBy(css=".price-breakdown")
    private WebElement priceBreakDownDetails;

    @FindBy(css="#rulesModal #myModalLabel")
    private WebElement rulesPopupHeader;

    @FindBy(css="#taxes-rules-container th")
    private List<WebElement> taxForPassenger;


    @FindBy(xpath="//div[contains(@class,'price-breakdown')]/div")
    private WebElement  priceBreakdownSection;




    //    ----avios--------
    @FindBy(xpath = "//div[p[text()='To collect Avios on this booking you will need to enter your Avios membership number']]")
    private WebElement aviosWarningMsgWhenLoggedInEle;

    String aviosWarningMsgWhenLoggedIn = "//div[p[text()='To collect Avios on this booking you will need to enter your Avios membership number']]";

    @FindBy(css = ".avios-bucket:nth-of-type(1) input")
    private WebElement aviosLowOption;

    @FindBy(css = ".avios-bucket:nth-of-type(2) input")
    private WebElement aviosMediumOption;

    @FindBy(css = ".avios-bucket:nth-of-type(3) input")
    private WebElement aviosHighOption;

    @FindBy(css = ".modal-sm")
    private WebElement aviosLoginMenu;

    @FindBy(css = "input[id^='frc-email']")
    private WebElement aviosUserName;

    @FindBy(css = "input[id^='frc-password']")
    private WebElement aviosPassword;

    @FindBy(css = ".btn-avios-login")
    private WebElement aviosLogin;

    @FindBy(css = ".noAviosNumberButton")
    private WebElement aviosDoNotForget;

    @FindBy(className = "aviosLoggedIn")
    private WebElement aviosBalance;

    @FindBy(css = ".avios-bucket:nth-of-type(1) .price>span:nth-child(2)")
    private WebElement aviosLowPrice;

    @FindBy(css = ".avios-bucket:nth-of-type(2) .price>span:nth-child(2)")
    private WebElement aviosMediumPrice;

    @FindBy(css = ".avios-bucket:nth-of-type(3) .price>span:nth-child(2)")
    private WebElement aviosHighPrice;


    @FindBy(css = ".price>span:nth-child(2)")
    private List<WebElement> aviosCurrencies;

    @FindBy(id = "btnAvios")
    private WebElement aviosContinueButton;

//    ------Footer--------------

    @FindBy(css = ".footer img[alt='Flybe']")
    private WebElement flybeLogo;

    @FindBy(css = ".footer li:nth-child(1)>span")
    private WebElement flybeCopyRight;

    @FindBy(css = ".footer li:nth-child(2)")
    private WebElement privacyPolicyLink;

    @FindBy(css = ".footer li:nth-child(3)>a")
    private WebElement cookiePolicyLink;

    @FindBy(css = ".footer li:nth-child(4)>a")
    private WebElement contactUsLink;


    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = ".footer p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = ".footer p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = ".footer p:nth-child(3)")
    private WebElement footerPara3;

    @FindBy(css = ".footer p:nth-child(4)")
    private WebElement footerPara4;


    @FindBy(css = "p.confD input")
    private List<WebElement> termsAndConditionsForEdit;

    @FindBy(css = "li.input input:not([type='hidden'])")
    private List<WebElement> termsAndConditions;

    @FindBy(xpath="//div[text()='Continue']/ancestor::div[not(contains(@style,'none'))]/button")
    private WebElement continueToPaymentButton ;

    @FindBy(xpath = "//button[text()='This is me']")
    WebElement thisIsMeButton;

    @FindBy(xpath = "//select[contains(@id,'title-')]")
    WebElement passengerOneTitle;

    @FindBy(xpath = "//input[contains(@id,'forename-')]")
    WebElement passengerOneForeName;

    @FindBy(xpath = "//input[contains(@id,'last-name-')]")
    WebElement passengerOneLastName;

//    String seatSelectionOptionsLocator1= "//div[contains(@class,'%s')]/div[contains(@class,'flight-stop')][%d]/descendant::td[%d]//input[not(@type='hidden')]";
    String seatSelectionOptionsLocator = "//div[contains(@class,'%s')]/div[contains(@class,'flight-stop')][%d]/descendant::div[@class='table-body']/div[1]/div[%d]//input[not(@type='hidden')]";
//    String seatSelector = "//div[contains(@class,'flight-stop')][1]//div[%d]//span[@class='selected-seat']/span[@class='seat']";
    String seatSelector = "//div[contains(@class,'flight-stop')][1]//div[1]//div[contains(@class,'seats-row')][%d]//span[@class='selected-seat']/span[@class='seat']";
    String passengerLoadingSpinner = "//span[contains(text(),'Loading...')]";




    public void yourDetailsSelectLogin(Hashtable<String, String> testData){
        //login on fare selection page
        try{
            String contactEmail = null, contactPassword = null;
            actionUtility.waitForElementClickable(logInButton,10);
            actionUtility.click(logInButton);
            actionUtility.waitForElementVisible(contactPassengerLoginMenu,5);
            if(testData.get("noAviosRegistered")!=null&&testData.get("noAviosRegistered").equalsIgnoreCase("true")) {
                contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName2").getAsString();
                contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            }
            else{
                contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName1").getAsString();
                contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            }
            actionUtility.sendKeys(contactPassengerEmailForLogin,contactEmail);
            actionUtility.sendKeys(contactPassengerPassword,contactPassword);
            actionUtility.hardClick(contactPassengerLogin);
            reportLogger.log(LogStatus.INFO, "successfully logged in from fare selection page");
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable login on fare selection page");
            throw e;
        }

    }

    public void verifyLoggedInMsg(){
        try {
            actionUtility.waitForElementVisible(loggedInMessage, 10);
            Assert.assertFalse(loggedInMessage.isEmpty());
            String forename = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("forename").getAsString();
            String lastname = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("lastname").getAsString();
            Assert.assertTrue(loggedInMessage.get(0).getText().equals("You are logged in as"));
            Assert.assertTrue(loggedInMessage.get(1).getText().equals(forename + " " + lastname));
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"logged-in message displayed doesnt match or is not displayed");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify logged-in message");
            throw e;
        }
    }

    public void enterPassengerDetails(Hashtable<String, String> testData) {
        // depending on the no. of passenger,calls a method to populate the passenger details
        try {

            try {
                actionUtility.waitForElementVisible(passengerDetailsSection, 8);
            }catch (TimeoutException e){}

            actionUtility.waitForElementNotPresent(passengerLoadingSpinner, 15);
            actionUtility.hardSleep(2000);
            for (int i = 0; i < passengerTypes.size(); i++) {

                populatePassengerDetails(testData, i);
            }
            actionUtility.hardSleep(1000);
            actionUtility.hardClick(savePassengerNames);
            reportLogger.log(LogStatus.INFO, "successfully populated the passenger details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to populate the passenger details");
            throw e;
        }
    }

    private void populatePassengerDetails(Hashtable<String, String> testData, int index) {
        //depending the type of the passenger, the passenger details are populated
        String[] passengerType = passengerTypes.get(index).getText().split(" ");
        String[] passengerName;
        System.out.println(testData);

        if (passengerType[0].contains("Adult")) {
            if(thisIsMeButton.isDisplayed()&&testData.get("thisIsMe")!=null&&testData.get("thisIsMe").equalsIgnoreCase("true")){
                actionUtility.click(thisIsMeButton);
                if(actionUtility.verifyIfElementIsDisplayed(contactPassengerLoginMenu)){
                    yourDetailsSelectLogin(testData);
                    actionUtility.hardSleep(2000);
                }

                if(testData.get("noAviosRegistered")==null||testData.get("noAviosRegistered").equalsIgnoreCase("false")){
                    try{
                        Assert.assertFalse(verifyIfAviosNumberIsPopulated());
                        reportLogger.log(LogStatus.INFO,"avios number is populated successfully");
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"avios number is not populated");
                        throw e;
                    }
                }
            }
            else{
                passengerName = testData.get("adultName" + passengerType[1]).split(" ");
                System.out.println(passengerName);
                enterPassengerName(passengerName, index);
            }
            if (testData.get("adultAviosNumber" + passengerType[1]) != null) {
                enterAviosNumber(testData.get("adultAviosNumber" + passengerType[1]), index);
            }

        }

        if (passengerType[0].contains("Teen")) {
            if(thisIsMeButton.isDisplayed()&&testData.get("thisIsMe")!=null&&testData.get("thisIsMe").equalsIgnoreCase("true")&&testData.get("noOfAdult").equals("0")){
                actionUtility.click(thisIsMeButton);
                if(actionUtility.verifyIfElementIsDisplayed(contactPassengerLoginMenu)){
                    yourDetailsSelectLogin(testData);
                    actionUtility.hardSleep(2000);
                }
                if(testData.get("noAviosRegistered")==null||testData.get("noAviosRegistered").equalsIgnoreCase("false")){
                    try{
                        Assert.assertFalse(verifyIfAviosNumberIsPopulated());
                        reportLogger.log(LogStatus.INFO,"avios number is populated successfully");
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"avios number is not populated");
                        throw e;
                    }
                }
            }else{
                passengerName = testData.get("teenName" + passengerType[1]).split(" ");
                enterPassengerName(passengerName, index);}
            if (testData.get("teentAviosNumber" + passengerType[1]) != null) {
                enterAviosNumber(testData.get("teentAviosNumber" + passengerType[1]), index);
            }
        }
        if (passengerType[0].contains("Child")) {
            passengerName = testData.get("childName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);
        }

        if (passengerType[0].contains("Infant")) {
            passengerName = testData.get("infantName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);

            String[] infantDob = testData.get("infantDob" + passengerType[1]).split("/");
            WebElement birthDate = infantBirthDates.get(Integer.parseInt(passengerType[1]) - 1);


            actionUtility.dropdownSelect(birthDate, ActionUtility.SelectionType.SELECTBYTEXT, new Integer(infantDob[0]).toString());
            WebElement birthMonth = infantBirthMonths.get(Integer.parseInt(passengerType[1]) - 1);
            actionUtility.dropdownSelect(birthMonth, ActionUtility.SelectionType.SELECTBYTEXT, infantDob[1]);
            WebElement birthYear = infantBirthYears.get(Integer.parseInt(passengerType[1]) - 1);
            actionUtility.dropdownSelect(birthYear, ActionUtility.SelectionType.SELECTBYTEXT, infantDob[2]);

        }
    }


    private Boolean verifyIfAviosNumberIsPopulated(){
        //returns false if avios number is populated in Passenger Information Section on clicking this is me
        System.out.println(aviosNumbers.get(0).getAttribute("value"));
        return aviosNumbers.get(0).getAttribute("value").isEmpty();
    }

    private void enterPassengerName(String[] passengerName, int index) {
        //enters the passenger name
        actionUtility.waitForElementVisible(passengerTitles.get(index), 10);
        actionUtility.dropdownSelect(passengerTitles.get(index), ActionUtility.SelectionType.SELECTBYTEXT, passengerName[0]);
        actionUtility.sendKeys(passengerFirstNames.get(index), passengerName[1]);
        actionUtility.sendKeys(passengerLastNames.get(index), passengerName[2]);
    }

    private void enterAviosNumber(String aviosNumberValue, int index) {
        // enters the avios number
        actionUtility.click(aviosButtons.get(index));
        actionUtility.sendKeys(aviosNumbers.get(index), aviosNumberValue);
    }

    public Hashtable<String, String> capturePassengerNames(Hashtable<String, String> testData) {
        // return the name for the passengerNumber
        try {
            Hashtable<String, Integer> passengerCount = getPassengerCountForEachType(testData);
            int adultCount = passengerCount.get("adultCount");
            int infantCount = passengerCount.get("infantCount");
            int teenCount = passengerCount.get("teenCount");
            int childCount = passengerCount.get("childCount");
            Hashtable<String, String> passengerNames = new Hashtable<String, String>();
            int count = 1;

            for (int i = 1; i <= adultCount; i++) {

                if(i==1 && testData.get("thisIsMe")!=null &&testData.get("thisIsMe").equalsIgnoreCase("TRUE")){
                    passengerNames.put("passengerName" + count,actionUtility.getValueSelectedInDropdown(passengerOneTitle)+" "+passengerOneForeName.getAttribute("value") + " " + passengerOneLastName.getAttribute("value"));
                }
                else {
                    passengerNames.put("passengerName" + count, testData.get("adultName" + i).split(" ")[0] + " " + testData.get("adultName" + i).split(" ")[1] + " " + testData.get("adultName" + i).split(" ")[2]);
                }
                count++;
            }

            for (int i = 1; i <= teenCount; i++) {

                if(testData.get("noOfAdult")!=null && testData.get("noOfAdult").equals("0")&& i==1 &&testData.get("thisIsMe")!=null && testData.get("thisIsMe").equalsIgnoreCase("TRUE")){

                    passengerNames.put("passengerName" + count,actionUtility.getValueSelectedInDropdown(passengerOneTitle)+" "+passengerOneForeName.getAttribute("value") + " " + passengerOneLastName.getAttribute("value"));
                }else {
                    passengerNames.put("passengerName" + count, testData.get("teenName" + i).split(" ")[0] + " " + testData.get("teenName" + i).split(" ")[1] + " " + testData.get("teenName" + i).split(" ")[2]);
                }
                count++;
            }

            for (int i = 1; i <= childCount; i++) {
                passengerNames.put("passengerName" + count, testData.get("childName" + i).split(" ")[0] + " " + testData.get("childName" + i).split(" ")[1] + " " + testData.get("childName" + i).split(" ")[2]);
                count++;
            }

            for (int i = 1; i <= infantCount; i++) {
                passengerNames.put("infantName" + i, testData.get("infantName" + i).split(" ")[0] + " " + testData.get("infantName" + i).split(" ")[1] + " " + testData.get("infantName" + i).split(" ")[2]);
                count++;
            }
            reportLogger.log(LogStatus.INFO, "Passenger names entered - " + passengerNames);
            reportLogger.log(LogStatus.INFO, "Successfully captured passenger Names");
            return passengerNames;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to capture passenger Names");
            throw e;
        }
    }

    private Hashtable<String, Integer> getPassengerCountForEachType(Hashtable<String, String> testData) {
        //returns the passenger Count
        Hashtable<String, Integer> passengerCount = new Hashtable<String, Integer>();
        passengerCount.put("adultCount", 1);
        passengerCount.put("infantCount", 0);
        passengerCount.put("teenCount", 0);
        passengerCount.put("childCount", 0);

        if (testData.get("noOfAdult") != null) {
            passengerCount.put("adultCount", Integer.parseInt(testData.get("noOfAdult")));
        }

        if (testData.get("noOfInfant") != null) {
            passengerCount.put("infantCount", Integer.parseInt(testData.get("noOfInfant")));
        }

        if (testData.get("noOfTeen") != null) {
            passengerCount.put("teenCount", Integer.parseInt(testData.get("noOfTeen")));
        }

        if (testData.get("noOfChild") != null) {
            passengerCount.put("childCount", Integer.parseInt(testData.get("noOfChild")));
        }

        return passengerCount;
    }

    public void selectPassengerBaggage(Hashtable<String, String> testData, String... editFlag) {
        // depending on the on. of passenger, calls the method to select the baggage
        try {

            String baggageToBeSelected;
            actionUtility.waitForElementVisible(passengerBaggageSection, 5);
            actionUtility.hardSleep(500);


            for (int i = 0; i < passengerWithBags.size(); i++) {
                baggageToBeSelected = checkBaggageType(testData, i, editFlag.length);

                selectBag(baggageToBeSelected, i);
                reportLogger.log(LogStatus.INFO, "successfully added baggage " + baggageToBeSelected + " for passenger" + (i + 1));

            }
            reportLogger.log(LogStatus.INFO, "successfully selected the passenger baggage");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the passenger bagage");
            throw e;
        }
    }

    private String checkBaggageType(Hashtable<String, String> testData, int index, int editFlag) {
        // checks the baggage type
        String baggageType = null;



        if (editFlag != 0) {
            baggageType = testData.get("editedPassengerBag" + (index + 1));
            if(baggageType==null){
                return getMinBaggage();
            }
            return baggageType;
        }

        baggageType = testData.get("passengerBag" + (index + 1));

        if(baggageType==null){
            return getMinBaggage();
        }
        return baggageType;
    }


    private String getMinBaggage(){
        // returns the min baggage type
        String minBaggage = "0kg";

         if(!availableBaggage.get((availableBaggage.size()-1)).getText().trim().equals("None")){
             return availableBaggage.get((availableBaggage.size()-1)).getText().trim().substring(0,4);
         }
          return minBaggage;


    }

    private void selectBag(String baggageToBeSelected, int index) {
        //depending on the type of the bagage, the respective baggage is selected
        if (baggageToBeSelected==null || baggageToBeSelected.toLowerCase().equals("0kg")) {
            actionUtility.selectOption(zeroBags.get(index));
        }else if (baggageToBeSelected.toLowerCase().equals("15kg")) {
            actionUtility.selectOption(fifteenBags.get(index));
        }
       else if (baggageToBeSelected.toLowerCase().equals("20kg")) {
           actionUtility.selectOption(twentyBags.get(index));
        }
        else if (baggageToBeSelected.toLowerCase().equals("23kg")) {
            actionUtility.selectOption(twentyThreeBags.get(index));
        }else if (baggageToBeSelected.toLowerCase().equals("30kg")) {
            actionUtility.selectOption(thirtyBags.get(index));
        }else if (baggageToBeSelected.toLowerCase().equals("46kg")) {
            actionUtility.selectOption(fortySixBags.get(index));
        }
    }

    public void selectSeats(Hashtable<String, String> testData, String tripType, String... editFlag) {
        // depending on the trip type calls the method to select the seats
        try {
                actionUtility.hardSleep(500);
                if (actionUtility.verifyIfElementIsDisplayed(passengerSeatSection)) {
                int seatSelectOptionType;
                String[] seatsToBeSelected = null;
                switch (tripType.toLowerCase()) {
                    case ("outbound"):
                        ArrayList<String> outboundSeatRates = new ArrayList<>();
                        selectRandomSeatForEachFlightEachPassenger(outboundRandomSeats);
                        if (testData.get("seatsForOutboundFlight") != null) {
                            seatsToBeSelected = testData.get("seatsForOutboundFlight").split(",");
                            seatSelectOptionType = 2;
                            selectSeatForEachFlight(outboundSeatOptionTables,seatsToBeSelected, tripType,seatSelectOptionType,outboundSeatRates);
                            if(outboundSelectedSeatPrice.size()>0){
                                for(int i = 0; i< outboundSelectedSeatPrice.size(); i++){
                                    try{
                                        Assert.assertEquals(outboundSelectedSeatPrice.get(0).getAttribute("data-price"),outboundSeatRates.get(0));
                                    }catch (AssertionError e){
                                        reportLogger.log(LogStatus.WARNING,"selected seat price are not matching for outbound flight Expected - "+outboundSeatRates.get(0) +" Actual - "+ outboundSelectedSeatPrice.get(0).getAttribute("data-price"));
                                        throw e;
                                    }
                                }
                            }
                        }
                        break;
                    case ("inbound"):
                        selectRandomSeatForEachFlightEachPassenger(inboundRandomSeats);
                        ArrayList<String> inboundSeatRates = new ArrayList<>();
                        if (testData.get("seatsForInboundFlight") != null) {
                            seatsToBeSelected = testData.get("seatsForInboundFlight").split(",");
                            seatSelectOptionType = 2;
                            selectSeatForEachFlight(returnSeatOptionTables,seatsToBeSelected, tripType,seatSelectOptionType,inboundSeatRates);
                            if(inboundSelectedSeatPrice.size()>0){
                                for(int i = 0; i< inboundSelectedSeatPrice.size(); i++){
                                    try{
                                        Assert.assertEquals(inboundSelectedSeatPrice.get(0).getAttribute("data-price"),inboundSeatRates.get(0));
                                    }catch (AssertionError e){
                                        reportLogger.log(LogStatus.WARNING,"selected seat price are not matching for outbound flight Expected - "+inboundSeatRates.get(0)+" Actual - "+inboundSelectedSeatPrice.get(0).getAttribute("data-price"));
                                        throw e;
                                    }
                                }
                            }

                        }
                        break;
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the seats");
            } else {
                reportLogger.log(LogStatus.WARNING, "seat section is not displayed");
                throw new RuntimeException("seat section is not displayed");

            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the seats");
            throw e;
        }
    }

    private void selectRandomSeatForEachFlightEachPassenger(List<WebElement> randomSeats){

        //selects all the random seats

        for(WebElement element:randomSeats){
            actionUtility.selectOption(element);
        }




    }
    private void selectSeatForEachFlight(List<WebElement> seatOptionTables, String[]seatsToBeSelected, String tripType, int seatSelectOptionType, ArrayList<String>seatRates) {
        //selects seat for each flight
        for (int i = 1; i <= seatOptionTables.size(); i++) {
            List<WebElement> selectSeatOptions = getSeatSelectionOptionForEachFlight(i, tripType.toLowerCase(), seatSelectOptionType);

            if (selectSeatOptions.size() != 0) {
                actionUtility.click(selectSeatOptions.get(0));
                actionUtility.waitForElementVisible(seatSelectionPanel,5);
                selectSeatForEachPassenger(seatsToBeSelected,seatRates);
            }
        }

    }

    private  void selectSeatForEachPassenger(String[] seatsToBeSelected, ArrayList<String>seatRates ){
        //selects seat for each passenger
            for(int j=0; j<seatsToBeSelected.length; j++) {
            actionUtility.click(seatForPassengers.get(j));
            if (seatsToBeSelected[j].toLowerCase().equals("infant")) {
                actionUtility.click(infantSeats.get(0));
                seatRates.add(standardSeatPrice.getAttribute("data-price"));

            } else if (seatsToBeSelected[j].toLowerCase().equals("standard")) {
                actionUtility.click(standardSeats.get(0));
                seatRates.add(standardSeatPrice.getAttribute("data-price"));
            } else if (seatsToBeSelected[j].toLowerCase().equals("emergencyExit")) {
                actionUtility.click(emergencySeats.get(0));
                if(actionUtility.verifyIfElementIsDisplayed(emergencySeatNotification)){
                    actionUtility.click(emergencySeatNotification);
                }
                seatRates.add(extraLegRoomSeatPrice.getAttribute("data-price"));

            }else if(seatsToBeSelected[j].toLowerCase().equals("extraLeg")){
                actionUtility.click(extraLegRoomSeats.get(0));
                seatRates.add(extraLegRoomSeatPrice.getAttribute("data-price"));
            }
        }

        actionUtility.click(selectSeat);

    }

    private List<WebElement> getSeatSelectionOptionForEachFlight(int index, String tripType, int seatSelectOptionType) {
        //return the seat selection option for the flight
        List<WebElement> elements = TestManager .getDriver().findElements(By.xpath(String.format(seatSelectionOptionsLocator, tripType, index, seatSelectOptionType)));
        return  elements;
    }

    public Hashtable<String,Hashtable<String,Hashtable>> captureSeatsSelectedForPassengers(){

        //captures the seat selected for each passenger

        Hashtable<String, Hashtable<String, Hashtable>> seatsSelectedForPassengers = new Hashtable<String, Hashtable<String, Hashtable>>();
        try {
            for (int i = 0; i < passengersForSeatSelection.size(); i++) {
                seatsSelectedForPassengers.put("passenger" + (i + 1), getSeatsForEachFlight(i));
            }
            reportLogger.log(LogStatus.INFO, "successfully captured the seats selected");
            System.out.println(seatsSelectedForPassengers);
            return seatsSelectedForPassengers;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to capture the seats selected");
            throw e;
        }
    }

    private Hashtable<String, Hashtable> getSeatsForEachFlight(int index) {

        //returns the seat selected in each flight for a passenger
        Hashtable<String, String> seatsSelectedForOutbound = new Hashtable<String, String>();
        Hashtable<String, String> seatsSelectedForInbound = new Hashtable<String, String>();
        Hashtable<String, Hashtable> seatsSelected = new Hashtable<String, Hashtable>();
        System.out.println(String.format(seatSelector, (index + 1)));
        List<WebElement> flightsForSeatSelection = TestManager.getDriver().findElements(By.xpath(String.format(seatSelector, (index + 1))));
        int inboundFlightCount = 0;
        String seatNo = "--";
        for (int i = 0; i < flightsForSeatSelection.size(); i++) {
            if (flightsForSeatSelection.get(i).getText() != null) {
                if (flightsForSeatSelection.get(i).getText().trim().length() > 0) {
                    seatNo = flightsForSeatSelection.get(i).getText().trim();
                }
            }

            if (outboundFlightListForSeatSelection.size() > i) {
                seatsSelectedForOutbound.put("flight" + (i + 1), seatNo);
            } else {
                seatsSelectedForInbound.put("flight" + (inboundFlightCount + 1), seatNo);
                inboundFlightCount++;
            }
        }
        seatsSelected.put("outbound", seatsSelectedForOutbound);
        seatsSelected.put("inbound", seatsSelectedForInbound);

        return seatsSelected;
    }


    public void acceptBags() {
        //click on accept bags button
        try {
                actionUtility.click(savePassengerBags);
            reportLogger.log(LogStatus.INFO, "successfully accepted bags");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to accept the bags");
            throw e;
        }
    }

    public void     acceptSeats() {
        //click on accept seats button
        try {
            actionUtility.hardSleep(500);
            actionUtility.click(saveSeats);
            reportLogger.log(LogStatus.INFO, "successfully accepted seats");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to accept the seats");
            throw e;
        }
    }


    public void selectCheckInOption(Hashtable<String,String> testData){

        //selects the check-in option

        try {
                actionUtility.hardSleep(500);
                Hashtable<String, String> passengerNames = capturePassengerNames(testData);

                if (testData.get("checkInOption")== null ||testData.get("checkInOption").toLowerCase().contains("no thanks")) {
                    if(Integer.parseInt(testData.get("departDateOffset"))>2) {
                        if(testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            actionUtility.selectOption(checkInLater);
                            actionUtility.click(saveCheckInOption);
                        }
                    }else if(Integer.parseInt(testData.get("departDateOffset"))==2){
                        if (testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            if(actionUtility.verifyIfElementIsDisplayed(checkInLater)) {
                                actionUtility.selectOption(checkInLater);
                                actionUtility.click(saveCheckInOption);
                            }
                        }
                    }

                }else if (testData.get("checkInOption").toLowerCase().contains("email")) {

                    if (Integer.parseInt(testData.get("departDateOffset")) > 2) {
                        if (testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            actionUtility.selectOption(checkInByEmail);
                            enterACIEmailDetails(testData, passengerNames);
                            actionUtility.click(saveCheckInOption);
                        }
                    } else if (Integer.parseInt(testData.get("departDateOffset")) == 2) {
                        if (testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            if (actionUtility.verifyIfElementIsDisplayed(checkInByEmail)) {
                                actionUtility.selectOption(checkInByEmail);
                                enterACIEmailDetails(testData, passengerNames);
                                actionUtility.click(saveCheckInOption);

                            }
                        }
                    }
                } else if (testData.get("checkInOption").toLowerCase().contains("mobile")) {



                    if (Integer.parseInt(testData.get("departDateOffset")) > 2) {
                        if (testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            actionUtility.selectOption(checkInByMobile);
                            enterACIMobileDetails(testData, passengerNames);
                            actionUtility.click(saveCheckInOption);

                        }
                    } else if (Integer.parseInt(testData.get("departDateOffset")) == 2) {
                        if (testData.get("outboundFlight1").toLowerCase().trim().equals("flybe")) {
                            if (actionUtility.verifyIfElementIsDisplayed(checkInByMobile)) {
                                actionUtility.selectOption(checkInByMobile);
                                enterACIMobileDetails(testData, passengerNames);
                                actionUtility.click(saveCheckInOption);

                            }
                        }
                    }

                }
            reportLogger.log(LogStatus.INFO, "successfully selected the check-in option");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the check-in option");
            throw e;
        }
    }

    private void enterACIEmailDetails(Hashtable<String,String> testData,Hashtable<String,String> passengerNames){
        // enter email address for aci check in
        List<String> passengerTitles = new ArrayList<String>(Arrays.asList(new String[]{"Rev.", "Dr.", "Prof."}));
        String title = null;
        for(int i=0; i<aciEmailAddress.size(); i++){
            title = passengerNames.get("passengerName"+(i+1)).split(" ")[0];
            if (passengerTitles.contains(title)){
                actionUtility.dropdownSelect(aciGender.get(i), ActionUtility.SelectionType.SELECTBYTEXT, testData.get("passengerACIGender"+(i+1)));
            }
            actionUtility.sendKeys(aciEmailAddress.get(i),testData.get("passengerACIEmail"+(i+1)));
            if(testData.get("sendAllBoardingPassToEmail")!=null) {
                if (i==0 && testData.get("sendAllBoardingPassToEmail").equals("true")) {
                    actionUtility.click(sendAllBoardingPassToEmail);
                    break;
                }
            }
        }

    }

    private void enterACIMobileDetails(Hashtable<String,String> testData,Hashtable<String,String> passengerNames){
        // enter mobile no. for aci check in
        List<String> passengerTitles = new ArrayList<String>(Arrays.asList(new String[]{"Rev.", "Dr.", "Prof."}));
        String title = null;
        for(int i=0; i<aciMobileNumbers.size(); i++){
            title = passengerNames.get("passengerName"+(i+1)).split(" ")[0];
            if (passengerTitles.contains(title)){
                actionUtility.dropdownSelect(aciGender.get(i), ActionUtility.SelectionType.SELECTBYTEXT, testData.get("passengerACIGender"+(i+1)));
            }
            actionUtility.dropdownSelect(aciMobileDiallingCodes.get(i),ActionUtility.SelectionType.SELECTBYVALUE,testData.get("passengerACIDialCode"+(i+1)));
            actionUtility.sendKeys(aciMobileNumbers.get(0),testData.get("passengerACIMobileNumber"+(i+1)));

            if(i==0 && testData.get("sendAllBoardingPassToMobile")!=null) {
                if (testData.get("sendAllBoardingPassToMobile").equals("true")) {
                    actionUtility.click(sendAllBoardingPassToEmail);
                    break;
                }
            }
        }

    }

    public void addFlexToBooking() {
        // adds the flex option
        try {

            actionUtility.selectOption(flexTicket);
            actionUtility.click(saveFlex);
            reportLogger.log(LogStatus.INFO, "added the ticket flexibility");
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "uanble to add the ticket flexibility");
            throw e;
        }
    }



    public void verifyChangeFeeIsDisplayed(boolean flag){
//        this method verifies whether change fee is applied or not in GBP only
        Double expectedChangeFeePrice = 40.0;
        Double actualChangeFeePrice = 0.0;
        try{
            if(flag){
                try {
                    actionUtility.waitForElementVisible(changeFeePrice,10);
                    actualChangeFeePrice = CommonUtility.convertStringToDouble(changeFeePrice.getText());
                    Assert.assertEquals(actualChangeFeePrice,expectedChangeFeePrice,"change fee price is not matching");
                    reportLogger.log(LogStatus.WARNING,"change fee price is matching");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change fee price is not matching Expected - "+expectedChangeFeePrice+"GBP Actual - "+actualChangeFeePrice+"GBP");
                    throw e;
                }
            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(changeFeePrice));
                    reportLogger.log(LogStatus.INFO, "change fee is not displayed");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change fee is displayed");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify change fee is displayed or not");
            throw e;
        }
    }




//    -------avios methods--------

    public void verifyWarningMessageDisplayedAfterUserAviosLoginIfUserIsNotRegisteredWithAvios(){
        try{
            Assert.assertTrue(actionUtility.flexibleWaitForElementVisible(aviosWarningMsgWhenLoggedIn,2));

        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"warning message is not displayed after avios login for user without avios membership");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if warning message is displayed after avios login for user without avios membership");
            throw e;
        }
    }

    public void retrieveAviosDetailsByLogin(Hashtable<String,String> testData){
        // Login to the avios account
        try{
            if(actionUtility.verifyIfElementIsDisplayed(saveFlex)){
                actionUtility.click(saveFlex);
            }

            actionUtility.click(aviosLowOption);
            actionUtility.waitForElementVisible(aviosLoginMenu,3);
            String userName = testData.get("aviosUserName");
            String password = testData.get("aviosPassword");
            actionUtility.sendKeys(aviosUserName,userName);
            actionUtility.sendKeys(aviosPassword,password);
           actionUtility.click( aviosLogin);
            if(actionUtility.verifyIfElementIsDisplayed(aviosDoNotForget)){
                actionUtility.click(aviosDoNotForget);
            }

            actionUtility.waitForElementVisible(aviosBalance, 10);
            reportLogger.log(LogStatus.INFO, "successfully logged into avios account");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to retrieve avios account details");
            throw e;
        }
    }

    public double selectAviosType(String aviosType){
        // to select the avios type
        WebElement selectedAviosType = null;
        double maxAviosPrice = 0.0;
        switch (aviosType.toLowerCase()){
            case "low":
                selectedAviosType = aviosLowOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
                break;
            case "medium":
                selectedAviosType = aviosMediumOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
                break;
            case "high":
                selectedAviosType = aviosHighOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
                break;
        }
        try{
            actionUtility.selectOption(selectedAviosType);
            actionUtility.waitForElementVisible(aviosBasketPrice,3);
            reportLogger.log(LogStatus.INFO,"avios option is successfully selected");
            System.out.println(maxAviosPrice);
            return maxAviosPrice;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the required avios option");
            throw e;
        }
    }



    public void verifyAviosBalanceIsDisplayed(){
        // verifies avios balance is displayed
        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBalance), "avios balance is not displayed");
            reportLogger.log(LogStatus.INFO, "avios balance is displayed - " + aviosBalance.getText());
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "avios balance is not displayed" );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if avios balance is displayed");
            throw e;
        }
    }

    public void verifyTheAviosOptions(){
        // verify all three avios options are displayed

        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosLowOption), "avios low option is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosMediumOption), "avios medium option is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosHighOption), "avios high option is not displayed");
            reportLogger.log(LogStatus.INFO, "avios options are displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "avios options are not displayed" );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify avios options");
            throw e;
        }
    }


    public void verifySeatSelectionPaneIsDisplayed() {
        //verifies if the seat selection pane is displayed
        try{
            actionUtility.click(getSeatSelectionOptionForEachFlight(1, "outbound", 2).get(0));

            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(seatSelectionPaneFront),"seat selection pane is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(seatSelectionPaneMid),"seat selection pane is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(seatSelectionPaneBack),"seat selection pane is not displayed");
            reportLogger.log(LogStatus.INFO,"seat selection pane is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"seat selection pane is not displayed");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if seat selection pane is displayed");
            throw e;
        }
    }

    public void verifyPassengerDetailsFooter() {
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "all fares quoted on flybe.com are subject to availability, and are inclusive of all taxes, fees and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            String fPara4 = "Certain Loganair fares have conditions attached requiring a minimum length of stay.";


            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(contactUsLink.isDisplayed(), "Contact Us Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            }



            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


            try {
                Assert.assertEquals(footerPara4.getText().toLowerCase(), fPara4.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify footer in passenger page");
            throw e;
        }
    }


    public void verifyACIOptionDisplayedForBlueIsland() {
//        verifies whether ACI option is displayed for Blue Islands
//        String pageURL = "initialisePassengerDetails";
//        actionUtility.waitForPageURL(pageURL,10);
        actionUtility.waitForPageLoad(10);
        try {
            Assert.assertFalse(aciHeading.isDisplayed(), "ACI heading is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI heading is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI heading is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI heading");
            throw e;
        }

        try {
            Assert.assertFalse(checkInByEmail.isDisplayed(), "ACI email option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI email option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI email option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI email option");
            throw e;
        }

        try {
            Assert.assertFalse(checkInByMobile.isDisplayed(), "ACI mobile option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI mobile option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI mobile option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI mobile option");
            throw e;
        }

        try {
            Assert.assertFalse(checkInLater.isDisplayed(), "ACI manual option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI manual option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI manual option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI manual option");
            throw e;
        }
    }



    public void verifySeatSelectionPaneNotDisplayed(){
        //To verify the seat selection section is not displayed for Air France Flight
        try{

            Assert.assertEquals(0,outBoundSelectSeatOptions.size());
            Assert.assertEquals(0,inBoundSelectSeatOptions.size());
            Assert.assertTrue(seatSectionUnavailable.size()>0);
            reportLogger.log(LogStatus.INFO,"seat selection pane is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"seat selection pane is displayed");
            throw e;
        }catch (NoSuchElementException e){
            reportLogger.log(LogStatus.INFO,"seat selection pane is not displayed");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if seat selection pane is displayed");
            throw e;
        }

    }



    public void verifyFlybeFlexTicketFlexibilityCheckBoxIsUnchecked(){
        // To Verify the Flybe Flex Checkbox is unchecked
        try {

            actionUtility.waitForPageLoad(5);
            try {
                Assert.assertFalse(flexTicket.isSelected(), "Flybe flex ticket flexibility is selected");
                reportLogger.log(LogStatus.INFO, "Flybe flex ticket flexibility is not selected");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe flex ticket flexibility is selected");
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe flex is not found");
                throw e;
            }
        }catch(AssertionError e){
            throw e;

        }catch (NoSuchElementException e) {
            throw e;

        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to  verify if Flybe flex field is not selected");
            throw e;

        }
    }


    public void acceptTermsAndConditionForEditing(){

        // select terms and conditions this method to be called only during amending the flight
        try {
            for (int i = 0; i < termsAndConditionsForEdit.size(); i++) {
                if (termsAndConditionsForEdit.get(i).isDisplayed()) {
                    actionUtility.click(termsAndConditionsForEdit.get(i));
                }
            }
            reportLogger.log(LogStatus.INFO, "Successfully confirmed terms and conditions while doing itenary changes");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to confirm terms and conditions while doing itenary changes");
            throw e;
        }
    }

    public void verifyBasketPriceWithBasketPriceOfFareSelectionPage(Hashtable<String,Double> previousBasketDetails){
        //verifies if the total basket price in the your details page is same as previous page
        double expectedPrice =0.0;
        double actualPrice = -1.0;
        try {
            try {
                expectedPrice = previousBasketDetails.get("totalPrice");
                actionUtility.waitForElementVisible(overallBasketTotal,10);

                DecimalFormat df = new DecimalFormat("####0.00");
                System.out.println(df.format(CommonUtility.convertStringToDouble(overallBasketTotal.getAttribute("data-price"))));
                actualPrice = CommonUtility.convertStringToDouble(df.format(CommonUtility.convertStringToDouble(overallBasketTotal.getAttribute("data-price"))));
//                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getAttribute("data-price"));
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in passenger details page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in passenger details page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getAttribute("data-price"));
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in passenger details page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in passenger details page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getAttribute("data-price"));
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in passenger details page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in passenger details page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify outbound, return and total price in the basket, in the passenger details page against outbound, return and total price of fare selection page");
            throw e;
        }




    }

    public void acceptTermsAndCondition(){
        //accepts the terms and conditions
        try {

            if (termsAndConditions.size() != 0) {

                for (WebElement condition : termsAndConditions) {
                    actionUtility.selectOption(condition);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the terms and conditions");
            } else {
                reportLogger.log(LogStatus.WARNING, "terms and conditions are not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the terms and conditions");
            throw e;
        }
    }



    public void verifyFirstNameLastNameAndTitleIsDisplayedForPassenger(int noOfPassengers){
        // to verify the passenger details section "Drop down, First name, Last name"
        try {
            actionUtility.waitForElementVisible(passengerDetailsSection, 10);
            Assert.assertEquals(passengerTitles.size(),noOfPassengers);
            Assert.assertEquals(passengerFirstNames.size(),noOfPassengers);
            Assert.assertEquals(passengerLastNames.size(),noOfPassengers);

            for(int i=0; i<noOfPassengers;i++){
                Assert.assertEquals(new Select(passengerTitles.get(0)).getFirstSelectedOption().getText(),"");
                Assert.assertEquals(passengerFirstNames.get(0).getText(),"");
                Assert.assertEquals(passengerLastNames.get(0).getText(),"");
            }
                reportLogger.log(LogStatus.INFO, "Passenger name input fields are  matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "Passenger name input fields are not matching");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify the number of passenger name input fields");
        }
    }



    public void openRulesTaxesFromBasket(String rulesToOpen){
        //open the respective rules from basket
        try {
            actionUtility.waitForElementVisible(passengerDetailsSection, 10);
            switch(rulesToOpen.toLowerCase()) {

                case "fare rules":
                    actionUtility.click(fareRules);
                    actionUtility.waitForElementVisible(rulesPopup, 10);
                    break;
                case "baggage rules":
                    actionUtility.click(baggageRules);
                    System.out.println(rulesPopup.isDisplayed());
                    actionUtility.waitForElementVisible(rulesPopup, 10);
                    break;
                case "taxes and charges":
                    actionUtility.click(taxes);
                    actionUtility.waitForElementVisible(rulesPopup, 10);
                    break;

                case "price breakdown":
                    actionUtility.click(priceBreakDown);
                    actionUtility.waitForElementVisible(priceBreakDownDetails, 10);
                    break;
            }
            reportLogger.log(LogStatus.INFO, "clicked on " + rulesToOpen + " link");

        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to click on " + rulesToOpen + " link");
            throw e;
        }
    }


    public void verifyRulesPopupOpened(String popupTypeToBeOpened) {
        //verifies if the right popup is opened

        try {
            switch (popupTypeToBeOpened.toLowerCase()) {

                case "fare rules":
                case "baggage rules":
                case "taxes and charges":
                    Assert.assertEquals(rulesPopupHeader.getText().toLowerCase(), popupTypeToBeOpened.toLowerCase());
                    break;
                case "price breakdown":
                    Assert.assertTrue(priceBreakdownSection.isDisplayed(), popupTypeToBeOpened.toLowerCase());
                    break;
            }
            reportLogger.log(LogStatus.INFO, popupTypeToBeOpened + " popup is opened ");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, popupTypeToBeOpened + " popup is not opened Actual: " + rulesPopupHeader.getText()+" Expected: "+popupTypeToBeOpened);
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if the " + popupTypeToBeOpened + " is opened" );
            throw e;
        }
    }


    public void verifyTermsAndConditionsInBaggageRulesPopup(){
        //To verify the Baggage Terms and Conditions inside baggage rule popup
        String baggageTermsHeadingExpected = "Baggage restrictions, terms and conditions";
        try {
            actionUtility.click(baggageTermsAndConditionsLink);
            switchToBaggageTermsAndConditionsWindow();
            actionUtility.waitForElementVisible(baggageTermsAndConditions,10);
            try {
                Assert.assertEquals(baggageTermsAndConditions.getText().toLowerCase().trim(), baggageTermsHeadingExpected.toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Baggage Terms and Conditions is not matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Baggage Terms and Conditions is not matching"+" Expected - "+baggageTermsHeadingExpected+" Actual - "+ baggageTermsAndConditions.getText());
                throw e;
            }
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify Baggage Terms and Conditions in baggage popup");
            throw e;
        }
    }


    private void switchToBaggageTermsAndConditionsWindow(){
        // This method is used for switching to baggage terms and conditions
        boolean isSwitchSuccess = actionUtility.switchWindow("Flybe.com");
        if (!isSwitchSuccess) {

            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
            if (isSwitchToCertificateErrorSuccess) {

                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");


            } else {
                reportLogger.log(LogStatus.WARNING,"unable to pass control to baggage terms and conditions window");
                throw new RuntimeException("unable to pass control to baggage terms and conditions window");
            }

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Switching to baggage terms and conditions window is Successful");
        }else{
            reportLogger.log(LogStatus.INFO,"Switching to baggage terms and conditions is successful");

        }

    }


    public void verifyAdultTitleOptions() {
        //To verify the adult passenger tiles in passenger details page
        String title = "Mr;Mrs;Mstr;Ms;Miss;Dr;Prof.;Lord;Lady;Sir;Rev.";
        String[] expectedTitles = title.split(";");
        Select actualPassengersType = new Select(passengerTitles.get(0));
        List<WebElement> actualTitles = actualPassengersType.getOptions();

        try {
            for(String expectedTitle : expectedTitles){
                boolean flag = false;
                for(WebElement actualTitle : actualTitles ) {
                    if (expectedTitle.equals(actualTitle.getText())){
                        flag = true;
                        reportLogger.log(LogStatus.INFO,"Passenger title- "+expectedTitle+" is present as adult titles" );
                        break;
                    }
                }
                if (!flag){
                    reportLogger.log(LogStatus.WARNING,"Passenger titles are not matching. " + expectedTitle +" is not present as adult titles ");
                    throw new AssertionError("Passenger titles are not matching");
                }
            }
        }catch (AssertionError e){
            throw e;
        }
        catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify the passenger titles");
            throw e;
        }
    }


    public void verifyBaggagePriceFreeFor23Kg() {
//        this method verifies the 23kg baggage is free in this route or not
        try {
            actionUtility.waitForPageLoad(20);
            actionUtility.waitForElementVisible(twentyThreeBagPrice,10);
            double twentyThreePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getAttribute("data-price"));
            if (twentyThreePrice == 0.0) {
                reportLogger.log(LogStatus.INFO, "baggage price for 23 kg is displayed as 0");
                actionUtility.click(twentyThreeBags.get(0));
                actionUtility.click(savePassengerBags);
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "baggage price of 23 kg is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"baggage price of 23 kg is displayed in the basket");
                    throw e;
                }
            }else {
                reportLogger.log(LogStatus.WARNING,"baggage price for 23 kg is not displayed as 0 instead showing as - "+twentyThreePrice);
                throw new RuntimeException("baggage price for 23 kg is not displayed as 0 instead showing as - "+twentyThreePrice);
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify baggage price of 23 kg");
            throw e;
        }
    }

    public void VerifyIncreaseBaggageAllowance() {
//        this method verifies increase baggage allowance
        try {
            Assert.assertTrue(baggageCurrencies.size()>2, "option increase baggage allowance is not displayed");
            reportLogger.log(LogStatus.INFO, "option increase baggage allowance is displayed");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"option increase baggage allowance is not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify increase baggage allowance");
            throw e;
        }
    }


    public void verifyBaggageIsIncluded(String bagType){
        //verifies if the baggage is included by default by checking price to zero

        try {
            double expectedPrice = 0.0;
            double actualPrice = getBagPrice(bagType);
            Assert.assertEquals(actualPrice, expectedPrice, bagType + " baggage is not inclusive");
            reportLogger.log(LogStatus.INFO, bagType + " baggage is  included");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, bagType + "  baggage is not inclusive");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify if "+ bagType + "  baggage is inclusive");
            throw e;
        }


    }


    public void verifyBaggagePriceForGetMoreDisplayedAtReducedCost(){
//        this method verifies the baggage price is displayed at a reduced cost if we select get more and try to increase it
        Double twentyThreePrice = 0.0;
        Double fourtySixPrice = 0.0;
//        Double twentyThreeBaggagePriceExpected = 8.01;
        Double fourtySixBaggagePriceExpected = 25.0;
        try{
//            actionUtility.waitForElementVisible(twentyThreeBagPrice,10);
//            twentyThreePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getAttribute("data-price"));
            actionUtility.waitForElementVisible(fortySixBagPrice,10);
            fourtySixPrice = CommonUtility.convertStringToDouble(fortySixBagPrice.getText());
//            try{
//                Assert.assertEquals(twentyThreePrice,twentyThreeBaggagePriceExpected,"baggage price for 23kg is not matching");
//                reportLogger.log(LogStatus.INFO,"baggage price for 23 kg is displayed at a reduced cost");
//            }catch (AssertionError e){
//                reportLogger.log(LogStatus.WARNING,"23kg baggage is not displayed at a reduced cost Expected - "+twentyThreeBaggagePriceExpected+" Actual - "+twentyThreePrice);
//                throw e;
//            }
            try{
                Assert.assertEquals(fourtySixPrice,fourtySixBaggagePriceExpected,"baggage price for 46kg is not matching");
                reportLogger.log(LogStatus.INFO,"baggage price for 46kg is displayed at a reduced cost");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"46kg baggage is not displayed at a reduced cost Expected - "+fourtySixBaggagePriceExpected+" Actual - "+fourtySixPrice);
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify baggage price at reduced cost");
            throw e;
        }


    }


    public void verifyCurrency(String expectedCurrency){
        // verifies if the currencySelected in the drop down is same as the argument passed
        String actualCurrency=null;
        try{
            actualCurrency=  currencySelected.getText();
            Assert.assertEquals(actualCurrency,expectedCurrency,"currencySelected selected by default in the your details page is not matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"currencySelected selected by default in the your details page is not matching Expected - "+ expectedCurrency+" Actual - "+actualCurrency);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currencySelected");
            throw e;
        }
    }


    public void verifySeatsSectionIsDisplayed(){
        // to verify the seat section

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(passengerSeatSection), "Seats section is not displayed");
            reportLogger.log(LogStatus.INFO,"Seats section is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "Seats section is not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to if verify seat selection is displayed");
            throw e;

        }
    }


    public void verifyBaggageSectionIsDisplayed(boolean flag){
        // to verify the baggage section

        if(flag) {
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(passengerBaggageSection), "Baggage section is not displayed");
                reportLogger.log(LogStatus.INFO, "Baggage section is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Baggage section is not displayed");
                throw e;
            } catch (Exception e) {
                reportLogger.log(LogStatus.INFO, "unable to if verify baggage section is displayed");
                throw e;

            }
        }else {
            try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(passengerBaggageSection), "Baggage section is displayed");
                reportLogger.log(LogStatus.INFO,"Baggage section is not displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "Baggage section is displayed");
                throw e;
            } catch (Exception e){
                reportLogger.log(LogStatus.INFO, "unable to if verify baggage section is displayed");
                throw e;

            }
        }
    }


    public void verifyTheSeatLegends() {
        // to verify the seat mapping legends in seat section


        try {

            actionUtility.click(getSeatSelectionOptionForEachFlight(1, "outbound", 2).get(0));
            actionUtility.waitForElementVisible(seatSelectionPanel,5);

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(selectedSeatLegend),"Selected seat Mapping is not displayed");
                reportLogger.log(LogStatus.INFO, "Selected seat legend is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Selected seat legend is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(standardSeatLegend),"Standard seat Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Standard seat key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Standard seat key mapping is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(emergencyExitSeatLegend),"Emergency exit seat Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Emergency exit seat key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Emergency exit seat key mapping is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(extraLegRoomSeatLegend),"Extra leg room key Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Extra leg room key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Extra leg room key mapping is not displayed");
                throw e;
            }

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(infantSeatLegend),"infant key Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "infant key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "infant key mapping is not displayed");
                throw e;
            }

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(notAvailableSeatLegend),"not available key Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "not available key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "not available key mapping is not displayed");
                throw e;
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the seat mapping keys");
            throw e;
        }
    }


    public void verifyCurrencyInBaggageType(String expectedCurrency){
        //verify the currency in baggage section

        try {

            baggageCurrencies.forEach((element)-> Assert.assertTrue(element.getText().toLowerCase().contains(expectedCurrency.toLowerCase()), "currency is not matching in bagage type"));
            reportLogger.log(LogStatus.INFO,"currency for each baggage type is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for each baggage type is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in baggage type");
            throw e;
        }
    }


    public void verifyCurrencyInSeatType(String currencyToBeVerified){
        //verify the currency in seat section
        try {

            actionUtility.click(getSeatSelectionOptionForEachFlight(1, "outbound", 2).get(0));
            actionUtility.waitForElementVisible(seatSelectionPanel,5);
            seatCurrencies.forEach((element)->Assert.assertTrue(element.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()),"currency is not matching in seat type"));
            reportLogger.log(LogStatus.INFO,"currency for seat type is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for seat type is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in seat type");
            throw e;
        }
    }


//    public void changeCurrency(String currency){
//        //used to select the currency in basket
//        try{
//            actionUtility.dropdownSelect(chooseCurrency,ActionUtility.SelectionType.SELECTBYTEXT,currency);
//            reportLogger.log(LogStatus.INFO,currency+"successfully changed currency");
//        }catch (Exception e){
//            reportLogger.log(LogStatus.WARNING,currency+"unable to changed currency");
//            throw e;
//        }
//
//    }


    public Hashtable verifyBasket(Hashtable<String,String> testData,int noOfPassengers,int noOfOutboundFlights, int noOfInboundFlights, boolean... editFlag) {
        //verifies the basket
        try {
            int noOfFlights = noOfOutboundFlights;
            if (noOfInboundFlights > 0) {
                verifyFlexPriceAddedToBasket(testData, noOfPassengers, noOfOutboundFlights, noOfInboundFlights);
                noOfFlights = noOfOutboundFlights + noOfInboundFlights;
            } else {
                verifyFlexPriceAddedToBasket(testData, noOfPassengers, noOfOutboundFlights);
            }
            if (editFlag.length > 0) {
                verifyBaggagePriceInBasket(testData, noOfFlights, editFlag.length);
            } else {
                verifyBaggagePriceInBasket(testData, noOfFlights);
            }

            verifySeatPriceInBasket(testData);
            verifyAviosPriceAddedToBasket();
            Hashtable <String,Double> basketDetails =verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();

            return basketDetails;
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }


    private Hashtable<String, Double> verifyTotalBasketPriceWithSummationOfOtherElementsInBasket(){
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double sum =0.0;
        double overallBasketPrice =1.0;
        double aviosPriceApplied = 0.0;
        double maxAviosPrice = 0.0;

        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();


        try{
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);



            if (actionUtility.verifyIfElementIsDisplayed(returnTotal,2)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice,2)){
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
                basketDetails.put("ticketFlexibilityPrice",ticketFlexibilityPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice,2)){
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
                basketDetails.put("baggagePrice",bagPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice,2)){
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
                basketDetails.put("seatsPrice",seatsPrice);
            }

            if(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice,2)){
                aviosPriceApplied = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText())*100.0)/100.0;
                basketDetails.put("aviosPriceApplied",aviosPriceApplied);
                if (aviosLowOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
                } else if (aviosMediumOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
                } else if (aviosHighOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
                }
                basketDetails.put("maxAviosPrice",maxAviosPrice);
            }
            sum = Math.round((outboundPrice+inboundPrice+ticketFlexibilityPrice+bagPrice+seatsPrice-aviosPriceApplied)*100.0)/100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            basketDetails.put("overallBasketPrice",overallBasketPrice);
            Assert.assertEquals(overallBasketPrice,sum,"total basket price in the passenger details page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO,"total basket price in the passenger details page is matching with summation of all the element prices in the basket");
            return basketDetails;
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"total basket price in the passenger details page is not matching with summation of all the element prices in the basket Expected - "+sum+" Actual -"+overallBasketPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify totals prices in the basket in passenger details page");
            throw e;
        }
    }


    private void verifyAviosPriceAddedToBasket(){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice();

            if (expectedPrice > 0) {

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                    throw e;
                }

                actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

                try {
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }

            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is not added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is not added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is added to basket");
                    throw e;
                }
            }
        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }



    private double calculateAviosPrice() {
        // to verify the avios price according to selection
        double maxAviosPrice = -1.0;
        double aviosPriceApplied = 0.0;
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        if (aviosLowOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
        } else if (aviosMediumOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
        } else if (aviosHighOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
        }
        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
        if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
            inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;


        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }



    private void verifySeatPriceInBasket(Hashtable<String,String> testData){
        // verifies the seat price in basket
        double expectedSeatPrice =0.0;
        double actualSeatPrice =-1.0;
        try{
            expectedSeatPrice = getOverallSeatPrice(testData);

            if (expectedSeatPrice!=0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketSeatPrice),"seat price in the basket is not added");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is added");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is not added");
                    throw e;
                }catch(Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is added");
                    throw e;
                }

                actualSeatPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());

                try {
                    Assert.assertEquals(actualSeatPrice,expectedSeatPrice,"seat price in the basket is not matching");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is matching");


                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is not matching Expected - "+expectedSeatPrice+" Actual -"+actualSeatPrice);
                    throw e;
                }catch (Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is matching");
                    throw e;
                }
            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketSeatPrice),"seat price in the basket is added");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is not added");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is added");
                    throw e;
                }catch(Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is added");
                    throw e;
                }
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            throw e;
        }
    }


    private double getOverallSeatPrice(Hashtable<String,String> testData){
        // depending on the trip type calls the method to get the seat price

        double overallSeatPrice=0.0;


         for(int i=0; i<overallSeatPrices.size(); i++){
             overallSeatPrice = overallSeatPrice+ CommonUtility.convertStringToDouble(overallSeatPrices.get(i).getText());
         }

        overallSeatPrice = Math.round(overallSeatPrice*100.0)/100.0;
        return overallSeatPrice;

    }

    private void verifyBaggagePriceInBasket(Hashtable<String,String> testData, int noOfFlights, int... editFlag){
        //validates the baggage price in the basket

        double expectedBaggagePrice = 0.0;
        double actualBaggagePrice = 1.0;
        boolean conditionFlag = false;

        try {
            expectedBaggagePrice = captureSelectedBaggagePrice(testData, editFlag.length);
            expectedBaggagePrice = expectedBaggagePrice * noOfFlights;
            expectedBaggagePrice = Math.round(expectedBaggagePrice * 100.0) / 100.0;

            if (expectedBaggagePrice!=0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice,2), "baggage price is not displayed in the basket");
                    reportLogger.log(LogStatus.INFO, "baggage price is displayed in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price is not displayed in the basket");
                    throw e;
                }
                actualBaggagePrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                actualBaggagePrice = Math.round(actualBaggagePrice * 100.0) / 100.0;
                try {
                    Assert.assertEquals(actualBaggagePrice, expectedBaggagePrice, "baggage price in basket is not matching");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching");
                }catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"baggage price in the basket is not matching Expected - "+expectedBaggagePrice+" Actual - "+actualBaggagePrice);
                    throw e;
                }

            }else {
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice,2), "baggage price in basket is added");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is not added");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "baggage price in the basket is added");
                    throw e;
                }catch (Exception e){
                    reportLogger.log(LogStatus.WARNING, "unable to verify the baggage price in the basket");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the baggage price in the basket");
            throw e;
        }


    }

    private double captureSelectedBaggagePrice(Hashtable<String,String> testData,int editFlag){
        //returns total baggage price
        String baggageToBeSelected;
        double baggagePrice = 0.0;

        for (int i = 0; i < passengerWithBags.size(); i++) {
            baggageToBeSelected = checkBaggageType(testData, i,editFlag);

            if (baggageToBeSelected != null) {
                baggagePrice += getBagPrice(baggageToBeSelected);
            }

        }
        baggagePrice = (Math.round(baggagePrice)*100.0)/100.0;
        return baggagePrice;

    }

    private double getBagPrice(String baggageSelected){
        // returns  baggage price for the type of the baggage

        double baggagePrice = 0.0;

        if (baggageSelected.toLowerCase().equals("15kg")){
            baggagePrice = CommonUtility.convertStringToDouble(fifteenBagPrice.getText());
        }
//        if (baggageSelected.toLowerCase().equals("20kg")){
//            baggagePrice = CommonUtility.convertStringToDouble(twentyBagPrice.getText());
//        }
        if (baggageSelected.toLowerCase().equals("23kg")){
            baggagePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getText());
        }
        if (baggageSelected.toLowerCase().equals("46kg")){
            baggagePrice = CommonUtility.convertStringToDouble(fortySixBagPrice.getText());
        }
        return baggagePrice;
    }

    private void verifyFlexPriceAddedToBasket(Hashtable<String,String> testData, int noOfPassengers, int noOfOutboundFlights, int...noOfInboundFlights){
        //verifies the flex price in basket
        double expectedFlexPrice =-1.0;
        double actualFlexPrice = 0.0;

        try {

            if (flexTicket.isSelected()) {
                expectedFlexPrice = getOverallFlexPrice(testData, noOfPassengers, noOfOutboundFlights, noOfInboundFlights);
            }

            if (expectedFlexPrice > 0.0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketFlexPrice,2), "ticket flexibility is not displayed in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is displayed in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is not displayed in the basket");
                    throw e;
                }

                try {
                    Assert.assertEquals(getOverallFlexPrice(testData, noOfPassengers, noOfOutboundFlights), CommonUtility.convertStringToDouble(basketFlexPrice.getText()), "ticket flexibility is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is not matching in the basket Expected - " + expectedFlexPrice + " Actual -" + CommonUtility.convertStringToDouble(basketFlexPrice.getText()));
                    throw e;
                }
            } else {
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketFlexPrice,2), "ticket flexibility is displayed in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is not displayed in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is displayed in the basket");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw  e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify ticket flexibility in the basket");
            throw e;
        }

    }

    private double getOverallFlexPrice(Hashtable<String,String> testData, int noOfPassengers, int noOfOutboundFlights, int...noOfInboundFlights){
        //returns the overall flex price

        double outboundFlexPrice =0.0;
        double inboundFlexPrice =0.0;

        String dateOffset = testData.get("departDateOffset");
        if (flightBreakDownOutboundFlightOption.getText().toLowerCase().contains("just fly")) {
            outboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset));
            if(testData.get("currency")!=null){
                outboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset),testData.get("currency"));
            }
            outboundFlexPrice = outboundFlexPrice * noOfOutboundFlights;
        }

        if (noOfInboundFlights.length>0 && flightBreakDownReturnFlightOption.getText().toLowerCase().contains("just fly")){
            dateOffset = testData.get("returnDateOffset");
            inboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset));
            if(testData.get("currency")!=null){
                inboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset),testData.get("currency"));
            }
            inboundFlexPrice = inboundFlexPrice* noOfInboundFlights[0];
        }

        outboundFlexPrice +=inboundFlexPrice;
        outboundFlexPrice = outboundFlexPrice*noOfPassengers;
        outboundFlexPrice = Math.round(outboundFlexPrice*100.0)/100.0;
        return outboundFlexPrice;
    }

    private double getFlexPrice(int date, String... currency) {
        //returns the flex price for the dateOffset

        double flexPrice = 0.0;
        if(currency.length>0){
            switch (currency[0]){
                case "EUR":
                    if (date >= 0 && date <= 14) {
                        flexPrice = 18.0;

                    } else if (date >= 15 && date <= 30) {
                        flexPrice = 14.0;

                    } else if (date >= 31) {
                        flexPrice = 8.0;
                    }
                    break;
                case "GBP":
                default:
                    if (date >= 0 && date <= 14) {
                        flexPrice = 12.99;

                    } else if (date >= 15 && date <= 30) {
                        flexPrice = 9.99;

                    } else if (date >= 31) {
                        flexPrice = 5.99;
                    }
                    break;
            }
        }else{
//              if nothing is passed as the currency then the default currency is being selected
            if (date >= 0 && date <= 14) {
                flexPrice = 12.99;

            } else if (date >= 15 && date <= 30) {
                flexPrice = 9.99;

            } else if (date >= 31) {
                flexPrice = 5.99;
            }
        }

        return flexPrice;

    }

    public void continueToCarHire() {
        //clicks on continue button to navigate to the payment page
        try {
            if(actionUtility.verifyIfElementIsDisplayed(saveFlex,1)){
                actionUtility.click(saveFlex);
            }
            actionUtility.waitForElementClickable(continueToPaymentButton,20);
            actionUtility.clickByAction(continueToPaymentButton);
            actionUtility.hardSleep(2000);
            actionUtility.waitForPageLoad(15);
            reportLogger.log(LogStatus.INFO, "successfully continued to extras");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to extras");
            throw e;
        }
    }



    public void verifyCurrencyInAvios(String currencyToBeVerified){
        //verify the currency in avios section
        try {
            aviosCurrencies.forEach(element -> Assert.assertTrue(element.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()),"currency for avios is not matching") );
            reportLogger.log(LogStatus.INFO,"currency for avios options is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for avios options is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in avios options");
            throw e;
        }
    }


    public void verifyElementsInBaggageSectionIsDisplayed(){
        // verify the elements in baggage section like

        String golfBaggageExpected = " / 1 bag + golf bag";
        try {
            try {

                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageNone), "No bag type is not displayed");
                reportLogger.log(LogStatus.INFO, "No bag type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "No bag type is not displayed");
                throw e;
            }

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageSmall),"small baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "small baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "small baggage type is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(standardBaggage),"medium baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "medium baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "medium baggage type is not displayed");
                throw e;
            }

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageLarge), "large baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "large baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "large baggage type is not displayed");
                throw e;
            }


            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageTwoBags), "extra large baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "extra large baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "extra large baggage type is not displayed");
                throw e;
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to if verify elements in baggage type");
            throw e;

        }
    }

    public void verifyBaggagesDisplayedInHoldLuggage(int noOfPassengers) {
        //verifies whether 46kg,23kg,15kg and none is displayed for each passenger
        try {


            int k = 0;
            for (int i = 0; i < noOfPassengers; i++) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fortySixBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(twentyThreeBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fifteenBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(zeroBags.get(k)));

                    k++;
                    reportLogger.log(LogStatus.INFO, "successfully verified that 0kg, 15kg, 23kg and 46kg baggages is displayed for passenger"+(i+1)+" in the Add Hold Luggage section on flight options page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.INFO, "baggages displayed for each passenger is not correct");
                    throw e;
                }
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify whether 0kg, 15kg, 23kg and 46kg baggages is displayed in the Add Hold Luggage section on flight options page");
            throw e;
        }
    }

    public void verifyBaggagePriceForStateLengthBetween250kmAnd750Km(){
//        this method verifies baggage price for state between 250km and 750Km
        double fifteenBagPriceExpected = 22.0;
        double twentyThreeBagPriceExpected = 26.0;
        double fortySixBagPriceExpected = 51.0;
        double fifteenBagPriceActual = 0.0;
        double twentyThreeBagPriceActual = 0.0;
        double fortySixBagPriceActual = 0.0;
        try {
            actionUtility.waitForElementVisible(passengerBaggageSection, 5);
            actionUtility.hardSleep(500);
            fifteenBagPriceActual = getBagPrice("15kg");
            verifyBaggagePrice(fifteenBagPriceActual,fifteenBagPriceExpected,"15kg");
            twentyThreeBagPriceActual = getBagPrice("23kg");
            verifyBaggagePrice(twentyThreeBagPriceActual,twentyThreeBagPriceExpected,"23kg");
            fortySixBagPriceActual = getBagPrice("46kg");
            verifyBaggagePrice(fortySixBagPriceActual,fortySixBagPriceExpected,"46kg");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the passenger bags");
            throw e;
        }

    }

    public void verifyBaggagePriceForStateLengthLessThan250km(){
//        this method verifies baggage price for state length less than 250km
        double fifteenBagPriceExpected = 19.0;
        double twentyThreeBagPriceExpected = 24.0;
        double fortySixBagPriceExpected = 48.0;
        double fifteenBagPriceActual = 0.0;
        double twentyThreeBagPriceActual = 0.0;
        double fortySixBagPriceActual = 0.0;
        try {
            actionUtility.waitForElementVisible(passengerBaggageSection, 5);
            actionUtility.hardSleep(500);
            fifteenBagPriceActual = getBagPrice("15kg");
            verifyBaggagePrice(fifteenBagPriceActual,fifteenBagPriceExpected,"15kg");
            twentyThreeBagPriceActual = getBagPrice("23kg");
            verifyBaggagePrice(twentyThreeBagPriceActual,twentyThreeBagPriceExpected,"23kg");
            fortySixBagPriceActual = getBagPrice("46kg");
            verifyBaggagePrice(fortySixBagPriceActual,fortySixBagPriceExpected,"46kg");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the passenger bags");
            throw e;
        }

    }

    private void verifyBaggagePrice(double priceActual, double priceExpected, String baggageValue){
//        this method verifies baggage price is displayed as expected
        try{
            Assert.assertEquals(priceActual,priceExpected,baggageValue+" baggage price is not matching");
            reportLogger.log(LogStatus.INFO,baggageValue+" baggage price is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,baggageValue+" baggage is not matching Expected - "+priceExpected+" Actual - "+priceActual);
            throw e;
        }
    }





}

