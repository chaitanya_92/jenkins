package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

/**
 * Created by STejas on 5/8/2017.
 */
public class LeapPLDConfirmation {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LeapPLDConfirmation()
    {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "h2 span")
    private WebElement referenceNumber;

    @FindBy(css = "div[class='checkin'] a img")
    private WebElement retrievePLDbutton;

    public String verifyPLDReferenceNumber(){
        // verifies the PLD reference no.
        try{

            actionUtility.waitForPageLoad(30);
            // actionUtility.waitForElementNotPresent(loadingLocator, 70);
            // actionUtility.waitForPageLoad(30);

            int attempt =0;
            boolean isDisplayed = false;
            while((!isDisplayed)&& attempt<1){
                try {
                    actionUtility.waitForElementVisible(referenceNumber, 35);
                    isDisplayed = true;
                }catch (TimeoutException e){ }
                catch (NullPointerException e){ }
                attempt++;
            }
            if(isDisplayed){
                reportLogger.log(LogStatus.INFO,"successfully completed the price lock down process with Price Lock-Down Reference: "+referenceNumber.getText());
            }else {
                reportLogger.log(LogStatus.WARNING,"unable to complete the price lock down process");
                throw new RuntimeException("unable to complete the price lock down process");
            }

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to complete the price lock down process");
            throw e;
        }
        return referenceNumber.getText();
    }


    public void navigateToPLDYourDetails()
    {
        try{
            actionUtility.waitForElementVisible(retrievePLDbutton,10);
            actionUtility.click(retrievePLDbutton);
            reportLogger.log(LogStatus.INFO,"successfully clicked Retrieve your Price Lock-Down");

        }catch(Exception e)
        {
            reportLogger.log(LogStatus.WARNING,"unable to click Retrieve your Price Lock-Down");
            throw e;
        }
    }


}
