package leapfrog.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class LeapYourDetails {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LeapYourDetails() {

        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id ="registeredUserN")
    private WebElement noFlyBeAccount;

    @FindBy(id = "registeredUserY")
    private WebElement yesFlyBeAccount;

    @FindBy(id = "Salutation")
    private WebElement contactPassengerSalutation;

    @FindBy(id = "firstname")
    private WebElement contactPassengerFirstName;

    @FindBy(id = "lastname")
    private WebElement contactPassengerLastName;

    @FindBy(id = "frequentFlyerNumber")
    private WebElement contactPassengerAviosNo;

    @FindBy(id = "country")
    private WebElement contactPassengerCountry;

    @FindBy(id = "postcode")
    private WebElement contactPassengerPostCode;

    @FindBy(id = "postalLookupLink")
    private WebElement contactPassengerFindAddress;

    @FindBy(id = "dontHavePostcode")
    private WebElement contactPassengerDontHavePostcode;

    @FindBy(id = "fancybox-content")
    private WebElement contactPassengerAddressListPopup;

    @FindBy(className = "pafleft")
    private List<WebElement> contactPassengerAddresses;

    @FindBy(xpath = "//div[@class='pafright']/input")
    private List<WebElement> contactPassengerAddressSelectors;

    @FindBy(id = "submit_button")
    private WebElement contactPassengerAddressContinue;

    @FindBy(id = "address")
    private WebElement contactPassengerAddressOne;

    @FindBy(id = "address2")
    private WebElement contactPassengerAddressTwo;

    @FindBy(id = "address2")
    private WebElement contactPassengerAddressThree;

    @FindBy(id = "town")
    private WebElement contactPassengerTown;

    @FindBy(id = "county")
    private WebElement contactPassengerCounty;

    @FindBy(css = "select#dayCountryDiallingCode")
    private WebElement contactPassengerLandCountryCode;

    @FindBy(id = "dayPhone")
    private WebElement contactPassengerLandLineNo;

    @FindBy(id = "mobileCountryDiallingCode")
    private WebElement contactPassengerMobileCountryCode;

    @FindBy(id = "mobilePhone")
    private WebElement contactPassengerMobileNo;

    @FindBy(id = "authenticationUsername")
    private WebElement contactPassengerEmail;

    @FindBy(id = "newPassword")
    private WebElement contactPassengerNewPassword;

    @FindBy(id = "authenticationPassword")
    private WebElement contactPassengerAuthenticationPassword;

    @FindBy(css = "a#retrieveDetails>span")
    private WebElement contactPassengerLogin;

    @FindBy(id = "bookerLead")
    private WebElement iAmPassenger;

    @FindBy(xpath = "//select[contains(@id,'passengerTitle')and not(contains(@id,'readonly'))]")
    private List<WebElement> passengerTitles;

    @FindBy(xpath = "//input[contains(@id,'FirstName')and not(contains(@id,'readonly'))]")
    private List<WebElement> passengerFirstNames;

    @FindBy(xpath = "//input[contains(@id,'LastName')and not(contains(@id,'readonly'))]")
    private List<WebElement> passengerLastNames;

    @FindBy(xpath = "//li[contains(@class,'rightHandWrapper')]/input[contains(@type,'checkbox')]")
    private List<WebElement> addTravelInsurances;

    @FindBy(xpath= "//li[strong[contains(text(),'Adult')]]/following-sibling::li/descendant::input[contains(@class,'insurance')]")
    private List<WebElement> adultInsurances;

    @FindBy(xpath= "//li[strong[contains(text(),'Teen')]]/following-sibling::li/descendant::input[contains(@class,'insurance')]")
    private List<WebElement> teenInsurances;

    @FindBy(xpath= "//li[strong[contains(text(),'Child')]]/following-sibling::li/descendant::input[contains(@class,'insurance')]")
    private List<WebElement> childInsurances;

    @FindBy(xpath = "//input[contains(@id,'AviosNumber')and not(contains(@id,'readonly'))]")
    private List<WebElement> aviosNumbers;

    @FindBy(xpath = "//ul[contains(@class,'passConDetailForm ')]/ul/li/strong")
    private List<WebElement> passengerTypes;

    @FindBy(xpath = "//select[contains(@id,'__day')]")
    private List<WebElement> infantBirthDates;

    @FindBy(xpath = "//select[contains(@id,'__month')]")
    private List<WebElement> infantBirthMonths;

    @FindBy(xpath = "//select[contains(@id,'__year')]")
    private List<WebElement> infantBirthYears;

    @FindBy(id = "insuranceSelect")
    private WebElement coverType;

    @FindBy(id = "ticketFlexCheckbox")
    private WebElement flexTicket;

    @FindBy(xpath = "//span[contains(@id,'_bags')][1]")
    private List<WebElement> passengerWithBags;

    @FindBy(xpath = "//div[contains(@class,'0Kg')]/a")
    private List<WebElement> zeroBags;

    @FindBy(xpath = "//div[contains(@class,'15Kg')]/a")
    private List<WebElement> fifteenBags;

    @FindBy(css = "li#baggageSMALL span.baggagePriceLabel")
    private WebElement fifteenBagPrice;

//    @FindBy(xpath = "//li[4]/div[contains(@class,'20Kg')]")
//    private List<WebElement> twentyBags;

//    @FindBy(css = "li#baggageMEDIUM span.baggagePriceLabel")
//    private WebElement twentyBagPrice;

    @FindBy(xpath = "//div[contains(@class,'23Kg')]/a")
    private List<WebElement> twentyThreeBags;

    @FindBy(xpath = "//div[contains(@class,'23Kg')]/a")
    private List<WebElement> twentyThreeBagsATag;

    @FindBy(css = "li#baggageMEDIUM span.baggagePriceLabel")
    private WebElement twentyThreeBagPrice;

    @FindBy(xpath = "//div[contains(@class,'46Kg')]/a")
    private List<WebElement> fortySixBags;

    @FindBy(css = "li#baggageX_LARGE span.baggagePriceLabel")
    private WebElement fortySixBagPrice;

    @FindBy(id = "acceptBagsLink")
    private WebElement acceptBags;

    @FindBy(css = "#acceptBagsLink>span")
    private WebElement acceptBagsStatus;



    @FindBy(id = "acceptSeats")
    private WebElement acceptSeats;

    @FindBy(css = "#acceptSeats>span")
    private WebElement acceptSeatStatus;

    @FindBy(css = "div#AutoCheckinWrapper h1.heading")
    private WebElement aciHeading;

    @FindBy(id = "aciOptionEmail")
    private WebElement checkInByEmail;

    @FindBy(id = "aciOptionMobile")
    private WebElement checkInByMobile;

    @FindBy(id = "aciOptionLater")
    private WebElement checkInLater;

    @FindBy(xpath = "//a[@id='newContinueButton']/span")
    private WebElement continueToPaymentButton;

    @FindBy(xpath = "//a[@id='ContinueButton']/span")
    private WebElement continueToPaymentButtonForChangeSeat;

    @FindBy(css = "ul.returnSeatLegList li[id^=sector] a")
    private List<WebElement> returnFlightListForSeatSelection;

    @FindBy(css = "ul.outboundSeatLegList li[id^=sector] a")
    private List<WebElement> outboundFlightListForSeatSelection;

    @FindBy(css = "div.plane")
    private WebElement seatSelectionPanel;

    @FindBy(xpath = "//td[@class='infant_Seat']")
    private List<WebElement> infantSeats;

    @FindBy(xpath = "//td[@class='bulkhead_Seat']")
    private List<WebElement> extraLegSeats;

    @FindBy(xpath = "//td[@class='standard_Seat']")
    private List<WebElement> standardSeats;

    @FindBy(xpath = "//td[@class='exit_Seat']")
    private List<WebElement> emergencySeats;

    @FindBy(css = "td [class$='selected_Seat']")
    private List<WebElement> selectedSeats;

    @FindBy(id = "fancybox-content")
    private WebElement nagWindow;

    @FindBy(css = "div.nag-head h3")
    private WebElement nagWindowHeader;

    @FindBy(id = "nagButton0")
    private WebElement confirm;

    @FindBy(id = "nagButton1")
    private WebElement reselect;

    @FindBy(css = "li.input input:not([type='hidden'])")
    private List<WebElement> termsAndConditions;

    @FindBy(xpath = "//tr[contains(@id,'passenger_')]/td[1]/li/a")
    private List<WebElement> passengersForSeatSelection;

    @FindBy(css = "p.confD input")
    private List<WebElement> termsAndConditionsForEdit;

    @FindBy(xpath = "//a[span[text()='Booking Details']]")
    private WebElement bookingDetails;

    @FindBy(id = "AutomaticCheckinSms_Gender0")
    private WebElement checkInByMobileGender;

    @FindBy(xpath = "//div[@id='aciMobiles']/select[@class='diallingCodeSelect']")
    private WebElement checkInByMobileDialingCode;

    @FindBy(xpath = "//div[@id='aciMobiles']//input[@class='phoneNumber']")
    private WebElement checkInByMobileMobileNumber;

    @FindBy(xpath = "//select[contains(@id,'passengerTitle0')]")
    private WebElement passengerOneTitle;

    @FindBy(xpath = "//select[contains(@id,'passengerTitle0_readonly')]")
    private WebElement passengerOneTitleReadOnly;

    @FindBy(xpath = "//input[contains(@id,'FirstName0')]")
    private WebElement passengerOneFirstName;

    @FindBy(xpath = "//input[contains(@id,'LastName0')]")
    private WebElement passengerOneLastName;

    @FindBy(xpath = "//div[h2[text()='Outbound']]/following-sibling::div[@class='flight'][1]/descendant::a")
    private WebElement flightBreakDownOutboundFlightOption;

    @FindBy(xpath = "//div[h2[text()='Return']]/following-sibling::div[@class='flight'][1]/descendant::a")
    private WebElement flightBreakDownReturnFlightOption;

    @FindBy(xpath = "//span[@class='column1' and contains(text(),'Operated by')]")
    private WebElement flightBreakDownOperatedBy;

    @FindBy(className = "seatsSectionWrapper")
    private WebElement seatSelectionPaneFront;

    @FindBy(id = "seatRowsMap")
    private WebElement seatSelectionPaneMid;

    @FindBy(className = "seatsSectionWrapper")
    private WebElement seatSelectionPaneBack;

    @FindBy(xpath = "//h1[normalize-space(text())='Choose your seats']")
    private WebElement ChooseYourSeats;

    @FindBy(css = "fieldset .fareRulesPopUp")
    private WebElement fareRulesLink;

    @FindBy(xpath = "//table[@class='tripCostTable camTripCostTableAlign']/descendant::td[contains(text(),'Available seat')]")
    private WebElement standardSeatPrice;

    @FindBy(xpath = "//table[@class='tripCostTable camTripCostTableAlign']/descendant::td[contains(text(),'Extra leg room seat')]")
    private WebElement legSeatPrice;

    @FindBy(xpath = "//table[@class='tripCostTable camTripCostTableAlign']/descendant::td[contains(text(),'Emergency')]")
    private WebElement emergencySeatPrice;

    @FindBy(css = "fieldset .baggageRulesPopUp")
    private WebElement baggageRulesLink;

    @FindBy(linkText = "Baggage Terms & Conditions")
    private WebElement baggageTermsAndConditionsLink;

    @FindBy(id="fancybox-content")
    private WebElement rulesPopup;

    @FindBy(xpath = "//div[@class='contentPanelContent']/div[5]/div[3]/div/span/a")
    private WebElement taxesAndChargesLink;

    @FindBy(xpath = "//div[@class='terms']/a")
    private WebElement termsAndConditionsLinkFlybeFlex;

    @FindBy(xpath = "//div[@id='baggageMessageTextDiv']")
    private WebElement addHoldLuggageMessage;

    @FindBy(css = ".content-page div h2")
    private WebElement termsAndConditionsFlybeFlex;

    @FindBy(css = "#fareRules_ruleDisplay h1")
    private  WebElement fareRulesPopupHeading;

    @FindBy(css = "#baggageRules_ruleDisplay h1")
    private  WebElement baggageRulesPopupHeading;

    @FindBy(css = "#pop h1")
    private  WebElement baggageTermsAndConditions;

    @FindBy(css = "#taxRules_ruleDisplay h1")
    private  WebElement taxesAndChargesPopupHeading;

    @FindBy(xpath = "//div[@id='contactDetailsSMSWrapper']/div/div/h1")
    private WebElement textSMSConfirmationHeader;

    @FindBy(xpath = "//li[@id='smsSection0']/ul/li[2]/input")
    private WebElement phoneNumberTextBox;

    @FindBy(xpath = "//li[@id='0_MEDIUM']/input")
    private WebElement default20KGSelection;

    @FindBy(id = "baggageMessageTextDiv")
    private WebElement baggageMessage;

    @FindBy(css = "#surround_bags b")
    private WebElement getMoreIncreaseBaggageText;

    @FindBy(xpath = "//span[contains(text(),'Increase Allowance')]")
    private WebElement getMoreIncreaseBaggageButton;

    @FindBy(id = "ticketFlexCheckbox")
    private WebElement flexTicketFlexibility;

    @FindBy(xpath = "//div[@class='surround_ASA surroundASA']/div/div[1]/div/h1")
    private WebElement chooseYourSeatsSection;

    @FindBy(xpath = "//div[@id='basket.travelInsurance']/div/span[1]")
    private WebElement basketTravelInsuranceTitle;

    @FindBy(xpath = "//div[@id='basket.travelInsurance']/div/span[2]")
    private WebElement basketTravelInsuranceValue;

    @FindBy(xpath = "//td[text()='Selected seat']")
    private  WebElement selectedSeatsLegends;

    @FindBy(xpath = "//td[text()='Available seat']")
    private  WebElement AvailableSeatsLegends;

    @FindBy(xpath = "//td[text()='Emergency exit']")
    private  WebElement emergencyExitLegend;

    @FindBy(xpath = "//td[text()='Extra leg room seat']")
    private  WebElement extraLegRoomLegend;

    @FindBy(id = "baggageChangeDiv")
    private  WebElement baggageSection;

    @FindBy(xpath = "//li[@id='baggageNONE']/a[normalize-space(text())='No Bag']")
    private WebElement baggageNone;

    @FindBy(xpath = "//li[@id='baggageSMALL']/a[normalize-space(text())='Small']")
    private WebElement baggageSmall;

    @FindBy(xpath = "//li[@id='baggageMEDIUM']/a[normalize-space(text())='Standard']")
    private WebElement standardBaggage;

//    @FindBy(xpath = "//li[@id='baggageLARGE']/a[normalize-space(text())='Large']")
//    private WebElement baggageLarge;

    @FindBy(xpath = "//li[@id='baggageX_LARGE']/descendant::span[normalize-space(text())='2 bags'][span[normalize-space(text())='/ 1 bag + golf bag']]")
    private WebElement baggageGolf;

    @FindBy(xpath = "//li[@id='baggageX_LARGE']/a[normalize-space(text())='2 bags']")
    private WebElement baggageTwoBags;

    @FindBy(id = "surround_ASA")
    private  WebElement seatsSection;

    @FindBy(xpath = "//ul[contains(@class,'headerRow baggageSelectors')]/descendant::span[@class='baggagePriceLabel']")
    private  List<WebElement> baggageCurrencies;

    @FindBy(xpath = "//table[@class='tripCostTable camTripCostTableAlign']/descendant::span[@class='seatCost']")
    private  List<WebElement> seatCurrencies;

    @FindBy(id = "mcpSelector")
    private  WebElement chooseCurrency;

    //----------------avios----------------------

    @FindBy(id = "aviosLogin")
    private WebElement aviosUserName;

    @FindBy(id = "aviosPassword")
    private WebElement aviosPassword;

    @FindBy(css = "a#aviosLoginBtn>span")
    private WebElement aviosLogin;

    @FindBy(id = "aviosPoints")
    private WebElement aviosBalance;

    @FindBy(id = "big")
    private WebElement aviosHighOption;

    @FindBy(id = "mid")
    private WebElement aviosMediumOption;

    @FindBy(id = "low")
    private WebElement aviosLowOption;

    @FindBy(css = "#smallBucket span")
    private WebElement aviosLowPrice;

    @FindBy(css = "#mediumBucket span")
    private WebElement aviosMediumPrice;

    @FindBy(css = "#bigBucket span")
    private WebElement aviosHighPrice;

    @FindBy(css = ".payWithAviosBuckets div span")
    private List<WebElement> aviosCurrency;

    @FindBy(id = "aviosPassword_displayPasswordLink")
    private WebElement displayPassword;

    @FindBy(xpath = "//a[contains(@href, 'https://www.avios.com/gb/en_gb/my-account/forgotten-my-password')]")
    private WebElement forgottenPassword;

    @FindBy(xpath = "//a[contains(@href, 'https://www.avios.com/gb/en_gb/my-account/forgotten-my-username')]")
    private WebElement forgottenUserName;

    @FindBy(xpath ="//a[@id='aviosLoginBtn']/div")
    private WebElement aviosLoginButton;

    @FindBy(id = "loginToolTip")
    private WebElement aviosLoginToolTip;

//----------------avios basket-----------

    @FindBy(id = "basket.aviosPartPay.value")
    private WebElement aviosBasketPrice;


    //----------Footer--------------
    @FindBy(css = "footer.container a[href='/'] img")
    private WebElement flybeLogo;

    @FindBy(css = "footer.container li:nth-child(1)")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    @FindBy(css = "footer.container a[href='/contact']")
    private WebElement ContactUsLink;

    @FindBy(css = "footer.container p a[href='/']")
    private WebElement flybeInFooter;

    @FindBy(css = "a[href='/terms/tariff.htm']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = "footer.container p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = "footer.container p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = "footer.container p:nth-child(3)")
    private WebElement footerPara3;


    @FindBy(css = "footer.container p:nth-child(4)")
    private WebElement footerPara4;



    //    ----basket outbound & inbound total ------
    @FindBy(id = "basket.sectors.outbound.total")
    private WebElement outboundTotal;

    @FindBy(id = "basket.sectors.return.total")
    private WebElement returnTotal;

    //    -----basket  price----------
    @FindBy(id = "basket.runningTotal.value")
    private WebElement overallBasketTotal;

    @FindBy(id = "basket.travelInsurance.value")
    private WebElement basketTravelInsurancePrice;

    @FindBy(id = "basket.passengerBags.value")
    private WebElement basketBaggagePrice;

    @FindBy(id = "basket.ticketFlexibility.value")
    private WebElement basketFlexPrice;

    @FindBy(id = "basket.passengerSeats.value")
    private WebElement basketSeatPrice;

    @FindBy(id = "mcpSelector")
    private WebElement currency;

//    ---------------------basket - itinearary change-------------

    @FindBy(id = "basket.itinChangeFee.value")
    private WebElement changeFeePrice;


//--------------From Manage Booking-----------------------------

    @FindBy(xpath = "//a[@id='continueButton']/span")
    private WebElement continueButtonManage;

    @FindBy(xpath = "//span[text()='Continue']")
    private WebElement continueToPaymentButtonInManageBooking;

    @FindBy(id = "AutomaticCheckinEmail_EmailAddress0")
    private WebElement checkInByEmailid;


    String contactPassengerAddressListPopupLocator ="//div[@id='fancybox-content']";

    String emailCheckInLocator ="//span[text()='Email Check-in']";

    String seatSelector = "//tr[@id='passenger_%d']/td[not(contains(@class,'camAdvanceSeatmapCol'))]";

    public void acceptTermsAndConditionForEditing(){

        // select terms and conditions this method to be called only during amending the flight
        try {
            for (int i = 0; i < termsAndConditionsForEdit.size(); i++) {
                if (termsAndConditionsForEdit.get(i).isDisplayed()) {
                    actionUtility.click(termsAndConditionsForEdit.get(i));
                }
            }
            reportLogger.log(LogStatus.INFO, "Successfully confirmed terms and conditions while doing itenary changes");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to confirm terms and conditions while doing itenary changes");
            throw e;
        }
    }

    @Deprecated
    public Hashtable<String, Hashtable> captureSeatsSelectedForPassengers2() {
        //captures the seat selected for each passenger--- this method not used

        Hashtable<String, Hashtable> seatsSelectedForPassengers = new Hashtable<String, Hashtable>();
        try {
            for (int i = 0; i < passengersForSeatSelection.size(); i++) {
                seatsSelectedForPassengers.put("passenger" + (i + 1), getSeatsForEachFlight(i));
            }
            reportLogger.log(LogStatus.INFO, "successfully captured the seats selected");
            return seatsSelectedForPassengers;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to capture the seats selected");
            throw e;
        }
    }

    @Deprecated
    private Hashtable<String, String> getSeatsForEachFlight2(int index) {
        //returns the seat selected in each flight for a passenger --- this method not used
        Hashtable<String, String> seatsSelected = new Hashtable<String, String>();
        List<WebElement> flightsForSeatSelection = TestManager.getDriver().findElements(By.xpath(String.format(seatSelector, index)));
        for (int i = 0; i < flightsForSeatSelection.size(); i++) {
            String seatNo = flightsForSeatSelection.get(i).getText().trim();
            if (!seatNo.equals("--")) {
                seatsSelected.put("flight" + (i + 1), seatNo);
            }
        }
        return seatsSelected;
    }

    public Hashtable<String,Hashtable<String,Hashtable>> captureSeatsSelectedForPassengers(){

        //captures the seat selected for each passenger

//        Hashtable<String,Hashtable<String,Hashtable>> seatsSelectedForPassengers=new Hashtable<String, Hashtable<String, Hashtable<String, Hashtable>>>();
        Hashtable<String, Hashtable<String, Hashtable>> seatsSelectedForPassengers = new Hashtable<String, Hashtable<String, Hashtable>>();
        try {
            for (int i = 0; i < passengersForSeatSelection.size(); i++) {
                seatsSelectedForPassengers.put("passenger" + (i + 1), getSeatsForEachFlight(i));
            }
            reportLogger.log(LogStatus.INFO, "successfully captured the seats selected");
            return seatsSelectedForPassengers;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to capture the seats selected");
            throw e;
        }
    }

    private Hashtable<String, Hashtable> getSeatsForEachFlight(int index) {
        //returns the seat selected in each flight for a passenger
        Hashtable<String, String> seatsSelectedForOutbound = new Hashtable<String, String>();
        Hashtable<String, String> seatsSelectedForInbound = new Hashtable<String, String>();
        Hashtable<String, Hashtable> seatsSelected = new Hashtable<String, Hashtable>();

        List<WebElement> flightsForSeatSelection = TestManager.getDriver().findElements(By.xpath(String.format(seatSelector, index)));
        int inboundFlightCount = 0;

        for (int i = 0; i < flightsForSeatSelection.size(); i++) {
            String seatNo = flightsForSeatSelection.get(i).getText().trim();

            if (outboundFlightListForSeatSelection.size() > i) {
                seatsSelectedForOutbound.put("flight" + (i + 1), seatNo);

            } else {

                seatsSelectedForInbound.put("flight" + (inboundFlightCount + 1), seatNo);

                inboundFlightCount++;
            }
        }
        seatsSelected.put("outbound", seatsSelectedForOutbound);
        seatsSelected.put("inbound", seatsSelectedForInbound);

        return seatsSelected;
    }

    public void continueToPayment() {
        //clicks on continue button to navigate to the payment page
        try {
            actionUtility.waitForElementClickable(continueToPaymentButton,25);
            actionUtility.clickByAction(continueToPaymentButton);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(15);
            reportLogger.log(LogStatus.INFO, "successfully continued to payment");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to payment");
            throw e;
        }
    }

    public void continueToPaymentForChangeSeat() {
        //clicks on continue button to navigate to the payment page
        try {
            actionUtility.waitForElementClickable(continueToPaymentButtonForChangeSeat,25);
            actionUtility.clickByAction(continueToPaymentButtonForChangeSeat);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(15);
            reportLogger.log(LogStatus.INFO, "successfully continued to payment");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to payment");
            throw e;
        }
    }

    public void handleNagWindow(String action) {
        //handles the Nag window that is displayed after clicking continue button
        try {
            if (nagWindow.isDisplayed()) {
                if (nagWindowHeader.getText().toLowerCase().contains("not selected")) {
                    if (action.toLowerCase().equals("confirm")) {
                        actionUtility.hardClick(confirm);
                    } else if (action.toLowerCase().equals("reselect")) {
                        actionUtility.hardClick(reselect);
                    }
                } else if (nagWindowHeader.getText().toLowerCase().contains("have booked seats and bags")) {
                    if (action.toLowerCase().equals("confirm")) {
                        actionUtility.hardClick(confirm);
                    } else if (action.toLowerCase().equals("reselect")) {
                        actionUtility.hardClick(reselect);
                    }


                } else if (nagWindowHeader.getText().toLowerCase().contains("special offer")) {
                    if (action.toLowerCase().equals("confirm")) {
                        actionUtility.hardClick(confirm);
                    } else if (action.toLowerCase().equals("reselect")) {
                        actionUtility.hardClick(reselect);
                    }
                }
            }

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "Successfully selected the nagWindow option");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the option in NagWindow");
            throw e;
        }
    }

    public void populateTelephoneNoManuallyWhenLoggedIn(Hashtable<String, String> testData) {
        //if telephone is not populated will be handle from here
        try {
            actionUtility.waitForElementClickable(bookingDetails, 25);
            actionUtility.hardClick(bookingDetails);
            actionUtility.hardSleep(2000);
            enterPassengerContactNumbersEmails(testData);
            reportLogger.log(LogStatus.INFO, "contact nos and email are populated");
        } catch (NoSuchElementException e) {

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to populate contact nos and emails");
            throw e;

        }
    }

    public void acceptBags() {
        //click on accept bags button
        try {
            actionUtility.click(acceptBags);
            reportLogger.log(LogStatus.INFO, "successfully accepted bags");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to accept the bags");
            throw e;
        }
    }

    public void acceptSeats() {
        //click on accept seats button
        try {
            actionUtility.click(acceptSeats);
            reportLogger.log(LogStatus.INFO, "successfully accepted seats");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to accept the seats");
            throw e;
        }
    }

    public void selectCheckInOption(Hashtable<String,String> testData,Hashtable<String,String> passengerNames){

        //selects the check-in option

        try {
            if (testData.get("checkInOption").toLowerCase().contains("email")) {
                actionUtility.click(checkInByEmail);
                if(checkInByEmailid.isDisplayed()){
                    actionUtility.sendKeys(checkInByEmailid,testData.get("checkInOptionEmailid"));
                }

            } else if (testData.get("checkInOption").toLowerCase().contains("mobile")) {
                actionUtility.click(checkInByMobile);
                List<String> passengerTitles = new ArrayList<String>(Arrays.asList(new String[]{"Rev.", "Dr.", "Prof."}));
                String title = passengerNames.get("passengerName1").split(" ")[0];

                if (passengerTitles.contains(title)) {
                    actionUtility.waitForElementVisible(checkInByMobileGender, 5);
                    actionUtility.dropdownSelect(checkInByMobileGender, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("checkInGender"));
                }
                actionUtility.waitForElementVisible(checkInByMobileDialingCode, 5);
                actionUtility.dropdownSelect(checkInByMobileDialingCode, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("checkInByMobileDialingCode"));
                checkInByMobileMobileNumber.sendKeys(testData.get("checkInByMobileMobileNumber"));

            } else if (testData.get("checkInOption").toLowerCase().contains("no thanks")) {
                actionUtility.click(checkInLater);
            }
            reportLogger.log(LogStatus.INFO, "successfully selected the check-in option");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the check-in option");
            throw e;
        }
    }


    public void selectFlybeAccountOption(boolean option) {
        //Selects option to Do you have a Flybe account?
        try {
            actionUtility.waitForElementNotPresent(emailCheckInLocator, 20);
            if (option) {
                actionUtility.waitForElementClickable(yesFlyBeAccount,15);
                actionUtility.selectOption(yesFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - Yes, I have a Flybe account");

            } else {
                actionUtility.waitForElementClickable(noFlyBeAccount,15);
                actionUtility.selectOption(noFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - No, I do not have a Flybe account");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING,"unable to select any option in - Do you have a Flybe account?");
            throw e;
        }
    }

    public void enterPassengerContact(Hashtable<String, String> testData) {
        //enter the passenger names and countries
        try {
            String[] contactPassengerName = testData.get("contactPassengerName").split(" ");
            actionUtility.dropdownSelect(contactPassengerSalutation, ActionUtility.SelectionType.SELECTBYTEXT, contactPassengerName[0]);
            actionUtility.sendKeys(contactPassengerFirstName,contactPassengerName[1]);
            actionUtility.sendKeys(contactPassengerLastName,contactPassengerName[2]);

            if (testData.get("contactPassengerAviosNo") != null) {
                actionUtility.sendKeys(contactPassengerAviosNo,testData.get("contactPassengerAviosNo"));
            }
            actionUtility.dropdownSelect(contactPassengerCountry, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("contactPassengerCountry"));

            if (testData.get("contactPassengerPostCode") != null) {
                actionUtility.sendKeys(contactPassengerPostCode,testData.get("contactPassengerPostCode"));
            }
            reportLogger.log(LogStatus.INFO, "successfully entered passenger contacts");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter passenger contacts");
            throw e;
        }
    }

    public void enterPassengerContactAddressManually(Hashtable<String, String> testData) {
        // enters the passenger the contact adress manually
        try {
            if (contactPassengerDontHavePostcode.isDisplayed()) {
                actionUtility.selectOption(contactPassengerDontHavePostcode);
            }
            contactPassengerAddressOne.sendKeys(testData.get("contactPassengerAddressOne"));

            if (testData.get("contactPassengerAddressTwo") != null) {
                actionUtility.sendKeys(contactPassengerAddressTwo,testData.get("contactPassengerAddressTwo"));
            }

            if (testData.get("contactPassengerAddressThree") != null) {
                actionUtility.sendKeys(contactPassengerAddressThree,testData.get("contactPassengerAddressThree"));
            }

            actionUtility.sendKeys(contactPassengerTown,testData.get("contactPassengerTown"));
            actionUtility.sendKeys(contactPassengerCounty,testData.get("contactPassengerCounty"));
            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact address manually");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact address manually");
            throw e;
        }
    }

    public void enterPassengerContactNumbersEmails(Hashtable<String, String> testData) {
        // entering the passenger contact number and emails
        try {

            if (testData.get("contactPassengerLandCountryCode") != null) {
                actionUtility.dropdownSelect(contactPassengerLandCountryCode, ActionUtility.SelectionType.SELECTBYVALUE, testData.get("contactPassengerLandCountryCode"));
            }
            if (testData.get("contactPassengerLandLineNo") != null) {
                contactPassengerLandLineNo.clear();
                contactPassengerLandLineNo.sendKeys(testData.get("contactPassengerLandLineNo"));
            }

            if (testData.get("contactPassengerMobileCountryCode") != null) {
                actionUtility.dropdownSelect(contactPassengerMobileCountryCode, ActionUtility.SelectionType.SELECTBYVALUE, testData.get("contactPassengerMobileCountryCode"));
            }
            if (testData.get("contactPassengerMobileNo") != null) {
                actionUtility.sendKeys(contactPassengerMobileNo,testData.get("contactPassengerMobileNo"));
            }
            try {
                actionUtility.waitForElementVisible(contactPassengerEmail, 2);
                actionUtility.sendKeys(contactPassengerEmail,testData.get("contactPassengerEmail"));

                if (testData.get("contactPassengerNewPassword") != null) {
                    actionUtility.waitForElementVisible(contactPassengerNewPassword, 2);
                    actionUtility.sendKeys(contactPassengerNewPassword,"contactPassengerNewPassword");
                }
            } catch (TimeoutException e) {

            }
            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact number and email");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact number and email");
            throw e;
        }
    }

    public void selectPassengerContactAddress(Hashtable<String, String> testData) {
        //selects the passenger contact address from the list
        try {
            actionUtility.click(contactPassengerFindAddress);
            actionUtility.waitForElementVisible(contactPassengerAddressListPopup, 50);
            for (int i = 0; i < contactPassengerAddresses.size(); i++) {

                if (contactPassengerAddresses.get(i).getText().toLowerCase().equals(testData.get("contactPassengerAddresses").toLowerCase())) {
                    actionUtility.selectOption(contactPassengerAddressSelectors.get(i));
                    break;
                }
            }
            actionUtility.waitForElementClickable(contactPassengerAddressContinue, 15);
            actionUtility.hardClick(contactPassengerAddressContinue);
            try {
                actionUtility.hardSleep(2000);
                actionUtility.click(contactPassengerAddressContinue);
            } catch (TimeoutException e) {
            }
            actionUtility.waitForElementNotPresent(contactPassengerAddressListPopupLocator, 10);
            reportLogger.log(LogStatus.INFO, "successfully selected the passenger contact address from the list");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the passenger contact address from the list");
            throw e;
        }
    }

    public void retrievePassengerContactDetailsByLogin(Hashtable<String, String> testData) {
        // retrieves the passenger contact details by logging in
        String contactEmail = null, contactPassword = null;
        try {
            actionUtility.hardSleep(3000);

            contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
            contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            actionUtility.waitForElementVisible(contactPassengerEmail,3);
            contactPassengerEmail.clear();
            contactPassengerEmail.sendKeys(contactEmail);
            actionUtility.hardSleep(2000);
            actionUtility.waitForElementVisible(contactPassengerAuthenticationPassword,3);
            actionUtility.sendKeys(contactPassengerAuthenticationPassword,contactPassword);

            actionUtility.click(contactPassengerLogin);
            int attempt = 0;
            while(actionUtility.verifyIfElementIsDisplayed(contactPassengerLogin)&&(attempt<3)) {
                try {
                    actionUtility.click(contactPassengerLogin);
                    actionUtility.hardSleep(2000);
                } catch (TimeoutException e) {
                } catch (ElementNotVisibleException e) {
                }
                attempt++;
            }
            if((attempt == 3)&&(actionUtility.verifyIfElementIsDisplayed(contactPassengerLogin))){
                throw new RuntimeException("unable to retrieve the passenger contacts by logging in with the email id - "+contactEmail);
            }
            reportLogger.log(LogStatus.INFO, "successfully retrieved the passenger contacts by logging in with the email id - "+contactEmail);
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the passenger contacts by logging in with the email id - "+contactEmail);
            throw e;
        }
    }




    public void enterPassengerDetails(Hashtable<String, String> testData) {
        // depending on the no. of passenger,calls a method to populate the passenger details
        try {
            for (int i = 0; i < passengerTypes.size(); i++) {
                if (i == 0) {

                    if (testData.get("iAmPassenger").toLowerCase().equals("true")) {
                        actionUtility.selectOption(iAmPassenger);
                        reportLogger.log(LogStatus.INFO,"successfully selected I am passenger");
                        if(testData.get("adultAddTravelInsurance1")!= null){
                            actionUtility.selectOption(addTravelInsurances.get(0));
                        }else if(testData.get("teenAddTravelInsurance1")!= null){
                            actionUtility.selectOption(addTravelInsurances.get(0));
                        }
                    } else {
                        populatePassengerDetails(testData, i);
                    }
                } else {
                    populatePassengerDetails(testData, i);
                }
            }
            if (testData.get("coverType") != null) {
                actionUtility.dropdownSelect(coverType, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("coverType"));
            }
            reportLogger.log(LogStatus.INFO, "successfully populated the passenger details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to populate the passenger details");
            throw e;
        }

    }

    private void enterPassengerName(String[] passengerName, int index) {
        //enters the passenger name
        actionUtility.dropdownSelect(passengerTitles.get(index), ActionUtility.SelectionType.SELECTBYTEXT, passengerName[0]);
        passengerFirstNames.get(index).sendKeys(passengerName[1]);
        passengerLastNames.get(index).sendKeys(passengerName[2]);
    }

    private void populatePassengerDetails(Hashtable<String, String> testData, int index) {
        //depending the type of the passenger, the passenger details are populated
        String[] passengerType = passengerTypes.get(index).getText().split(" ");
        String[] passengerName;

        if (passengerType[0].contains("Adult")) {
            passengerName = testData.get("adultName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);
            if (testData.get("adultAviosNumber" + passengerType[1]) != null) {
                aviosNumbers.get(index).sendKeys(testData.get("adultAviosNumber" + passengerType[1]));
            }
            if (testData.get("adultAddTravelInsurance" + passengerType[1]) != null) {
                actionUtility.selectOption(addTravelInsurances.get(index));
            }
        }

        if (passengerType[0].contains("Teen")) {
            passengerName = testData.get("teenName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);
            if (testData.get("teentAviosNumber" + passengerType[1]) != null) {
                aviosNumbers.get(index).sendKeys(testData.get("teentAviosNumber" + passengerType[1]));
            }
            if (testData.get("teenAddTravelInsurance" + passengerType[1]) != null) {
                actionUtility.selectOption(addTravelInsurances.get(index));
            }
        }

        if (passengerType[0].contains("Child")) {
            passengerName = testData.get("childName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);
            if (testData.get("childAddTravelInsurance" + passengerType[1]) != null) {
                actionUtility.selectOption(addTravelInsurances.get(index));
            }
        }

        if (passengerType[0].contains("Infant")) {
            passengerName = testData.get("infantName" + passengerType[1]).split(" ");
            enterPassengerName(passengerName, index);
            String[] infantDob = testData.get("infantDob" + passengerType[1]).split("/");
            WebElement birthDate = infantBirthDates.get(Integer.parseInt(passengerType[1]) - 1);
            WebElement birthMonth = infantBirthMonths.get(Integer.parseInt(passengerType[1]) - 1);
            WebElement birthYear = infantBirthYears.get(Integer.parseInt(passengerType[1]) - 1);
            actionUtility.dropdownSelect(birthDate, ActionUtility.SelectionType.SELECTBYTEXT, infantDob[0]);
            actionUtility.dropdownSelect(birthMonth, ActionUtility.SelectionType.SELECTBYTEXT, infantDob[1]);
            actionUtility.dropdownSelect(birthYear, ActionUtility.SelectionType.SELECTBYTEXT, infantDob[2]);
        }
    }

    private void verifyInsurancePriceAddedToBasket(Hashtable<String,String> testData) {
        //calculates the insurance price and validates it in basket
        double insurancePriceInBasket = 0.0;
        double expectedInsurancePrice = -1.0;
        try {

            expectedInsurancePrice = calculateInsurancePriceForAllPassenger(testData);

            if (expectedInsurancePrice > 0.0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketTravelInsurancePrice));
                    reportLogger.log(LogStatus.INFO, "Insurance price is added in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price is not added in the basket");
                    throw e;
                }
                insurancePriceInBasket = CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText());
                try {
                    Assert.assertEquals(insurancePriceInBasket, expectedInsurancePrice);
                    reportLogger.log(LogStatus.INFO, "Insurance price in the basket is matching");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price in the basket is not matching Expected - " + expectedInsurancePrice + " Actual - " + insurancePriceInBasket);
                    throw e;
                }
            } else {
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketTravelInsurancePrice));
                    reportLogger.log(LogStatus.INFO, "Insurance price is not added in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price is added in the basket");
                    throw e;
                }
            }

        }catch (AssertionError e){
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the insurance price in the basket");
            throw e;
        }
    }

    private double calculateInsurancePriceForAllPassenger(Hashtable<String,String> testData){
        //calculates the insurance price for all selected passengers
        int totalPassengerWithInsurance=0;
        double insuranceCoveredValue =0.0;
        double totalInsurancePriceForAllPassenger=0.0;


        if(testData.get("noOfAdult")!=null){
            if (!testData.get("noOfAdult").equals("0")){

                for(int i=0; i<Integer.parseInt(testData.get("noOfAdult")); i++){
                    if(adultInsurances.get(i).isSelected())
                        totalPassengerWithInsurance++;
                }
            }
        }else {
            if(adultInsurances.get(0).isSelected()) {
                totalPassengerWithInsurance++;
            }
        }

        if(testData.get("noOfTeen")!=null){
            for(int i=0; i<Integer.parseInt(testData.get("noOfTeen")); i++){
                if(teenInsurances.get(i).isSelected())
                    totalPassengerWithInsurance++;
            }
        }

        if(testData.get("noOfChild")!=null){
            for(int i=0; i<Integer.parseInt(testData.get("noOfTeen")); i++){
                if(childInsurances.get(i).isSelected())
                    totalPassengerWithInsurance++;
            }
        }

        insuranceCoveredValue =  (CommonUtility.convertStringToDouble(new Select(coverType).getFirstSelectedOption().getText()));
        totalInsurancePriceForAllPassenger = insuranceCoveredValue*totalPassengerWithInsurance;
        return  totalInsurancePriceForAllPassenger;
    }

    public void selectPassengerBaggage(Hashtable<String,String> testData,String...editFlag){
        // depending on the on. of passenger, calls the method to select the baggage
        try {
            String baggageToBeSelected;


            for (int i = 0; i < passengerWithBags.size(); i++) {
                baggageToBeSelected = checkBaggageType(testData, i, editFlag.length);
                if (baggageToBeSelected != null) {
                    selectBag(baggageToBeSelected, i);
                    reportLogger.log(LogStatus.INFO,"successfully added baggage "+baggageToBeSelected+" for passenger"+(i+1));
                }
            }

//            reportLogger.log(LogStatus.INFO, "successfully selected the passenger baggage");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the passenger bagage");
            throw e;
        }
    }

    private String checkBaggageType(Hashtable<String, String> testData, int index, int editFlag) {
        // checks the baggage type
        String baggageType=null;


        baggageType = testData.get("passengerBag" + (index + 1));
        if (editFlag != 0) {
            baggageType = testData.get("editedPassengerBag" + (index + 1));
        }
        return baggageType;
    }

    private void selectBag(String baggageToBeSelected, int index) {
        //depending on the type of the bagage, the respective baggage is selected
        if (baggageToBeSelected.toLowerCase().equals("0kg")) {
            actionUtility.click(zeroBags.get(index));
        }
        if (baggageToBeSelected.toLowerCase().equals("15kg")) {
            actionUtility.click(fifteenBags.get(index));
        }
//        if (baggageToBeSelected.toLowerCase().equals("20kg")) {
//            actionUtility.clickByAction(twentyBags.get(index));
//        }
        if (baggageToBeSelected.toLowerCase().equals("23kg")) {
            actionUtility.click(twentyThreeBags.get(index));
        }
        if (baggageToBeSelected.toLowerCase().equals("46kg")) {
            actionUtility.click(fortySixBags.get(index));
        }
    }

    private void verifyBaggagePriceInBasket(Hashtable<String,String> testData, int noOfFlights, int... editFlag){
        //validates the baggage price in the basket

        double expectedBaggagePrice = 0.0;
        double actualBaggagePrice = 1.0;
        boolean conditionFlag = false;
        try {
            conditionFlag = acceptBagsStatus.getText().toLowerCase().equals("bags added to booking");
        }catch (NoSuchElementException e){}


        if (editFlag.length>0||conditionFlag) {
            try {
                expectedBaggagePrice = captureSelectedBaggagePrice(testData, editFlag.length);
                expectedBaggagePrice = expectedBaggagePrice * noOfFlights;
                expectedBaggagePrice = Math.round(expectedBaggagePrice * 100.0) / 100.0;

                if (expectedBaggagePrice!=0) {
                    try {
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price is not displayed in the basket");
                        reportLogger.log(LogStatus.INFO, "baggage price is displayed in the basket");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "baggage price is not displayed in the basket");
                        throw e;
                    }
                    actualBaggagePrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    actualBaggagePrice = Math.round(actualBaggagePrice * 100.0) / 100.0;
                    try {
                        Assert.assertEquals(actualBaggagePrice, expectedBaggagePrice, "baggage price in basket is not matching");
                        reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching");
                    }catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING,"baggage price in the basket is not matching Expected - "+expectedBaggagePrice+" Actual - "+actualBaggagePrice);
                        throw e;
                    }

                }else {
                    try {
                        Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price in basket is added");
                        reportLogger.log(LogStatus.INFO, "baggage price in the basket is not added");
                    }catch (AssertionError e){
                        reportLogger.log(LogStatus.WARNING, "baggage price in the basket is added");
                        throw e;
                    }catch (Exception e){
                        reportLogger.log(LogStatus.WARNING, "unable to verify the baggage price in the basket");
                        throw e;
                    }
                }
            }catch (AssertionError e){
                throw e;
            }catch(Exception e){
                reportLogger.log(LogStatus.WARNING,"unable to verify the baggage price in the basket");
                throw e;
            }

        }else{
            try {
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price in basket is added");
                reportLogger.log(LogStatus.INFO, "baggage price in the basket is not added");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "baggage price in the basket is added");
                throw e;
            }catch (Exception e){
                reportLogger.log(LogStatus.WARNING, "unable to verify the baggage price in the basket");
                throw e;
            }
        }
    }

    private double captureSelectedBaggagePrice(Hashtable<String,String> testData,int editFlag){
        //returns total baggage price
        String baggageToBeSelected;
        double baggagePrice = 0.0;

        for (int i = 0; i < passengerWithBags.size(); i++) {
            baggageToBeSelected = checkBaggageType(testData, i,editFlag);

            if (baggageToBeSelected != null) {
                baggagePrice += getBagPrice(baggageToBeSelected);
            }

        }
//        baggagePrice = (Math.round(baggagePrice)*100.0)/100.0;
        return baggagePrice;

    }

    private double getBagPrice(String baggageSelected){
        // returns  baggage price for the type of the baggage

        double baggagePrice = 0.0;

        if (baggageSelected.toLowerCase().equals("15kg")){
            baggagePrice = CommonUtility.convertStringToDouble(fifteenBagPrice.getText());
        }
//        if (baggageSelected.toLowerCase().equals("20kg")){
//            baggagePrice = CommonUtility.convertStringToDouble(twentyBagPrice.getText());
//        }
        if (baggageSelected.toLowerCase().equals("23kg")){
            baggagePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getText());
        }
        if (baggageSelected.toLowerCase().equals("46kg")){
            baggagePrice = CommonUtility.convertStringToDouble(fortySixBagPrice.getText());
        }
        return baggagePrice;
    }

    public void selectSeats(Hashtable<String,String> testData, String tripType, String... editFlag){
        // depending on the trip type calls the method to select the seats
        try {

            if (seatSelectionPanel.isDisplayed()) {
                switch (tripType.toLowerCase()) {
                    case ("inbound"):
                        selectRouteAndSelectSeats(testData, returnFlightListForSeatSelection, "Inbound", editFlag.length);
                        break;
                    case ("outbound"):
                        selectRouteAndSelectSeats(testData, outboundFlightListForSeatSelection, "Outbound", editFlag.length);
                        break;
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the seats");
            } else {
                reportLogger.log(LogStatus.INFO, "seat selection is not allowed for the selected flights");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the seats");
            throw e;
        }
    }

    private void selectRouteAndSelectSeats(Hashtable<String, String> testData, List<WebElement> routes, String tripType, int editFlag) {
        //selects the Route and then checks availability of seat selection and calls method to select seat
        for (int i = 0; i < routes.size(); i++) {
            actionUtility.hardClick(routes.get(i));
            if (!actionUtility.checkAlertAndPerform("accept")) {

                if(selectedSeats.size()==0) {
                    selectSeatsForRoute(testData, tripType);
                }
                if(editFlag!=0){
                    selectSeatsForRoute(testData, tripType);

                }
            }
        }
    }

    private void selectSeatsForRoute(Hashtable<String,String> testData,String tripType){
        //select the seat depending on the type
        String[] seats = testData.get("seatsFor" + tripType + "Flight").split(",");
        for (int i = 0; i < seats.length; i++) {
            if (seats[i].toLowerCase().contains("emergency")) {
                actionUtility.click(emergencySeats.get(0));
                actionUtility.checkAlertAndPerform("accept");

            } else if (seats[i].toLowerCase().contains("infant")) {
                actionUtility.click(infantSeats.get(0));

            } else if (seats[i].toLowerCase().contains("standard")) {
                actionUtility.hardClick(standardSeats.get(0));

            } else if (seats[i].toLowerCase().contains("leg")) {
                actionUtility.click(extraLegSeats.get(0));
            }
        }
    }

    public Hashtable verifyBasket(Hashtable<String,String> testData,int noOfPassengers,int noOfOutboundFlights, int noOfInboundFlights, boolean... editFlag){
        //verifies the basket
        try {
            int noOfFlights = noOfOutboundFlights;
            if (noOfInboundFlights > 0) {
                verifyFlexPriceAddedToBasket(testData, noOfPassengers, noOfOutboundFlights, noOfInboundFlights);
                noOfFlights = noOfOutboundFlights + noOfInboundFlights;
            } else {
                verifyFlexPriceAddedToBasket(testData, noOfPassengers, noOfOutboundFlights);
            }
            if (editFlag.length > 0){
                verifyBaggagePriceInBasket(testData,noOfFlights,editFlag.length);
            }else {
                verifyBaggagePriceInBasket(testData,noOfFlights);
            }

            verifySeatPriceInBasket(testData);
            verifyInsurancePriceAddedToBasket(testData);
            verifyAviosPriceAddedToBasket();
            Hashtable <String,Double> basketDetails =verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();

            return basketDetails;
        }catch (AssertionError e){
            throw e;
        }catch(Exception e){
            throw e;
        }

    }


    private void verifyAviosPriceAddedToBasket(){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice();

            if (expectedPrice > 0) {

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                    throw e;
                }

                actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

                try {
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }

            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is not added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is not added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is added to basket");
                    throw e;
            }
            }
        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }

    private double calculateAviosPrice() {
        // to verify the avios price according to selection
        double maxAviosPrice = -1.0;
        double aviosPriceApplied = 0.0;
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        if (aviosLowOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
        } else if (aviosMediumOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
        } else if (aviosHighOption.isSelected()) {
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
        }
        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
        if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
            inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;


        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }


    private Hashtable<String, Double> verifyTotalBasketPriceWithSummationOfOtherElementsInBasket(){
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double sum =0.0;
        double overallBasketPrice =1.0;
        double aviosPriceApplied = 0.0;
        double maxAviosPrice = 0.0;

        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();


        try{
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);



            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
                basketDetails.put("ticketFlexibilityPrice",ticketFlexibilityPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
                basketDetails.put("basketBaggagePrice",bagPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
                basketDetails.put("seatsPrice",seatsPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketTravelInsurancePrice)){
                travelInsurancePrice = Math.round(CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText())*100.0)/100.0;
                basketDetails.put("travelInsurancePrice",travelInsurancePrice);
            }
            if(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice)){
                aviosPriceApplied = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText())*100.0)/100.0;
                basketDetails.put("aviosPriceApplied",aviosPriceApplied);
                if (aviosLowOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
                } else if (aviosMediumOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
                } else if (aviosHighOption.isSelected()) {
                    maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
                }
                basketDetails.put("maxAviosPrice",maxAviosPrice);
            }
            sum = Math.round((outboundPrice+inboundPrice+ticketFlexibilityPrice+bagPrice+seatsPrice+travelInsurancePrice-aviosPriceApplied)*100.0)/100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            basketDetails.put("overallBasketPrice",overallBasketPrice);
            Assert.assertEquals(overallBasketPrice,sum,"total basket price in the passenger details page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO,"total basket price in the passenger details page is matching with summation of all the element prices in the basket");
            return basketDetails;
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"total basket price in the passenger details page is not matching with summation of all the element prices in the basket Expected - "+sum+" Actual -"+overallBasketPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify totals prices in the basket in passenger details page");
            throw e;
        }
    }

    public void verifyBasketPriceWithBasketPriceOfFareSelectionPage(Hashtable<String,Double> previousBasketDetails){
        //verifies if the total basket price in the your details page is same as previous page
        double expectedPrice =0.0;
        double actualPrice = -1.0;
        try {
            try {
                expectedPrice = previousBasketDetails.get("totalPrice");
                actionUtility.waitForElementVisible(overallBasketTotal,10);
//                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                actualPrice =  Math.round(CommonUtility.convertStringToDouble(overallBasketTotal.getText())*100.0)/100.0;
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in passenger details page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in passenger details page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in passenger details page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in passenger details page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in passenger details page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in passenger details page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in passenger details page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify outbound, return and total price in the basket, in the passenger details page against outbound, return and total price of fare selection page");
            throw e;
        }




    }

    private void verifySeatPriceInBasket(Hashtable<String,String> testData){
        // verifies the seat price in basket
        double expectedSeatPrice =0.0;
        double actualSeatPrice =1.0;
        try{
            expectedSeatPrice = getOverallSeatPrice(testData);
            actualSeatPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());
            if (expectedSeatPrice!=0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketSeatPrice),"seat price in the basket is not added");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is added");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is not added");
                    throw e;
                }catch(Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is added");
                    throw e;
                }

                try {
                    Assert.assertEquals(actualSeatPrice,expectedSeatPrice,"seat price in the basket is not matching");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is matching");


                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is not matching Expected - "+expectedSeatPrice+" Actual -"+actualSeatPrice);
                    throw e;
                }catch (Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is matching");
                    throw e;
                }
            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketSeatPrice),"seat price in the basket is added");
                    reportLogger.log(LogStatus.INFO,"seat price in the basket is not added");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"seat price in the basket is added");
                    throw e;
                }catch(Exception e){
                    reportLogger.log(LogStatus.WARNING,"unable to verify if seat price in the basket is added");
                    throw e;
                }
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            throw e;
        }
    }

    private double getOverallSeatPrice(Hashtable<String,String> testData){
        // depending on the trip type calls the method to get the seat price

        double overallSeatPrice=0.0;

        if (acceptSeatStatus.getText().toLowerCase().equals("seats added to booking")) {
            overallSeatPrice += Math.round(getSeatPriceForEachRoute(testData, outboundFlightListForSeatSelection, "Outbound") * 100.0) / 100.0;
            if (testData.get("seatsForInboundFlight") != null) {
                overallSeatPrice += Math.round(getSeatPriceForEachRoute(testData, returnFlightListForSeatSelection, "Inbound") * 100.0) / 100.0;
            }
        }
        overallSeatPrice = Math.round(overallSeatPrice*100.0)/100.0;
        return overallSeatPrice;

    }

    private double getSeatPriceForEachRoute(Hashtable<String,String> testData, List<WebElement> routes, String tripType) {
        //selects the Route and then checks availability of seat selection and captures the seat price
        double seatPriceForTrip = 0.0;
        for (int i = 0; i < routes.size(); i++) {
            actionUtility.hardClick(routes.get(i));
            if (!actionUtility.checkAlertAndPerform("accept")) {
                seatPriceForTrip += getSeatPriceForEachFlight(testData, tripType);
            }
        }
        seatPriceForTrip = Math.round(seatPriceForTrip*100.0)/100.0;
        return seatPriceForTrip;
    }

    private double getSeatPriceForEachFlight(Hashtable<String,String> testData, String tripType){
        //select the seat depending on the type

        double seatPriceForFlight = 0.0;

        String[] seats = testData.get("seatsFor"+tripType+"Flight").split(",");

        tripType = tripType.toLowerCase();
        for (int i=0; i<seats.length; i++){

            if (seats[i].toLowerCase().contains("emergency")){

                if(testData.get(tripType+"FlightOption").toLowerCase().equals("just fly")){
                    if(legSeatPrice.getText().toLowerCase().equals("incl")){
                        reportLogger.log(LogStatus.WARNING,"Just fly includes emergency exit seat price");
                        throw new RuntimeException("Just fly includes emergency exit seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("get more")) {
                    if (legSeatPrice.getText().toLowerCase().equals("incl")) {
                        reportLogger.log(LogStatus.WARNING,"Get more does not include emergency exit seat price");
                        throw new RuntimeException("Get more does not include emergency exit seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("all in")){
                    if (!legSeatPrice.getText().toLowerCase().equals("incl")) {
                        reportLogger.log(LogStatus.WARNING,"All in does not include emergency exit seat price");
                        throw new RuntimeException("All in does not include emergency exit seat price");
                    }
                }
                seatPriceForFlight += CommonUtility.convertStringToDouble(legSeatPrice.getText());

            }else if(seats[i].toLowerCase().contains("infant")){
                if(testData.get(tripType+"FlightOption").toLowerCase().equals("just fly")){
                    if(standardSeatPrice.getText().toLowerCase().contains("incl")){
                        reportLogger.log(LogStatus.WARNING,"Just fly includes infant seat price");
                        throw new RuntimeException("Just fly includes infant seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("get more")) {
                    if (!standardSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"Get more does not include infant seat price");
                        throw new RuntimeException("Get more does not include infant seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("all in")){
                    if (!standardSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"All in does not include infant seat price");
                        throw new RuntimeException("All in does not include infant seat price");
                    }
                }
                seatPriceForFlight += CommonUtility.convertStringToDouble(standardSeatPrice.getText());

            }else if(seats[i].toLowerCase().contains("standard")){
                if(testData.get(tripType+"FlightOption").toLowerCase().equals("just fly")){
                    if(standardSeatPrice.getText().toLowerCase().contains("incl")){
                        reportLogger.log(LogStatus.WARNING,"Just fly includes standard seat price");
                        throw new RuntimeException("Just fly includes standard seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("get more")) {
                    if (!standardSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"Get more does not include standard seat price");
                        throw new RuntimeException("Get more does not include standard seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("all in")){
                    if (!standardSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"All in does not include standard seat price");
                        throw new RuntimeException("All in does not include standard seat price");
                    }
                }
                seatPriceForFlight += CommonUtility.convertStringToDouble(standardSeatPrice.getText());

            }else if(seats[i].toLowerCase().contains("leg")){

                if(testData.get(tripType+"FlightOption").toLowerCase().equals("just fly")){
                    if(legSeatPrice.getText().toLowerCase().contains("incl")){
                        reportLogger.log(LogStatus.WARNING,"Just fly includes extra leg room seat price");
                        throw new RuntimeException("Just fly includes extra leg room seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("get more")) {
                    if (legSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"Get more does not include extra leg room seat price");
                        throw new RuntimeException("Get more does not include extra leg room seat price");
                    }
                }else if(testData.get(tripType+"FlightOption").toLowerCase().equals("all in")){
                    if (!legSeatPrice.getText().toLowerCase().contains("incl")) {
                        reportLogger.log(LogStatus.WARNING,"All in does not include extra leg room seat price");
                        throw new RuntimeException("All in does not include extra leg room seat price");
                    }
                }

                seatPriceForFlight += CommonUtility.convertStringToDouble(legSeatPrice.getText());
            }
        }
        return seatPriceForFlight;
    }

    public void acceptTermsAndCondition(){
        //accepts the terms and conditions
        try {

            if (termsAndConditions.size() != 0) {

                for (WebElement condition : termsAndConditions) {
                    actionUtility.selectOption(condition);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the terms and conditions");
            } else {
                reportLogger.log(LogStatus.WARNING, "terms and conditions are not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the terms and conditions");
            throw e;
        }
    }

    public Hashtable<String, String> capturePassengerNames(Hashtable<String, String> testData) {
        // return the name for the passengerNumber
        try {
            Hashtable<String, Integer> passengerCount = getPassengerCountForEachType(testData);
            int adultCount = passengerCount.get("adultCount");
            int infantCount = passengerCount.get("infantCount");
            int teenCount = passengerCount.get("teenCount");
            int childCount = passengerCount.get("childCount");
            Hashtable<String, String> passengerNames = new Hashtable<String, String>();
            int count = 1;
            for (int i = 1; i <= adultCount; i++) {
                if (i == 1) {
                    passengerNames.put("passengerName" + count, getFirstPassengerName(testData));
                } else {
                    passengerNames.put("passengerName" + count, testData.get("adultName" + i).split(" ")[0] + " " + testData.get("adultName" + i).split(" ")[1] + " " + testData.get("adultName" + i).split(" ")[2]);
                }
                count++;
            }
            for (int i = 1; i <= teenCount; i++) {
                if (i == 1 && adultCount == 0) {
                    passengerNames.put("passengerName" + count, getFirstPassengerName(testData));
                } else {
                    passengerNames.put("passengerName" + count, testData.get("teenName" + i).split(" ")[0] + " " + testData.get("teenName" + i).split(" ")[1] + " " + testData.get("teenName" + i).split(" ")[2]);

                }
                count++;
            }
            for (int i = 1; i <= childCount; i++) {
                passengerNames.put("passengerName" + count, testData.get("childName" + i).split(" ")[0] + " " + testData.get("childName" + i).split(" ")[1] + " " + testData.get("childName" + i).split(" ")[2]);
                count++;
            }
            for (int i = 1; i <= infantCount; i++) {
                passengerNames.put("infantName" + i, testData.get("infantName" + i).split(" ")[0] + " " + testData.get("infantName" + i).split(" ")[1] + " " + testData.get("infantName" + i).split(" ")[2]);
                count++;
            }
            reportLogger.log(LogStatus.INFO,"Passenger names entered - "+passengerNames);
            reportLogger.log(LogStatus.INFO, "Successfully captured passenger Names");
            return passengerNames;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to capture passenger Names");
            throw e;
        }
    }

    private String getFirstPassengerName(Hashtable<String, String> testData) {
        //get first passenger name from UI
        if (testData.get("iAmPassenger").toLowerCase().equals("true")) {
            actionUtility.hardSleep(2000);
            return actionUtility.getValueSelectedInDropdown(passengerOneTitleReadOnly) + " " + passengerOneFirstName.getAttribute("value") + " " + passengerOneLastName.getAttribute("value");
        }

        return actionUtility.getValueSelectedInDropdown(passengerOneTitle) + " " + passengerOneFirstName.getAttribute("value") + " " + passengerOneLastName.getAttribute("value");

    }

    private Hashtable<String, Integer> getPassengerCountForEachType(Hashtable<String, String> testData) {
        //returns the passenger Count
        Hashtable<String, Integer> passengerCount = new Hashtable<String, Integer>();
        passengerCount.put("adultCount", 1);
        passengerCount.put("infantCount", 0);
        passengerCount.put("teenCount", 0);
        passengerCount.put("childCount", 0);

        if (testData.get("noOfAdult") != null) {
            passengerCount.put("adultCount", Integer.parseInt(testData.get("noOfAdult")));
        }

        if (testData.get("noOfInfant") != null) {
            passengerCount.put("infantCount", Integer.parseInt(testData.get("noOfInfant")));
        }

        if (testData.get("noOfTeen") != null) {
            passengerCount.put("teenCount", Integer.parseInt(testData.get("noOfTeen")));
        }

        if (testData.get("noOfChild") != null) {
            passengerCount.put("childCount", Integer.parseInt(testData.get("noOfChild")));
        }

        return passengerCount;
    }

    public void verifyOperatedByInFlightBreakDown(Hashtable<String, String> SelectedFlights) {
        //verifies the operated by value in flight breakdown section
        String expectedFlightName = null;
        String actualFlightName = null;
        try {

            expectedFlightName = SelectedFlights.get("flightName1").trim().substring(17);
            actionUtility.waitForElementVisible(flightBreakDownOperatedBy, 15);
            actualFlightName = flightBreakDownOperatedBy.getText().trim();
            Assert.assertEquals(actualFlightName, expectedFlightName, "operated by did not match in the flight breakdown section");
            reportLogger.log(LogStatus.INFO, "operated by matched in the flight breakdown section");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "operated by did not match in the flight breakdown section Actual: " + actualFlightName + " Expected: " + expectedFlightName);
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the operated by in flight break down section");
            throw e;
        }

    }

    public void verifySeatSelectionPaneIsDisplayed() {
        //verifies if the seat selection pane is displayed
        try{
            Assert.assertTrue(seatSelectionPaneFront.isDisplayed(),"seat selection pane is not displayed");
            Assert.assertTrue(seatSelectionPaneMid.isDisplayed(),"seat selection pane is not displayed");
            Assert.assertTrue(seatSelectionPaneBack.isDisplayed(),"seat selection pane is not displayed");
            reportLogger.log(LogStatus.INFO,"seat selection pane is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"seat selection pane is not displayed");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if seat selection pane is displayed");
            throw e;
        }
    }

    public void verifyAddressIsPopulatedAfterPostCodeIsSelected() {
        // verifies if the addresses are popuplated after choosing the post code
        try {
            String address = contactPassengerAddressOne.getAttribute("value");
            String town = contactPassengerTown.getAttribute("value");
            String county = contactPassengerCounty.getAttribute("value");

            try {
                Assert.assertTrue(address.length() > 2, " address is not populated");
                reportLogger.log(LogStatus.INFO, "address is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "address is not populated after selecting the post code");
                throw e;
            }

            try {

                Assert.assertTrue(town.length() > 2, " town is not populated");
                reportLogger.log(LogStatus.INFO, "town is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "town is not populated after selecting the post code");
                throw e;
            }

            try {
                Assert.assertTrue(county.length() > 2, " county is not populated");
                reportLogger.log(LogStatus.INFO, "county is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "country is not populated after selecting the post code");
                throw e;
            }

        } catch (AssertionError e) {

            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if address is populated after selecting the post code");
            throw e;
        }


    }

    public void verifyPassengerDetailsFooter() {
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on Flybe.com are subject to availability, inclusive of taxes and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            String fPara4 = "Certain Loganair fares have conditions attached requiring a minimum length of stay.";

            actionUtility.waitForElementNotPresent(emailCheckInLocator, 20);

            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(ContactUsLink.isDisplayed(), "Contact Us Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeInFooter.isDisplayed(), "Flybe.com Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Flybe.com Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(ancillaryCharges.isDisplayed(), "Ancillary Charges Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Ancillary Charges Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara4.getText().toLowerCase(), fPara4.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify footer in passenger page");
            throw e;
        }
    }

    public void verifyACIOptionDisplayedForBlueIsland() {
//        verifies whether ACI option is displayed for Blue Islands
//        String pageURL = "initialisePassengerDetails";
//        actionUtility.waitForPageURL(pageURL,10);
        actionUtility.waitForPageLoad(10);
        try {
            Assert.assertFalse(aciHeading.isDisplayed(), "ACI heading is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI heading is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI heading is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI heading");
            throw e;
        }

        try {
            Assert.assertFalse(checkInByEmail.isDisplayed(), "ACI email option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI email option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI email option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI email option");
            throw e;
        }

        try {
            Assert.assertFalse(checkInByMobile.isDisplayed(), "ACI mobile option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI mobile option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI mobile option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI mobile option");
            throw e;
        }

        try {
            Assert.assertFalse(checkInLater.isDisplayed(), "ACI manual option is displayed for Blue Islands");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "ACI manual option is displayed for Blue Islands");
            throw e;
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.INFO, "ACI manual option is not displayed for Blue Islands");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify ACI manual option");
            throw e;
        }
    }

    public void verifySeatSelectionPaneNotDisplayed(){
        //To verify the seat selection section is not displayed for Air France Flight
        try{
            Assert.assertFalse(seatSelectionPaneFront.isDisplayed(),"seat selection pane is not displayed");
            Assert.assertFalse(seatSelectionPaneMid.isDisplayed(),"seat selection pane is not displayed");
            Assert.assertFalse(seatSelectionPaneBack.isDisplayed(),"seat selection pane is not displayed");
            reportLogger.log(LogStatus.INFO,"seat selection pane is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"seat selection pane is displayed");
            throw e;
        }catch (NoSuchElementException e){
            reportLogger.log(LogStatus.INFO,"seat selection pane is not displayed");

        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if seat selection pane is displayed");
            throw e;
        }

    }




    public void verifyTextSMSConfirmationAndTextbox(){
        // To Verify the SMS textbox should be empty when the user is not logged in
        try {
            actionUtility.waitForElementVisible(textSMSConfirmationHeader, 30);
            Assert.assertEquals(phoneNumberTextBox.getAttribute("value"), "");
            reportLogger.log(LogStatus.INFO,"Phone number text box is empty");

        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING, "Phone number text box is not empty"+" Actual "+ phoneNumberTextBox.getAttribute("value")+" Expected : Phone number text box should be empty");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Phone number text box is empty in your Details");
            throw e;

        }


    }

    public void verifyAddHoldLuggageDefaultSelection(){
        //To Verify the default luggage selection should be 20KG for just fly
        try {
            try {
                Assert.assertEquals(default20KGSelection.getAttribute("value"), "1","The default value for selected baggage is 20 KG");
                reportLogger.log(LogStatus.INFO, "Default bag selection is Standard Most Popular 20KG for Just Fly");
            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Default bag selection for Just fly should be 20KG"+" Expected : The default luggage selection for Just Fly should be 20KG"+" Actual -"+ default20KGSelection.getAttribute("value"));
                throw e;
            }catch(NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING, "Default bag selection for Just fly is not displayed");
                throw e;
            }
        }catch(AssertionError e){
            throw e;
        }catch (NoSuchElementException e) {
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify Baggage section");
            throw e;
        }
    }

    public void verifyBaggageMessageForGetMore(){
        // To Verify the Baggage Message when Get More is selected
        String getMoreExpectedMessage = "A 23kg baggage allowance is included for all passengers travelling in Get More.";
        try {

            try {
                Assert.assertEquals(baggageMessage.getText().toLowerCase().trim(), getMoreExpectedMessage.toLowerCase().trim(),"");
                reportLogger.log(LogStatus.INFO, "Get More Baggage message is displayed as expected");
            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Get More Baggage message is matching"+" Expected - "+getMoreExpectedMessage+" Actual - "+ baggageMessage.getText());
                throw e;
            }catch(NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING, "Get More Baggage message is not displayed");
                throw e;

            }
        }catch(AssertionError e){

            throw e;
        }catch (NoSuchElementException e) {
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify baggage message for Get More");
            throw e;

        }
    }

    public void verifyFlybeFlexTicketFlexibilityCheckBoxIsUnchecked(){
        // To Verify the Flybe Flex Checkbox is unchecked
        try {

            actionUtility.waitForPageLoad(5);
            try {
                Assert.assertFalse(flexTicketFlexibility.isSelected(), "Flybe flex ticket flexibility is not selected");
                reportLogger.log(LogStatus.INFO, "Flybe flex ticket flexibility is not selected");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe flex ticket flexibility is selected");
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe flex is not found");
                throw e;
            }
        }catch(AssertionError e){
            throw e;

        }catch (NoSuchElementException e) {
            throw e;

        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to  verify if Flybe flex field is not selected");
            throw e;

        }
    }

    public void verifyFirstNameLastNameAndTitleIsDisplayedForPassenger(){
        // to verify the passenger details section "Drop down, First name, Last name"
        try {
            String actualPassengerTitle =  new Select(passengerOneTitle).getFirstSelectedOption().getText();
            try {
                Assert.assertTrue(actualPassengerTitle.equals(""),"Passenger Title dropdown is not displayed");
                reportLogger.log(LogStatus.INFO, "Passenger Title by default is empty");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Passenger Title by default is not empty"+" Expected - Passenger Title dropdown should be empty. Actual - "+actualPassengerTitle);
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Passenger Title is not found");
                throw e;

            }

            try {
                Assert.assertTrue(passengerOneFirstName.getAttribute("value").equals(""),"Passenger First Name is not displayed");
                reportLogger.log(LogStatus.INFO, "Passenger First Name by default is empty");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Passenger First Name by default is not empty"+" Expected - Passenger First Name should be empty. Actual - "+passengerOneFirstName.getAttribute("value"));
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Passenger First Name is not found");
                throw e;

            }

            try {
                Assert.assertTrue(passengerOneLastName.getAttribute("value").equals(""),"Passenger Last Name is not displayed");
                reportLogger.log(LogStatus.INFO, "Passenger Last Name by default is empty");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Passenger Last Name by default is not empty"+" Expected - Passenger Last Name should be empty. Actual - "+passengerOneLastName.getAttribute("value"));
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Passenger Last Name is not found");
                throw e;

            }
            try {
                Assert.assertFalse(addTravelInsurances.get(0).isSelected(), "Travel Insurance is not displayed");
                reportLogger.log(LogStatus.INFO, "Travel Insurance is not selected");
            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Travel Insurance is selected");
                throw e;
            }catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Travel Insurance is not found");
                throw e;
            }
        }catch(AssertionError e){
            throw e;

        }catch (NoSuchElementException e) {
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify first name, last name and title is displayed for passenger");
            throw e;

        }
    }

    public void verifyTextSMSSectionWithMobileNumberPrepopulated(){
        // to verify the mobile number displayed in SMS section
        try{
            actionUtility.click(bookingDetails);
            Assert.assertEquals(contactPassengerMobileNo.getAttribute("value"), phoneNumberTextBox.getAttribute("value"));
            reportLogger.log(LogStatus.INFO, "The number displayed in SMS section is matching");

        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "The number displayed in SMS section is not matching"+" Expected - "+contactPassengerMobileNo.getAttribute("value")+" Actual - "+ phoneNumberTextBox.getAttribute("value"));
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify the Mobile Number Pre-populated in SMS section");
            throw e;
        }
    }

    public void openRulesTaxesFromBasket(String rulesToOpen){

        //open the respective rules link from passenger details page
        try {
            switch(rulesToOpen.toLowerCase()) {

                case "fare rules":
                    actionUtility.click(fareRulesLink);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;
                case "baggage rules":
                    actionUtility.click(baggageRulesLink);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;
                case "taxes and charges":
                    actionUtility.click(taxesAndChargesLink);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;
            }
            reportLogger.log(LogStatus.INFO, "clicked on " + rulesToOpen + " link in passenger details page");

        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to click on " + rulesToOpen + " link in passenger details page");
            throw e;
        }
    }

    public void verifyTheRulesPopupInPassengerDetails(String rulesPopup){
        //To verify the header in rules popup of passenger details
        String fareRulesExpected = "Fare rules";
        String baggageRulesExpected = "Baggage Rules";
        String taxesAndChargesExpected = "Taxes and Charges";
        try {
            switch(rulesPopup.toLowerCase()) {

                case "fare rules":
                    try {
                        Assert.assertEquals(fareRulesPopupHeading.getText().toLowerCase().trim(), fareRulesExpected.toLowerCase().trim(), "Fare rules is not matching");
                        reportLogger.log(LogStatus.INFO,"Fare Rule popup header is matching");
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"Fare Rule popup is not matching"+" Expected - "+fareRulesExpected+" Actual - "+ fareRulesPopupHeading.getText());
                        throw e;
                    }
                    break;
                case "baggage rules":
                    try {
                        Assert.assertEquals(baggageRulesPopupHeading.getText().toLowerCase().trim(), baggageRulesExpected.toLowerCase().trim(), "Baggage rules is not matching");
                        reportLogger.log(LogStatus.INFO,"Baggage Rule popup header is matching");
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"Baggage Rule popup is not matching"+" Expected - "+baggageRulesExpected+" Actual - "+ baggageRulesPopupHeading.getText());
                        throw e;
                    }
                    break;
                case "taxes and charges":
                    try {
                        Assert.assertEquals(taxesAndChargesPopupHeading.getText().toLowerCase().trim(), taxesAndChargesExpected.toLowerCase().trim(),"Taxes & Charges is not matching");
                        reportLogger.log(LogStatus.INFO,"Taxes and Charges popup header is matching");
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"Taxes and Charges popup is not matching"+" Expected - "+taxesAndChargesExpected+" Actual - "+ taxesAndChargesPopupHeading.getText());
                        throw e;
                    }
                    break;
            }
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify"+ rulesPopup +"popup in passenger details page");
            throw e;
        }
    }

    public void verifyTheTermsAndConditionsLinkAndPopupInFlybeFlex(){
        //To Verify the Terms and Conditions Link and Popup in flybe flex section
        String termsAndConditionsExpected = "Terms and Conditions";
        try {

            actionUtility.waitForElementClickable(termsAndConditionsLinkFlybeFlex,15);
            actionUtility.click(termsAndConditionsLinkFlybeFlex);
            try {
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"fancybox-frame");
                Assert.assertEquals(termsAndConditionsFlybeFlex.getText().toLowerCase().trim(), termsAndConditionsExpected.toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Terms and Conditions popup header is matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Terms and Conditions popup is not matching"+" Expected - "+termsAndConditionsExpected+" Actual - "+ termsAndConditionsFlybeFlex.getText());
                throw e;
            }
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify Terms and Conditions popup in Flybe flex section");
            throw e;

        }
    }

    public void verifyTheAddHoldLuggageSectionForAllIn(){
        //To Verify the Add hold luggage section & message when All In ticket is displayed
        String addHoldLuggageMessageExpected = "A 46kg baggage allowance is included for all passengers travelling in All In. Each passenger may carry up to two bags.";
        try{
            Assert.assertEquals(addHoldLuggageMessage.getText().toLowerCase().trim(), addHoldLuggageMessageExpected.toLowerCase().trim(),"Add hold luggage section is displayed when All In is selected");
            reportLogger.log(LogStatus.INFO, "Add hold luggage message is displayed for All In tickets as expected");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "Add hold luggage message is not matching when All In ticket is selected"+" Expected - "+addHoldLuggageMessageExpected+" Actual - "+ addHoldLuggageMessage.getText());
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Add hold luggage message is not displayed when All In is selected.");
            throw e;
        }
    }

    private void switchToBaggageTermsAndConditionsWindow(){
        // This method is used for switching to baggage terms and conditions
        boolean isSwitchSuccess = actionUtility.switchWindow("Flybe.com");
        if (!isSwitchSuccess) {

            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
            if (isSwitchToCertificateErrorSuccess) {

                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");


            } else {
                reportLogger.log(LogStatus.WARNING,"unable to pass control to baggage terms and conditions window");
                throw new RuntimeException("unable to pass control to baggage terms and conditions window");
            }

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Switching to baggage terms and conditions window is Successful");
        }else{
            reportLogger.log(LogStatus.INFO,"Switching to baggage terms and conditions is successful");

        }

    }

    public void verifyTermsAndConditionsInBaggageRulesPopup(){
        //To verify the Baggage Terms and Conditions inside baggage rule popup
        String baggageTermsHeadingExpected = "Baggage restrictions, terms and conditions";
        try {
            actionUtility.click(baggageTermsAndConditionsLink);
            switchToBaggageTermsAndConditionsWindow();
            actionUtility.waitForElementVisible(baggageTermsAndConditions,10);
            try {
                Assert.assertEquals(baggageTermsAndConditions.getText().toLowerCase().trim(), baggageTermsHeadingExpected.toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Baggage Terms and Conditions is not matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Baggage Terms and Conditions is not matching"+" Expected - "+baggageTermsHeadingExpected+" Actual - "+ baggageTermsAndConditions.getText());
                throw e;
            }
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify Baggage Terms and Conditions in baggage popup");
            throw e;
        }
    }

    private void verifyFlexPriceAddedToBasket(Hashtable<String,String> testData, int noOfPassengers, int noOfOutboundFlights, int...noOfInboundFlights)
    {
        //verifies the flex price in basket
        double expectedFlexPrice =-1.0;
        double actualFlexPrice = 0.0;

        try {

            if (actionUtility.verifyIfElementIsDisplayed(flexTicket) && flexTicket.isSelected()) {
                expectedFlexPrice = getOverallFlexPrice(testData, noOfPassengers, noOfOutboundFlights, noOfInboundFlights);
            }

            if (expectedFlexPrice > 0.0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketFlexPrice), "ticket flexibility is not displayed in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is displayed in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is not displayed in the basket");
                    throw e;
                }

                try {
                    Assert.assertEquals(getOverallFlexPrice(testData, noOfPassengers, noOfOutboundFlights), CommonUtility.convertStringToDouble(basketFlexPrice.getText()), "ticket flexibility is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is not matching in the basket Expected - " + expectedFlexPrice + " Actual -" + CommonUtility.convertStringToDouble(basketFlexPrice.getText()));
                    throw e;
                }
            } else {
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketFlexPrice), "ticket flexibility is displayed in the basket");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility is not displayed in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility is displayed in the basket");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw  e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify ticket flexibility in the basket");
            throw e;
        }

    }

    private double getOverallFlexPrice(Hashtable<String,String> testData, int noOfPassengers, int noOfOutboundFlights, int...noOfInboundFlights){
        //returns the overall flex price

        double outboundFlexPrice =0.0;
        double inboundFlexPrice =0.0;

        String dateOffset = testData.get("departDateOffset");
        if (flightBreakDownOutboundFlightOption.getText().toLowerCase().contains("just fly")) {
            outboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset));
            if(testData.get("currency")!=null){
                outboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset),testData.get("currency"));
            }
            outboundFlexPrice = outboundFlexPrice * noOfOutboundFlights;
        }

        if (noOfInboundFlights.length>0 && flightBreakDownReturnFlightOption.getText().toLowerCase().contains("just fly")){
            dateOffset = testData.get("returnDateOffset");
            inboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset));
            if(testData.get("currency")!=null){
                inboundFlexPrice = getFlexPrice(Integer.parseInt(dateOffset),testData.get("currency"));
            }
            inboundFlexPrice = inboundFlexPrice* noOfInboundFlights[0];
        }

        outboundFlexPrice +=inboundFlexPrice;
        outboundFlexPrice = outboundFlexPrice*noOfPassengers;
        outboundFlexPrice = Math.round(outboundFlexPrice*100.0)/100.0;
        return outboundFlexPrice;
    }

    private double getFlexPrice(int date, String... currency) {
        //returns the flex price for the dateOffset

        double flexPrice = 0.0;
        if(currency.length>0){
            switch (currency[0]){
                case "EUR":
                    if (date >= 0 && date <= 14) {
                        flexPrice = 18.0;

                    } else if (date >= 15 && date <= 30) {
                        flexPrice = 14.0;

                    } else if (date >= 31) {
                        flexPrice = 8.0;
                    }
                    break;
                case "GBP":
                default:
                    if (date >= 0 && date <= 14) {
                        flexPrice = 12.99;

                    } else if (date >= 15 && date <= 30) {
                        flexPrice = 9.99;

                    } else if (date >= 31) {
                        flexPrice = 5.99;
                    }
                    break;
            }
        }else{
//              if nothing is passed as the currency then the default currency is being selected
            if (date >= 0 && date <= 14) {
                flexPrice = 12.99;

            } else if (date >= 15 && date <= 30) {
                flexPrice = 9.99;

            } else if (date >= 31) {
                flexPrice = 5.99;
            }
        }

        return flexPrice;

    }

    public void verifyAdultTitleOptions() {
        //To verify the adult passenger tiles in passenger details page
        String title = "Mr;Mrs;Mstr.;Ms;Miss;Dr.;Prof.;Lord;Lady;Sir;Rev.";
        String[] expectedTitles = title.split(";");
        Select actualPassengersType = new Select(passengerOneTitle);
        List<WebElement> actualTitles = actualPassengersType.getOptions();

        try {
            for(String expectedTitle : expectedTitles){
                boolean flag = false;
                for(WebElement actualTitle : actualTitles ) {
                    if (expectedTitle.equals(actualTitle.getText())){
                        flag = true;
                        reportLogger.log(LogStatus.INFO,"Passenger title- "+expectedTitle+" is present as adult titles" );
                        break;
                    }
                }
                if (!flag){
                    reportLogger.log(LogStatus.WARNING,"Passenger titles are not matching. " + expectedTitle +" is not present as adult titles ");
                    throw new AssertionError("Passenger titles are not matching");
                }
            }

        }catch (AssertionError e){

            throw e;
        }
        catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify the passenger titles");
            throw e;
        }
    }

    public void verifyBaggagePriceFreeFor23Kg() {
//        this method verifies the 23kg baggage is free in this route or not
        try {
            actionUtility.waitForPageLoad(20);
            actionUtility.waitForElementVisible(twentyThreeBagPrice,10);
            double twentyThreePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getText());
            if (twentyThreePrice == 0.0) {
                reportLogger.log(LogStatus.INFO, "baggage price for 23 kg is displayed as 0");
                actionUtility.click(twentyThreeBags.get(0));
                actionUtility.click(acceptBags);
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "baggage price of 23 kg is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING,"baggage price of 23 kg is displayed in the basket");
                    throw e;
                }
            }else {
                reportLogger.log(LogStatus.WARNING,"baggage price for 23 kg is not displayed as 0 instead showing as - "+twentyThreePrice);
                throw new RuntimeException("baggage price for 23 kg is not displayed as 0 instead showing as - "+twentyThreePrice);
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify baggage price of 23 kg");
            throw e;
        }
    }

    public void verifyBaggageTextForLoganair(String flightOption) {
//        this method verifies the baggage text message for laganair route
        try {
            actionUtility.waitForPageLoad(15);
            actionUtility.waitForElementVisible(baggageMessage,10);
            String justFlyMsg = "A 20kg baggage allowance is included for all passengers travelling.";
            String getMoreMsg = "A 20kg hold luggage allowance is included for all passengers travelling in Get More";
            String allInMsg = "A 30kg baggage allowance is included for all passengers travelling in All In.";
            String expected = null;
            switch (flightOption){
                case "just fly":
                    expected = justFlyMsg;
                    break;
                case "get more":
                    expected = getMoreMsg;
                    break;
                case "all in":
                    expected = allInMsg;
                    break;
            }
            try {
                Assert.assertEquals(expected, baggageMessage.getText(), "hold baggage price message is not displayed");
                reportLogger.log(LogStatus.INFO, "hold baggage message is displayed for "+flightOption);
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "hold baggage is not matching Expected - " + expected + " Actual - " + baggageMessage.getText());
                throw e;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify free hold baggage section");
            throw e;
        }
    }

    public void verifyIncreaseBaggageAllowanceForGetMore() {
//        this method verifies whether increase baggage allowance is displayed for get more option or not
        try {
            actionUtility.waitForPageLoad(15);
            actionUtility.waitForElementVisible(baggageMessage, 15);
            String getMoreMsg = "A 23kg baggage allowance is included for all passengers travelling in Get More.";
            String getMoreIncreaseAllowanceMsg = "Increase your hold luggage allowance to 46kg and press add to update.";
            try {
                Assert.assertEquals(getMoreMsg, baggageMessage.getText(), "23kg baggage price is not included on this route");
                reportLogger.log(LogStatus.INFO, "23kg hold baggage message is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "23kg hold baggage is not matching Expected - " + getMoreMsg + " Actual - " + baggageMessage.getText());
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(getMoreIncreaseBaggageButton), "increase allowance button is not displayed");
                reportLogger.log(LogStatus.INFO, "increase allowance button is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "increase allowance button is not displayed");
                throw e;
            }
            try {
                Assert.assertEquals(getMoreIncreaseAllowanceMsg, getMoreIncreaseBaggageText.getText(), "increase baggage allowance text is not matching");
                reportLogger.log(LogStatus.INFO, "increase baggage allowance text is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "increase baggage allowance text is not matching Expected - " + getMoreIncreaseAllowanceMsg + " Actual - " + getMoreIncreaseBaggageText.getText());
                throw e;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify get more increase allowance options");
            throw e;
        }
    }

    public void toSelectTheContactDetails(){
        // To Select the contact details
        try {
            actionUtility.waitForElementVisible(bookingDetails,15);
            actionUtility.click(bookingDetails);
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to click on booking details button");
            throw e;
        }
    }

    public void verifyBaggagePriceForGetMoreDisplayedAtReducedCost(){
//        this method verifies the baggage price is displayed at a reduced cost if we select get more and try to increase it
        Double twentyThreePrice = 0.0;
        Double fourtySixPrice = 0.0;
//        Double twentyThreeBaggagePriceExpected = 9.01;
        Double fourtySixBaggagePriceExpected = 25.00;
        try{
            actionUtility.click(getMoreIncreaseBaggageButton);
//            actionUtility.waitForElementVisible(twentyThreeBagPrice,10);
//            twentyThreePrice = CommonUtility.convertStringToDouble(twentyThreeBagPrice.getText());
            actionUtility.waitForElementVisible(fortySixBagPrice,10);
            fourtySixPrice = CommonUtility.convertStringToDouble(fortySixBagPrice.getText());
//            try{
//                Assert.assertEquals(twentyThreePrice,twentyThreeBaggagePriceExpected,"baggage price for 23kg is not matching");
//                reportLogger.log(LogStatus.INFO,"baggage price for 23 kg is displayed at a reduced cost");
//            }catch (AssertionError e){
//                reportLogger.log(LogStatus.WARNING,"23kg baggage is not displayed at a reduced cost Expected - "+twentyThreeBaggagePriceExpected+" Actual - "+twentyThreePrice);
//                throw e;
//            }
            try{
                Assert.assertEquals(fourtySixPrice,fourtySixBaggagePriceExpected,"baggage price for 46kg is not matching");
                reportLogger.log(LogStatus.INFO,"baggage price for 46kg is displayed at a reduced cost");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"46kg baggage is not displayed at a reduced cost Expected - "+fourtySixBaggagePriceExpected+" Actual - "+fourtySixPrice);
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify baggage price at reduced cost");
            throw e;
        }


    }

    public void verifyCurrency(String expectedCurrency){
        // verifies if the currency in the drop down is same as the argument passed
        String actualCurrency=null;
        try{
            actualCurrency=  new Select(currency).getFirstSelectedOption().getText();
            Assert.assertEquals(actualCurrency,expectedCurrency,"currency selected by default in the your details page is not matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"currency selected by default in the your details page is not matching Expected - "+ expectedCurrency+" Actual - "+actualCurrency);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency");
            throw e;
        }
    }

    public void addFlexToBooking() {
        // adds the flex option
        try {
            actionUtility.waitForElementClickable(flexTicket, 5);
            actionUtility.selectOption(flexTicket);
            actionUtility.waitForElementVisible(basketFlexPrice, 5);
            reportLogger.log(LogStatus.INFO, "added the ticket flexibility");
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "uanble to add the ticket flexibility");
        }
    }

    public void verifyAllinHoldBaggageMessage(){
//      this method verifies all in hold baggage message
        String baggageMessageExpected = "A 46kg baggage allowance is included for all passengers travelling in All In. Each passenger may carry up to two bags.";
        try{
            actionUtility.waitForElementVisible(baggageMessage,10);
            Assert.assertEquals(baggageMessage.getText().trim(),baggageMessageExpected,"baggage text for 46kg is not matching");
            reportLogger.log(LogStatus.INFO,"baggage message for 46kg is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"baggage text for 46kg is not matching Expected - "+baggageMessageExpected+" Actual - "+baggageMessage.getText().trim());
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify hold baggage message");
            throw e;
        }
    }

    public void verifyChangeFeeIsDisplayed(boolean flag){
//        this method verifies whether change fee is applied or not in GBP only
        Double expectedChangeFeePrice = 40.0;
        Double actualChangeFeePrice = 0.0;
        try{
            if(flag){
                try {
                    actionUtility.waitForElementVisible(changeFeePrice,10);
                    actualChangeFeePrice = CommonUtility.convertStringToDouble(changeFeePrice.getText());
                    Assert.assertEquals(actualChangeFeePrice,expectedChangeFeePrice,"change fee price is not matching");
                    reportLogger.log(LogStatus.WARNING,"change fee price is matching");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change fee price is not matching Expected - "+expectedChangeFeePrice+"GBP Actual - "+actualChangeFeePrice+"GBP");
                    throw e;
                }
            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(changeFeePrice));
                    reportLogger.log(LogStatus.INFO, "change fee is not displayed");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change fee is displayed");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify change fee is displayed or not");
            throw e;
        }
    }

    public void verifySeatsSectionIsDisplayed(){
        // to verify the seat section

             try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(seatsSection), "Seats section is not displayed");
                reportLogger.log(LogStatus.INFO,"Seats section is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "Seats section is not displayed");
                throw e;
            }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to if verify seat selection is displayed");
            throw e;

        }
    }

    public void verifyBaggageSectionIsDisplayed(){
        // to verify the baggage section
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageSection), "Baggage section is not displayed");
                reportLogger.log(LogStatus.INFO,"Baggage section is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "Baggage section is not displayed");
                throw e;
            } catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to if verify baggage section is displayed");
            throw e;

        }
    }

    public void verifyElementsInBaggageSectionIsDisplayed(){
        // verify the elements in baggage section like

        String golfBaggageExpected = " / 1 bag + golf bag";
        try {
            try {

                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageNone), "No bag type is not displayed");
                reportLogger.log(LogStatus.INFO, "No bag type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "No bag type is not matching");
                throw e;
            }

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageSmall),"small baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "small baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "small baggage type is not matching");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(standardBaggage),"medium baggage type is not displayed");
                reportLogger.log(LogStatus.INFO, "medium baggage type is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "medium baggage type is not matching");
                throw e;
            }

//            try {
//                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(baggageLarge), "large baggage type is not displayed");
//                reportLogger.log(LogStatus.INFO, "large baggage type is displayed");
//            } catch (AssertionError e) {
//                reportLogger.log(LogStatus.WARNING, "large baggage type is not matching");
//                throw e;

//            }

            if(actionUtility.verifyIfElementIsDisplayed(baggageGolf)||actionUtility.verifyIfElementIsDisplayed(baggageTwoBags)){
                reportLogger.log(LogStatus.INFO, "extra large baggage type is displayed");
            }else{
                reportLogger.log(LogStatus.WARNING,"extra large baggage type is not displayed");
                throw new AssertionError("extra large baggage type is not displayed");
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to if verify elements in baggage type");
            throw e;

        }
    }

    public void verifyTheSeatLegends() {
    // to verify the seat mapping legends in seat section
        try {
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(selectedSeatsLegends),"Selected seat Mapping is not displayed");
                reportLogger.log(LogStatus.INFO, "Selected seat legend is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Selected seat legend is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(AvailableSeatsLegends),"Available seat Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Available seat key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Available seat key mapping is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(emergencyExitLegend),"Emergency exit seat Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Emergency exit seat key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Emergency exit seat key mapping is not displayed");
                throw e;
            }
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(extraLegRoomLegend),"Extra leg room key Mapping is not correct");
                reportLogger.log(LogStatus.INFO, "Extra leg room key mapping is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Extra leg room key mapping is not displayed");
                throw e;
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the seat mapping keys");
            throw e;
        }
    }

    public void verifyCurrencyInBaggageType(String expectedCurrency){
        //verify the currency in baggage section

        try {

            baggageCurrencies.forEach((element)-> Assert.assertTrue(element.getText().toLowerCase().contains(expectedCurrency.toLowerCase()), "currency is not matching in bagage type"));
            reportLogger.log(LogStatus.INFO,"currency for each baggage type is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for each baggage type is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in baggage type");
            throw e;
        }
    }

    public void verifyCurrencyInSeatType(String currencyToBeVerified){
        //verify the currency in seat section
        try {

            seatCurrencies.forEach((element)->Assert.assertTrue(element.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()),"currency is not matching in seat type"));
            reportLogger.log(LogStatus.INFO,"currency for seat type is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for seat type is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in seat type");
            throw e;
        }
    }



    public void retrieveAviosDetailsByLogin(Hashtable<String,String> testData){
        // Login to the avios account
        try{
            actionUtility.waitForElementVisible(aviosUserName,3);
            String userName = testData.get("aviosUserName");
            String password = testData.get("aviosPassword");
            aviosUserName.clear();
            aviosUserName.sendKeys(userName);
            aviosPassword.clear();
            aviosPassword.sendKeys(password);
            aviosLogin.click();
            actionUtility.waitForElementVisible(aviosBalance, 10);
            reportLogger.log(LogStatus.INFO, "successfully logged into avios account");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to retrieve avios account details");
            throw e;
        }
    }


    public double selectAviosType(String aviosType){
        // to select the avios type
        WebElement selectedAviosType = null;
        double maxAviosPrice = 0.0;
        switch (aviosType.toLowerCase()){
            case "low":
                selectedAviosType = aviosLowOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
                break;
            case "medium":
                selectedAviosType = aviosMediumOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
                break;
            case "high":
                selectedAviosType = aviosHighOption;
                maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
                break;
        }
        try{
            actionUtility.selectOption(selectedAviosType);
            actionUtility.waitForElementVisible(aviosBasketPrice,3);
            reportLogger.log(LogStatus.INFO,"avios option is successfully selected");
            return maxAviosPrice;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the required avios option");
            throw e;
        }
    }

    public void verifyCurrencyInAvios(String currencyToBeVerified){
        //verify the currency in avios section
        try {
            aviosCurrency.forEach(element -> Assert.assertTrue(element.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()),"currency for avios is not matching") );
            reportLogger.log(LogStatus.INFO,"currency for avios options is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"currency for avios options is not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verified the currency in avios options");
            throw e;
        }
    }

    public void verifyAviosDisplayPasswordForgottenPasswordAndYourUserNameAreDisplayed() {
        // to verify Display Password, Forgotten Password , Forgotten User Name in Avios Section
        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(displayPassword), "Display Password is not displayed in Avios");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(forgottenPassword), "Forgotten Password is not displayed in Avios");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(forgottenUserName), "Forgotten User Name is not displayed in Avios");
            reportLogger.log(LogStatus.INFO, "Display Password, Forgotten Password,Forgotten User Name are displayed in avios");

        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "Display Password, Forgotten Password,Forgotten User Name are not displayed in avios");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify if Display Password, Forgotten Password,Forgotten User Name in avios");
            throw e;
        }
    }

    public void verifyBaggagePriceForStateLengthLessThan250km(){
//        this method verifies baggage price for state length less than 250km
        double fifteenBagPriceExpected = 19.0;
        double twentyThreeBagPriceExpected = 24.0;
        double fortySixBagPriceExpected = 48.0;
        double fifteenBagPriceActual = 0.0;
        double twentyThreeBagPriceActual = 0.0;
        double fortySixBagPriceActual = 0.0;
        try {
            actionUtility.waitForElementNotPresent(emailCheckInLocator, 20);
            fifteenBagPriceActual = getBagPrice("15kg");
            verifyBaggagePrice(fifteenBagPriceActual,fifteenBagPriceExpected,"15kg");
            twentyThreeBagPriceActual = getBagPrice("23kg");
            verifyBaggagePrice(twentyThreeBagPriceActual,twentyThreeBagPriceExpected,"23kg");
            fortySixBagPriceActual = getBagPrice("46kg");
            verifyBaggagePrice(fortySixBagPriceActual,fortySixBagPriceExpected,"46kg");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the passenger bags");
            throw e;
        }

    }

    private void verifyBaggagePrice(double priceActual, double priceExpected, String baggageValue){
//        this method verifies baggage price is displayed as expected
        try{
            Assert.assertEquals(priceActual,priceExpected,baggageValue+" baggage price is not matching");
            reportLogger.log(LogStatus.INFO,baggageValue+" baggage price is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,baggageValue+" baggage is not matching Expected - "+priceExpected+" Actual - "+priceActual);
            throw e;
        }
    }

//    TODO: need to implement this method
    public void verifyTheToolTipMessageInAviosLogin(){
        //to verify the tool tip message over login button in avios section
        String aviosLoginMessageExpected = "Only one Avios Customer can use points to Part Pay for the Flight.";
        try{
            Assert.assertEquals(aviosLoginToolTip.getText().toLowerCase().trim(),aviosLoginMessageExpected.toLowerCase().trim(),"avios login tool tip is not displayed");
            reportLogger.log(LogStatus.INFO, aviosLoginButton.getAttribute("title"));

        }catch(Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify if tool tip message in avios login button");
            throw e;

        }

    }

    public void verifyAviosBalanceIsDisplayed(){

        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBalance), "avios balance is not displayed");
            reportLogger.log(LogStatus.INFO, "avios balance is displayed - " + aviosBalance.getText());
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "avios balance is not displayed" );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if avios balance is displayed");
            throw e;
        }
    }

    public void verifyTheAviosOptions(){
        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosLowOption), "avios low option is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosMediumOption), "avios medium option is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosHighOption), "avios high option is not displayed");
            reportLogger.log(LogStatus.INFO, "avios options are displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "avios options are not displayed" );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify avios options");
            throw e;
        }
    }

    public void verifyAviosNotAppliedForTravelInsuranceInBasket(){
        double maxAviosPrice = -1.0;
        double mediumAviosPrice = -1.0;
        double lowAviosPrice = -1.0;

        double appliedAviosPrice = -1.0;
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double aviosSum =0.0;
        double overallBasketPrice =1.0;
        double aviosPrice = 0.0;
        double totalBasketAfterAvios = 0.0;

        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();

        try{
            actionUtility.unSelectOption(addTravelInsurances.get(0));
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
            mediumAviosPrice = CommonUtility.convertStringToDouble(aviosMediumPrice.getText());
            lowAviosPrice = CommonUtility.convertStringToDouble(aviosLowPrice.getText());
            appliedAviosPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);

            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
            aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText())*100.0)/100.0;
            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (maxAviosPrice > appliedAviosPrice ) {

                try {
                    Assert.assertEquals(appliedAviosPrice, outboundPrice+inboundPrice, "applied avios in basket are not equal to outbound flight");
                    reportLogger.log(LogStatus.INFO, "applied avios options is matching with selected fares");
                }catch(AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "applied avios options is not matching with selected fares");
                    throw e;
                }
                aviosSum = Math.round((outboundPrice+inboundPrice-aviosPrice)*100.0)/100.0;
                overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                basketDetails.put("overallBasketPrice",overallBasketPrice);
                try {
                    Assert.assertEquals(overallBasketPrice, aviosSum, "applied avios in basket are not equal to outbound flight");
                }catch(AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios options is not displayed");
                    throw e;
                }

            }else {
                reportLogger.log(LogStatus.WARNING, "Unable to verify the applied avios in basket");
                throw new RuntimeException("Unable to verify the applied avios in basket");
            }

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify basket");
            throw e;
        }

    }


    public void verifyAviosAppliedForFlightsSeatsAndBaggagesInBasket(){
        double maxAviosPrice = -1.0;
        double mediumAviosPrice = -1.0;
        double lowAviosPrice = -1.0;

        double appliedAviosPrice = -1.0;
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double aviosSum =0.0;
        double overallBasketPrice =1.0;
        double aviosPrice = 0.0;
        double totalBasketAfterAvios = 0.0;

        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();

        try{
            actionUtility.unSelectOption(addTravelInsurances.get(0));
            maxAviosPrice = CommonUtility.convertStringToDouble(aviosHighPrice.getText());
            appliedAviosPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);


            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
                basketDetails.put("basketBaggagePrice",bagPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
                basketDetails.put("seatsPrice",seatsPrice);
            }

            if(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice)){
                aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText())*100.0)/100.0;
                basketDetails.put("aviosPrice",aviosPrice);
            }


            if (maxAviosPrice > appliedAviosPrice ) {

                try {
                    Assert.assertEquals(appliedAviosPrice, outboundPrice+inboundPrice+seatsPrice+bagPrice, "applied avios in basket are not equal to outbound flight");
                    reportLogger.log(LogStatus.INFO, "avios options is not displayed");
                }catch(AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios options is not displayed");
                    throw e;
                }
                aviosSum = Math.round((outboundPrice+inboundPrice+seatsPrice+bagPrice-aviosPrice)*100.0)/100.0;
                overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                basketDetails.put("overallBasketPrice",overallBasketPrice);
                try {
                    Assert.assertEquals(overallBasketPrice, aviosSum, "applied avios in basket are not equal to outbound flight");
                }catch(AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios options is not displayed");
                    throw e;
                }

            }else {
                reportLogger.log(LogStatus.WARNING, "Unable to verify the applied avios in basket");
                throw new RuntimeException("Unable to verify the applied avios in basket");
            }

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify basket");
            throw e;
        }

    }

    public void verifyBaggagesDisplayedInHoldLuggage(int noOfPassengers)
    {
        //verifies whether 46kg,23kg,15kg and none is displayed for each passenger
        try {
            int k = 0;
            for (int i = 0; i < noOfPassengers; i++) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fortySixBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(twentyThreeBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fifteenBags.get(k)));
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(zeroBags.get(k)));

                    k++;
                    reportLogger.log(LogStatus.INFO, "successfully verified that 0kg, 15kg, 23kg and 46kg baggages is displayed for passenger"+(i+1)+" in the Add Hold Luggage section on flight options page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.INFO, "baggages displayed for each passenger is not correct");
                    throw e;
                }
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify whether 0kg, 15kg, 23kg and 46kg baggages is displayed in the Add Hold Luggage section on flight options page");
            throw e;
        }
    }

    public void verify23KgIsSelected(int noOfPassengers){
        //verifies whether 23Kg is selected by default for all passengers
        try{
            actionUtility.waitForElementVisible(twentyThreeBags,10);
            for(int i=0;i<noOfPassengers;i++){
                try{
                    Assert.assertTrue(twentyThreeBags.get(i).getAttribute("class").equals("selected"));
                    reportLogger.log(LogStatus.INFO,"23Kg baggage is selected by default for passenger"+(i+1));
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"23Kg baggage is not selected by default for passenger"+(i+1));
                    throw e;
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if 23Kg baggage is selected by default");
            throw e;
        }
    }

    public void continueToPaymentOnManageBooking() {
        //clicks on continue button to navigate to the payment page
        try {
            actionUtility.waitForElementClickable(continueButtonManage,25);
            actionUtility.hardClick(continueButtonManage);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(15);
            reportLogger.log(LogStatus.INFO, "successfully continued to payment");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to payment");
            throw e;
        }
    }

    public void continueToPaymentInManageBooking(){
        try {
            actionUtility.waitForElementClickable(continueToPaymentButtonInManageBooking,25);
            actionUtility.hardClick(continueToPaymentButtonInManageBooking);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(15);
            reportLogger.log(LogStatus.INFO, "successfully continued to payment");
            } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to payment");
            throw e;
            }
    }


}