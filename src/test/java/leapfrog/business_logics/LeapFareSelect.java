package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


public class LeapFareSelect {

    ActionUtility actionUtility;
    ExtentTest reportLogger;
    public LeapFareSelect() {
        this.actionUtility = new ActionUtility();
        PageFactory.initElements(TestManager.getDriver(), this);
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = ".logo>a")
    private WebElement flybeMainHeaderLogo;

    @FindBy (className ="no-flights-message")
    private WebElement noFlightsMessage;

    @FindBy (xpath ="//div[@class='flights-table outbound']")
    private WebElement outboundFlightList;

    @FindBy (xpath ="//div[@class='flights-table inbound']")
    private WebElement inboundFlightList;

    @FindBy(xpath = "//input[@value='Continue']")
    private WebElement proceed;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']")
    private  WebElement flightStopDetailList;

    @FindBy(xpath="//div[contains(@class,'col-sm-4 col-xs-9 links')]")
    private  WebElement footerPanel;

    @FindBy(css = "a[href='/contact']")
    private WebElement contactUs;

    @FindBy(xpath = "//div[contains(@class,'active')]/descendant::div[div[normalize-space(text())='All in']]/following-sibling::div/descendant::li[contains(text(),'hold luggage')]")
    private WebElement allInHoldLuggage;

    @FindBy(xpath = "//div[contains(@class,'active')]/descendant::div[div[normalize-space(text())='Get more']]/following-sibling::div/descendant::li[contains(text(),'hold luggage')]")
    private WebElement getMoreHoldLuggage;

    @FindBy(xpath = "//div[contains(@class,'active')]/descendant::div[div[normalize-space(text())='Just fly']]/following-sibling::div/descendant::li[contains(text(),'hold luggage')]")
    private WebElement justFlyHoldLuggage;


    //    ----------Search widget-------------
    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[contains(@class,'chosen-container')]/a/div/b")
    private  WebElement flightFromDropDown;

    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[contains(@class,'chosen-container')]/div/div/input")
    private  WebElement flightFrom;

    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[@id='flightTo_chosen']/a/div/b")
    private  WebElement flightToDropDown;

    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[contains(@id,'flightTo')]/div/div/input")
    private  WebElement flightTo;

    @FindBy(xpath = "//div[contains(@class,'menu-shadow')]")
    private WebElement passengerSelector;

    @FindBy(xpath = "//div[contains(@class,'teenItaly')]")
    private WebElement unaccompaniedMinorMessageForItaly;

    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[contains(@class,'unaccompaniedChild')]")
    private WebElement unaccompaniedChildMessage;

    @FindBy(xpath = "//div[@class='card-charges start-hidden']/div/div/div/span")
    private WebElement cardChargeInformation;

//  ----Outbound Two Change Flights----------------

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='2 changes']")
    private List<WebElement> outboundTwoStopCheckers;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]//span[text()='2 changes']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div/descendant::input[@type='radio']")
    private List <WebElement> twoStopOutboundFlights;

    @FindBy(xpath = "//div[@class='flights-table outbound']//span[text()='2 changes']/ancestor::label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/following-sibling::div[@class='flights-stops-details']//div[@class='flight-code'][2]")
    private List <WebElement> twoStopOutboundFlightNames;

//  ----Outbound one Change Flights----------------

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='1 change']")
    private List<WebElement> outboundOneStopCheckers;

    @FindBy(xpath = "//div[@class='flights-table outbound']//span[text()='1 change']/ancestor::label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/following-sibling::div[@class='flights-stops-details']//div[@class='flight-code'][2]")
    private List <WebElement> oneStopOutboundFlightNames;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]//span[text()='1 change']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div/descendant::input[@type='radio']")
    private List <WebElement> oneStopOutboundFlights;

    @FindBy(xpath = "//div[@class='flight-code' and contains(text(),'Air France')]/ancestor::div[@class='flights-table outbound']/descendant::div[@class='validation-error-messages']/div[contains(@data-original-title,'Full or not enough seats')]")
    private List <WebElement> oneStopOutboundDisabledFlightNamesWithMoreThanFourPax;

    @FindBy(xpath = "//div[@class='flight-code' and contains(text(),'Air France')]/ancestor::div[@class='flights-table outbound']/descendant::div[@class='validation-error-messages']/div[contains(@data-original-title,'Teens cannot fly')]")
    private List <WebElement> oneStopOutboundDisabledFlightNamesWithTeens;


//  ------Direct Outbound Flight----------

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='flight-code'][2]")
    private List <WebElement> directOutboundFlightNames;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='flight-code'][1]")
    private List <WebElement> directOutboundFlightCodes;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']")
    private List <WebElement> directOutboundFlightsDepartureTime;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/following-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']")
    private List <WebElement> directOutboundFlightsArrivalTime;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::input[@type='radio']")
    private List <WebElement> directOutboundFlights;

    @FindBy(xpath = "//div[@class='flights-table outbound']/descendant::label[@class='flight-details']/descendant::div[@class='flight-code' and contains(text(),'Air France')]/ancestor::label[@class='flight-details']/following-sibling::div[@class='validation-error-messages']/div[contains(@data-original-title,'Full or not enough seats')]")
    private List <WebElement> directOutboundDisabledFlightsWithMoreThanFourPax;


    @FindBy(xpath = "//div[@class='flights-table outbound']/descendant::label[@class='flight-details']/descendant::div[@class='flight-code' and contains(text(),'Air France')]/ancestor::label[@class='flight-details']/following-sibling::div[@class='validation-error-messages']/div[contains(@data-original-title,'Teens cannot fly')]")
    private List <WebElement> directOutboundDisabledFlightsWithTeens;



    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::input[@type='radio']")
    private List<WebElement> allOutboundFlights;

//  -------outbound Flight Options--------

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'All in')]]]/preceding-sibling::div//input[contains(@name,'outbound')]")
    private WebElement outboundAllIn;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'Just fly')]]]/preceding-sibling::div//input[contains(@name,'outbound')]")
    private WebElement outboundJustFly;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'Get more')]]]/preceding-sibling::div//input[contains(@name,'outbound')]")
    private WebElement outboundGetMore;



    @FindBy(xpath="//div[contains(@style,'block') and @class='flight-options']//div[contains(text(),'Just fly')]//following-sibling::div/span[@class='formatted-currency']")
    private WebElement outboundJustFlyPrice;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flight-options']//div[contains(text(),'Get more')]//following-sibling::div/span[@class='formatted-currency']")
    private WebElement outboundGetMorePrice;


    @FindBy(xpath="//div[div[contains(text(),'Just fly')]]/following-sibling::div//li")
    private List<WebElement> outboundJustFlyDescriptions;

    @FindBy(xpath="//div[div[contains(text(),'Get more')]]/following-sibling::div//li")
    private List<WebElement> outboundGetMoreDescriptions;

    @FindBy(xpath="//div[div[contains(text(),'All in')]]/following-sibling::div//li")
    private List<WebElement> outboundAllInDescriptions;




//  -----------------------------------------------Return FLights---------------------------------------------------------------------------

    @FindBy (xpath ="//div[@class='flights-container flights-inbound']")
    private WebElement returnFlightList;

//  ----Return Two Change Flights----------------

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='2 changes']")
    private List<WebElement> returnTwoStopCheckers;

    @FindBy(xpath = "//div[@class='flights-table inbound']//span[text()='2 changes']/ancestor::label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/following-sibling::div[@class='flights-stops-details']//div[@class='flight-code'][2]")
    private List <WebElement> twoStopReturnFlightNames;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]//span[text()='2 changes']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div/descendant::input[@type='radio']")
    private List <WebElement> twoStopReturnFlights;

//  ----Return One Change Flights----------------

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='1 change']")
    private List<WebElement> returnOneStopCheckers;

    @FindBy(xpath = "//div[@class='flights-table inbound']//span[text()='1 change']/ancestor::label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/following-sibling::div[@class='flights-stops-details']//div[@class='flight-code'][2]")
    private List <WebElement> oneStopReturnFlightNames;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]//span[text()='1 change']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div/descendant::input[@type='radio']")
    private List <WebElement> oneStopReturnFlights;

    @FindBy(xpath = "//div[@class='flight-code' and contains(text(),'Air France')]/ancestor::div[@class='flights-table inbound']/descendant::div[@class='validation-error-messages']/div[contains(@data-original-title,'Full or not enough seats')]")
    private List <WebElement> oneStopReturnDisabledFlightNamesWithMoreThanFourPax;

    @FindBy(xpath = "//div[@class='flight-code' and contains(text(),'Air France')]/ancestor::div[@class='flights-table inbound']/descendant::div[@class='validation-error-messages']/div[contains(@data-original-title,'Teens cannot fly')]")
    private List <WebElement> oneStopReturnDisabledFlightNamesWithTeens;

    //  ------Direct Return Flight----------
    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='flight-code'][2]")
    private List <WebElement> directReturnFlightNames;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='flight-code'][1]")
    private List <WebElement> directReturnFlightCodes;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']")
    private List <WebElement> directReturnFlightsDepartureTime;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/following-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']")
    private List <WebElement> directReturnFlightsArrivalTime;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::input[@type='radio']")
    private List <WebElement> directReturnFlights;

    @FindBy(xpath = "//div[@class='flights-table inbound']/descendant::label[@class='flight-details']/descendant::div[@class='flight-code' and contains(text(),'Air France')]/ancestor::label[@class='flight-details']/following-sibling::div[@class='validation-error-messages']/div[contains(@data-original-title,'Full or not enough seats')]")
    private List <WebElement> directReturnDisabledFlightsWithMoreThanFourPax;

    @FindBy(xpath = "//div[@class='flights-table inbound']/descendant::label[@class='flight-details']/descendant::div[@class='flight-code' and contains(text(),'Air France')]/ancestor::label[@class='flight-details']/following-sibling::div[@class='validation-error-messages']/div[contains(@data-original-title,'Teens cannot fly')]")
    private List <WebElement> directReturnDisabledFlightsWithTeens;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::input[@type='radio']")
    private List<WebElement> allReturnFlights;

    //  -------Return Flight Options--------
    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'All in')]]]/preceding-sibling::div//input[contains(@name,'inbound')]")
    private WebElement returnAllIn;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'Just fly')]]]/preceding-sibling::div//input[contains(@name,'inbound')]")
    private WebElement returnJustFly;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[contains(text(),'Get more')]]]/preceding-sibling::div//input[contains(@name,'inbound')]")
    private WebElement returnGetMore;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[normalize-space(text())='All in']]]/preceding-sibling::div[input[contains(@name,'inbound')]]/following-sibling::div[@class='disabled-by-flexible-message']")
    private WebElement returnValidationMessageForAllIn;

    @FindBy(xpath = "//div[contains(@style,'block') and @class='flight-options']//div[div[div[normalize-space(text())='Just fly']]]/preceding-sibling::div[input[contains(@name,'inbound')]]/following-sibling::div[@class='disabled-by-flexible-message']")
    private WebElement returnValidationMessageForJustFly;



//  ----Selected MultiSector Flight Details-----

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='flight-code'][2]")
    private  List<WebElement> selectedFlightNames;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='flight-code'][1]")
    private  List<WebElement> selectedFlightNos;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='depart-time']//div[@class='time']")
    private  List<WebElement> selectedFlightsDepartureTime;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='arrive-time']//div[@class='time']")
    private  List<WebElement> selectedFlightsArrivalTime;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='depart-time']//div[@class='time']/span")
    private  List<WebElement> selectedFlightSources;

    @FindBy(xpath="//div[contains(@style,'block') and @class='flights-stops-details']//div[@class='arrive-time']//div[@class='time']/span")
    private  List<WebElement> selectedFlightDestinations;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']/span")
    private List <WebElement> directOutboundFlightSources;

    @FindBy(xpath = "//div[@class='flights-table outbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/following-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']/span")
    private List <WebElement> directOutboundFlightDestinations;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/preceding-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']/span")
    private List <WebElement> directReturnFlightSources;

    @FindBy(xpath = "//div[@class='flights-table inbound']//label[@class='flight-details' and not(following-sibling::div[@class='validation-error-messages'])]/descendant::span[text()='non-stop']/ancestor::div[@class='col-xs-2 col-sm-3']/following-sibling::div[@class='col-xs-4 col-sm-3']/descendant::div[@class='time']/span")
    private List <WebElement> directReturnFlightDestinations;

//    ------------fares outbound----------------

    @FindBy(xpath = "//ul[@data-direction='outbound']/descendant::li[@class='active']/descendant::span[@class='formatted-currency']")
    private WebElement lowestPriceDisplayedOutBound;

    @FindBy(xpath = "//div[@class='flights-table outbound']/descendant::div[@class='amount']")
    private List<WebElement> outBoundFares;

    @FindBy(id = "outbound")
    private WebElement additionalAirportChargeOutBound;

//    -----------fares inbound---------------------

    @FindBy(xpath = "//ul[@data-direction='inbound']/descendant::li[@class='active']/descendant::span[@class='formatted-currency']")
    private WebElement lowestPriceDisplayedInBound;

    @FindBy(xpath = "//div[@class='flights-table inbound']/descendant::div[@class='amount']")
    private List<WebElement> inBoundFares;

    @FindBy(id = "inbound")
    private WebElement additionalAirportChargeInBound;

    //    ----------basket-------------
    @FindBy(css=".js-baggage-rules")
    private WebElement baggageRules;

    @FindBy(css=".js-fare-rules")
    private WebElement fareRules;

    @FindBy(css=".js-tax-rules")
    private WebElement taxes;

    @FindBy(css=".breakdown-btn")
    private WebElement priceBreakDown;

    @FindBy(className = "currency-text")
    private WebElement currencySelected;

    //    ---------rules popup---------
    @FindBy(className = "modal-dialog")
    private WebElement rulesPopup;

    @FindBy(css=".price-breakdown")
    private WebElement priceBreakDownDetails;

    @FindBy(id="myModalLabel")
    private WebElement rulesPopupHeader;

    @FindBy(css="#taxes-rules-container th")
    private List<WebElement> taxForPassenger;


    @FindBy(xpath="//div[contains(@class,'price-breakdown')]/div")
    private WebElement  priceBreakdownSection;

    //-------------------PLD---------------------

    @FindBy(xpath = "//input[@value='Price Lock-Down']")
    private WebElement pldButton;



    private String outboundFlightLoader = "//div[@class='flights-table outbound']/descendant::div[@class='flights-loading']/span";
    private String inboundFlightLoader = "//div[@class='flights-table inbound']/descendant::div[@class='flights-loading']/span";
    private String returnFlightLoader = "//div[@class='flights-table inbound']/descendant::div[@class='flights-loading']/span";
    private String currencyOption ="//a[@class='currency-option' and contains(text(),'%s')]";

    //----------Footer-in-Home-Page-------------
    @FindBy(css = "div.container a[href='/'] img[src='/web-app/assets/img/flybe-logo-sm.png']")
    private WebElement flybeLogo;

    @FindBy(css = "//li[@id='footer-copyright']/span")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    @FindBy(css = "div.container a[href='/contact']")
    private WebElement ContactUsLink;

    @FindBy(css = "a[href='/terms/tariff.htm']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = "div.container p a[href='/']")
    private WebElement flybeInFooter;

    @FindBy(css = "div.container p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = "div.container p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = "div.container p:nth-child(3)")
    private WebElement footerPara3;

    //    -----------Flight Prices---------------
    @FindBy(xpath = "//div[contains(@class,'outbound')]/descendant::label[contains(@class,'active')]/descendant::span[@class='formatted-currency']")
    private  WebElement selectedOutboundFlightPrice;

    @FindBy(xpath = "//div[contains(@class,'inbound')]/descendant::label[contains(@class,'active')]/descendant::span[@class='formatted-currency']")
    private  WebElement selectedReturnFlightPrice;


    //    ---fares in the price breakdown in basket------
    @FindBy(xpath = "//div[@class='fare-price'][contains(., 'Fare per adult')]")
    private WebElement farePerAdultText;

    @FindBy(xpath = "//div[@class='fare-price'][contains(., 'Fare per teen')]")
    private WebElement farePerTeenText;

    @FindBy(xpath = "//div[@class='fare-price'][contains(., 'Fare per child')]")
    private WebElement farePerChildText;

    @FindBy(xpath = "//div[@class='fare-price'][contains(., 'Fare per infant')]")
    private WebElement farePerInfantText;

    @FindBy(xpath = "//div[@class='fare-price'][contains(., 'Get more bundle cost')]")
    private WebElement getMoreBundleCostText;

    @FindBy(xpath = "//div[@class='fare-price']/text()[contains(., 'Fare per adult')]/preceding-sibling::span[1]")
    private WebElement farePerAdult;

    @FindBy(xpath = "//div[@class='fare-price']/text()[contains(., 'Fare per teen')]/preceding-sibling::span[1]")
    private WebElement farePerTeen;

    @FindBy(xpath = "//div[@class='fare-price']/text()[contains(., 'Fare per child')]/preceding-sibling::span[1]")
    private WebElement farePerChild;

    @FindBy(xpath = "//div[@class='fare-price']/text()[contains(., 'Fare per infant')]/preceding-sibling::span[1]")
    private WebElement farePerInfant;

    @FindBy(xpath = "//div[@class='fare-price']/text()[contains(., 'Get more bundle')]/preceding-sibling::span[1]")
    private WebElement getMoreBundleCost;

//    ------tax & charges in price break down in basket------

    @FindBy(xpath = "//div[@class='tax-price'][contains(., 'Taxes & charges per adult')]")
    private WebElement taxPerAdultText;

    @FindBy(xpath = "//div[@class='tax-price'][contains(., 'Taxes & charges per teen')]")
    private WebElement taxPerTeenText;

    @FindBy(xpath = "//div[@class='tax-price'][contains(., 'Taxes & charges per child')]")
    private WebElement taxPerChildText;

    @FindBy(xpath = "//div[@class='tax-price'][contains(., 'Taxes & charges per infant')]")
    private WebElement taxPerInfantText;

    @FindBy(xpath = "//div[@class='tax-price']/text()[contains(., 'Taxes & charges per adult')]/preceding-sibling::span[1]")
    private WebElement taxPerAdult;

    @FindBy(xpath = "//div[@class='tax-price']/text()[contains(., 'Taxes & charges per teen')]/preceding-sibling::span[1]")
    private WebElement taxPerTeen;

    @FindBy(xpath = "//div[@class='tax-price']/text()[contains(., 'Taxes & charges per child')]/preceding-sibling::span[1]")
    private WebElement taxPerChild;

    @FindBy(xpath = "//div[@class='tax-price']/text()[contains(., 'Taxes & charges per infant')]/preceding-sibling::span[1]")
    private WebElement taxPerInfant;

    //    ----basket outbound & inbound total ------
    @FindBy(xpath = "//div[@class='outbound']/descendant::span[@class='formatted-currency']")
    private WebElement outboundTotal;

    @FindBy(xpath = "//div[@class='inbound start-hidden']/descendant::span[@class='formatted-currency']")
    private WebElement returnTotal;

    //    -----basket total price----------
    @FindBy(css = ".total .amount .formatted-currency")
    private WebElement overallBasketTotal;

    //    -----selection of passengers-------
    @FindBy(className = "pax-display")
    private WebElement openSelectPassengerMenu;

    @FindBy(xpath = "//div[contains(@class,'search-widget-0')]/descendant::div[contains(@class,'pax-selector')]")
    private WebElement passengerMenu;

    @FindBy(xpath = "//div[contains(@class,'pax-close-button')]")
    private WebElement closeSelectPassengerMenu;

    @FindBy(xpath = "//input[contains(@class,'inputAdults')]")
    private WebElement adultNos;

    @FindBy(xpath = "//input[contains(@class,'inputTeens')]")
    private WebElement teenNos;

    @FindBy(xpath = "//input[contains(@class,'inputChildren')]")
    private WebElement childrenNos;

    @FindBy(xpath = "//input[contains(@class,'inputInfants')]")
    private WebElement infantNos;

    @FindBy(xpath = "//div[contains(@class,'adult-up')]")
    private WebElement adultIncrement;

    @FindBy(xpath = "//div[contains(@class,'adult-down')]")
    private WebElement adultDecrement;

    @FindBy(xpath = "//div[contains(@class,'teen-up')]")
    private WebElement teenIncrement;

    @FindBy(xpath = "//div[contains(@class,'teen-down')]")
    private WebElement teenDecrement;

    @FindBy(xpath = "//div[contains(@class,'child-up')]")
    private WebElement childIncrement;

    @FindBy(xpath = "//div[contains(@class,'child-down')]")
    private WebElement childDecrement;

    @FindBy(xpath = "//div[contains(@class,'infant-up')]")
    private WebElement infantIncrement;

    @FindBy(xpath = "//div[contains(@class,'infant-down')]")
    private WebElement infantDecrement;


    //----------month view outbound---------

    @FindBy(css=" div[data-direction='outbound'] div.btn-primary")
    private WebElement outboundMonthView;

    @FindBy(css="div [data-direction='outbound'] td:not(.ui-state-disabled)> span.day-price")
    private List<WebElement> outboundMonthViewPrices;

    @FindBy(css="div [data-direction='outbound'] td:not(.ui-state-disabled)> a.ui-state-default")
    private List<WebElement> outboundMonthViewDates;

    @FindBy(css="div[data-direction='outbound'] a[data-handler='next']")
    private WebElement outboundMonthViewNextMonth;

    @FindBy(css = "div[data-direction='outbound'] .ui-datepicker-month")
    private WebElement outboundMonthViewMonthSelected;


    //    ----------month view inbound---------
    @FindBy(css=" div[data-direction='inbound'] div.btn-primary")
    private WebElement returnMonthView;

    @FindBy(css="div [data-direction='inbound'] td:not(.ui-state-disabled)>span.day-price")
    private List<WebElement> returnMonthViewPrices;

    @FindBy(css="div [data-direction='inbound'] td:not(.ui-state-disabled)>a.ui-state-default")
    private List<WebElement> returnMonthViewDates;

    @FindBy(css="div[data-direction='inbound'] a[data-handler='next']")
    private WebElement returnMonthViewNextMonth;

    @FindBy(css = "div[data-direction='inbound'] .ui-datepicker-month")
    private WebElement returnMonthViewMonthSelected;

    @FindBy(xpath = "//div[div[a[img[@src='assets/img/logo.png']]]]/following-sibling::div//li")
    private List<WebElement> flightBookingSteps;




    public void changeCurrency(String currency){
        //change the currency
        try {
            WebElement currencyToBeSelected = TestManager.getDriver().findElement(By.xpath(String.format(currencyOption, currency)));
            actionUtility.hardClick(currencyToBeSelected);
            reportLogger.log(LogStatus.INFO,"successfully changed the currency to " +currency);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to change the currency to " +currency);
            throw e;
        }
    }

    private Hashtable<String,String> getTestData(Hashtable<String,String> mainTestData,String tripType,int noOfFlights ){
        // creating subset of test data depending on the trip type
        Hashtable<String,String> testData= new Hashtable();
        String date=null;
        String flightOption=null;
        if (tripType.equals("inbound")){
            date = calculateDateFromOffSet(mainTestData,tripType);
            flightOption = mainTestData.get("inboundFlightOption");
            for(int i=1; i<= noOfFlights; i++){
                testData.put("flight"+i,mainTestData.get("inboundFlight"+(i)));
                testData.put("flightDate"+i,date);
                if(flightOption!=null){
                    testData.put("flightOption"+i,flightOption);
                }
            }
        }else if (tripType.equals("outbound")){
            date = calculateDateFromOffSet(mainTestData,tripType);
            flightOption = mainTestData.get("outboundFlightOption");
            for(int i=1; i<= noOfFlights; i++){
                testData.put("flight"+i,mainTestData.get("outboundFlight"+(i)));
                testData.put("flightDate"+i,date);
                if(flightOption!=null){
                    testData.put("flightOption"+i,flightOption);
                }
            }
        }
        return testData;
    }

    private String calculateDateFromOffSet(Hashtable<String,String> mainTestData, String tripType){
        int date;
        if(tripType.equals("outbound")) {
            date= Integer.parseInt(mainTestData.get("departDateOffset"));
        }else {
            date = Integer.parseInt(mainTestData.get("returnDateOffset"));
        }
        return actionUtility.getDateAsString(date,"EEE dd MMM yyyy");
    }

    public void verifyFlightListDisplayed(){
        //verifies if the flight lists are displayed
        actionUtility.waitForElementVisible(outboundFlightList, 10);
        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
        try {
            Assert.assertTrue(outboundFlightList.isDisplayed(), "outbound flight list is not displayed");
            reportLogger.log(LogStatus.INFO,"outbound flight list is displayed");
        }catch(Exception e) {
            reportLogger.log(LogStatus.WARNING,"outbound flight list is not displayed");
            throw e;
        }
        try {
            Assert.assertTrue(returnFlightList.isDisplayed(), "inbound flight list is not displayed");
            reportLogger.log(LogStatus.INFO,"inbound flight list is displayed");
        }catch(Exception e) {
            reportLogger.log(LogStatus.WARNING,"inbound flight list is not displayed");
            throw e;
        }
    }

    public void verifyNoFlightsMessageDisplayed(){
        //verify if no flight message is displayed
        actionUtility.waitForElementVisible(outboundFlightList, 15);
        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
        try {
            Assert.assertTrue(noFlightsMessage.isDisplayed(), "outbound flight list is not displayed");
            reportLogger.log(LogStatus.INFO,"outbound flight list is displayed");
        }catch(Exception e) {
            reportLogger.log(LogStatus.WARNING,"outbound flight list is not displayed");
            throw e;
        }
    }

    public Hashtable<String, String> selectFlight(Hashtable<String, String> mainTestData, String sector, String tripType){
        // depending on the trip type and sector, respective select flights methods are called
        try {
            Hashtable<String, String> testData = null;
            Hashtable<String, String> selectedFlightDetails = null;
            switch (sector) {
                case ("twoChange"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        testData = getTestData(mainTestData, tripType, 3);
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
//                        String url = TestManager.getDriver().getCurrentUrl();
//                        TestManager.getDriver().navigate().to(url+"?new=1");
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
                        selectedFlightDetails = selectMultiSectorFlight(testData, 3, outboundTwoStopCheckers, twoStopOutboundFlights,tripType);
                    } else if (tripType.toLowerCase().equals("inbound")) {
                        testData = getTestData(mainTestData, tripType, 3);
                        actionUtility.waitForElementVisible(returnFlightList, 10);
                        actionUtility.waitForElementNotPresent(returnFlightLoader, 75);
                        selectedFlightDetails = selectMultiSectorFlight(testData, 3, returnTwoStopCheckers, twoStopReturnFlights,tripType);
                    }
                    break;
                case ("oneChange"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        testData = getTestData(mainTestData, tripType, 2);
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
//                        String url = TestManager.getDriver().getCurrentUrl();
//                        TestManager.getDriver().navigate().to(url+"?new=1");
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
                        selectedFlightDetails = selectMultiSectorFlight(testData, 2, outboundOneStopCheckers, oneStopOutboundFlights,tripType);

                    } else if (tripType.toLowerCase().equals("inbound")) {
                        testData = getTestData(mainTestData, tripType, 2);
                        actionUtility.waitForElementVisible(returnFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 40);
                        selectedFlightDetails = selectMultiSectorFlight(testData, 2, returnOneStopCheckers, oneStopReturnFlights,tripType);
                    }
                    break;
                case ("nonStop"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        testData = getTestData(mainTestData, tripType, 1);
                        actionUtility.waitForElementVisible(outboundFlightList, 30);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 40);
//                        String url = TestManager.getDriver().getCurrentUrl();
//                        TestManager.getDriver().navigate().to(url+"?new=1");
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
                        selectedFlightDetails = selectDirectFlight(testData, directOutboundFlights, directOutboundFlightNames,directOutboundFlightCodes, directOutboundFlightsDepartureTime, directOutboundFlightsArrivalTime, directOutboundFlightSources, directOutboundFlightDestinations, tripType);

                    } else if (tripType.toLowerCase().equals("inbound")) {
                        testData = getTestData(mainTestData, tripType, 1);
                        actionUtility.waitForElementVisible(returnFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 40);
                        selectedFlightDetails = selectDirectFlight(testData, directReturnFlights, directReturnFlightNames,directReturnFlightCodes, directReturnFlightsDepartureTime, directReturnFlightsArrivalTime,directReturnFlightSources, directReturnFlightDestinations,tripType);
                    }
                    break;
            }
            reportLogger.log(LogStatus.INFO,"Flights selected for the "+tripType+" trip");
            reportLogger.log(LogStatus.INFO,tripType+" Flight details - "+selectedFlightDetails);
            return selectedFlightDetails;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,tripType,"unable to select the flight for the "+tripType+" trip");
            throw e;
        }
    }

    private Hashtable<String, String> selectDirectFlight(Hashtable<String, String> testData, List<WebElement> flights, List<WebElement> flightNames, List<WebElement> flightCodes, List<WebElement> flightDepartureTime, List<WebElement> flightArrivalTime, List<WebElement> flightSource,List<WebElement> flightDestination,String tripType){
        // selects the direct flight and returns the selected flight details
        Hashtable<String,String> selectedFlightDetails = new Hashtable<String,String>();
        boolean flag= true;
        for (int i=0; i<flights.size(); i++){
            if (flightNames.get(i).getText().toLowerCase().equals(testData.get("flight1").toLowerCase())){
                selectedFlightDetails.put("flightName1", flightNames.get(i).getText().trim());
                selectedFlightDetails.put("flightNo1", flightCodes.get(i).getText().trim());
                selectedFlightDetails.put("flightDepartTime1", flightDepartureTime.get(i).getText().trim());
                selectedFlightDetails.put("flightArriveTime1", flightArrivalTime.get(i).getText().trim());
                selectedFlightDetails.put("flightSource1", flightSource.get(i).getAttribute("data-original-title").trim());
                selectedFlightDetails.put("flightDestination1", flightDestination.get(i).getAttribute("data-original-title").trim());
                selectedFlightDetails.put("flightDate1",testData.get("flightDate1"));
                if(testData.get("flightOption1")!=null){
                    selectedFlightDetails.put("flightOption1",testData.get("flightOption1"));
                }

                if (flights.get(i).isEnabled()) {
                    actionUtility.hardSelectOption(flights.get(i));

                    flag = selectFlightOption(testData,tripType);
                    if(flag){
                        break;
                    }
                }
            }
        }

        if (!flag){
            throw new RuntimeException("unable to select flight and flight options");
        }

        return selectedFlightDetails;
    }

    private Hashtable<String, String> selectMultiSectorFlight(Hashtable<String, String> testData, int noOfFlights, List<WebElement> stopChecker, List<WebElement> flights, String tripType){
        // select the multi sector flights and returns the selected flight details

        Hashtable<String,String> selectedFlightDetails = null;
        boolean flag= true;
        for (int i=0; i < stopChecker.size(); i++){
            List <Boolean> flightFlags = initializeFlightFlag(noOfFlights);
            actionUtility.click(stopChecker.get(i));
            actionUtility.hardSleep(3000);
            selectedFlightDetails = checkFlightNames(flightFlags, testData);
            actionUtility.click(stopChecker.get(i));

            if (selectedFlightDetails != null){
                // if the flight flags are set, the respective flights are selected
                actionUtility.hardSelectOption(flights.get(i));
                flag = selectFlightOption(testData,tripType);
                if(flag){
                    break;
                }


            }
        }
        if (!flag){
            throw new RuntimeException("unable to select flight and flight options");
        }
        return selectedFlightDetails;
    }



    private Hashtable<String, String> checkFlightNames(List<Boolean> flightFlags, Hashtable<String,String> tesData){
        // checks the flight names in the UI. if the flight name matches with test data flight flags are set

        Hashtable<String,String> selectedFlightDetails = null;
        for(WebElement selectedFlightName: selectedFlightNames) {

            if (selectedFlightName.isEnabled()) {

                for (int i=0; i<flightFlags.size();i++){

                    if (flightFlags.get(i) == false && selectedFlightName.getText().toLowerCase().equals(tesData.get("flight"+(i+1)).toLowerCase())) {
                        flightFlags.set(i, true);
                        break;
                    }
                }
            }
        }

        if (checkFlightFlags(flightFlags)){
            selectedFlightDetails = captureFlightDetails(tesData);
        }

        return selectedFlightDetails;
    }

    private boolean checkFlightFlags(List<Boolean> flags){
        // checks if the flight flags are set
        boolean status = false;
        for (Boolean flag: flags){
            if (flag){
                status = true;
            }else {
                status =false;
                break;
            }
        }
        return status;
    }

    private List <Boolean> initializeFlightFlag(int noOfFlights){
        // returns the no. of flight flags depending on the type of sectors
        List <Boolean> flags = new ArrayList();
        for(int i=1;i<=noOfFlights;i++){
            flags.add(false);
        }
        return  flags;
    }


    public boolean selectFlightOption(Hashtable<String, String> testData, String tripType) {
        // depending on the trip type the select option is method is called
        String option = null;
        boolean flag = true;
        try {
//
//            if (tripType.toLowerCase().equals("outbound")) {
//                option = testData.get("flightOption1");
//            } else if (tripType.equals("inbound")) {
//                option = testData.get("flightOption1");
//            }

            option = testData.get("flightOption1");

            if (option != null) {
                flag = false;
            }

            switch (option) {
                case ("All in"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.selectOption(outboundAllIn);

                    } else if (tripType.equals("inbound")) {
                        actionUtility.selectOption(returnAllIn);

                    }
                    flag = true;
                    break;
                case ("Just fly"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.selectOption(outboundJustFly);
                    } else if (tripType.equals("inbound")) {
                        actionUtility.selectOption(returnJustFly);
                    }
                    flag = true;
                    break;
                case ("Get more"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.selectOption(outboundGetMore);
                    } else if (tripType.equals("inbound")) {
                        actionUtility.selectOption(returnGetMore);
                    }
                    flag = true;
                    break;
            }
            reportLogger.log(LogStatus.INFO, option, "fare type for " + tripType + " trip - " + option);
            return flag;
        }catch (RuntimeException e){
            return  flag;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the fare type for "+tripType+" trip - "+option);
            throw e;
        }
    }

    public void verifyContinueEnabled(){
        // verifies the if the continue button is enabled
        try{
            Assert.assertTrue(proceed.isEnabled(),"Continue button is disabled");
            reportLogger.log(LogStatus.INFO,"Continue button is enabled");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Continue button is disabled");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the continue button status");
            throw e;
        }
    }

    public void continueToPassengerDetails(){
        // clicks on continue button after selecting the  flights
        try {
            if (proceed.isEnabled()) {
                actionUtility.click(proceed);
                actionUtility.waitForPageLoad(10);
                reportLogger.log(LogStatus.INFO, "proceeded to entering passenger details");
            } else {
                reportLogger.log(LogStatus.WARNING, "Continue button is disabled");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to click on continue to proceed to the passenger details");
            throw e;
        }
    }

    private Hashtable captureFlightDetails(Hashtable<String, String> testData){
        //captures the multi sector flight details
        Hashtable<String,String> selectedFlightDetails = new Hashtable<String,String>();
        for(int i = 0; i< selectedFlightNames.size(); i++){
            selectedFlightDetails.put("flightName"+(i+1), selectedFlightNames.get(i).getText().trim());
            selectedFlightDetails.put("flightNo"+(i+1), selectedFlightNos.get(i).getText().trim());
            selectedFlightDetails.put("flightDepartTime"+(i+1), selectedFlightsDepartureTime.get(i).getText().trim());
            selectedFlightDetails.put("flightArriveTime"+(i+1), selectedFlightsArrivalTime.get(i).getText().trim());
            selectedFlightDetails.put("flightSource"+(i+1), selectedFlightSources.get(i).getAttribute("data-original-title").trim());
            selectedFlightDetails.put("flightDestination"+(i+1), selectedFlightDestinations.get(i).getAttribute("data-original-title").trim());
            selectedFlightDetails.put("flightDate"+(i+1),testData.get("flightDate"+(i+1)));
            if(testData.get(("flightOption") + (i + 1))!=null){
                selectedFlightDetails.put("flightOption" + (i + 1), testData.get(("flightOption") + (i + 1)));
            }
        }
        return selectedFlightDetails;
    }

    public Hashtable<String, String> getOverallFlightDetails(Hashtable<String,String> outboundFlightDetails, Hashtable<String,String> inboundFlightDetails){
        int count =1;
        try {

            for (int i = ((outboundFlightDetails.size() / 6) + 1); count <= (inboundFlightDetails.size() / 6); i++) {
                outboundFlightDetails.put("flightNo" + i, inboundFlightDetails.get("flightNo" + count));
                outboundFlightDetails.put("flightName" + i, inboundFlightDetails.get("flightName" + count));
                outboundFlightDetails.put("flightDepartTime" + i, inboundFlightDetails.get("flightDepartTime" + count));
                outboundFlightDetails.put("flightArriveTime" + i, inboundFlightDetails.get("flightArriveTime" + count));
                outboundFlightDetails.put("flightSource" + i, inboundFlightDetails.get("flightSource" + count));
                outboundFlightDetails.put("flightDestination" + i, inboundFlightDetails.get("flightDestination" + count));
                outboundFlightDetails.put("flightDate" + i,inboundFlightDetails.get("flightDate" + count));
                if(inboundFlightDetails.get("flightOption" + count)!=null){
                    outboundFlightDetails.put("flightOption" + i, inboundFlightDetails.get("flightOption" + count));
                }
                count++;

            }
            reportLogger.log(LogStatus.INFO,"got the overall flight details");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to get the overall flight details");
            throw e;
        }
        return outboundFlightDetails;
    }

    public void verifyAirFranceIsDisabled(String tripType,String sector){
        // verifies if air france flight is disbaled
        String actualValidationMessage = null;
        String expectedValidationMessage = "Teens cannot fly unaccompanied on Airfrance flights.";
        try {
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);

            switch(sector.toLowerCase()){
                case "onechange":

                case "twochange":
                    if(tripType.toLowerCase().equals("outbound")){
                        actualValidationMessage = oneStopOutboundDisabledFlightNamesWithTeens.get(0).getText();
                    }else if (tripType.toLowerCase().equals("inbound")){
                        actualValidationMessage = oneStopReturnDisabledFlightNamesWithTeens.get(0).getText();
                    }
                    break;
                case "nonstop":
                    if(tripType.toLowerCase().equals("outbound")){
                        actualValidationMessage = directOutboundDisabledFlightsWithTeens.get(0).getText();
                    }else if (tripType.toLowerCase().equals("inbound")){
                        actualValidationMessage = directReturnDisabledFlightsWithTeens.get(0).getText();
                    }
                    break;
            }
            Assert.assertEquals(actualValidationMessage.trim(), expectedValidationMessage.trim());
            reportLogger.log(LogStatus.INFO,"right message displayed for unaccompanied teens on Air France flights");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"wrong message displayed for unaccompanied teens on Air France flights"+" Actual message: "+actualValidationMessage+" Expected message: "+ expectedValidationMessage);
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the message displayed for unaccompanied teens on Air France flights");
            throw e;
        }

    }

    public void verifyMessageForCitiJetHopRegionalWhenMoreThanFourPassengerIsSelected(String tripType,String sector){
        // verifies the message displayed for unaccompanied teens on air france flight
        String actualValidationMessage = null;
        String expectedValidationMessage = "Full or not enough seats";
        try {
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);

            switch(sector.toLowerCase()){
                case "onechange":

                case "twochange":
                    if(tripType.toLowerCase().equals("outbound")){
                        actualValidationMessage = oneStopOutboundDisabledFlightNamesWithMoreThanFourPax.get(0).getText();
                    }else if (tripType.toLowerCase().equals("inbound")){
                        actualValidationMessage = oneStopReturnDisabledFlightNamesWithMoreThanFourPax.get(0).getText();
                    }
                    break;
                case "nonstop":
                    if(tripType.toLowerCase().equals("outbound")){
                        actualValidationMessage = directOutboundDisabledFlightsWithMoreThanFourPax.get(0).getText();
                    }else if (tripType.toLowerCase().equals("inbound")){
                        actualValidationMessage = directReturnDisabledFlightsWithMoreThanFourPax.get(0).getText();
                    }
                    break;
            }
            Assert.assertEquals(actualValidationMessage.trim(), expectedValidationMessage.trim());
            reportLogger.log(LogStatus.INFO,"right message displayed for unaccompanied teens on Air France flights");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"wrong message displayed for unaccompanied teens on Air France flights"+" Actual message: "+actualValidationMessage+" Expected message: "+ expectedValidationMessage);
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the message displayed for unaccompanied teens on Air France flights");
            throw e;
        }

    }

    public void navigateToSupportPage(){
        //clicks on contact us link in the footer
        try {
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
            actionUtility.waitForElementVisible(footerPanel, 20);
            actionUtility.click(contactUs);
            actionUtility.waitForPageURL("http://flybe.custhelp.com/", 25);
            reportLogger.log(LogStatus.INFO,"Successfully navigated to support page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to navigate to support page");
            throw e;
        }

    }

    public void verifyHoldLuggage(String flightOption, String expectedBaggage){
        //verifies the hold luggage in the flight option
        try{
            switch (flightOption.toLowerCase()){
                case "just fly":
                    actionUtility.waitForElementVisible(justFlyHoldLuggage,10);
                    try{
                        Assert.assertTrue(justFlyHoldLuggage.getText().toLowerCase().contains(expectedBaggage.toLowerCase()),"hold baggage is not matching for just fly option");
                        reportLogger.log(LogStatus.INFO,"hold baggage is matching for just fly option");
                    }catch (AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"hold baggage is not matching for just fly option expected value : " + expectedBaggage.toLowerCase() + " but found :" + justFlyHoldLuggage.getText().toLowerCase().toLowerCase());
                        throw e;
                    }

                    break;
                case "get more":
                    actionUtility.waitForElementVisible(getMoreHoldLuggage,10);
                    try{
                        Assert.assertTrue(getMoreHoldLuggage.getText().toLowerCase().contains(expectedBaggage.toLowerCase()),"hold baggage is not matching for get more option");
                        reportLogger.log(LogStatus.INFO,"hold baggage is matching for get more option");
                    }catch (AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"hold baggage is not matching for get more option expected value : " + expectedBaggage.toLowerCase() + " but found :" + getMoreHoldLuggage.getText().toLowerCase().toLowerCase());
                        throw e;
                    }

                    break;
                case "all in":
                    actionUtility.waitForElementVisible(allInHoldLuggage,10);
                    try{
                        Assert.assertTrue(allInHoldLuggage.getText().toLowerCase().contains(expectedBaggage.toLowerCase()),"hold baggage is not matching for all in option");
                        reportLogger.log(LogStatus.INFO,"hold baggage is matching for get more option");
                    }catch (AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"hold baggage is not matching for all in option expected value : " + expectedBaggage.toLowerCase() + " but found :" + allInHoldLuggage.getText().toLowerCase().toLowerCase());
                        throw e;
                    }

                    break;

            }

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the hold baggage in fligh option");
            throw e;
        }

    }

    public void changeDestinationToItaly() {
        //change destination to MXP(Italy)
        try {
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
            actionUtility.hardSleep(2000);
//            actionUtility.hardClick(flightFromDropDown);
            actionUtility.click(flightToDropDown);
            actionUtility.hardSleep(1000);
            actionUtility.waitForElementVisible(flightTo, 3);
            actionUtility.sendKeysByAction(flightTo, "MXP" + Keys.RETURN);
            actionUtility.waitForElementVisible(passengerSelector, 3);
            reportLogger.log(LogStatus.INFO, "successfully changed the destination to MXP");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to change the destination to MXP");
            throw e;
        }
    }

    public void verifyMessageUnaccompaniedMinorForTeensForItaly() {
        //verifies the message displayed only teen is traveling from/to Italy

        String messageDisplayed =null;
        String messageTobeDisplayed = null;

        try{
            messageDisplayed = unaccompaniedMinorMessageForItaly.getText();
            messageTobeDisplayed = "Teens cannot fly unaccompanied on flights to or from Italy.";

            Assert.assertEquals(messageDisplayed.trim(),messageTobeDisplayed.trim(),"wrong message displayed for unaccompanied teen traveling from/to italy  ");
            reportLogger.log(LogStatus.INFO,"right message is displayed for unaccompanied teen traveling to/from italy");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message is displayed for unaccompanied teen traveling to/from italy "+"Expected: "+messageTobeDisplayed+" Actual: "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message displayed for teen traveling to/from italy");
            throw e;
        }

    }

    public void openRulesTaxesFromBasket(String rulesToOpen){
        //open the respective rules from basket
        try {
            switch(rulesToOpen.toLowerCase()) {

                case "fare rules":
                    actionUtility.click(fareRules);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;
                case "baggage rules":
                    actionUtility.click(baggageRules);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;
                case "taxes":
                    actionUtility.click(taxes);
                    actionUtility.waitForElementVisible(rulesPopup, 5);
                    break;

                case "price breakdown":
                    actionUtility.click(priceBreakDown);
                    actionUtility.waitForElementVisible(priceBreakDownDetails, 5);
                    break;
            }
            reportLogger.log(LogStatus.INFO, "clicked on " + rulesToOpen + " link");

        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to click on " + rulesToOpen + " link");
            throw e;
        }
    }

    public void verifyRulesPopupOpened(String popupTypeToBeOpened) {
        //verifies if the right popup is opened

        try {
            switch (popupTypeToBeOpened.toLowerCase()) {

                case "fare rules":
                case "baggage rules":
                case "taxes and charges":
                    Assert.assertEquals(rulesPopupHeader.getText().toLowerCase(), popupTypeToBeOpened.toLowerCase());
                    break;
                case "price breakdown":
                    Assert.assertTrue(priceBreakdownSection.isDisplayed(), popupTypeToBeOpened.toLowerCase());
                    break;
            }
            reportLogger.log(LogStatus.INFO, popupTypeToBeOpened + " popup is opened ");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, popupTypeToBeOpened + " popup is not opened Actual: " + rulesPopupHeader.getText()+" Expected: "+popupTypeToBeOpened);
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if the " + popupTypeToBeOpened + " is opened" );
            throw e;
        }
    }

    public void verifyTaxesAndChargesAreDisplayedForEachPassengerTypeSelected(Hashtable<String,String> testData){
        // verifies if the taxes are displayed to all types of passenger selected
        try {
            boolean adult = true;
            boolean teen = false;
            boolean child = false;
            boolean infant = false;

            int totalPassengerTypeToBeDisplayed = 1;

            try {
                if (testData.get("noOfAdult")!=null) {
                    if ((!testData.get("noOfAdult").equals("1"))&& (!testData.get("noOfAdult").equals("0"))) {
                        totalPassengerTypeToBeDisplayed++;
                    }else if((testData.get("noOfAdult").equals("0"))){
                        totalPassengerTypeToBeDisplayed--;
                        adult = false;
                    }
                }
            }catch (NullPointerException e){}

            try {
                if (testData.get("noOfTeen") == null &&(!testData.get("noOfTeen").equals("0")))  {
                    teen = true;
                    totalPassengerTypeToBeDisplayed++;
                }
            }catch (NullPointerException e){}

            try {
                if (testData.get("noOfChild") == null && (!testData.get("noOfChild").equals("0"))) {
                    child = true;
                    totalPassengerTypeToBeDisplayed++;
                }
            }catch (NullPointerException e){}


            try {
                if (testData.get("noOfInfant") == null &&(!testData.get("noOfInfant").equals("0"))) {
                    infant = true;
                    totalPassengerTypeToBeDisplayed++;
                }
            }catch (NullPointerException e){}

//        Assert.assertEquals(taxForPassenger.size(),(totalPassengerTypeToBeDisplayed+1), "taxes and charges are not displayed for some of the passenger type");

            boolean flag = false;
            for (int i = 1; i < taxForPassenger.size(); i++) {

                if (taxForPassenger.get(i).getText().trim().toLowerCase().equals("adult")) {
                    adult = false;
                } else if (taxForPassenger.get(i).getText().trim().toLowerCase().equals("teen")) {
                    teen = false;
                } else if (taxForPassenger.get(i).getText().trim().toLowerCase().equals("child")) {
                    child = false;
                } else if (taxForPassenger.get(i).getText().trim().toLowerCase().equals("infant")) {
                    infant = false;
                }
            }

            boolean status = false;
            if (adult) {
                reportLogger.log(LogStatus.WARNING, "taxes and charges are not displayed for adult type");
                status = true;
            } else {
                reportLogger.log(LogStatus.INFO, "taxes and charges are displayed for adult type");
            }

            if (teen) {
                reportLogger.log(LogStatus.WARNING, "taxes and charges are not displayed for teen type");
                status = true;
            } else {
                reportLogger.log(LogStatus.INFO, "taxes and charges are displayed for teen type");
            }

            if (child) {
                reportLogger.log(LogStatus.WARNING, "taxes and charges are not displayed for child type");
                status = true;
            } else {
                reportLogger.log(LogStatus.INFO, "taxes and charges are displayed for child type");
            }

            if (infant) {
                reportLogger.log(LogStatus.WARNING, "taxes and charges are not displayed for infant type");
                status = true;
            } else {
                reportLogger.log(LogStatus.INFO, "taxes and charges are displayed for infant type");
            }

            if (status) {
                throw new RuntimeException("taxes and charges are not displayed for some of the passenger type");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify if taxes and charges are displayed for passenger type selected");
            throw e;
        }

    }

    public void verifyJustFlyAllInOptionsAreDisableWhenGetMoreOptionIsSelectedInOutbound(){
//        verifies if the all in and just fly option are disable in the inbound flight section
        try{
            try{
                Assert.assertTrue(returnValidationMessageForAllIn.isDisplayed(),"All in option is not disabled");
                Assert.assertEquals(returnValidationMessageForAllIn.getText().trim().toLowerCase(),"get more cannot be combined with just fly or all in","The validation message displayed for All in is not matching");
                reportLogger.log(LogStatus.INFO,"the proper message displayed for all in option when it is disabled");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "all in option is not disabled or the improper message displayed for all in option when it is disabled Actual: " + returnValidationMessageForAllIn.getText() + " Expected: Get More cannot be combined with Just Fly or All In");
                throw e;
            }
            try{
                Assert.assertTrue(returnValidationMessageForAllIn.isDisplayed(),"Just fly option is not disabled");
                Assert.assertEquals(returnValidationMessageForJustFly.getText().trim().toLowerCase(),"get more cannot be combined with just fly or all in","The validation message displayed for Just fly is not matching");
                reportLogger.log(LogStatus.INFO,"the proper message displayed for just fly option when it is disabled");

            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "just fly option is not disabled or the improper message displayed for just fly option when it is disabled Actual: " + returnValidationMessageForAllIn.getText() + " Expected: Get More cannot be combined with Just Fly or All In");
                throw e;
            }

        }catch (AssertionError e){

            throw e;
        }
        catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify if the all in and just fly are disabled");
            throw  e;

        }


    }

    public void openFlightStopDetailsForMultiSectorFlight(String sector, String tripType) {
        // open flight detail for multi sector flight
        try {
            switch (sector) {
                case ("twoChange"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
                        openFlightStopDetails(outboundTwoStopCheckers);

                    } else if (tripType.toLowerCase().equals("inbound")) {
                        actionUtility.waitForElementVisible(returnFlightList, 10);
                        actionUtility.waitForElementNotPresent(returnFlightLoader, 75);
                        openFlightStopDetails(returnTwoStopCheckers);
                    }
                    break;
                case ("oneChange"):
                    if (tripType.toLowerCase().equals("outbound")) {

                        actionUtility.waitForElementVisible(outboundFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
                        openFlightStopDetails(outboundOneStopCheckers);

                    } else if (tripType.toLowerCase().equals("inbound")) {
                        actionUtility.waitForElementVisible(returnFlightList, 10);
                        actionUtility.waitForElementNotPresent(outboundFlightLoader, 40);
                        openFlightStopDetails(returnOneStopCheckers);
                    }
                    break;

            }
            reportLogger.log(LogStatus.INFO, "successfully opened flight details for multi sector flight");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to open flight details for multi sector flight");
            throw e;
        }
    }

    private void openFlightStopDetails (List<WebElement> stopCheckers){
        // opens the first flight details by clicking on stop checker

        actionUtility.waitForElementVisible(outboundFlightList, 10);
        actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
        actionUtility.hardSleep(2000);
        actionUtility.click(stopCheckers.get(0));
    }

    public void verifyFlightStopDetailIsDisplayed(boolean flag){
        // verifies if the flight stop details are displayed
        try{
            if(flag){
                Assert.assertTrue(flightStopDetailList.isDisplayed(),"flight stop details are not displayed");
                reportLogger.log(LogStatus.INFO, "flight stop details are displayed on clicking the number of changes link");
            }else {
                try{
                    actionUtility.waitForElementVisible(flightStopDetailList,2);
                    reportLogger.log(LogStatus.INFO, "flight stop details are displayed on clicking the number of changes link");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING, "flight stop details are hidden on clicking the number of changes link");
                }
            }

        }catch (AssertionError e){
            if(flag){
                reportLogger.log(LogStatus.WARNING, "flight stop details are hidden on clicking the number of changes link");
                throw e;
            }

        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to verify if flight stop details are displayed");
            throw e;
        }

    }

    public void verifyFareSelectPageFooter(){

        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on Flybe.com are subject to availability, inclusive of taxes and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";


            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertEquals(flybeCopyRight.getText(), "Flybe 2017", "Copy Right image is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(ContactUsLink.isDisplayed(), "Contact Us Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeInFooter.isDisplayed(), "Flybe.com Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Flybe.com Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(ancillaryCharges.isDisplayed(), "Ancillary Charges Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Ancillary Charges Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in fare select page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in fare select page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in fare select page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in fare select page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in fare select page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in fare select page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in fare select page");
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch(NoSuchElementException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify the footer in fare select page");
            throw e;
        }
    }

    public Hashtable<String,Double> verifyBasket(Hashtable<String,String> testData){
        try{
            verifyPriceBreakDownText(testData);
            verifyTotalBasketCostWithPriceBreakDown(testData);
            Hashtable<String,Double> basketPriceDetail  = verifyTotalBasketCostWithOutboundAndInboundCost();
            verifyTotalAdultCostWithSelectedFlightOption(testData);
            return basketPriceDetail;
        }catch (AssertionError e){
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the price in the basket");
            throw e;
        }



    }

    private void verifyTotalAdultCostWithSelectedFlightOption(Hashtable<String,String> testData){
        // verify sum of price break down for adult is equal to sum of outbound and inbound

        double selectedOutboundCost = 0.0;
        double selectedReturnCost=0.0;
        double sumOfSelectedOutboundAndInbound = 0.0;
        double sumOfPriceBreakdownForAdultOrTeen=0.0;

        boolean getMoreFlag =false;

        selectedOutboundCost = Math.round(CommonUtility.convertStringToDouble(selectedOutboundFlightPrice.getText())*100.0)/100.0;

        try {
            actionUtility.waitForElementVisible(selectedReturnFlightPrice,2);
            if (selectedReturnFlightPrice.isDisplayed()) {
                selectedReturnCost = Math.round(CommonUtility.convertStringToDouble(selectedReturnFlightPrice.getText())*100.0)/100.0;
            }
        }catch (TimeoutException e){}

        sumOfSelectedOutboundAndInbound = Math.round((selectedOutboundCost+selectedReturnCost)*100.0)/100.0;

        if(testData.get("outboundFlightOption")!=null ){
            if (testData.get("outboundFlightOption").equals("Get more")){
                getMoreFlag = true;
            }
        }

        if (testData.get("noOfAdult")!= null) {
            if (!testData.get("noOfAdult").equals("0")) {
                sumOfPriceBreakdownForAdultOrTeen += getPriceBreakBreakdownForEachPassengerType("adult",getMoreFlag);
            }else {
                if (testData.get("noOfTeen")!= null) {
                    if (!testData.get("noOfTeen").equals("0")) {
                        sumOfPriceBreakdownForAdultOrTeen += getPriceBreakBreakdownForEachPassengerType("teen", getMoreFlag);
                    }
                }
            }

        }else {
            sumOfPriceBreakdownForAdultOrTeen += getPriceBreakBreakdownForEachPassengerType("adult",getMoreFlag);
        }
        sumOfPriceBreakdownForAdultOrTeen = Math.round(sumOfPriceBreakdownForAdultOrTeen*100.0)/100.0;

        try{
            Assert.assertEquals(sumOfSelectedOutboundAndInbound,sumOfPriceBreakdownForAdultOrTeen,"sum of selected outbound and inbound price is not equal to sum of price breakdown for adult or teen");
            reportLogger.log(LogStatus.INFO,"sum of selected outbound and inbound price is equal to sum of price breakdown for adult or teen");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"sum of selected outbound and inbound price is not equal to sum of price breakdown for adult or teen. selected outbound flight price +selected inbound flight price - "+"total basket price - " +sumOfSelectedOutboundAndInbound+" sum of price break down for adult or teen - "+sumOfPriceBreakdownForAdultOrTeen);
            throw e;
        }
    }

    public Hashtable<String, Double> verifyTotalBasketCostWithOutboundAndInboundCost(){
        //verifies the total basket cost is sum of inbound and outbound cost
        double outboundTotalCost=0.00;
        double returnTotalCost=0.00;
        double totalBasketPrice=0.00;
        double sum = 0.00;
        Hashtable<String, Double> overallBasketPriceDetails = new Hashtable<String, Double>();

        outboundTotalCost = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
        overallBasketPriceDetails.put("outboundPrice", outboundTotalCost);

        try{
            actionUtility.waitForElementVisible(returnTotal,1);
            returnTotalCost= Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
            overallBasketPriceDetails.put("inboundPrice", returnTotalCost);
        }catch (TimeoutException e){}

        sum = Math.round((outboundTotalCost+returnTotalCost)*100.0)/100.0;
        totalBasketPrice=Math.round(CommonUtility.convertStringToDouble(overallBasketTotal.getText())*100.0)/100.0;
        try{
            Assert.assertEquals(sum,totalBasketPrice,"sum of inbound and outbound price is not equal to total basket price");
            reportLogger.log(LogStatus.INFO,"sum of inbound and outbound is equal to total basket price");
            overallBasketPriceDetails.put("totalPrice",sum);

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"sum of inbound and outbound price is not equal to total basket price. (outbound+inbound) price in basket - "+sum+" total basket price - " +totalBasketPrice);
            throw e;
        }
        return overallBasketPriceDetails;


    }

    private void verifyPriceBreakDownText(Hashtable<String,String> testData){
        // verify if the fare and tax are displayed for each selected passenger type
        if(testData.get("outboundFlightOption")!=null){
            if (testData.get("outboundFlightOption").equals("Get more")){
                try{
                    actionUtility.waitForElementVisible(getMoreBundleCostText,1);
                    reportLogger.log(LogStatus.INFO,"get more cost is added in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"get more cost is not added in the price break down");
                    throw new RuntimeException("get more cost is not added in the price break down");
                }
            }

        }

        if (testData.get("noOfAdult")!= null) {
            if (!testData.get("noOfAdult").equals("0")) {
                try{
                    actionUtility.waitForElementVisible(farePerAdultText,1);
                    reportLogger.log(LogStatus.INFO,"fare per adult is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"fare per adult is not displayed in the basket");
                    throw new RuntimeException("fare per adult is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerAdultText,1);
                    reportLogger.log(LogStatus.INFO,"tax per adult is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"tax per adult is not displayed in the basket");
                    throw new RuntimeException("tax per adult is not displayed in the basket");
                }
            }else {
                try{
                    actionUtility.waitForElementVisible(farePerAdultText,1);
                    reportLogger.log(LogStatus.WARNING,"fare per adult is displayed in the price break down");
                    throw new RuntimeException("fare per adult is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"fare per adult is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerAdultText,1);
                    reportLogger.log(LogStatus.WARNING,"tax per adult is displayed in the price break down");
                    throw new RuntimeException("tax per adult is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"tax per adult is not displayed in the basket");

                }
            }
        }else {
            try{
                actionUtility.waitForElementVisible(farePerAdultText,1);
                reportLogger.log(LogStatus.INFO,"fare per adult is displayed in the price break down");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.WARNING,"fare per adult is not displayed in the basket");
                throw new RuntimeException("fare per adult is not displayed in the basket");
            }

            try{
                actionUtility.waitForElementVisible(taxPerAdultText,1);
                reportLogger.log(LogStatus.INFO,"tax per adult is displayed in the price break down");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.WARNING,"tax per adult is not displayed in the basket");
                throw new RuntimeException("tax per adult is not displayed in the basket");
            }
        }

        if (testData.get("noOfTeen")!= null) {
            if (!testData.get("noOfTeen").equals("0")) {
                try{
                    actionUtility.waitForElementVisible(farePerTeenText,1);
                    reportLogger.log(LogStatus.INFO,"fare per teen is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"fare per teen is not displayed in the basket");
                    throw new RuntimeException("fare per teen is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerTeenText,1);
                    reportLogger.log(LogStatus.INFO,"tax per teen is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"tax per teen is not displayed in the basket");
                    throw new RuntimeException("tax per teen is not displayed in the basket");
                }
            }else {
                try{
                    actionUtility.waitForElementVisible(farePerTeenText,1);
                    reportLogger.log(LogStatus.WARNING,"fare per teen is displayed in the price break down");
                    throw new RuntimeException("fare per teen is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"fare per teen is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerTeenText,1);
                    reportLogger.log(LogStatus.WARNING,"tax per teen is displayed in the price break down");
                    throw new RuntimeException("tax per teen is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"tax per teen is not displayed in the basket");

                }
            }
        }else {
            try{
                actionUtility.waitForElementVisible(farePerTeenText,1);
                reportLogger.log(LogStatus.WARNING,"fare per teen is displayed in the price break down");
                throw new RuntimeException("fare per teen is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"fare per teen is not displayed in the basket");
            }

            try{
                actionUtility.waitForElementVisible(taxPerTeenText,1);
                reportLogger.log(LogStatus.WARNING,"tax per teen is displayed in the price break down");
                throw new RuntimeException("tax per teen is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"tax per teen is not displayed in the basket");

            }
        }

        if (testData.get("noOfChild")!= null) {
            if (!testData.get("noOfChild").equals("0")) {
                try{
                    actionUtility.waitForElementVisible(farePerChildText,1);
                    reportLogger.log(LogStatus.INFO,"fare per child is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"fare per child is not displayed in the basket");
                    throw new RuntimeException("fare per child is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerChildText,1);
                    reportLogger.log(LogStatus.INFO,"tax per child is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"tax per child is not displayed in the basket");
                    throw new RuntimeException("tax per child is not displayed in the basket");
                }

            }else{
                try{
                    actionUtility.waitForElementVisible(farePerChildText,1);
                    reportLogger.log(LogStatus.WARNING,"fare per child is displayed in the price break down");
                    throw new RuntimeException("fare per child is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"fare per child is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerChildText,1);
                    reportLogger.log(LogStatus.WARNING,"tax per child is displayed in the price break down");
                    throw new RuntimeException("tax per child is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"tax per child is not displayed in the basket");

                }
            }
        }else{
            try{
                actionUtility.waitForElementVisible(farePerChildText,1);
                reportLogger.log(LogStatus.WARNING,"fare per child is displayed in the price break down");
                throw new RuntimeException("fare per child is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"fare per child is not displayed in the basket");
            }

            try{
                actionUtility.waitForElementVisible(taxPerChildText,1);
                reportLogger.log(LogStatus.WARNING,"tax per child is displayed in the price break down");
                throw new RuntimeException("tax per child is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"tax per child is not displayed in the basket");

            }
        }

        if (testData.get("noOfInfant")!= null) {
            if (!testData.get("noOfInfant").equals("0")) {
                try{
                    actionUtility.waitForElementVisible(farePerInfantText,1);
                    reportLogger.log(LogStatus.INFO,"fare per infant is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"fare per infant is not displayed in the basket");
                    throw new RuntimeException("fare per infant is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerInfantText,1);
                    reportLogger.log(LogStatus.INFO,"tax per infant is displayed in the price break down");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.WARNING,"tax per infant is not displayed in the basket");
                    throw new RuntimeException("tax per infant is not displayed in the basket");
                }

            }else {
                try{
                    actionUtility.waitForElementVisible(farePerInfantText,1);
                    reportLogger.log(LogStatus.WARNING,"fare per infant is displayed in the price break down");
                    throw new RuntimeException("fare per infant is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"fare per infant is not displayed in the basket");
                }

                try{
                    actionUtility.waitForElementVisible(taxPerInfantText,1);
                    reportLogger.log(LogStatus.WARNING,"tax per infant is displayed in the price break down");
                    throw new RuntimeException("tax per infant is displayed in the basket");
                }catch (TimeoutException e){
                    reportLogger.log(LogStatus.INFO,"tax per infant is not displayed in the basket");

                }
            }

        }else {

            try{
                actionUtility.waitForElementVisible(farePerInfantText,1);
                reportLogger.log(LogStatus.WARNING,"fare per infant is displayed in the price break down");
                throw new RuntimeException("fare per infant is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"fare per infant is not displayed in the basket");
            }

            try{
                actionUtility.waitForElementVisible(taxPerInfantText,1);
                reportLogger.log(LogStatus.WARNING,"tax per infant is displayed in the price break down");
                throw new RuntimeException("tax per infant is displayed in the basket");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"tax per infant is not displayed in the basket");

            }
        }

    }

    private void verifyTotalBasketCostWithPriceBreakDown(Hashtable<String,String> testData){
        // validate the basket cost with price break down

        double totalPriceBreakDownCost = 0.00;
        double overallBasketCost = 1.0;

        try{
            totalPriceBreakDownCost = Math.round(getTotalPriceBreakDownCost(testData)*100)/100.0;
            overallBasketCost = Math.round(CommonUtility.convertStringToDouble(overallBasketTotal.getText())*100)/100.0;
            Assert.assertEquals(overallBasketCost,totalPriceBreakDownCost);
            reportLogger.log(LogStatus.INFO,"price breakdown is matching with total basket price in fare selection page");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"price breakdown is not matching with total basket price in fare selection page. Basket price - "+overallBasketTotal.getText()+" total price break down cost - "+ totalPriceBreakDownCost);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to validate the total basket price against price breakdown");
            throw e;
        }

    }

    private double getTotalPriceBreakDownCost(Hashtable<String,String> testData){
        // will return the summation of price break down

        double totalCostOfPriceBreakDown=0.0;
        boolean getMoreFlag = false;

        if(testData.get("outboundFlightOption")!=null){
            if (testData.get("outboundFlightOption").equals("Get more")){
                getMoreFlag = true;
            }
        }

        if (testData.get("noOfAdult")!= null) {
            if (!testData.get("noOfAdult").equals("0")) {
                double priceBreakDownPerAdult=getPriceBreakBreakdownForEachPassengerType("adult",getMoreFlag);
                totalCostOfPriceBreakDown += (priceBreakDownPerAdult*Integer.parseInt(testData.get("noOfAdult")));
                totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
            }
        }else {
            totalCostOfPriceBreakDown += getPriceBreakBreakdownForEachPassengerType("adult",getMoreFlag);
            totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
        }

        if (testData.get("noOfTeen")!= null) {
            if (!testData.get("noOfTeen").equals("0")) {
                double priceBreakDownPerTeen = getPriceBreakBreakdownForEachPassengerType("teen", getMoreFlag);
                totalCostOfPriceBreakDown += (priceBreakDownPerTeen*Integer.parseInt(testData.get("noOfTeen")));
                totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
            }
        }

        if (testData.get("noOfChild")!= null) {
            if (!testData.get("noOfChild").equals("0")) {
                double priceBreakDownPerTeen = getPriceBreakBreakdownForEachPassengerType("child", getMoreFlag);
                totalCostOfPriceBreakDown += (priceBreakDownPerTeen*Integer.parseInt(testData.get("noOfChild")));
                totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
            }
        }

        if (testData.get("noOfInfant")!= null) {
            if (!testData.get("noOfInfant").equals("0")) {
                double priceBreakDownPerInfant = getPriceBreakBreakdownForEachPassengerType("infant", getMoreFlag);
                totalCostOfPriceBreakDown += (priceBreakDownPerInfant*Integer.parseInt(testData.get("noOfInfant")));
                totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
            }
        }
        totalCostOfPriceBreakDown = Math.round(totalCostOfPriceBreakDown*100.0)/100.0;
        return totalCostOfPriceBreakDown;
    }

    private double getPriceBreakBreakdownForEachPassengerType(String passengerType, boolean getMoreFlag){
        //will return the sum of price breakdown for the passenger type

        double getMoreCost=0;
        double totalFareForPassenger =0;


        if (getMoreFlag) {
            getMoreCost += CommonUtility.convertStringToDouble(getMoreBundleCost.getText());
        }

        switch (passengerType){
            case "adult":
                totalFareForPassenger = CommonUtility.convertStringToDouble(farePerAdult.getText())+CommonUtility.convertStringToDouble(taxPerAdult.getText())+getMoreCost;
                break;
            case "teen":
                totalFareForPassenger = CommonUtility.convertStringToDouble(farePerTeen.getText())+CommonUtility.convertStringToDouble(taxPerTeen.getText())+getMoreCost;
                break;
            case "child":
                totalFareForPassenger = CommonUtility.convertStringToDouble(farePerChild.getText())+CommonUtility.convertStringToDouble(taxPerChild.getText())+getMoreCost;
                break;
            case "infant":
                totalFareForPassenger = CommonUtility.convertStringToDouble(farePerInfant.getText())+CommonUtility.convertStringToDouble(taxPerInfant.getText());

        }

        totalFareForPassenger = Math.round(totalFareForPassenger*100.0)/100.0;
        return totalFareForPassenger;
    }

    public void verifyTheCardChargeInformationAboveFooter(){
        //to Verify the card charge information message above the footer
        String cardChargeExpected = "Flybe does not charge for the use of debit cards. Bookings made by credit card or Paypal will incur a fee of 3% of the total transaction value with a minimum charge of \u00a35 regardless of the number of passengers in a booking.";
        try{
            try{
                Assert.assertEquals(cardChargeInformation.getText().toLowerCase(),cardChargeExpected.toLowerCase(),"Card Charge Information above Footer is not matching");
                reportLogger.log(LogStatus.INFO,"Card Charge Information is displayed above Footer");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Card Charge Information is displayed above footer is not matching"+" Expected - "+cardChargeExpected+" Actual - "+ cardChargeInformation.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Card Charge Information is not displayed above Footer");
                throw e;
            }

        }catch(AssertionError e){
            throw e;
        }catch(NoSuchElementException e){
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify Card Charge Information above footer");
            throw e;
        }


    }

    public void verifyTaxesFaresRulesBaggageAndPriceBreakdownAreDisplayedInBasket() {
        // to verify the taxes, baggage rules, fare rules, price breakdown in basket
        try {
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(taxes), "Taxes is displayed in basket");
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(fareRules), "Fare rules is displayed in basket");
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(baggageRules), "Baggage rules is displayed in basket");
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(priceBreakDown), "Price breakdown is displayed in basket");
            reportLogger.log(LogStatus.INFO, "Taxes, Fare rules,Baggage rules and Price breakdown are not displayed in basket");

        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "Taxes, Fare rules,Baggage rules and Price breakdown are displayed in basket");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify if taxes, fare rules, baggage rules, price breakdown in basket");
            throw e;
        }
    }

    public void verifyMessageAdditionalAirportCharges(Hashtable<String,String> testData){
//        this method verifies whether the additional airport charges are displayed for NWI/NQY airport
        String NWIMessage = "Please note that Norwich Airport levy and collect a standalone airport development fee of GBP 10.00 per adult. There is no charge for passengers under 16 years of age. Tickets can be purchased in advance online at www.norwichinternational.com or at the airport prior to your departure and therefore this forms no part of the fares quoted on flybe.com.";
        String NQYMessage = "Please note - All flights departing from Newquay Airport for travel until 31MAR16 are subject to a GBP5.00 development fee, per passenger. This is an airport charge and will be collected at the airport. This is not a Flybe charge and has not been added to the cost of your booking.";
        String messageExpected = null;
        try{
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
            if(testData.get("flightFrom").equals("NQY")){
                messageExpected = NQYMessage;
            }else if (testData.get("flightFrom").equals("NWI")){
                messageExpected = NWIMessage;
            }
            Assert.assertEquals(additionalAirportChargeOutBound.getText().toLowerCase().trim(),messageExpected.toLowerCase().trim());
            reportLogger.log(LogStatus.INFO,"additional airport charge text is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"additional airport charge text is not matching Expected - "+messageExpected+" Actual - "+additionalAirportChargeOutBound.getText());
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify additional airport charge");
            throw e;
        }
    }

    public void verifyLowestFareIsDisplayedInFareSliderOutbound(){
//        this verifies whether lowest fare in the outbound fare slider is matching with the lowest fare in the flight list displayed
        double lowestFareFromFareSlider=0.0,min=0.0;
        try{
            actionUtility.waitForElementVisible(outboundFlightList, 10);
            actionUtility.waitForElementNotPresent(outboundFlightLoader, 75);
            try{
                actionUtility.waitForElementVisible(lowestPriceDisplayedInBound,5);
                reportLogger.log(LogStatus.INFO,"return fares are displayed");
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.WARNING,"return fares are not displayed");
                throw e;
            }
            lowestFareFromFareSlider = CommonUtility.convertStringToDouble(lowestPriceDisplayedOutBound.getText());
            lowestFareFromFareSlider = Math.round(lowestFareFromFareSlider*100.0)/100.0;
            min = CommonUtility.convertStringToDouble(outBoundFares.get(0).getText());
            min = Math.round(min*100.0)/100.0;
            for(int i = 1; i< outBoundFares.size(); i++){

                double temp = CommonUtility.convertStringToDouble(outBoundFares.get(i).getText());
                temp = Math.round(temp*100.0)/100.0;
                min = Math.min(min,temp);
            }
            Assert.assertEquals(lowestFareFromFareSlider,min,"lowest fare in the fare slider is not matching with the lowest fare displayed");
            reportLogger.log(LogStatus.INFO,"fare in the fare slider is matching with the lowest fare displayed in the list of flights");
        }catch (TimeoutException e){
            throw e;
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"fare in the fare slider is not matching with the lowest fare displayed in the list of flights Expected - "+min+ " Actual - "+lowestFareFromFareSlider);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify fare displayed in the fare slider is matching with the lowest fare displayed in the list of flights");
            throw e;
        }

    }

    public void toVerifyTheDefaultCurrencyType(String CurrencyType){
        // to Verify the default currency
        try{
            Assert.assertEquals(currencySelected.getText(), CurrencyType, "Default currency type is not matching");
            reportLogger.log(LogStatus.INFO, "Default " + CurrencyType + " currency type is matching ");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING, "Default currency type is not matching" + " Expected - " + CurrencyType + " Actual - " + currencySelected.getText());
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify default currency type");
            throw e;
        }
    }

    public void verifyGetMoreBundleCostForRouteJER_GCI(){
//        verifies whether get more bundle cost is 6.50 or not for blue islands flight
        double bundleCostActual = 0.0;
        double bundleCostExpected = 6.5;
        try{
            actionUtility.click(priceBreakDown);
            actionUtility.waitForElementVisible(getMoreBundleCost,5);
            bundleCostActual= CommonUtility.convertStringToDouble(getMoreBundleCost.getText());
            bundleCostActual = Math.round(bundleCostActual*100.0)/100.0;
            Assert.assertEquals(bundleCostActual,bundleCostExpected,"get more bundle cost is not matching");
            reportLogger.log(LogStatus.INFO,"get more bundle cost is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"get more bundle cost is not matching Expected - "+bundleCostExpected+" Actual - "+bundleCostActual);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify get more bundle cost in route JER-GCI");
            throw e;
        }
    }

    public int selectNoOfPassengers(Hashtable<String,String> testData){
        //select the no.of passengers
        try {
            int noOfPassengers=1;
            actionUtility.waitForElementVisible(openSelectPassengerMenu,30);
            actionUtility.click(openSelectPassengerMenu);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;
            if (testData.get("noOfTeenFareSelect") != null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeenFareSelect"));

                noOfPassengers += noOfTeen;
                if(noOfTeen == 8)
                {
                    selectPassengers("teens",noOfTeen,Integer.parseInt(testData.get("noOfAdultFareSelect")));
                }
                else{
                    selectPassengers("teens",noOfTeen);
                }
            }

            if (testData.get("noOfAdultFareSelect") != null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdultFareSelect"));
                selectPassengers("adults", noOfAdult);
                noOfPassengers += (noOfAdult-1);
            }

            if (testData.get("noOfChildFareSelect") != null) {
                noOfChild = Integer.parseInt(testData.get("noOfChildFareSelect"));
                selectPassengers("children", noOfChild);
                noOfPassengers += noOfChild;
            }
            if (testData.get("noOfInfantFareSelect") != null) {
                selectPassengers("infants", Integer.parseInt(testData.get("noOfInfantFareSelect")));
            }

            reportLogger.log(LogStatus.INFO,"successfully selected all types of passengers");
            reportLogger.log(LogStatus.INFO,"Adults - "+noOfAdult+" Teens - "+noOfTeen+" Children - "+noOfChild+" Infant - "+noOfInfant);

            return noOfPassengers;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the number of passengers");
            throw e;
        }
    }

    private void selectPassengers(String passengerType, int noOfPassenger, int... adultPassenger){
        // selects the no. of passengers
        String passengerNos ;
        boolean flag = true;
        int attempt =0;
        boolean attemptFlag = false;
        while(attempt<3 && attemptFlag == false) {
            try {
                if (!actionUtility.verifyIfElementIsDisplayed(passengerMenu)) {
                    actionUtility.click(openSelectPassengerMenu);

                }

                switch (passengerType) {
                    case ("adults"):
                        while (flag) {

                            actionUtility.waitForElementVisible(passengerMenu,3);
                            passengerNos = adultNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(adultIncrement);

                            } else if (Integer.parseInt(passengerNos) > noOfPassenger) {
                                actionUtility.click(adultDecrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("teens"):
                        int counter = 0;
                        while (flag) {
                            actionUtility.waitForElementVisible(passengerMenu,3);
                            passengerNos = teenNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(teenIncrement);
                                counter++;
                                if (counter == 7)
                                    if (adultPassenger.length > 0)
                                        if (adultPassenger[0] == 0)
                                            actionUtility.click(adultDecrement);//Decrement adult when there are 8 teens
                            } else {
                                flag = false;
                            }

                        }
                        attemptFlag = true;
                        break;

                    case ("children"):
                        while (flag) {
                            actionUtility.waitForElementVisible(passengerMenu,3);
                            passengerNos = childrenNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(childIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("infants"):
                        while (flag) {
                            actionUtility.waitForElementVisible(passengerMenu,3);
                            passengerNos = infantNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(infantIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;
                }
            } catch (Exception e) {

                attempt++;
            }
            System.out.println("from while for passenger - "+attempt);
        }
        if (!attemptFlag){
            throw  new RuntimeException("unable to select the passengers");
        }
    }

    public void verifyMessageUnaccompaniedMinorForChild() {
        //verifies the message displayed only teen is traveling from/to Italy
        String messageDisplayed =null;
        String messageTobeDisplayed = null;
        try{
            actionUtility.hardSleep(1000);
            messageDisplayed = unaccompaniedChildMessage.getText();
            messageTobeDisplayed = "Trying to book for an unaccompanied child? Visit our";

            System.out.println(messageDisplayed.toLowerCase());
            Assert.assertTrue(messageDisplayed.toLowerCase().contains(messageTobeDisplayed.toLowerCase()),"wrong message displayed for unaccompanied child traveling");
            reportLogger.log(LogStatus.INFO,"right message is displayed for unaccompanied child traveling");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message is displayed for unaccompanied child traveling "+"Expected - "+messageTobeDisplayed+" Actual - "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message");
            throw e;
        }
    }

    public void verifyPaxIsNotDisplayedInPriceBreakDown(String paxType){
//        verifies the paxType passed is not present in price breakdown
        WebElement paxTypeRequired = null;
        switch (paxType.toLowerCase()){
            case "adult":
                paxTypeRequired = farePerAdult;
                break;
            case "teen":
                paxTypeRequired = farePerTeen;
                break;
            case "child":
                paxTypeRequired = farePerChild;
                break;
            case "infant":
                paxTypeRequired = farePerInfant;
                break;
        }

        try{
            actionUtility.waitForElementClickable(priceBreakDown,10);
            actionUtility.click(priceBreakDown);
            actionUtility.hardSleep(3000);
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(paxTypeRequired));
            reportLogger.log(LogStatus.INFO,paxType+" is not displayed in price breakdown");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,paxType+" is displayed in price breakdown");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify "+paxType+" in price break down");
            throw e;
        }
    }

    public void selectLowestPriceDate(double price, String tripType){
        //selects the available date of the lowest price in the 3 months
        WebElement monthView = outboundMonthView;
        WebElement monthViewMonthSelected = outboundMonthViewMonthSelected;
        List<WebElement> monthViewDates = outboundMonthViewDates;
        List<WebElement> monthViewPrices = outboundMonthViewPrices;
        WebElement monthViewNextMonth = outboundMonthViewNextMonth;
        int j = 0;
        if(tripType.toLowerCase().equals("inbound")){
            monthView = returnMonthView;
            monthViewMonthSelected = returnMonthViewMonthSelected;
            monthViewDates = returnMonthViewDates;
            monthViewPrices = returnMonthViewPrices;
            monthViewNextMonth = returnMonthViewNextMonth;
            j = 1;
        }
        try {
            int monthCount = 0;
            boolean flag = false;
            actionUtility.waitForElementVisible(monthView, 10);
            actionUtility.click(monthView);
            while (monthCount < 3) {
                actionUtility.waitForElementVisible(monthViewMonthSelected, 8);
                double lowestDatePrice=5000.0;
                String priceText;
                for (int i = j; i < monthViewDates.size(); i++) {
                    priceText = monthViewPrices.get(i).getText();
                    if (!priceText.contains("-")) {
                        lowestDatePrice = CommonUtility.convertStringToDouble(priceText);
                    }
                    if (lowestDatePrice <= price) {
                        actionUtility.click(monthViewDates.get(i));
                        flag = true;
                        break;
                    }
                }
                if (flag == false) {
                    actionUtility.click(monthViewNextMonth);
                    actionUtility.hardSleep(3000);
                    monthCount++;
                }else {
                    break;
                }
            }
            if(flag == false){
                throw new RuntimeException("lowest fare date is not available for next 3 months");
            }
            reportLogger.log(LogStatus.INFO,"successfully selected the lowest fare date");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the lowest fare date");
            throw e;
        }
    }

    public void toSelectLowestFlightAvailableForDaySelected(String tripType){
//        this method will selects the flight which has lowest fare for that particular day
        double lowestFareFromFareSlider = 0.0;
        boolean flag = false;
        List<WebElement> flights = allOutboundFlights;
        WebElement lowestPriceDisplayed = lowestPriceDisplayedOutBound;
        List<WebElement> faresList = outBoundFares;
        WebElement flightList = outboundFlightList;
        String flightLoader = outboundFlightLoader;
        if(tripType.toLowerCase().equals("inbound")){
            flights = allReturnFlights;
            lowestPriceDisplayed = lowestPriceDisplayedInBound;
            faresList = inBoundFares;
            flightList = inboundFlightList;
            flightLoader = inboundFlightLoader;
        }
        try {
            actionUtility.waitForElementVisible(flightList, 10);
            actionUtility.waitForElementNotPresent(flightLoader, 75);
            lowestFareFromFareSlider = CommonUtility.convertStringToDouble(lowestPriceDisplayed.getText());
            lowestFareFromFareSlider = Math.round(lowestFareFromFareSlider*100.0)/100.0;
            for(int i = 0; i< faresList.size(); i++){
                double temp = CommonUtility.convertStringToDouble(faresList.get(i).getText());
                temp = Math.round(temp*100.0)/100.0;
                if((temp == lowestFareFromFareSlider)&&(flights.get(i).isEnabled())){
                    actionUtility.hardSelectOption(flights.get(i));
                    flag = true;
                    break;
                }
            }
            if(!flag){
                reportLogger.log(LogStatus.WARNING,"unable to select the lowest flight available");
                throw new RuntimeException("unable to select the lowest flight available");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the flight with the lowest fare for the selected date");
            throw e;
        }
    }


    public void verifyPriceDifferenceBtwnGetMoreAndJustFly(){
        double actualPrice=0.0;
        try{
            double justFlyPrice = CommonUtility.convertStringToDouble(outboundJustFlyPrice.getText());
            double getMoreFlyPrice = CommonUtility.convertStringToDouble(outboundGetMorePrice.getText());
            actualPrice = getMoreFlyPrice-justFlyPrice;
            Assert.assertEquals(actualPrice,25.00);
            reportLogger.log(LogStatus.INFO,"price difference between Get more & Just flye is 25 ");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"price difference between Get more & Just fly is not 25 Expected - 25.00 Actual - "+ actualPrice);
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the price difference between get more & just fly");
            throw e;
        }
    }


    public void verifyFlightBookingStepsAreDisplayed(){
        try{
            Assert.assertEquals(flightBookingSteps.size(),4,"flight booking steps are not displayed");
            reportLogger.log(LogStatus.INFO,"flight booking steps are not displayed");
        }catch (AssertionError e){

            reportLogger.log(LogStatus.WARNING,"flight booking steps are not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the flight booking steps are displayed");
            throw e;
        }

    }

    public void navigateToPLDPayment()
    {
        // clicks on Price Lock-Down button after selecting the  flights
        try {
            if (pldButton.isEnabled()) {
                actionUtility.click(pldButton);
                actionUtility.waitForPageLoad(10);
                reportLogger.log(LogStatus.INFO, "proceeded to payment");
            } else {
                reportLogger.log(LogStatus.WARNING, "Price Lock-Down button is disabled");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Price Lock-Down button is not displayed to proceed to payment");
            throw e;
        }
    }

    public void navigateToHomePage(){
//        navigates to home page by clicking on flybe main logo
        try{
            actionUtility.waitForElementVisible(flybeMainHeaderLogo,3);
            actionUtility.click(flybeMainHeaderLogo);
            reportLogger.log(LogStatus.INFO,"successfully navigated to home page");
            actionUtility.waitForPageLoad(20);
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to home page");
            throw e;
        }
    }





}

