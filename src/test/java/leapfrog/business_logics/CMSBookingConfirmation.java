package leapfrog.business_logics;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSBookingConfirmation {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSBookingConfirmation() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();

    }

    @FindBy(css = "h2 span")
    private WebElement referenceNumber;

    @FindBy(css = "a.miniconf")
    private WebElement addApiLink;

    @FindBy(css = "p.screen>a>img")
    private WebElement addApiButton;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr/td[contains(text(),'Operated by')]")
    private List<WebElement> operatedBy;

    @FindBy(xpath = "//table[@class='booking_details_table1']/descendant::tr[not(@bgcolor)]/td[@valign='top'and contains(text(),'Operated by')]")
    private List<WebElement> flightName;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr[not(@bgcolor)]/td[2]")
    private List<WebElement> flightNo;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr[not(@bgcolor)]/td[2]/preceding-sibling::td")
    private List<WebElement> flightDates;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr/td[contains(text(),'Operated by')]/a")
    private List<WebElement> flightOptions;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr[not(@bgcolor)]/td[3]")
    private List<WebElement> flightRoute;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr[not(@bgcolor)]/td[4]")
    private List<WebElement> flightDepartTimes;

    @FindBy(xpath = "//table[@class='booking_details_table1']//tr[not(@bgcolor)]/td[5]")
    private List<WebElement> flightArriveTimes;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/td[1]")
    private List<WebElement> passengerFlightNos;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/td[2]")
    private List<WebElement> flightFrom;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/td[3]")
    private List<WebElement> flightTo;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/td[4]")
    private List<WebElement> seats;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/td[5]")
    private List<WebElement> passengerBaggage;

    @FindBy(xpath = "//table[@class='booking_details_table2']//tr[not(@bgcolor)]/th[(@align)]")
    private List<WebElement> passengerNames;

    @FindBy(xpath = "//td[contains(text(),'TRANSACTION')]/strong")
    private WebElement currency;

    @FindBy(className ="loading-spinner")
    private WebElement checkInLoader;

    @FindBy(xpath = "//table[@class='booking_details_table2']/tbody/tr[4]/td[9]/a")
    private WebElement changeFlightInItinerary;

    //    -------------car hire--------------
    @FindBy(xpath = "//td[@class='title']")
    private WebElement carHireTitle;

    @FindBy(xpath = "//td[@class='title']/following-sibling::td[@align='right']")
    private WebElement carHireImage;

    @FindBy(xpath="//td[@class='title']/following-sibling::td[@align='right']/img[@alt='Avis car hire']")
    private WebElement carHireImageAvis;

    @FindBy(xpath="//td[@class='title']/following-sibling::td[@align='right']/img[@alt='Budget car hire']")
    private WebElement carHireImageBudget;

    @FindBy(xpath="//td[contains(text(),'Confirmation number')]")
    private WebElement carHireConfirmationNumber;

    @FindBy(xpath = "//td[contains(text(),'Rental Station')]")
    private WebElement carHireRentalStation;

    @FindBy(xpath="//td[contains(text(),'Transmission')]")
    private WebElement transmission;

    @FindBy(xpath = "//td[contains(text(),'Return')]/following-sibling::td[2]")
    private WebElement returnDate;

    @FindBy(xpath = "//td[contains(text(),'Doors')]")
    private WebElement noOfDoors;

    @FindBy(xpath="//td[contains(text(),'Collect')]/following-sibling::td[2]")
    private WebElement hireDate;

    @FindBy(xpath = "//div[@class='conf_section']/table/descendant::td[contains(text(),'Total')]")
    private WebElement carHireAmount;

    @FindBy(xpath = "//td[contains(text(),'TRANSACTION AMOUNT')]/strong")
    private WebElement transactionAmount;

    @FindBy(xpath = "//td[contains(text(),'Paid using')]")
    private WebElement paymentMode;

    //    ---------------------------car parking------------------------

    @FindBy(xpath="//td[contains(text(),'Your parking confirmation summary')]")
    private WebElement carParkingTitle;

    @FindBy(xpath = "//td[contains(text(),'Booking reference:')]")
    private WebElement carParkingReference;

    @FindBy(xpath = "//td[contains(text(),'Car Park')]")
    private WebElement carParkType;

    @FindBy(xpath = "//td[contains(text(),'From:')]/following-sibling::td[2]")
    private WebElement carParkingFromDate;

    @FindBy(xpath = "//td[contains(text(),'To:')]/following-sibling::td[2]")
    private WebElement carParkingToDate;

    @FindBy(xpath = "//td[contains(text(),'Car registration:')]/following-sibling::td[2]")
    private WebElement carParkingRegNo;

    @FindBy(xpath = "//td[contains(text(),'Booking Name')]/following-sibling::td[2]")
    private WebElement carParkingBookerName;


    //    -------------------avios--------------
    @FindBy(id = "aviosawarded")
    private WebElement aviosAwardedSection;

    @FindBy(css = "#aviosawarded > p")
    private  WebElement aviosAwardedAmount;

    @FindBy(xpath = "//td[contains(text(),'Your discount using Avios')]/strong")
    private WebElement aviosDiscountPrice;



//    String loadingLocator ="//div[@class='loading pu']";

    String loadingLocator ="//div[span[@class='loading-spinner-large']][@class='flights-loading']";
    String checkInLoaderIndicator = "//span[text()='Loading...']";


    public void verifySecondSectorBagageForMSCFlights(Hashtable<String,String> testData) {
        try {
            Assert.assertTrue(passengerBaggage.get(1).getText().equals(testData.get("passengerBag1")));
            reportLogger.log(LogStatus.INFO, "baggage for flight-2 in booking confirmation is matching");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "baggage for flight-2 in booking confirmation is not matching");
            throw e;
        }
    }


    public void verifyCurrency(String currencyToBeVerified){
        // verify the currency displayed
        try{
            Assert.assertTrue(currency.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()),"currency is not matching");
            reportLogger.log(LogStatus.INFO,currencyToBeVerified + "currency successfully verified");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,currencyToBeVerified + "currency is not matching Expected - "+currencyToBeVerified+" Actual - "+currency.getText());
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency");
            throw e;
        }
    }

    public void verifyBookingDetails(Hashtable<String,String>testData, Hashtable<String,String> selectedFlightDetails, Hashtable<String,String>capturedPassengerNames, int noOfFlights, int noOfPassengers, boolean baggageEditFlag, Hashtable<String,Hashtable<String,Hashtable>>... seatSelected){
        //verifies the booking details
        try {
            verifyOverallFlightDetails(selectedFlightDetails, noOfFlights);
            String baggage;
            for (int i = 0; i < noOfPassengers; i++) {

                verifyPassengerName(capturedPassengerNames,i);

                if(baggageEditFlag){
                    baggage = testData.get("editedPassengerBag" + (i + 1));
                }else {
                    baggage = testData.get("passengerBag" + (i + 1));
                }
                if(seatSelected.length>0) {
                    verifyFlightDetailsForEachPassenger(selectedFlightDetails, i, noOfFlights, baggage, seatSelected[0]);
                }else {
                    verifyFlightDetailsForEachPassenger(selectedFlightDetails, i, noOfFlights, baggage);
                }
            }
            reportLogger.log(LogStatus.INFO,"all the booking details in booking confirmation page are matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"booking details in booking confirmation page are not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the details in the booking confirmation page");
            throw e;
        }
    }

    private void verifyPassengerName(Hashtable<String,String>capturedPassengerNames,int passengerNo){
        //verifies the passenger Name
        String[] expectedPassengerName = capturedPassengerNames.get("passengerName"+(passengerNo+1)).split(" ");
        String [] expectedInfantName= null;
        if (capturedPassengerNames.get("infantName"+(passengerNo+1))!=null){
            expectedInfantName = capturedPassengerNames.get("infantName"+(passengerNo+1)).split(" ");
        }

        String[] actualPassengerNames = passengerNames.get(passengerNo).getText().toLowerCase().trim().split(",");
        try {
            Assert.assertTrue(actualPassengerNames[0].trim().contains(expectedPassengerName[0].trim().toLowerCase()));
            Assert.assertTrue(actualPassengerNames[0].trim().contains(expectedPassengerName[1].trim().toLowerCase()));
            Assert.assertTrue(actualPassengerNames[0].trim().contains(expectedPassengerName[2].trim().toLowerCase()));
            reportLogger.log(LogStatus.INFO, "passenger name is matching");
        }catch ( AssertionError e){

            reportLogger.log(LogStatus.WARNING,"passenger name is not matching Expected - "+expectedPassengerName[0]+" "+expectedPassengerName[1]+" "+expectedPassengerName[2]+" Actual - "+actualPassengerNames[0].trim());
            throw e;
        }

        if(expectedInfantName != null){
            try {
                Assert.assertTrue(actualPassengerNames[1].trim().contains(expectedInfantName[0].trim().toLowerCase()));
                Assert.assertTrue(actualPassengerNames[1].trim().contains(expectedInfantName[1].trim().toLowerCase()));
                Assert.assertTrue(actualPassengerNames[1].trim().contains(expectedInfantName[2].trim().toLowerCase()));
                reportLogger.log(LogStatus.INFO, "passenger name for infant is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "passenger name for infant is not matching Expected - " + expectedInfantName[0] + " " + expectedInfantName[1] + " " + expectedInfantName[2] + " Actual - " + actualPassengerNames[0].trim());
                throw e;
            }
        }

    }

    @Deprecated
    private void verifyFlightDetailsForEachPassenger2(Hashtable<String,String>selectedFlightDetails,int passenger,int noOFFlights,String baggage,Hashtable<String,Hashtable>...seatSelected) {
        // verifies the passenger details----This method is not used
        int flightCount = noOFFlights * (passenger);
        for (int i = 0; i < noOFFlights; i++) {
            try {
                Assert.assertEquals(selectedFlightDetails.get("flightNo" + (i + 1)).trim(), passengerFlightNos.get(flightCount).getText().trim());

            }catch (AssertionError e){

            }

            Assert.assertEquals(selectedFlightDetails.get("flightDepartTime" + (i + 1)).substring(7, 10).trim(), flightFrom.get(flightCount).getText().trim());
            Assert.assertEquals(selectedFlightDetails.get("flightArriveTime" + (i + 1)).substring(7, 10).trim(), flightTo.get(flightCount).getText().trim());

            if(baggage!=null) {

                Assert.assertTrue(passengerBaggage.get(passenger).getText().toLowerCase().contains( baggage.toLowerCase()));

            }else {

                Assert.assertTrue(passengerBaggage.get(passenger).getText().contains("0Kg"));
            }

            if(seatSelected.length>0){


                if(seatSelected[0].get("Passenger"+(passenger+1)).get("flight"+(i+1)).toString()!="--") {
                    Assert.assertTrue((seats.get(flightCount).getText().contains(seatSelected[0].get("passenger" + (passenger + 1)).get("flight" + (i + 1)).toString())));
                }

            }

            flightCount++;
        }
    }

    private void verifyFlightDetailsForEachPassenger(Hashtable<String,String>selectedFlightDetails,int passenger,int noOFFlights,String baggage,Hashtable<String,Hashtable<String,Hashtable>>...seatSelected) {
        //verifies the passenger details
        int flightCount = noOFFlights * (passenger);
        int count=0;
        for (int i = 0; i < noOFFlights; i++) {

            try{
                Assert.assertEquals(selectedFlightDetails.get("flightNo" + (i + 1)).trim(), passengerFlightNos.get(flightCount).getText().trim(),"Selected flights numbers matching");
                reportLogger.log(LogStatus.INFO, "flight no. of flight-"+(i+1)+" in booking confirmation for passenger-" + (passenger + 1) + " is matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING, "flight no. of flight-"+(i+1)+" in booking confirmation for passenger-" + (passenger + 1) + " is not matching Expected - "+selectedFlightDetails.get("flightNo" + (i + 1)).trim() + " Actual - "+passengerFlightNos.get(flightCount).getText().trim());
                throw e;
            }
            try{
                Assert.assertEquals(selectedFlightDetails.get("flightDepartTime" + (i + 1)).substring(7, 10).trim(), flightFrom.get(flightCount).getText().trim());
                reportLogger.log(LogStatus.INFO, "flight depart time of flight-"+(i+1)+" in booking confirmation for passenger-" + (passenger + 1) + " is matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING, "flight depart time of flight-" + (i+1) + " in booking confirmation for passenger-"+(passenger + 1)+" is not matching Expected - "+selectedFlightDetails.get("flightDepartTime" + (i + 1)).substring(7, 10).trim() + " Actual - "+flightFrom.get(flightCount).getText().trim());
                throw e;
            }
            try{
                Assert.assertEquals(selectedFlightDetails.get("flightArriveTime" + (i + 1)).substring(7, 10).trim(), flightTo.get(flightCount).getText().trim());
                reportLogger.log(LogStatus.INFO,"flight arrival time of flight-" + (i+1) + " in booking confirmation for passenger-"+(passenger + 1)+" is matching");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"flight arrival time of flight-" + (i+1) + " in booking confirmation for passenger-"+(passenger + 1)+" is not matching Expected - "+selectedFlightDetails.get("flightArriveTime" + (i + 1)).substring(7, 10).trim() + " Actual - "+flightTo.get(flightCount).getText().trim());
                throw e;
            }




            if(baggage!=null) {
                try{
                    Assert.assertTrue(passengerBaggage.get(flightCount).getText().toLowerCase().contains( baggage.toLowerCase()));
//                    Assert.assertEquals(passengerBaggage.get(passenger).getText().toLowerCase(),baggage.toLowerCase());
                    reportLogger.log(LogStatus.INFO, "baggage for flight-"+(i+1)+" in booking confirmation for Passenger-"+(passenger + 1)+" is matching");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "baggage for flight-"+(i+1)+" in booking confirmation for Passenger-"+(passenger + 1)+" is not matching Expected - "+baggage.toLowerCase()+" Actual - "+passengerBaggage.get(flightCount).getText().toLowerCase());
                    throw e;
                }
            }else {
                try{
                    Assert.assertTrue(passengerBaggage.get(flightCount).getText().contains("0Kg"));
//                    Assert.assertEquals(passengerBaggage.get(passenger).getText().toLowerCase(),"0kg");
                    reportLogger.log(LogStatus.INFO, "baggage for flight-"+(i+1)+" in booking confirmation for Passenger-"+(passenger + 1)+" is matching");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "baggage for flight-"+(i+1)+" in booking confirmation for Passenger-"+(passenger + 1)+" is not matching Expected - 0kg Actual - "+passengerBaggage.get(flightCount).getText().toLowerCase());
                    throw e;
                }

            }

            if(seatSelected.length>0){

                String expectedSeat = null;
                if (seatSelected[0].get("passenger" + (passenger + 1)).get("outbound").get("flight" + (i + 1))!=null) {
                    expectedSeat = seatSelected[0].get("passenger" + (passenger + 1)).get("outbound").get("flight" + (i + 1)).toString();
                }else {
                    expectedSeat = seatSelected[0].get("passenger" + (passenger + 1)).get("inbound").get("flight" + (count + 1)).toString();
                    count++;
                }

                if(!expectedSeat.trim().equals("--")) {
                    try {
                        System.out.println(seats.get(flightCount).getText());
                        System.out.println(expectedSeat);
                        Assert.assertTrue((seats.get(flightCount).getText().trim().contains(expectedSeat.trim())));
                        reportLogger.log(LogStatus.INFO, "seat for flight-"+(i+1)+" in booking confirmation for Passenger-"+(passenger + 1)+" is matching");
                    }catch (AssertionError e){
                        reportLogger.log(LogStatus.WARNING,"seat for flight-" + (i+1) + " in booking confirmation for passenger-"+(passenger + 1)+" is not matching Expected - "+expectedSeat.trim()+ " Actual - "+seats.get(flightCount).getText().trim().substring(0,2));
                        throw e;
                    }
                }
            }
            flightCount++;
        }
    }

    private void verifyOverallFlightDetails(Hashtable<String,String>selectedFlightDetails, int noOFFlights){
        for(int i=1; i<=noOFFlights; i++) {
            try{
                Assert.assertEquals(flightDates.get(i-1).getText(),selectedFlightDetails.get("flightDate"+i), "flight date is not matching");
                reportLogger.log(LogStatus.INFO,"flight date for flight-"+ (i) + " in the booking confirmation is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"flight date for flight-"+ (i) + " in the booking confirmation is not matching Expected - "+selectedFlightDetails.get("flightDate"+i)+" Actual - "+flightDates.get(i-1).getText());
                throw e;
            }

            if(selectedFlightDetails.get("flightOption"+i)!=null){
                try{
                    Assert.assertEquals(flightOptions.get(i-1).getText().toLowerCase(),selectedFlightDetails.get("flightOption"+i).toLowerCase(), "flight option is not matching");
                    reportLogger.log(LogStatus.INFO,"flight option for flight-"+ (i) + " in the booking confirmation is matching");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"flight date for flight-"+ (i) + " in the booking confirmation is not matching Expected - "+selectedFlightDetails.get("flightOption"+i).toLowerCase()+" Actual - "+flightOptions.get(i-1).getText().toLowerCase());
                    throw e;
                }
            }

            try {
                System.out.println(flightName.get(i - 1).getText().toLowerCase());
                System.out.println(selectedFlightDetails.get("flightName" + i).toLowerCase());
                Assert.assertTrue(flightName.get(i - 1).getText().toLowerCase().contains(selectedFlightDetails.get("flightName" + i).toLowerCase()), "flight name in flight detail section is not matching");
                reportLogger.log(LogStatus.INFO, "flight name for flight-" + (i) + " in flight details section, in booking confirmation is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight name for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + selectedFlightDetails.get("flightName" + i).toLowerCase() + " Actual - " + flightName.get(i - 1).getText().toLowerCase());
                throw e;
            }

            try {
                Assert.assertTrue(selectedFlightDetails.get("flightNo" + i).contains(flightNo.get(i - 1).getText()), "flight no. in flight detail section is not matching");
                reportLogger.log(LogStatus.INFO, "flight No for flight-" + (i) + " in flight details section, in booking confirmation is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight No for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + selectedFlightDetails.get("flightNo" + i) + " Actual - " + flightNo.get(i - 1).getText());
                throw e;
            }


            String flightDepartTime = selectedFlightDetails.get("flightDepartTime" + i).split(" ")[0];
            String flightArriveTime = selectedFlightDetails.get("flightArriveTime" + i).split(" ")[0];

            try {
                Assert.assertEquals(flightArriveTimes.get(i - 1).getText().toLowerCase(), flightArriveTime, "flight arrival time in flight detail section is not matching");
                reportLogger.log(LogStatus.INFO, "Flight arrival time  for flight-" + (i) + " in flight details section, in booking confirmation is matching");

            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight arrival time for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + flightArriveTime + " Actual - " + flightArriveTimes.get(i - 1).getText().toLowerCase());
                throw e;
            }

            try {
                Assert.assertEquals(flightDepartTimes.get(i - 1).getText().toLowerCase(), flightDepartTime, "flight depart time in flight detail section is not matching");
                reportLogger.log(LogStatus.INFO, "Flight depart time  for flight-" + (i) + " in flight details section, in booking confirmation is matching");

            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight depart time for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + flightDepartTime + " Actual - " + flightDepartTimes.get(i - 1).getText().toLowerCase());
                throw e;
            }

            /// Needs to write try catch block
            String[] actualRoute = flightRoute.get(i - 1).getText().split(" to ");

            try {

                Assert.assertEquals(actualRoute[0].trim(), selectedFlightDetails.get("flightSource" + i), "flight source in flight detail section is not matching");
                reportLogger.log(LogStatus.INFO, "flight source  for flight-" + (i) + " in flight details section, in booking confirmation is matching");

            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight source for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + selectedFlightDetails.get("flightSource" + i) + " Actual - " + actualRoute[0].trim());
                throw e;
            }

            try {
                Assert.assertEquals(actualRoute[1].trim(), selectedFlightDetails.get("flightDestination" + i));
                reportLogger.log(LogStatus.INFO, "flight destination  for flight-" + (i) + " in flight details section, in booking confirmation is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "flight destination for flight-" + (i) + " in flight details section, in booking confirmation is not matching Expected - " + selectedFlightDetails.get("flightDestination" + i) + " Actual - " + actualRoute[1].trim());
                throw e;
            }
        }
    }

    public String verifyBookingReferenceNumber(){
        // verifies the booking reference no.
        try{

            actionUtility.waitForPageLoad(30);
            actionUtility.waitForElementNotPresent(loadingLocator, 70);
            actionUtility.waitForPageLoad(30);

            int attempt =0;
            boolean isDisplayed = false;
            while((!isDisplayed)&& attempt<1){
                try {
                    actionUtility.waitForElementVisible(referenceNumber, 35);
                    isDisplayed = true;
                }catch (TimeoutException e){

                }
                catch (NullPointerException e){}
                attempt++;
            }
            if(isDisplayed){
                reportLogger.log(LogStatus.INFO,"successfully booked the ticket with refno: "+referenceNumber.getText());
            }else {
                reportLogger.log(LogStatus.WARNING,"unable to book the ticket");
                throw new RuntimeException("unable to book the ticket");
            }

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to check if the ticket is booked");
            throw e;
        }
        return referenceNumber.getText();
    }

    public void verifyAddApiInfoIsDisplayed(boolean flag){
        // verifies the addApiInfo option is displayed
        try{
            Assert.assertTrue(addApiLink.isDisplayed(),"add API option is not displayed");
            Assert.assertTrue(addApiButton.isDisplayed(),"add API option is not displayed");
            if (flag) {
                reportLogger.log(LogStatus.INFO, "add API option is displayed");
            }else {
                reportLogger.log(LogStatus.WARNING,"add API option is displayed");
                throw new RuntimeException("API option is displayed");
            }
        }catch (NoSuchElementException e){
            if(flag) {
                reportLogger.log(LogStatus.WARNING, "add API option is not displayed");
                throw e;
            }else {
                reportLogger.log(LogStatus.INFO,"add API option is not displayed displayed");
            }
        }
    }

    public void ContinueToCheckInThroughAddAPILink() {
        //clicks on the addAPI link and then switches to frame in the check in page
        try {
            boolean flag = true;
            int attempt = 0;
            actionUtility.click(addApiLink);
            while (flag && attempt<3) {
                actionUtility.waitForPageLoad(25);
                try{
                    actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                    flag =false;
                }catch (TimeoutException e){
                    actionUtility.hardSleep(5000);
                }
                attempt++;
            }
            if(!flag) {
                actionUtility.hardSleep(2000);

                try {
                    actionUtility.waitForElementVisible(checkInLoader, 15);
                }catch (TimeoutException e){}

                int loadChecker=0;
                while (actionUtility.verifyIfElementIsDisplayed(checkInLoader) && loadChecker<3) {
                    try {
                        actionUtility.waitForElementNotPresent("//span[@class='loading-spinner']", 30);

                    }catch (TimeoutException e){
                        loadChecker++;
                    }
                }
                actionUtility.waitForElementNotPresent(checkInLoaderIndicator,75);
                reportLogger.log(LogStatus.INFO, "unable to navigate to the check-in throw Add API Link");
            }else {
                reportLogger.log(LogStatus.INFO, "successfully navigated to the check-in throw Add API Link");
                throw new RuntimeException("unable to navigate to the check-in throw Add API Link");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the check-in throw Add API Link");
            throw e;
        }
    }

    public void verifyTotalAmount(double expectedAmount){
        //verifies the total transaction amount
        double actualAmount =-1.0;
        if(expectedAmount!=0) {
            try {
                actualAmount = CommonUtility.convertStringToDouble(transactionAmount.getText());
                Assert.assertEquals(actualAmount, expectedAmount, "total amount in booking confirmation page is not matching");
                reportLogger.log(LogStatus.INFO, "total amount in booking confirmation page is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total amount in booking confirmation page is not matching Expected - " + expectedAmount + " Actual- " + actualAmount);
                throw e;
            } catch (Exception e) {
                reportLogger.log(LogStatus.WARNING, "unable to verify the total amount in booking confirmation page");
                throw e;
            }
        }
        else {
            try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(transactionAmount),"total amount is displayed");
                reportLogger.log(LogStatus.INFO,"total amount in booking confirmation is not displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"total amount in booking confirmation is displayed");
            }
        }
    }

    public void verifyCarHire(boolean flag,String... carHireTypeAndOutboundFlights){
//      verifies whether car is present in the booking confirmation page if present then verifies all the fields including budget or avis
        try{
            if(carHireTypeAndOutboundFlights.length>0&&flag){
                verifyCarHireDetails(carHireTypeAndOutboundFlights);
            }else if(!flag){
                verifyCarHireNotPresent();
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify car hire section in booking confirmation page");
            throw e;
        }
    }

    private void verifyCarHireDetails(String[] carHireType){
//        verifies the selected car hire (Avis or Budget)
        String titleToVerify="Your car hire confirmation summary";

        try{
            Assert.assertEquals(carHireTitle.getText().toLowerCase().trim(),titleToVerify.toLowerCase().trim());
            reportLogger.log(LogStatus.INFO,"car hire title is displayed and matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire title is not matching"+" Expected - "+carHireTitle.getText().toLowerCase()+" Actual - "+titleToVerify.toLowerCase());
            throw e;
        }

        if(carHireType[0].equals("Avis")){
            carHireImage = carHireImageAvis;
        }else if(carHireType[0].equals("Budget")){
            carHireImage = carHireImageBudget;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carHireImage));
            reportLogger.log(LogStatus.INFO, carHireType[0]+" car hire image is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire image is not matching");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carHireConfirmationNumber));
            reportLogger.log(LogStatus.INFO,"car hire confirmation number is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire confirmation number is not displayed");
            throw e;
        }

        int noOfOutBoundFlights = Integer.parseInt(carHireType[1]);
        String[] carRentalLocation = flightRoute.get(noOfOutBoundFlights-1).getText().split("to");
        try{
            Assert.assertEquals(carHireRentalStation.getText().split(":")[1].trim().toLowerCase(),carRentalLocation[1].trim().toLowerCase());
            reportLogger.log(LogStatus.INFO,"car rental station is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car rental station is not matching"+" Expected - "+ carRentalLocation[1].trim().toLowerCase()+ " Actual - "+carHireRentalStation.getText().split(":")[1].trim().toLowerCase());
            throw e;
        }

        String carHireAmountActual = String.valueOf(CommonUtility.convertStringToDouble(carHireAmount.getText()));
        try{
            Assert.assertEquals(carHireAmountActual, carHireType[2], "car hire amount is not matching");
            reportLogger.log(LogStatus.INFO,"car hire amount is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire amount is not matching Expected - "+ carHireType[2]+" Actual - "+carHireAmountActual);
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(transmission));
            reportLogger.log(LogStatus.INFO,"car hire transmissions is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire transmissions is not displayed");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(noOfDoors));
            reportLogger.log(LogStatus.INFO,"car hire doors is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire doors is not displayed");
            throw e;
        }

        String hireDateCalculated = CommonUtility.convertDateFormat("EEE d MMM yyyy", "EEE, d MMM yyyy", flightDates.get(noOfOutBoundFlights-1).getText());

        try{
            Assert.assertTrue(hireDate.getText().contains(hireDateCalculated),"car hire date is not matching");
            reportLogger.log(LogStatus.INFO,"car hire date is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"car hire date is not matching Expected - "+hireDateCalculated+" Actual - "+hireDate.getText());
            throw e;
        }

        String returnDateCalculated = CommonUtility.convertDateFormat("EEE d MMM yyyy", "EEE, d MMM yyyy", flightDates.get(noOfOutBoundFlights).getText());

        try{
            Assert.assertTrue(returnDate.getText().contains(returnDateCalculated),"car hire date is not matching");
            reportLogger.log(LogStatus.INFO,"car hire date is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"car hire date is not matching Expected - "+returnDateCalculated+" Actual - "+returnDate.getText());
            throw e;
        }
    }

    private void verifyCarHireNotPresent(){
//        verifies car hire is not present in the booking confirmation
        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(carHireTitle));
            reportLogger.log(LogStatus.INFO,"car hire title is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire title is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(carHireImage));
            reportLogger.log(LogStatus.INFO,"car hire image is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire image is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(carHireConfirmationNumber));
            reportLogger.log(LogStatus.INFO,"car hire confirmation number is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire confirmation number is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(carHireRentalStation));
            reportLogger.log(LogStatus.INFO,"car hire rental station is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire rental station is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(transmission));
            reportLogger.log(LogStatus.INFO,"car hire transmission is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire transmission is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(noOfDoors));
            reportLogger.log(LogStatus.INFO,"car hire doors is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire doors is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(hireDate));
            reportLogger.log(LogStatus.INFO,"car hire date is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire collect date is displayed");
            throw e;
        }

        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(returnDate));
            reportLogger.log(LogStatus.INFO,"car hire return date is not displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car hire return date is displayed");
            throw e;
        }
    }

    public void verifyLastFourDigitsOfPaidCard(Hashtable<String,String> testData){
//        this method verifies whether last 4 digits of the card are displayed in the booking confirmation
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        String cardNo = null;
        if (testData.get("savedCardName") != null) {
            cardNo = cardDetails.getAsJsonObject(testData.get("savedCardName")).get("cardNumber").getAsString();
        }else {
            cardNo = cardDetails.getAsJsonObject(testData.get("cardName")).get("cardNumber").getAsString();
        }
        cardNo = cardNo.replaceAll("\\s","");//replaces the white spaces from the string
        int cardNoLen = cardNo.length();
        cardNo = cardNo.substring((cardNoLen-4),(cardNoLen));
        try{
            Assert.assertTrue(paymentMode.getText().contains(cardNo), "last four digits are not present in the booking confirmation page");
            reportLogger.log(LogStatus.INFO,"last four digits of the card are present in booking confirmation page");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"last four digits are not present paid with card -"+ cardNo + " but found card of - "+ paymentMode);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify last 4 digits of the card paid with");
            throw e;
        }
    }

    public void verifyPaidUsingPaypalIsDisplayed(){
//        verifies whether paid using paypal is being displayed when booking is made with paypal
        String paidUsingPaypalMsg = "Paid using PayPal.";
        try {
            Assert.assertEquals(paidUsingPaypalMsg, paymentMode.getText(),"paid using paypal is not displayed");
            reportLogger.log(LogStatus.INFO,"paid using paypal is displayed in the booking confirmation page");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"paid using paypal is not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify whether booking is paid by paypal");
            throw e;
        }

    }

    public void verifyCarParking(Hashtable<String,String> testData, int noOfOutBoundFlights){
//        this method verifies whether car parking is present in the booking confirmation
        try{
            String title = "Your parking confirmation summary";
            try{
                Assert.assertEquals(carParkingTitle.getText().trim().toLowerCase(),title.toLowerCase(),"car parking title is not matching");
                reportLogger.log(LogStatus.INFO,"car parking title is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking title is not matching Expected - "+title+" Actual - "+carParkingTitle.getText());
                throw e;
            }
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carParkingReference),"car parking booking reference is not displayed");
                reportLogger.log(LogStatus.INFO,"car parking booking reference number is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking booking reference is not displayed");
                throw e;
            }
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carParkType),"car parking type is not displayed");
                reportLogger.log(LogStatus.INFO,"car parking type is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking type is not displayed");
                throw e;
            }
            String carParkingHireDate = flightDates.get(noOfOutBoundFlights-1).getText();
            try{
                Assert.assertTrue(carParkingFromDate.getText().contains(carParkingHireDate),"car parking from date is not matching");
                reportLogger.log(LogStatus.INFO,"car parking from date is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking from date is not matching, "+carParkingHireDate+" is not present in "+ carParkingFromDate);
                throw e;
            }
            String carParkingReturnDate = flightDates.get(noOfOutBoundFlights).getText();
            try{
                Assert.assertTrue(carParkingToDate.getText().contains(carParkingReturnDate),"car parking to date is not matching");
                reportLogger.log(LogStatus.INFO,"car parking to date is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking to date is not matching, "+carParkingReturnDate+" is not present in "+ carParkingToDate);
                throw e;
            }
            try{
                Assert.assertEquals(carParkingRegNo.getText().trim().toString(),testData.get("carParkingRegNo").toString(),"car parking registration number is not matching");
                reportLogger.log(LogStatus.INFO,"car parking registration number is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking registration number is not matching Expected - "+testData.get("carParkingRegNo").toString()+" Actual - "+carParkingRegNo.getText().trim().toString());
                throw e;
            }
            try{
                Assert.assertEquals(carParkingBookerName.getText().trim(),passengerNames.get(0).getText().trim(),"car parking booker name is not matching");
                reportLogger.log(LogStatus.INFO,"car parking booker name is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car parking booker name is not matching Expected - "+passengerNames.get(0).getText()+" Actual - "+carParkingBookerName.getText().trim());
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify car parking");
            throw e;
        }
    }

    //// TODO: 9/21/2016
    public void toClickOnChangeFlightLinkInBookingConfirmationPage() {
        //this method clicks on change flight link in booking confirmation page
        try{
            //actionUtility.waitForPageLoad(100);
            actionUtility.waitForElementVisible(changeFlightInItinerary,100);
            actionUtility.click(changeFlightInItinerary);
            reportLogger.log(LogStatus.INFO,"successfully clicked on change flight link in booking confirmation");
            //switchToItineraryPage();
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to click on change flight link in booking confirmation page");
            throw e;
        }

    }

    private void switchToItineraryPage(){
        // This method is used for switching to itinerary window
        boolean isSwitchSuccess = actionUtility.switchWindow("flybe.com - Change your flight");
        if (!isSwitchSuccess) {
            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
            if (isSwitchToCertificateErrorSuccess) {

                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");
            } else {
                reportLogger.log(LogStatus.WARNING,"unable to pass control to itinerary window");
                throw new RuntimeException("unable to pass control itinerary  window");
            }
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Switching to itinerary window is Successful");
        }else{
            reportLogger.log(LogStatus.INFO,"Switching to itinerary is unsuccessful");

        }
    }


    public void compareBookingRefNo(String firstRefNo, String secondRefNo){
//        compares two booking ref nos
        try {
            Assert.assertEquals(firstRefNo, secondRefNo);
            reportLogger.log(LogStatus.INFO, "first and second booking reference numbers are matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"booking reference numbers are not matching after flight plan change Expected - "+firstRefNo+" Actual - "+secondRefNo);
            throw e;
        }
    }

    public void verifyAviosSectionIsDisplayedInBookingConfirmation(){
        //to verify the avios section in booking confirmation
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosAwardedSection));
            reportLogger.log(LogStatus.INFO,"avios awarded section is displayed in the booking confirmation");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"avios awarded section is not displayed in the booking confirmation");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify avios awarded section is displayed in booking confirmation");
            throw e;
        }
    }

    public void verifyAviosPricesInBookingConfirmation(double expectedAviosSavingPrice){
        //to verify the avios price and avios section displayed in booking confirmation
        double actualAviosSectionPrice = -1.0, actualAviosDiscountPrice = -1.0;

        try{

            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosAwardedAmount));
                reportLogger.log(LogStatus.INFO,"avios section price is displayed in the booking confirmation");
            }catch(AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "avios section price is displayed in the booking confirmation");
                throw e;
            }

            actualAviosSectionPrice = CommonUtility.convertStringToDouble(aviosAwardedAmount.getText());

            try {
                Assert.assertEquals(actualAviosSectionPrice, expectedAviosSavingPrice, "avios saving in basket and avios section price is not matching in booking confirmation ");
                reportLogger.log(LogStatus.INFO,"avios saving in basket and avios section price is matching in booking confirmation");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"avios saving in basket and avios section price is not matching in the booking confirmation Expected - " +expectedAviosSavingPrice+ "Actual - "+actualAviosSectionPrice);
                throw e;
            }

            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosDiscountPrice));
                reportLogger.log(LogStatus.INFO,"avios discount price is displayed in the booking confirmation");
            }catch(AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "avios discount price is displayed in the booking confirmation");
                throw e;
            }

            actualAviosDiscountPrice = CommonUtility.convertStringToDouble(aviosDiscountPrice.getText());
            try {
                Assert.assertEquals(actualAviosDiscountPrice, expectedAviosSavingPrice, "avios discount price is not matching in booking confirmation ");
                reportLogger.log(LogStatus.INFO," avios discount price is matching in booking confirmation");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"avios discount price is not matching in the booking confirmation Expected - " +expectedAviosSavingPrice+ "Actual - "+actualAviosDiscountPrice);
                throw e;
            }
        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify avios prices in the booking confirmation");
            throw e;
        }
    }


    public void verifyAviosCurrency(String currencyToBeVerified) {
        // verify the currency displayed in avios discount
        try {
            Assert.assertTrue(aviosDiscountPrice.getText().toLowerCase().contains(currencyToBeVerified.toLowerCase()), " currency is not displayed in avios discount");
            reportLogger.log(LogStatus.INFO, currencyToBeVerified + " currency is displayed in avios discount");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, currencyToBeVerified + " is not matching in avios discount Expected - " + currencyToBeVerified + " Actual - " + aviosDiscountPrice.getText());
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the avios discount currency");
            throw e;
        }
    }

}
