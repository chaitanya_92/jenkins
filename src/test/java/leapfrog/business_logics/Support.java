package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

public class Support {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public Support(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    public void verifyPageTitle(){
        //verifies the page title

        String actualPageTitle = null;
        String expectedPageTitle= "Support Home Page";
        try {
            actualPageTitle = TestManager.getDriver().getTitle().trim();
            Assert.assertEquals(actualPageTitle,expectedPageTitle, "support page title didnot match");
            reportLogger.log(LogStatus.INFO,"support page title matched");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"support page title did not match Expected: "+expectedPageTitle+ "Actual: "+actualPageTitle);
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the support page title");

        }

    }
}
