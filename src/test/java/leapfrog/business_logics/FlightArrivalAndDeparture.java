package leapfrog.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.Hashtable;

public class FlightArrivalAndDeparture {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public FlightArrivalAndDeparture() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "bynumber")
    private WebElement searchFlightByFlightNumber;

    @FindBy(id = "byairport")
    private WebElement searchFlightByAirport;

    @FindBy(id = "flightDate")
    private WebElement flightDate;

    @FindBy(id = "flightNumber")
    private WebElement flightNumber;

    @FindBy(id = "searchFlightInformation_0")
    private WebElement search;

    @FindBy(className = "comments")
    private WebElement southEndAirportLink;

    @FindBy(css = "#contentWrapper>h1")
    private WebElement heading;


    public void searchByFlightNumber(Hashtable<String,String> testData){
        //searches by flight no.

        try{
            String searchDate = testData.get("searchDate");
            actionUtility.waitForElementVisible(flightDate,7);
            flightDate.clear();
            flightDate.sendKeys(searchDate);
            actionUtility.click(heading);
            actionUtility.click(searchFlightByFlightNumber);
            actionUtility.sendKeys(flightNumber,testData.get("flightNumber"));
            actionUtility.click(search);


            reportLogger.log(LogStatus.INFO,"sucessfully searched flight by flight number");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to search the flight by flight number");
            throw e;
        }
    }

    public void verifySouthEndAirportLink(){
        try{
            actionUtility.verifyIfElementIsDisplayed(southEndAirportLink,8);
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"southend airport link is not displayed");
            throw e;

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to veriy southend airport link is displayed");

        }
    }







}
