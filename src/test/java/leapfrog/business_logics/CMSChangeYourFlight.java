package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSChangeYourFlight {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSChangeYourFlight() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    private String currencyOption ="//a[@class='currency-option' and contains(text(),'%s')]";

    @FindBy(css = "a#reselectSubmit>span")
    private WebElement changeRoute;

    @FindBy(id="newContinueButton")
    private WebElement continueButton;

    @FindBy(id="departureDateDay")
    private WebElement departureDay;

    @FindBy(id="departureDateMonthYear")
    private WebElement departureDateMonthYear;

    @FindBy(id="returnDateDay")
    private WebElement returnDay;

    @FindBy(id="returnDateMonthYear")
    private WebElement returnDateMonthYear;

    @FindBy(xpath = "//div[@id='outboundWrapper']/descendant::li[contains(@id,'lowProductTrip')]/input")
    private List<WebElement> justFlyFlightsOutbound;

    @FindBy(xpath = "//div[@id='returnWrapper']/descendant::li[contains(@id,'lowProductTrip')]/input")
    private List<WebElement> justFlyFlightsInbound;

    @FindBy(xpath = "//div[@id='outboundWrapper']/descendant::li[contains(@id,'mediumProductTrip')]/input")
    private List<WebElement> getMoreFlightsOutbound;

    @FindBy(xpath = "//div[@id='returnWrapper']/descendant::li[contains(@id,'mediumProductTrip')]/input")
    private List<WebElement> getMoreFlightsInbound;

    @FindBy(xpath = "//div[@id='outboundWrapper']/descendant::li[contains(@id,'highProductTrip')]/input")
    private List<WebElement> allInFlightsOutbound;

    @FindBy(xpath = "//div[@id='returnWrapper']/descendant::li[contains(@id,'highProductTrip')]/input")
    private List<WebElement> allInFlightsInbound;


    @FindBy(id="mcpSelector")
    private WebElement currencyDropDown;

    public void continueToYourDetails(){
        //clicks on continue button to navigate to your details page
        try{
            actionUtility.waitForElementClickable(continueButton,20);
            actionUtility.hardClick(continueButton);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to your details page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to your details page");
            throw e;
        }
    }

    public void changeDateAndRoute(Hashtable<String,String> testData){
        //selects the new departure date and/or return date and clicks on change flight
        try{
            String[] newFlightDate = null;
            actionUtility.hardSleep(2000);
            if(testData.get("departDateOffset")!=null) {
                newFlightDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("departDateOffset")), "dd/MMMMM/yyyy").split("/");
                actionUtility.waitForElementVisible(departureDay, 25);
                actionUtility.dropdownSelect(departureDay, ActionUtility.SelectionType.SELECTBYTEXT, newFlightDate[0]);
                actionUtility.dropdownSelect(departureDateMonthYear, ActionUtility.SelectionType.SELECTBYTEXT, newFlightDate[1] + " " + newFlightDate[2]);
            }
            if(testData.get("returnDateOffset")!=null) {
                newFlightDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("returnDateOffset")),"dd/MMMMM/yyyy").split("/");
                actionUtility.waitForElementVisible(returnDay,25);
                actionUtility.dropdownSelect(returnDay, ActionUtility.SelectionType.SELECTBYTEXT, newFlightDate[0]);
                actionUtility.dropdownSelect(returnDateMonthYear, ActionUtility.SelectionType.SELECTBYTEXT, newFlightDate[1] + " " + newFlightDate[2]);
            }
            actionUtility.hardClick(changeRoute);

            try {
                actionUtility.click(changeRoute);

            }catch (TimeoutException e){

            }
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully  changed date and route");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to change date and route");
            throw e;
        }

    }


    public void selectChangedFlight(Hashtable<String,String> itineraryTestData,String tripType){
        //selects the required flight based on the test data passed
        String option = null;
        try {

            if (tripType.toLowerCase().equals("outbound")) {
                option = itineraryTestData.get("outboundFlightOption");
            } else if (tripType.equals("inbound")) {
                option = itineraryTestData.get("inboundFlightOption");
            }

            switch (option) {
                case ("All in"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.hardClick(allInFlightsOutbound.get(0));
                    } else if (tripType.equals("inbound")) {
                        actionUtility.hardClick(allInFlightsInbound.get(0));
                    }
                    break;
                case ("Just fly"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.hardClick(justFlyFlightsOutbound.get(0));
                    } else if (tripType.equals("inbound")) {
                        actionUtility.hardClick(justFlyFlightsInbound.get(0));
                    }
                    break;
                case ("Get more"):
                    if (tripType.toLowerCase().equals("outbound")) {
                        actionUtility.hardClick(getMoreFlightsOutbound.get(0));
                    } else if (tripType.equals("inbound")) {
                        actionUtility.hardClick(getMoreFlightsInbound.get(0));
                    }
                    break;
            }

            reportLogger.log(LogStatus.INFO,"successfully selected the new flight for the "+tripType+" of type "+option);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the new flight for the "+tripType+" of type "+option);
            throw e;
        }
    }

    public void changeCurrency(String currency){
        //change the currency
        try {
            actionUtility.dropdownSelect(currencyDropDown, ActionUtility.SelectionType.SELECTBYTEXT,currency);
            reportLogger.log(LogStatus.INFO,"successfully changed the currency to " +currency);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to change the currency to " +currency);
            throw e;
        }
    }

    public void verifyCurrency(String currencyToBeVerified){
        // verify the currency displayed
        String currency = null;
        try{
            currency = new Select(currencyDropDown).getFirstSelectedOption().getText();
            Assert.assertEquals(currency, currencyToBeVerified  ,"selected currency is not matching");
            reportLogger.log(LogStatus.INFO,currencyToBeVerified + "selected currency is matching");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"selected currency is not matching Expected - "+currencyToBeVerified+" Actual - "+currency);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency");
            throw e;
        }
    }

}
