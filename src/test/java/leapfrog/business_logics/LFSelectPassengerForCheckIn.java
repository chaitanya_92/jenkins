package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

public class LFSelectPassengerForCheckIn {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LFSelectPassengerForCheckIn() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='male']")
    private List<WebElement> males;

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='female']")
    private List<WebElement> females;

    @FindBy(xpath = "//div[@class='col-xs-12']/button[contains(text(),'Check-in')]")
    private WebElement checkInButton;

    @FindBy(id = "accept-restrictions")
    private WebElement acceptRestrictedGoods;

    @FindBy(css = "div[data-direction='outbound'] input.passenger-select")
    private List<WebElement> outboundPassengers;

    @FindBy(css = "div[data-direction='inbound'] input.passenger-select")
    private List<WebElement> inboundPassengers;

    String checkInOptionLocatorOutbound = "//div[@data-direction='outbound']/div[@class='row sector'][%d]/descendant::div[@class='row sector-row passengers']/descendant::input";
    String checkInOptionLocatorInbound =  "//div[@data-direction='return']/div[@class='row sector'][%d]/descendant::div[@class='row sector-row passengers']/descendant::input";
    String passengerLocatorOutbound ="//div[@data-direction='outbound']/div[@class='row sector'][%d]/descendant::div[@class='row sector-row passengers'][%d]/descendant::input[@type='checkbox']";
    String passengerLocatorInbound ="//div[@data-direction='inbound']/div[@class='row sector'][%d]/descendant::div[@class='row sector-row passengers'][%d]/descendant::input[@type='checkbox']";
    String checkInContainerLocator = "//div[@id='checkInNowModal']/descendant::div[@class='modal-content']";


    public void selectPassengerAndFlightForCheckIn(Hashtable<String,String> testData,Hashtable<String,String> selectedFlightDetails,int noOfPassengers,int noOfFlights, String tripType) {
        //selects the flight for checkIn for each passenger based on the flight operator
        try {
            String CheckInTrip = "OutboundCheckIn";
            String passengerLocator = passengerLocatorOutbound;
            if(tripType.toLowerCase().equals("inbound")){
                CheckInTrip = "InboundCheckIn";
                passengerLocator = passengerLocatorInbound;
            }
            for (int i = 1; i <= noOfPassengers; i++) {

                String passengerFlightForCheckIn = testData.get("passenger" + (i) + CheckInTrip);

                if (passengerFlightForCheckIn != null) {
                    String[] flightNamesForCheckIn = passengerFlightForCheckIn.split(",");

                    for (int j = 0; j <flightNamesForCheckIn.length; j++) {

                        for (int k = 1; k <= noOfFlights; k++) {
                            if ((selectedFlightDetails.get("flightName" + k).toLowerCase().equals(flightNamesForCheckIn[j].toLowerCase()))) {
                                if (!TestManager.getDriver().findElement(By.xpath(String.format(passengerLocator, k, i))).isSelected()) {
                                    actionUtility.selectOption(TestManager.getDriver().findElement(By.xpath(String.format(passengerLocator, k, i))));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            reportLogger.log(LogStatus.INFO,"Successfully selected the passengers and flights for check-in");
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING,"unable to select the passengers and flights for check-in");
            throw e;
        }
    }

    public void deSelectAllPassengersForCheckIn(String tripType) {
//          calls the method to Deselect the passengers for the specified trip type
        try {
            switch (tripType.toLowerCase()) {
                case "outbound":
                    deSelectPassengerForTrip(outboundPassengers);
                    break;
                case "inbound":
                    deSelectPassengerForTrip(inboundPassengers);
            }
            reportLogger.log(LogStatus.INFO, "successfully deselected all the passengers for check in");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to deselect all the passengers for check in");
            throw e;
        }
    }

    private void deSelectPassengerForTrip(List<WebElement> selectAllPassengers) {
//        Deselect all the passengers for specified trip type
        for (int i = 0; i < selectAllPassengers.size(); i++)
            if(selectAllPassengers.get(i).isEnabled()) {
                actionUtility.unSelectOption(selectAllPassengers.get(i));
            }

    }

    public void checkIn() {
//        selects the accept restrictions check-box and clicks on the check in button
        try {
            actionUtility.selectOption(acceptRestrictedGoods);
            actionUtility.click(checkInButton);
            actionUtility.waitForElementNotPresent(checkInContainerLocator, 60);
            reportLogger.log(LogStatus.INFO, "successfully checked in");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to check in");
            throw e;
        }
    }

    public Hashtable<String, Hashtable> captureCheckInDetails(int numberOfPassengers, Hashtable<String,String>flightDetails, int flightNos, String tripType){
        // returns the checked in details
        try {
            Hashtable<String, Hashtable> overAllCheckedInInfo = new Hashtable<String, Hashtable>();
            String checkInOptionLocator= checkInOptionLocatorOutbound;
            if(tripType.toLowerCase().equals("inbound")){
                checkInOptionLocator = checkInOptionLocatorInbound;
            }
            for (int i = 0; i < flightNos; i++) {
                List<WebElement> checkInSelectors = TestManager.getDriver().findElements(By.xpath(String.format(checkInOptionLocator, (i + 1))));
                String flightName = flightDetails.get("flightName" + (i + 1));

                if (!(flightName.toLowerCase().contains("air france") || flightName.toLowerCase().contains("hop regional"))) {
                    overAllCheckedInInfo.put("flight" + (i + 1), captureCheckInDetailForEachFlight(checkInSelectors, numberOfPassengers));

                }else {
                    Hashtable<String,Boolean> checkedInInfo = new Hashtable<String, Boolean>();

                    for (int passenger=0; i<numberOfPassengers; passenger++){
                        checkedInInfo.put("passenger"+(passenger+1),false);
                    }
                    overAllCheckedInInfo.put("flight" + (i + 1), checkedInInfo);
                }

            }
            reportLogger.log(LogStatus.INFO,"successfully to captured the checked-in details for "+tripType +" trip");
            return overAllCheckedInInfo;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to capture the checked-in details for "+ tripType +" trip");
            throw e;
        }
    }

    private Hashtable<String, Boolean> captureCheckInDetailForEachFlight(List<WebElement> checkInSelectors, int numberOfPassengers) {
        //calls the method to capture the check-in for each passenger per flight
        Hashtable<String, Boolean> checkedInInfo = new Hashtable<String, Boolean>();
        LinkedList<Boolean> isPassengerSelected = captureCheckInDetailForEachPassenger(checkInSelectors);

        for (int passenger = 0; passenger < numberOfPassengers; passenger++) {
            checkedInInfo.put("passenger"+(passenger+1),isPassengerSelected.get(0));
            isPassengerSelected.remove(0);
        }
        return checkedInInfo;
    }

    private LinkedList<Boolean> captureCheckInDetailForEachPassenger(List<WebElement> checkInSelectors){
        //capture the check-in for each passenger
        LinkedList<Boolean> isFlightSelected = new LinkedList<Boolean>();

        for (int j=0;j<checkInSelectors.size();j++){

            if (checkInSelectors.get(j).isSelected()){
                isFlightSelected.add(true);

            }else {
                isFlightSelected.add(false);
            }
        }
        return isFlightSelected;
    }

    public void selectGenderForPassengers(Hashtable<String, String> testData) {
//        selects the genders for all the specified passengers
        String[] genders = testData.get("checkInGender").split(",");
        try {
            int count = 0;

            for (int i = 0; i < genders.length; i++) {

                for (int j = count; j < males.size(); j++) {

                    if (males.get(j).isEnabled() && females.get(j).isEnabled()) {

                        if (genders[i].toLowerCase().equals("male")) {
                            actionUtility.selectOption(males.get(j));
                        } else if (genders[i].toLowerCase().equals("female")) {
                            actionUtility.selectOption(females.get(j));
                        }
                        count = ++j;
                        break;
                    }

                }

            }
            reportLogger.log(LogStatus.INFO, "successfully selected the genders for all the specified passengers");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the genders for all the specified passengers");
            throw e;
        }
    }


}







