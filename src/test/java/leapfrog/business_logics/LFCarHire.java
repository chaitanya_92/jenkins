package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import javafx.print.PageLayout;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

public class LFCarHire {

    ActionUtility actionUtility;
    ExtentTest reportLogger;


    public LFCarHire(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();

    }


    @FindBy(id="insuranceContainerWrapper")
    private WebElement insuranceSection;

    @FindBy(xpath = "//span[text()='Yes I need Travel Insurance']/preceding-sibling::input")
    private List<WebElement>  needInsurance;

    @FindBy(xpath = "//span[contains(text(),'covered')]/preceding-sibling::input")
    private List<WebElement>  insuranceCovered;


    @FindBy(id = "insurance")
    private WebElement coverType;

//    -------Car Hire---------

    @FindBy(css = ".car-hire")
    private WebElement  carHireSection;

    @FindBy(xpath = "//div[div[img[contains(@src,'Avis.png')]]]/preceding-sibling::div//button[text()='Add this car']")
    private List<WebElement>  avisCarHireButton;

    @FindBy(xpath = " //div[div[img[contains(@src,'Budget.png')]]]/preceding-sibling::div//button[text()='Add this car']")
    private List<WebElement>  budgetCarHireButton;

    @FindBy(xpath = "//div[div[button[text()='Remove this car']]]/div[@class='price-container']/span[2]")
    private WebElement carHirePrice;


    @FindBy(css = ".car-hire .submit>button")
    private WebElement  saveCarHire;

    @FindBy(xpath = "//div[contains(@class,'car-hire')]//button[text()='Show more']")
    private WebElement showMoreCarHire;


    @FindBy(css = ".modal-body")
    private WebElement carHireInformationPopup;



    @FindBy(xpath = "//div[contains(@class,'car-hire')]/h2[text()='Car hire']")
    private WebElement carHireSectionHeading;


    @FindBy(css = ".price+div+div button")
    private List<WebElement>  carHireInformation;

    @FindBy(css = ".box_headSmall>h2")
    private WebElement  carHireInformationHeading;

    @FindBy(css = "#surcharges>h2")
    private WebElement  youngDriverHeading;

    @FindBy(css = "#excesses>h2")
    private WebElement  carHireHeading;


//--------------car parking------------

    @FindBy(css = ".parking")
    private WebElement carParkingSection;

    @FindBy(xpath = "//button[text()='Add this parking']")
    private List <WebElement> addThisCarParking;

    @FindBy(xpath="//div[div[button[text()='Remove this parking']]]/div/span[2]")
    private WebElement carParkingPrice;


    @FindBy(xpath = "//div[contains(@class,'parking')]//button[text()='Show more']")
    private WebElement showMoreCarParking;

    @FindBy(id = "car-registration-number-input")
    private WebElement carParkingRegNo;

    @FindBy(css = ".parking .submit>button")
    private WebElement  saveCarParking;

    @FindBy(xpath = "//div[contains(@class,'parking')]/h2[text()='Car parking']")
    private WebElement carParkingSectionHeading;


    @FindBy(xpath = "//button[div[text()='Continue']]")
    private WebElement continueButton;

    //    ----basket outbound & inbound total ------
    @FindBy(css = ".outbound .price>span:nth-of-type(2)")
    private WebElement outboundTotal;

    @FindBy(css = ".inbound .price>span:nth-of-type(2)")
    private WebElement returnTotal;

    //    -----basket  price----------
    @FindBy(css = ".total .amount .price>span:nth-of-type(2)")
    private WebElement overallBasketTotal;

    @FindBy(css = ".baggageAmount >.price>span:nth-of-type(2)")
    private WebElement basketBaggagePrice;

    @FindBy(css = ".flexAmount>.price>span:nth-of-type(2)")
    private WebElement basketFlexPrice;

    @FindBy(css = ".insuranceAmount>.price>span:nth-of-type(2)")
    private WebElement basketInsurancePrice;

    @FindBy(css = ".seatsAmount>.price>span:nth-of-type(2)")
    private WebElement basketSeatPrice;

    @FindBy(className = "currencySelected-text")
    private WebElement currencySelected;

    @FindBy(css = ".carparkingAmount>div.price>span:nth-of-type(2)")
    private WebElement basketCarParkingPrice;

    @FindBy(css = ".discountAmount>.price>span:nth-of-type(3)")
    private WebElement aviosBasketPrice;

    @FindBy(css = ".carhireAmount>div.price>span:nth-of-type(2)")
    private WebElement basketCarHirePrice;


//    ------------------footer-------------------------
@FindBy(css = ".footer img[alt='Flybe']")
private WebElement flybeLogo;

    @FindBy(css = ".footer li:nth-child(1)>span")
    private WebElement flybeCopyRight;

    @FindBy(css = ".footer li:nth-child(2)")
    private WebElement privacyPolicyLink;

    @FindBy(css = ".footer li:nth-child(3)>a")
    private WebElement cookiePolicyLink;

    @FindBy(css = ".footer li:nth-child(4)>a")
    private WebElement contactUsLink;


    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = ".footer p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = ".footer p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = ".footer p:nth-child(3)")
    private WebElement footerPara3;

    @FindBy(css = ".footer p:nth-child(4)")
    private WebElement footerPara4;

    //-----------logIn---------------------------------------------

    @FindBy(xpath = "//button[text()='Log in']")
    WebElement logInButton;

    @FindBy(xpath = "//form/h1[text()='Log in to your account']")
    private WebElement contactPassengerLoginMenu;

    @FindBy(css=".modal-body input[id^='frc-email']")
    private WebElement contactPassengerEmailForLogin;

    @FindBy(css=".modal-body input[id^='frc-password']")
    private WebElement contactPassengerPassword;

    @FindBy(css=".modal-body button")
    private WebElement contactPassengerLogin;

    @FindBy(xpath = "//div[@class='vertically-centered']/span")
    private List<WebElement> loggedInMessage;


    public void extraPageLogin(Hashtable<String, String> testData){
        //login on extra page
        try{
            String contactEmail = null, contactPassword = null;
            actionUtility.waitForElementClickable(logInButton,10);
            actionUtility.click(logInButton);
            actionUtility.waitForElementVisible(contactPassengerLoginMenu,5);
            if(testData.get("noAviosRegistered")!=null&&testData.get("noAviosRegistered").equalsIgnoreCase("true")) {
                contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName2").getAsString();
                contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            }
            else{
                contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName1").getAsString();
                contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            }
            contactPassengerEmailForLogin.clear();
            actionUtility.sendKeys(contactPassengerEmailForLogin,contactEmail);
            contactPassengerPassword.clear();
            actionUtility.sendKeys(contactPassengerPassword,contactPassword);
            actionUtility.hardClick(contactPassengerLogin);
            reportLogger.log(LogStatus.INFO, "successfully logged in from fare selection page");
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable login on fare selection page");
            throw e;
        }

    }

    public void verifyLoggedInMsg(){
        try {
            actionUtility.waitForElementVisible(loggedInMessage, 10);
            Assert.assertFalse(loggedInMessage.isEmpty());
            String forename = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("forename").getAsString();
            String lastname = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("lastname").getAsString();
            Assert.assertTrue(loggedInMessage.get(0).getText().equals("You are logged in as"));
            Assert.assertTrue(loggedInMessage.get(1).getText().equals(forename + " " + lastname));
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"logged-in message displayed doesnt match");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify logged-in message");
            throw e;
        }
    }


    public void addInsurance(Hashtable<String, String> testData){
        // selects the insurance for the passneger
        try {


            if(!actionUtility.verifyIfElementIsDisplayed(insuranceSection,1)) {
                if (actionUtility.verifyIfElementIsDisplayed(saveCarHire,1)) {
                    actionUtility.click(saveCarHire);
                }

                if (actionUtility.verifyIfElementIsDisplayed(saveCarParking, 1)) {
                    actionUtility.click(saveCarParking);
                }
            }


                Hashtable<String, Integer> passengerCount = getPassengerCountForEachType(testData);

                int adultCount = passengerCount.get("adultCount");
                int teenCount = passengerCount.get("teenCount");
                int childCount = passengerCount.get("childCount");

                int count = 0;


                if (adultCount > 0) {
                    for (int i = 1; i <= adultCount; i++) {
                        if (testData.get("adultAddTravelInsurance" + i) != null && testData.get("adultAddTravelInsurance" + i).equals("true")) {
                            actionUtility.click(needInsurance.get(count));
                        } else if (insuranceCovered.size() != 0) {
                            actionUtility.click(insuranceCovered.get(count));
                        }
                        count++;

                    }

                }
                if (teenCount > 0) {
                    for (int i = 1; i <= teenCount; i++) {
                        if (testData.get("teenAddTravelInsurance" + i) != null && testData.get("teenAddTravelInsurance" + i).equals("true")) {
                            actionUtility.click(needInsurance.get(count));
                        } else if (insuranceCovered.size() != 0) {
                            actionUtility.click(insuranceCovered.get(count));
                        }
                        count++;

                    }
                }
                if (childCount > 0) {
                    for (int i = 1; i <= childCount; i++) {
                        if (testData.get("childAddTravelInsurance" + i) != null && testData.get("childAddTravelInsurance" + i).equals(true)) {
                            actionUtility.click(needInsurance.get(count));
                        } else if (insuranceCovered.size() != 0) {
                            actionUtility.click(insuranceCovered.get(count));
                        }
                        count++;

                    }
                }


                if (testData.get("coverType") != null) {
                    actionUtility.dropdownSelect(coverType, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("coverType"));
                }



                reportLogger.log(LogStatus.INFO, "Successfully added the insurance to the passengers");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to add the insurance to the passengers");
            throw e;
        }


    }

    private Hashtable<String, Integer> getPassengerCountForEachType(Hashtable<String, String> testData) {
        //returns the passenger Count
        Hashtable<String, Integer> passengerCount = new Hashtable<String, Integer>();
        passengerCount.put("adultCount", 1);
        passengerCount.put("infantCount", 0);
        passengerCount.put("teenCount", 0);
        passengerCount.put("childCount", 0);

        if (testData.get("noOfAdult") != null) {
            passengerCount.put("adultCount", Integer.parseInt(testData.get("noOfAdult")));
        }

        if (testData.get("noOfInfant") != null) {
            passengerCount.put("infantCount", Integer.parseInt(testData.get("noOfInfant")));
        }

        if (testData.get("noOfTeen") != null) {
            passengerCount.put("teenCount", Integer.parseInt(testData.get("noOfTeen")));
        }

        if (testData.get("noOfChild") != null) {
            passengerCount.put("childCount", Integer.parseInt(testData.get("noOfChild")));
        }

        return passengerCount;
    }

    public String addCarHire(String carHireType){
//        this method adds the required car hire to the booking and returns the price of car hire selected
        String carHirePriceCalculated = null;
        try{
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(carHireSection,15);
        }catch (TimeoutException e){
            reportLogger.log(LogStatus.WARNING,"car hire section is not displayed");
            throw e;
        }
        try{
            while(actionUtility.verifyIfElementIsDisplayed(showMoreCarHire)) {
                actionUtility.click(showMoreCarHire);
                actionUtility.hardSleep(2000);
            }

            if(carHireType.toLowerCase().equals("avis")){
                actionUtility.click(avisCarHireButton.get(0));
                reportLogger.log(LogStatus.INFO,"successfully selected "+carHireType+" car");
            }else if(carHireType.toLowerCase().equals("budget")){
                actionUtility.click(budgetCarHireButton.get(0));
                reportLogger.log(LogStatus.INFO,"successfully selected "+carHireType+" car");
            }
            carHirePriceCalculated = String.valueOf(CommonUtility.convertStringToDouble(carHirePrice.getText()));
            actionUtility.click(saveCarHire);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to choose "+carHireType+" car");
            throw e;
        }
        return carHirePriceCalculated;
    }

    public void addCarParking(Hashtable<String,String> testData){
//        this method adds the car parking to the current booking
        try{
            if(actionUtility.verifyIfElementIsDisplayed(saveCarHire)){
                actionUtility.click(saveCarHire);
            }
            actionUtility.waitForElementVisible(carParkingSection,10);
        }catch (TimeoutException e){
            reportLogger.log(LogStatus.WARNING,"car parking section is not displayed");
            throw e;
        }
        try{
            while(actionUtility.verifyIfElementIsDisplayed(showMoreCarParking)) {
                actionUtility.click(showMoreCarParking);
                actionUtility.hardSleep(2000);
            }
            actionUtility.click(addThisCarParking.get(0));
            actionUtility.waitForElementVisible(carParkingRegNo,5);
            carParkingRegNo.clear();
            actionUtility.sendKeysByAction(carParkingRegNo,testData.get("carParkingRegNo"));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify car parking");
            throw e;
        }
    }

    public void continueToPayment(){
        try{
            if(actionUtility.verifyIfElementIsDisplayed(continueButton,3)) {
                actionUtility.click(continueButton);
            }else {
                if (actionUtility.verifyIfElementIsDisplayed(saveCarHire, 1)) {
                    actionUtility.click(saveCarHire);
                }


                if (actionUtility.verifyIfElementIsDisplayed(saveCarParking, 2)) {
                    actionUtility.click(saveCarParking);
                }

                if (actionUtility.verifyIfElementIsDisplayed(continueButton, 3)) {
                    actionUtility.click(continueButton);
                }
            }

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to the payment page");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable continue to the payment page");
            throw e;
        }
    }

    public void verifyCarHireIsDisplayedInExtrasPage() {
        // to verify the car parking heading is displayed in extras page
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carHireSection));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carHireSectionHeading));
            reportLogger.log(LogStatus.INFO,"Car hire is displayed in extras page");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"Car hire is not displayed in extras page");
            throw e;
        }catch (TimeoutException e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify car hire section in extras page");
            throw e;
        }
    }

    public void verifyCarParkingIsDisplayedInExtrasPage() {
        // to verify the car parking heading is displayed in extras page
        try{
            if(actionUtility.verifyIfElementIsDisplayed(saveCarHire)){
                actionUtility.click(saveCarHire);
            }
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carParkingSection));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carParkingSectionHeading));
            reportLogger.log(LogStatus.INFO,"Car parking is displayed in extras page");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING,"Car parking is not displayed in extras page");
            throw e;
        }catch (TimeoutException e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify car parking section in extras page");
            throw e;
        }
    }

    public void verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(Hashtable<String,Double> previousBasketDetails){
        //verifies if the total basket price in the extras page is same as previous page


        double expectedPrice =0.0;
        double actualPrice = -1.0;

        try {
            try {
                expectedPrice = previousBasketDetails.get("overallBasketPrice");
                actionUtility.waitForElementVisible(overallBasketTotal,10);
                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in extras page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in extras page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in extras page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in extras page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("ticketFlexibilityPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("ticketFlexibilityPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketFlexPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "ticket flexibility price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("baggagePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("baggagePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "baggage price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("seatPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("seatPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "seat Price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "seat Price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "seat Price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }


            if (previousBasketDetails.get("basketAviosPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketAviosPrice");
                    actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "avios price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify prices in the basket, in the extras page against prices of basket in passenger details page");
            throw e;
        }
    }

    public Hashtable verifyBasket(Hashtable<String,String> testData, double... maxAviosPrice){

        verifyCarHireAndCarParkingAddedToBasket();
        verifyInsuranceAddedToBasket(testData);
        if(maxAviosPrice.length>0){
            verifyAviosPriceAddedToBasket(maxAviosPrice[0]);
        }
        Hashtable<String,Double> basketDetails = verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();
        return  basketDetails;
    }

    private void verifyCarHireAndCarParkingAddedToBasket() {
//        verifies if car hire and car parking is added to basket if selected
        double expectedPrice = -1.0;
        double actualPrice = 0.0;

        try {

            if (actionUtility.verifyIfElementIsDisplayed(carParkingPrice,2)) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice,2));
                    reportLogger.log(LogStatus.INFO, "car parking price is added to the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is not added to the basket");
                    throw e;
                }

                expectedPrice = Math.round(CommonUtility.convertStringToDouble(carParkingPrice.getText())*100.0)/100.0;
                actualPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText())*100.0)/100.0;

                try {
                    Assert.assertEquals(actualPrice,expectedPrice,"car parking price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "car parking price is matching in the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is not matching in the basket Expected -"+expectedPrice+" Actual - "+actualPrice);
                    throw e;
                }

            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice,2));
                    reportLogger.log(LogStatus.INFO, "car parking price is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is added to the basket");
                    throw e;
                }
            }

            if (actionUtility.verifyIfElementIsDisplayed(carHirePrice,2)) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice,2));
                    reportLogger.log(LogStatus.INFO, "car hire price is added to the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is not added to the basket");
                    throw e;
                }


                expectedPrice = Math.round(CommonUtility.convertStringToDouble(carHirePrice.getText())*100.0)/100.0;
                actualPrice = Math.round(CommonUtility.convertStringToDouble(basketCarHirePrice.getText())*100.0)/100.0;

                try {
                    Assert.assertEquals(actualPrice,expectedPrice,"car hire price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "car parking hire is matching in the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is not matching in the basket Expected -"+expectedPrice+" Actual - "+actualPrice);
                    throw e;
                }

            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice));
                    reportLogger.log(LogStatus.INFO, "car hire price is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is added to the basket");
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the car hire and car parking in extras page");
            throw e;
        }
    }

    private void verifyInsuranceAddedToBasket(Hashtable<String,String> testData) {

//        verifies if car hire and car parking is added to basket if selected
        double insurancePriceInBasket = 0.0;
        double expectedInsurancePrice = -1.0;

        try {
            expectedInsurancePrice = calculateInsurancePriceForAllPassenger(testData);

            if (expectedInsurancePrice > 0.0) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketInsurancePrice,2));
                    reportLogger.log(LogStatus.INFO, "Insurance price is added in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price is not added in the basket");
                    throw e;
                }
                insurancePriceInBasket = CommonUtility.convertStringToDouble(basketInsurancePrice.getText());
                try {
                    Assert.assertEquals(insurancePriceInBasket, expectedInsurancePrice);
                    reportLogger.log(LogStatus.INFO, "Insurance price in the basket is matching");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price in the basket is not matching Expected - " + expectedInsurancePrice + " Actual - " + insurancePriceInBasket);
                    throw e;
                }
            } else {
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketInsurancePrice,2));
                    reportLogger.log(LogStatus.INFO, "Insurance price is not added in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Insurance price is added in the basket");
                    throw e;
                }
            }




            reportLogger.log(LogStatus.INFO, "Successfully added the insurance to the passengers");


        }catch(AssertionError e){

                throw e;
        }catch(Exception e){
                reportLogger.log(LogStatus.WARNING, "unable to verify the car hire and car parking in extras page");
                throw e;
        }

    }

    private double calculateInsurancePriceForAllPassenger(Hashtable<String,String> testData){
        //calculates the overall insurance price

        Hashtable<String, Integer> passengerCount = getPassengerCountForEachType(testData);

        int adultCount = passengerCount.get("adultCount");
        int teenCount = passengerCount.get("teenCount");
        int childCount = passengerCount.get("childCount");
        int count = 0;

        if (adultCount > 0) {
            for (int i = 1; i <= adultCount; i++) {
                if (testData.get("adultAddTravelInsurance" + i) != null && testData.get("adultAddTravelInsurance" + i).equals("true")) {
                    count++;
                }
            }
        }

        if (teenCount > 0) {
            for (int i = 1; i <= adultCount; i++) {
                if (testData.get("teenAddTravelInsurance" + i) != null && testData.get("teenAddTravelInsurance" + i).equals("true")) {
                    count++;
                }
            }
        }

        if (childCount > 0) {
            for (int i = 1; i <= adultCount; i++) {
                if (testData.get("childAddTravelInsurance" + i) != null && testData.get("childAddTravelInsurance" + i).equals("true")) {
                    count++;
                }
            }
        }
        if(count>0){
            System.out.println(Math.round(count*(CommonUtility.convertStringToDouble(new Select(coverType).getFirstSelectedOption().getText()))*100.0)/100.0);
            return Math.round(count*(CommonUtility.convertStringToDouble(new Select(coverType).getFirstSelectedOption().getText()))*100.0)/100.0;
        }else {
            return 0.0;
        }

    }

    private Hashtable<String, Double> verifyTotalBasketPriceWithSummationOfOtherElementsInBasket(){
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double carParkingPrice =0.0;
        double carHirePrice = 0.0;
        double sum =0.0;
        double overallBasketPrice =1.0;
        double aviosPrice = 0.0;


        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();


        try{
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);

            if (actionUtility.verifyIfElementIsDisplayed(returnTotal,2)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice,2)){
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
                basketDetails.put("ticketFlexibilityPrice",ticketFlexibilityPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice,2)){
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
                basketDetails.put("baggagePrice",bagPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice,2)){
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
                basketDetails.put("seatPrice",seatsPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketInsurancePrice,2)){
                travelInsurancePrice = Math.round(CommonUtility.convertStringToDouble(basketInsurancePrice.getText())*100.0)/100.0;
                basketDetails.put("travelInsurancePrice",travelInsurancePrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice,2)){
                carHirePrice = Math.round(CommonUtility.convertStringToDouble(basketCarHirePrice.getText())*100.0)/100.0;
                basketDetails.put("carHirePrice",carHirePrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice,2)){
                carParkingPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText())*100.0)/100.0;
                basketDetails.put("carParkingPrice",carParkingPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice,2)) {
                aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText()) * 100.0) / 100.0;
                basketDetails.put("aviosPrice",aviosPrice);

            }

            sum = Math.round((outboundPrice+inboundPrice+ticketFlexibilityPrice+bagPrice+seatsPrice+travelInsurancePrice+carParkingPrice-aviosPrice)*100.0)/100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            basketDetails.put("overallBasketPrice",overallBasketPrice);
            Assert.assertEquals(overallBasketPrice,sum,"total basket price in the extras page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO,"total basket price in the extras page is matching with summation of all the element prices in the basket");
            return basketDetails;
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"total basket price in the extras page is not matching with summation of all the element prices in the basket Expected - "+sum+" Actual -"+overallBasketPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify total prices in the basket, in the extras page");
            throw e;
        }
    }

    private void verifyAviosPriceAddedToBasket(double maxAviosPrice){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice(maxAviosPrice);

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice,2), "avios price is added to the basket");
                reportLogger.log(LogStatus.INFO, "avios price is added to basket");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                throw e;
            }

            actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

            try {
                Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;
            }

        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }

    private double calculateAviosPrice(double maxAviosPrice) {
        // to verify the avios price according to selection
        double aviosPriceApplied = 0.0;
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;


        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
        inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;

        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;

        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }


    public void verifyExtrasPageFooter() {
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "all fares quoted on flybe.com are subject to availability, and are inclusive of all taxes, fees and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            String fPara4 = "Certain Loganair fares have conditions attached requiring a minimum length of stay.";


            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(contactUsLink.isDisplayed(), "Contact Us Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            }



            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


            try {
                Assert.assertEquals(footerPara4.getText().toLowerCase(), fPara4.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify footer in passenger page");
            throw e;
        }
    }


    public void verifyCarHireBookingPopup() {
        // to verify the headings inside the car hire popup
        String firstHeadingExpected = "1. Car hire information";
        String secondHeadingExpected = "2. Young driver surcharges";
        String thirdHeadingExpected = "3. Car hire excesses";
        try {
            actionUtility.click(carHireInformation.get(0));
            //actionUtility.waitForPageURL(CarHireRuleInfoURL, 10);
//            switchToCarHireRuleInfoWindow();
            actionUtility.waitForElementVisible(carHireInformationPopup,5);
            try {
                Assert.assertEquals(firstHeadingExpected.toLowerCase().trim(), carHireInformationHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Car Hire Information heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Car Hire Information is not matching"+" Expected "+firstHeadingExpected+ " Actual "+ carHireInformationHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Car Hire Information is not displayed"+" Expected "+firstHeadingExpected+ " Actual "+ carHireInformationHeading.getText());
                throw e;
            }
            try {
                Assert.assertEquals(secondHeadingExpected.toLowerCase().trim(), youngDriverHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Young driver surcharges heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Young driver surcharges is not matching"+" Expected "+secondHeadingExpected+" Actual "+ youngDriverHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Young driver surcharges is not displayed"+" Expected "+secondHeadingExpected+" Actual "+ youngDriverHeading.getText());
                throw  e;
            }
            try {
                Assert.assertEquals(thirdHeadingExpected.toLowerCase().trim(), carHireHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Car hire excesses heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Car hire excesses is not matching"+" Expected "+thirdHeadingExpected +" Actual "+ carHireHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Car hire excesses is not displayed"+" Expected "+thirdHeadingExpected +" Actual "+ carHireHeading.getText());
                throw  e;
            }

        }catch (AssertionError e){
            throw e;
        }catch (NoSuchElementException e) {
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Car hire booking page popup windows is not displayed and not verified");
            throw e;
        }

    }


}
