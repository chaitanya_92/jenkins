package leapfrog.business_logics;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.*;


public class LeapPayment {
    ActionUtility actionUtility;
    ExtentTest reportLogger;


    public LeapPayment() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "paymentMethodTypeCard")
    private WebElement paymentByCard;

    @FindBy(id = "paymentMethodTypePayPal")
    private WebElement paymentByPayPal;

    @FindBy(css = "form#tripForm p input")
    private List<WebElement> tripPurposes;

    @FindBy(css = "form#tripForm div p")
    private List<WebElement> tripPurposeOptions;

    @FindBy(id = "newContinueButton")
    private WebElement continueToBooking;

    @FindBy(css = "li.input input:not([type='hidden'])")
    private List<WebElement> termsAndConditions;

    @FindBy(id = "NewCardType")
    private WebElement cardType;

    @FindBy(id = "CardNumber")
    private WebElement cardNumber;

    @FindBy(id = "ExpiryDate")
    private WebElement expiryDate;

    @FindBy(id = "ExpiryDateYear")
    private WebElement expiryDateYear;

    @FindBy(id = "CardName")
    private WebElement nameOnCard;

    @FindBy(id = "SaveCardCheck")
    private WebElement saveCard;

    @FindBy(id = "SecurityNumber")
    private WebElement securityNumber;

    @FindBy(css = "div#paymentFormWrapperDiv>div>input")
    private List<WebElement> billingAddressOption;

    @FindBy(id = "billingCountry")
    private WebElement billingCountry;

    @FindBy(id = "billingPostalLookupLink")
    private WebElement findAddress;

    @FindBy(id = "fancybox-content")
    private WebElement billingAddressListPopup;

    @FindBy(className = "pafleft")
    private List<WebElement> billingAddresses;

    @FindBy(xpath = "//div[@class='pafright']/input")
    private List<WebElement> billingAddressSelectors;

    @FindBy(css = "input#billingPostcode")
    private WebElement billingPostCode;

    @FindBy(id = "submit_button")
    private WebElement billingAddressContinue;

    @FindBy(id = "dontHavePostcode")
    private WebElement dontHavePostCode;

    @FindBy(id = "billingAddress")
    private WebElement billingAddressOne;

    @FindBy(id = "billingAddress2")
    private WebElement billingAddressTwo;

    @FindBy(id = "billingAddress3")
    private WebElement billingAddressThree;

    @FindBy(id = "billingTown")
    private WebElement billingTown;

    @FindBy(id = "billingCounty")
    private WebElement billingCounty;

    @FindBy(css = "input[id^='savedCard']:not([type='hidden'])")
    private List<WebElement> savedCards;

    @FindBy(css = "label[for^='savedCard']")
    private List<WebElement> savedCardNames;

    @FindBy(id = "paymentMethodCard")
    private WebElement differentPaymentCard;

    @FindBy(xpath = "//span[contains(text(),\"Continue with new basket price\")]")
    private WebElement continueWithNewPrice;

    @FindBy(id = "newTotalPayable")
    private WebElement newBasketPrice;


    @FindBy(css = "a.button.medium.fbyellow>span")
    private WebElement reselectFlight;

    @FindBy(xpath = "//ul[contains(@class,'alert-message error')]/li/strong[contains(text(),'supply a valid Security number')]")
    private WebElement securityNumberValidation;

    @FindBy(id = "email")
    private WebElement payPalEmail;

    @FindBy(id = "password")
    private WebElement payPalPassword;

    @FindBy(id = "btnLogin")
    private WebElement submitPayPal;

    @FindBy(id = "confirmButtonTop")
    private WebElement payNowPayPal;

    @FindBy(xpath = "//p[@id='spinner-message']")
    private WebElement payPalSpinner;

    String payPalProcessingSpinner = "//p[@id='spinner-message']";

    @FindBy(className = "heading")
    private WebElement validationMessageHeader;

    @FindBy(css = ".alert-message p:nth-child(1)")
    private WebElement validationMessage;

    @FindBy(css = ".contentPanelContent>ul>li")
    private List<WebElement> validationMessageDetails;

    @FindBy(css = ".contentPanelContent>p:nth-of-type(1)")
    private WebElement solution;

    @FindBy(css = ".contentPanelContent>p:nth-of-type(2)")
    private WebElement solutionDetail;

    @FindBy(css = ".alert-message>li")
    private List<WebElement> mandatoryValidationMessage;

    @FindBy(css = "#lastChanceWrap .routeHeading h1.heading")
    private WebElement lastChanceHeading;

    @FindBy(css = ".lastChancePrice>span:nth-of-type(2)")
    private WebElement lastChanceBaggagePrice;

    @FindBy(css = "#addBagsButton>span")
    private WebElement lastChanceAddBaggage;

    @FindBy(css = "#removeBagsButton>span")
    private WebElement lastChanceRemoveBaggage;

    @FindBy(css = "#fareRulesPopUp")
    private WebElement fareRulesLink;

    @FindBy(id = "fancybox-content")
    private WebElement rulePopup;

    @FindBy(css = "#fareRules_ruleDisplay h1")
    private WebElement fareRulesPopupHeading;

    @FindBy(xpath = "//a[contains(text(),'general conditions of carriage')]")
    private WebElement generalConditionsOfCarriageLink;

    @FindBy(xpath = "//div[@id='tabs-1']/h1")
    private WebElement generalConditionsOfCarriageHeader;

    @FindBy(xpath = "//a[contains(text(),'dangerous goods in baggage')]")
    private WebElement dangerousGoodsInBaggageLink;

    @FindBy(xpath = "//div[@id='dangerousgoodPopId']/h2")
    private WebElement dangerousGoodsInBaggageHeader;

    @FindBy(css = "select#billingCountry")
    private WebElement billingAddressCountry;

    //-------avios-------------------------

    @FindBy(id = "big")
    private WebElement aviosHighOption;

    @FindBy(id = "mid")
    private WebElement aviosMediumOption;

    @FindBy(id = "low")
    private WebElement aviosLowOption;

    @FindBy(css = "#smallBucket span")
    private WebElement aviosLowPrice;

    @FindBy(css = "#mediumBucket span")
    private WebElement aviosMediumPrice;

    @FindBy(css = "#bigBucket span")
    private WebElement aviosHighPrice;

    @FindBy(css = ".payWithAviosBuckets > div>div> label>span")
    private WebElement aviosBucket;

    //----------------avios basket-----------

    @FindBy(id = "basket.aviosPartPay.value")
    private WebElement aviosBasketPrice;

    //----------Footer-in-Home-Page-------------
    @FindBy(css = "footer.container a[href='/'] img")
    private WebElement flybeLogo;

    @FindBy(css = "footer.container li:nth-child(1)")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    @FindBy(css = "footer.container a[href='/contact']")
    private WebElement ContactUsLink;

    @FindBy(css = "footer.container p a[href='/']")
    private WebElement flybeInFooter;

    @FindBy(css = "a[href='/terms/tariff.htm']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = "footer.container p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = "footer.container p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = "footer.container p:nth-child(3)")
    private WebElement footerPara3;


    //    ----basket outbound & inbound total ------
    @FindBy(id = "basket.sectors.outbound.total")
    private WebElement outboundTotal;

    @FindBy(id = "basket.sectors.return.total")
    private WebElement returnTotal;

    //    -----basket  price----------
    @FindBy(id = "basket.runningTotal.value")
    private WebElement overallBasketTotal;

    @FindBy(id = "basket.travelInsurance.value")
    private WebElement basketTravelInsurancePrice;

    @FindBy(id = "basket.passengerBags.value")
    private WebElement basketBaggagePrice;


    @FindBy(id = "basket.ticketFlexibility.value")
    private WebElement basketFlexPrice;

    @FindBy(id = "basket.passengerSeats.value")
    private WebElement basketSeatPrice;

    @FindBy(id = "basket.carParking.value")
    private WebElement basketCarParkingPrice;

    @FindBy(id = "basket.carHire.payLater.value")
    private WebElement basketCarHirePrice;

    @FindBy(id = "basket.cardCharges.value")
    private WebElement basketCardPrice;


    public void enterCardDetails(Hashtable<String, String> testData) {
        //selects the payment to be made from saved or different card and populate the card details
        try {
            boolean isDifferentCard = selectCardTypeOption(testData);

            if (isDifferentCard) {
                populateCardDetails(testData.get("cardName"));
            } else {
                selectSavedCard(testData.get("savedCardName"));
            }

            reportLogger.log(LogStatus.INFO, "successfully selected/entered cards details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter cards details");
            throw e;
        }
    }

    public boolean selectCardTypeOption(Hashtable<String, String> testData) {
        // select the card type option
        try {
            boolean isDifferentCard = false;

            if (testData.get("savedCardName") != null) {
                return isDifferentCard;
            } else {

                try {
                    isDifferentCard = true;
                    actionUtility.waitForElementVisible(differentPaymentCard, 3);
                    actionUtility.selectOption(differentPaymentCard);
                    actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, "");

                } catch (TimeoutException e) {
                }

            }
            reportLogger.log(LogStatus.INFO, "selected card type option");
            return isDifferentCard;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select card type option");
            throw e;
        }
    }

    private void selectSavedCard(String savedCardName) {
        //selects the saved card
        boolean flag = false;

        for (int i = 0; i < savedCards.size(); i++) {

            if (savedCardNames.get(i).getText().toLowerCase().contains(savedCardName)) {
                actionUtility.selectOption(savedCards.get(i));
                reportLogger.log(LogStatus.INFO, "successfully selected " + savedCardName + " from the list");
                flag = true;
                break;
            }
        }

        if (flag == false) {
            reportLogger.log(LogStatus.WARNING, "unable to select saved cards");
            throw new RuntimeException("unable to select saved cards");
        }

    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.waitForElementVisible(cardType, 5);
        actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, cardDetails.getAsJsonObject(cardName).get("cardType").getAsString());
        cardNumber.sendKeys(cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryDate, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[0]);
        actionUtility.dropdownSelect(expiryDateYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        nameOnCard.clear();
        nameOnCard.sendKeys(cardDetails.getAsJsonObject(cardName).get("nameOnCard").getAsString());
        securityNumber.sendKeys(cardDetails.getAsJsonObject(cardName).get("securityNumber").getAsString());
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
    }



    public void enterPayPalCardDetails(Hashtable<String, String> testData) {
//        Selects the paypal login details and enters the username and password and completes the booking
        try {
            System.out.println("*******");
            actionUtility.waitForPageLoad(10);
            try {
                actionUtility.waitForElementVisible(payPalSpinner,5);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 15);
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID, "injectedUl");
            if (actionUtility.verifyIfElementIsDisplayed(payPalEmail)) {
                if ((testData.get("payPalUserName") != null) && (testData.get("payPalPassword")) != null) {
                    payPalEmail.clear();
                    payPalEmail.sendKeys(testData.get("payPalUserName"));
                    payPalPassword.clear();
                    payPalPassword.sendKeys(testData.get("payPalPassword"));
                    actionUtility.click(submitPayPal);
                    actionUtility.switchBackToWindowFromFrame();
                    actionUtility.waitForElementVisible(payPalSpinner, 20);
                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 40);
                    actionUtility.click(payNowPayPal);
                    actionUtility.waitForElementVisible(payPalSpinner, 20);
                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 40);
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully entered paypal details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter paypal details");
            throw e;
        }

    }



    public void enterPassengerBillingAddressManually(Hashtable<String, String> testData) {
        // enters the passenger the contact address manually
        try {

            if (new Select(billingCountry).getFirstSelectedOption().getText().trim().equals("UNITED KINGDOM")) {
                actionUtility.selectOption(dontHavePostCode);
            }

            if (testData.get("billingPostCode") != null) {
                billingPostCode.clear();
                billingPostCode.sendKeys(testData.get("billingPostCode"));

            }

            billingAddressOne.sendKeys(testData.get("billingAddressOne"));

            if (testData.get("billingAddressTwo") != null) {
                billingAddressTwo.sendKeys(testData.get("billingAddressTwo"));
            }

            if (testData.get("billingAddressThree") != null) {
                billingAddressThree.sendKeys(testData.get("billingAddressThree"));
            }

            billingTown.sendKeys(testData.get("billingTown"));

            billingCounty.sendKeys(testData.get("billingCounty"));

            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact address manually");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact address manually");
            throw e;
        }
    }

    public void selectBillingAddress(Hashtable<String, String> testData) {
        //selects the passenger billing address from the list
        billingPostCode.clear();
        billingPostCode.sendKeys(testData.get("billingPostCode"));
        try {
            actionUtility.click(findAddress);
            actionUtility.waitForElementVisible(billingAddressListPopup, 5);
            for (int i = 0; i < billingAddresses.size(); i++) {

                if (billingAddresses.get(i).getText().toLowerCase().equals(testData.get("contactPassengerAddresses").toLowerCase())) {
                    actionUtility.selectOption(billingAddressSelectors.get(i));
                    break;
                }
            }
            actionUtility.click(billingAddressContinue);
            reportLogger.log(LogStatus.INFO, "successfully selected the billing address from the list");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the billing contact address from the list");
            throw e;
        }
    }

    public void selectTheBillingAddressCountryDropDown(Hashtable<String, String> testData) {
        // to select the country drop down under my billing address section
        try {
            actionUtility.dropdownSelect(billingCountry, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("billingAddressCountry"));
            reportLogger.log(LogStatus.INFO, "successfully selected the country in my billing address");

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the country in my billing address");
            throw e;
        }

    }

    public void selectBillingAddressOption(Boolean flag) {
        //select the billing address option
        try {
            if (flag) {
                actionUtility.selectOption(billingAddressOption.get(0));
            } else {
                actionUtility.selectOption(billingAddressOption.get(1));
            }
            reportLogger.log(LogStatus.INFO, "successfully to select the billing address option");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the billing address option");
            throw e;
        }
    }

    public void selectPaymentOption(Hashtable<String, String> testData) {
        // selects the payement type and enters the detail
        try {
            if (testData.get("paymentType").toLowerCase().contains("card")) {
                actionUtility.selectOption(paymentByCard);
                reportLogger.log(LogStatus.INFO, "successfully selected card payment option");

            } else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
                actionUtility.selectOption(paymentByPayPal);
                reportLogger.log(LogStatus.INFO, "successfully selected paypal payment option");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select payment option");
            throw e;
        }
    }

//    public void verifySaveCardIsDisplayed(boolean flag){
////        this method selects the save card radio button
//        try{
//            actionUtility.selectOption(saveCard);
//            reportLogger.log(LogStatus.INFO,"save card option is selected successfully");
//        }catch (Exception e){
//            reportLogger.log(LogStatus.WARNING,"unable to select save card option");
//            throw e;
//        }
//    }





    public void selectTripPurpose(Hashtable<String, String> testData) {
        //selects the trip purpose
        try {
            String tripPurpose = testData.get("tripPurpose").toLowerCase();

            if (tripPurposes.size() != 0) {

                if (tripPurpose.contains("business")) {
                    actionUtility.selectOption(tripPurposes.get(0));
                } else if (tripPurpose.contains("short break")) {
                    actionUtility.selectOption(tripPurposes.get(1));
                } else if (tripPurpose.contains("longer holiday")) {
                    actionUtility.selectOption(tripPurposes.get(2));
                } else if (tripPurpose.contains("visiting")) {
                    actionUtility.selectOption(tripPurposes.get(3));
                } else if (tripPurpose.contains("other")) {
                    actionUtility.selectOption(tripPurposes.get(4));
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the trip purpose - " + tripPurpose);
            } else {
                reportLogger.log(LogStatus.WARNING, tripPurpose + " trip purpose is not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the trip purpose");
            throw e;
        }
    }

    public void acceptTermsAndCondition() {
        //accepts the terms and conditions
        try {

            if (termsAndConditions.size() != 0) {

                for (WebElement condition : termsAndConditions) {
                    actionUtility.selectOption(condition);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the terms and conditions");
            } else {
                reportLogger.log(LogStatus.WARNING, "terms and conditions are not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the terms and conditions");
            throw e;
        }
    }


    public void continueToBooking() {
        //click on continue button for payment and booking
        try {
            actionUtility.click(continueToBooking);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully clicked the option continue to booking");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to booking");
            throw e;
        }
    }

    public double handlePriceChange(String option, double... oldBasketPrice) {
        //handles if there is any price change in between booking process
        try {
            double updatedBasketPrice = 0.0;
            if (oldBasketPrice.length > 0) {
                updatedBasketPrice = oldBasketPrice[0];
            }
            boolean flag = false;
            try {
                actionUtility.waitForPageTitle("flybe.com - Payment", 25);
                flag = true;
            } catch (TimeoutException e) {

            }

            if (flag) {
                if (option.toLowerCase().equals("continue")) {

                    try {
                        updatedBasketPrice = CommonUtility.convertStringToDouble(newBasketPrice.getText());


                        actionUtility.clickByAction(continueWithNewPrice);
                    } catch (NoSuchElementException e) {
                    }


                } else if (option.toLowerCase().equals("reselect flight")) {

                    try {
                        actionUtility.clickByAction(reselectFlight);
                    } catch (NoSuchElementException e) {
                    }
                }

                reportLogger.log(LogStatus.INFO, "successfully selected " + option + "from price change page");

            }

            return updatedBasketPrice;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to " + option + "from price change page");
            throw e;
        }
    }



    public void verifySecurityNumberValidation() {
        // verifies the security validation message
        try {
            Assert.assertTrue(securityNumberValidation.isDisplayed(), "No validation for security number");
            reportLogger.log(LogStatus.INFO, "validation message is displayed when security number is not entered");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "validation message is not displayed when security number is not entered");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, " unable to verify if validation message is displayed when security number is not entered");
            throw e;
        }
    }

    public void verifyValidationForIncorrectPaymentDetails() {
        // verifies the security validation message when incorrect cards details are entered

        try {
            actionUtility.waitForElementVisible(validationMessageHeader, 50);

            String validationHeader = "Payment failed";
            String validation = "Sorry, payment against your card was declined for one of the following reasons:";
            String validationMessageDetailsOne = "the card number is incorrect";
            String validationMessageDetailsTwo = "the security code is incorrect";
            String validationMessageDetailsThree = "the start/expiry date is incorrect";
            String validationMessageDetailsFour = "the issue number is incorrect";
            String validationMessageDetailsFive = "the address entered does not match the card's billing address";
            String validationMessageDetailsSix = "insufficient funds are available";
            String solutionOne = "Please re-submit your details or try again with a different card.";
            String solutionTwo = "If you are experiencing problems, please contact us.";
            try {
                Assert.assertEquals(validationMessageHeader.getText().toLowerCase().trim(), validationHeader.toLowerCase().trim(), "validation message header is not matching");
                reportLogger.log(LogStatus.INFO, "validation message header is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message header is not matching Actual: " + validationMessageHeader.getText().trim() + " Expected: " + validationHeader);
                throw e;
            }

            try {
                Assert.assertEquals(validationMessage.getText().toLowerCase().trim(), validation.toLowerCase().trim(), "validation message is not matching");
                reportLogger.log(LogStatus.INFO, "validation message is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message is not matching  Actual: " + validationMessage.getText().trim() + " Expected: " + validation);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(0).getText().toLowerCase().trim(), validationMessageDetailsOne.toLowerCase().trim(), "validation message detail in Line-1 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-1 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-1 is matching  Actual: " + validationMessageDetails.get(0).getText().trim() + " Expected: " + validationMessageDetailsOne);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(1).getText().toLowerCase().trim(), validationMessageDetailsTwo.toLowerCase().trim(), "validation message detail in Line-2 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-2 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-2 is matching  Actual: " + validationMessageDetails.get(1).getText().trim() + " Expected: " + validationMessageDetailsTwo);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(2).getText().toLowerCase().trim(), validationMessageDetailsThree.toLowerCase().trim(), "validation message detail in Line-3 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-3 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-3 is matching  Actual: " + validationMessageDetails.get(2).getText().trim() + " Expected: " + validationMessageDetailsThree);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(3).getText().toLowerCase().trim(), validationMessageDetailsFour.toLowerCase().trim(), "validation message detail in Line-4 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-4 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-4 is matching  Actual: " + validationMessageDetails.get(3).getText().trim() + " Expected: " + validationMessageDetailsFour);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(4).getText().toLowerCase().trim(), validationMessageDetailsFive.toLowerCase().trim(), "validation message detail in Line-5 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-5 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-5 is matching  Actual: " + validationMessageDetails.get(4).getText().trim() + " Expected: " + validationMessageDetailsFive);
                throw e;

            }

            try {
                Assert.assertEquals(validationMessageDetails.get(5).getText().toLowerCase().trim(), validationMessageDetailsSix.toLowerCase().trim(), "validation message detail in Line-6 is not matching");
                reportLogger.log(LogStatus.INFO, "validation message detail in Line-6 is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message detail in Line-6 is matching  Actual: " + validationMessageDetails.get(0).getText().trim() + " Expected: " + validationMessageDetailsSix);
                throw e;

            }

            try {
                Assert.assertEquals(solution.getText().toLowerCase().trim(), solutionOne.toLowerCase().trim(), "solution one in validation message is not matching");
                reportLogger.log(LogStatus.INFO, "solution one in validation message is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "solution one in validation message is not matching Actual: " + solution.getText().trim() + " Expected: " + solutionOne);
                throw e;

            }

            try {
                Assert.assertEquals(solutionDetail.getText().toLowerCase().trim(), solutionTwo.toLowerCase().trim(), "solution two in validation message is not matching");
                reportLogger.log(LogStatus.INFO, "solution two in validation message is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "solution two in validation message is not matching Actual: " + this.solutionDetail.getText().trim() + " Expected: " + solutionTwo);
                throw e;

            }


        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the validation messages when incorrect card details are entered");
            throw e;
        }
    }

    public void verifyValidationForBlankMandatoryFields() {
//        verifies the validation message when mandatory fields are left blank
        actionUtility.waitForElementVisible(mandatoryValidationMessage.get(0), 5);
        try {
            String cardValidation = "please choose a payment card";
            String dateValidation = "Please supply a valid Expiry date.";
            String nameValidation = "Please supply a valid Name on card.";
            String numberValidation = "Please supply a valid Security number.";
            try {
                Assert.assertEquals(mandatoryValidationMessage.get(0).getText().toLowerCase().trim(), cardValidation.toLowerCase().trim(), "validation message for card is not matching");
                reportLogger.log(LogStatus.INFO, "validation message for card is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message for card is not matching Actual: " + mandatoryValidationMessage.get(0).getText().trim() + " Expected: " + cardValidation);
                throw e;
            }

            try {
                Assert.assertEquals(mandatoryValidationMessage.get(1).getText().toLowerCase().trim(), dateValidation.toLowerCase().trim(), "validation message for expiry date is not matching");
                reportLogger.log(LogStatus.INFO, "validation message for expiry date is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message for expiry date is not matching  Actual: " + mandatoryValidationMessage.get(1).getText().trim() + " Expected: " + dateValidation);
                throw e;

            }

            try {
                Assert.assertEquals(mandatoryValidationMessage.get(2).getText().toLowerCase().trim(), nameValidation.toLowerCase().trim(), "validation message for name on card is not matching");
                reportLogger.log(LogStatus.INFO, "validation message for name on card is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message for name on card is not matching  Actual: " + mandatoryValidationMessage.get(2).getText().trim() + " Expected: " + nameValidation);
                throw e;

            }

            try {
                Assert.assertEquals(mandatoryValidationMessage.get(3).getText().toLowerCase().trim(), numberValidation.toLowerCase().trim(), "validation message for card number is not matching");
                reportLogger.log(LogStatus.INFO, "validation message for card number is not matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "validation message for card number is not matching  Actual: " + mandatoryValidationMessage.get(3).getText().trim() + " Expected: " + numberValidation);
                throw e;

            }


        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the validation messages when mandatory details are left blank for payment");
            throw e;
        }


    }









    public void verifyPaymentPageFooter() {
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on Flybe.com are subject to availability, inclusive of taxes and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            actionUtility.waitForElementVisible(paymentByCard, 20);
            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(ContactUsLink.isDisplayed(), "Contact Us Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeInFooter.isDisplayed(), "Flybe.com Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Flybe.com Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(ancillaryCharges.isDisplayed(), "Ancillary Charges Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Ancillary Charges Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in Payment Page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Payment Page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in Payment Page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in Payment Page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in Payment Page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in Payment Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in Payment Page");
                throw e;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the footer in payment page");
            throw e;
        }
    }



    public void verifyListOfEligibleCardsAndAssociatedFee(Hashtable<String, String> testData,Hashtable <String,Double> basketDetails) {
        // to Verify the card type drop down according to the selection
        boolean isDifferentCard = selectCardTypeOption(testData);
        String cardName;
        for (int i = 1; i <= 8; i++) {
            if (isDifferentCard) {

                cardName = testData.get("cardName"+i);
                verifyListOfEligibleCards(cardName);
                verifyCalculationOFCardCharge(cardName, basketDetails.get("overallBasketPrice"));
                verifyCardChargesAddedToBasket();
            } else {
                reportLogger.log(LogStatus.WARNING, "Unable to verify the list of eligible cards since save card option is chosen");
                throw new RuntimeException("Unable to verify the list of eligible cards since save card option is chosen");
            }

        }

    }

    private void verifyListOfEligibleCards(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.waitForElementVisible(cardType, 5);
        try {
            actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, cardDetails.getAsJsonObject(cardName).get("cardType").getAsString());
            reportLogger.log(LogStatus.INFO, cardName + " Card is displayed");
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.WARNING, cardName + " Card is not displayed");
            throw e;
        }

    }

    private void verifyCalculationOFCardCharge(String selectedCard, double basketTotalPrice){
        //verifies the calculation of card charges for all type of cards in pounds

        try {
            String[] chargedCards = {"visa", "masterCard", "americanExpress", "diners"};

            List<String> chargedCardNames = Arrays.asList(chargedCards);
            double cardCharge = 0.0;
            double basketPrice = 0.0;
            double threePercentOfBasketPrice = 0.0;
            double maxPrice = 0.0;
            String selectedCardValue = new Select(cardType).getFirstSelectedOption().getText();
            if (chargedCardNames.contains(selectedCard)) {

                cardCharge = Math.round(CommonUtility.convertStringToDouble(selectedCardValue) * 100.0) / 100.0;
                basketPrice = Math.round(basketTotalPrice * 100.0) / 100.0;
                threePercentOfBasketPrice = Math.round((0.03 * basketPrice) * 100.0) / 100.0;
                maxPrice = Math.max(threePercentOfBasketPrice, 5.00);
                try {
                    Assert.assertEquals(cardCharge, maxPrice);
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "card charges are not matching Expected - "+maxPrice+" Actual - "+cardCharge);
                    throw e;
                }


            } else {
                try {
                    Assert.assertTrue(selectedCardValue.toLowerCase().contains("free"));
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "card charges are not free");
                    throw e;
                }

            }

            reportLogger.log(LogStatus.INFO, "card charges are matching for the card - " + selectedCard);
        }catch (AssertionError e){

            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "successfully verified the card charges");
            throw e;
        }





    }


    public void verifyDefaultValueOfTermsAndConditionsCheckbox(String checkBoxes) {
        // to verify the terms and conditions check boxes in payment page
        try {
            switch (checkBoxes.toLowerCase()) {
                case "i confirm":
                    try {
                        Assert.assertFalse(termsAndConditions.get(0).isSelected(), "i confirm check box is selected");
                        reportLogger.log(LogStatus.INFO, "i confirm check box is not selected by default");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "i confirm check box is selected by default");
                        throw e;
                    }
                    break;

                case "please keep me update":
                    try {
                        Assert.assertTrue(termsAndConditions.get(1).isSelected(), "please keep me updated check box is not selected");
                        reportLogger.log(LogStatus.INFO, "please keep me updated check box is selected by default");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "please keep me updated check box is not selected by default");
                        throw e;
                    }
                    break;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify terms and conditions check box");
            throw e;
        }

    }


    public void openRulesLinkInTermsAndConditionSectionInPaymentPage(String rulesToOpen) {
        //open the respective rules link from payment page
        try {
            switch (rulesToOpen.toLowerCase()) {

                case "fare rules":
                    actionUtility.click(fareRulesLink);
                    actionUtility.waitForElementVisible(rulePopup, 3);
                    break;
                case "general conditions of carriage":
                    actionUtility.click(generalConditionsOfCarriageLink);
                    actionUtility.waitForElementVisible(rulePopup, 3);
                    break;
                case "dangerous goods":
                    actionUtility.click(dangerousGoodsInBaggageLink);
                    actionUtility.waitForElementVisible(rulePopup, 3);
                    break;

            }
            reportLogger.log(LogStatus.INFO, "successfully opened " + rulesToOpen + " in payment page");

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to open " + rulesToOpen + " in payment page");
            throw e;
        }
    }

    public void verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage(String rulesPopup) {
        //To verify the header in rules popup of payment page
        String fareRulesExpected = "Fare rules";
        String generalConditionsOfCarriageExpected = "General conditions of carriage (Passenger and Baggage) - Flybe Limited";
        String dangerousGoodsInBaggageExpected = "Dangerous goods";

        try {
            switch (rulesPopup.toLowerCase()) {

                case "fare rules":
                    try {
                        Assert.assertEquals(fareRulesPopupHeading.getText().toLowerCase().trim(), fareRulesExpected.toLowerCase().trim(), "Fare rules popup header is not matching in payment page");
                        reportLogger.log(LogStatus.INFO, "Fare Rule popup header is matching in payment page");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "Fare Rule popup header is not matching in payment page" + " Expected - " + fareRulesExpected + " Actual - " + fareRulesPopupHeading.getText());
                        throw e;
                    }
                    break;

                case "general conditions of carriage":
                    try {
                        actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID, "fancybox-frame");
                        Assert.assertEquals(generalConditionsOfCarriageHeader.getText().toLowerCase().trim(), generalConditionsOfCarriageExpected.toLowerCase().trim(), "General conditions of carriage is not matching in payment page");
                        reportLogger.log(LogStatus.INFO, "General conditions of carriage popup header is matching in payment page");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "General conditions of carriage popup header is not matching in payment page" + " Expected - " + fareRulesExpected + " Actual - " + fareRulesPopupHeading.getText());
                        throw e;
                    }
                    break;

                case "dangerous goods":
                    try {
                        actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID, "fancybox-frame");
                        Assert.assertEquals(dangerousGoodsInBaggageHeader.getText().toLowerCase().trim(), dangerousGoodsInBaggageExpected.toLowerCase().trim(), "Dangerous goods in baggage is not matching in payment page");
                        reportLogger.log(LogStatus.INFO, "Dangerous goods in baggage popup header is matching in payment page");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "Dangerous goods in baggage popup header is not matching in payment page" + " Expected - " + fareRulesExpected + " Actual - " + fareRulesPopupHeading.getText());
                        throw e;
                    }
                    break;


            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify" + rulesPopup + "popup in payment page");
            throw e;

        }
    }

    public void verifyTripPurposeOptions() {
        //verify trip purpose check boxes are present
        String expectedTripPurpose[] = {"Business", "Short break", "Longer holiday", "Visiting friends / relatives", "Other"};
        ArrayList<String> tripPurposeValues = new ArrayList<>();
        for (int i = 0; i < tripPurposeOptions.size(); i++) {
            tripPurposeValues.add(tripPurposeOptions.get(i).getText());

        }
        try {
            for (int i = 0; i < expectedTripPurpose.length; i++) {
                if (!(tripPurposeValues.contains(expectedTripPurpose[i]))) {
                    reportLogger.log(LogStatus.WARNING, "trip purpose option Expected - " + expectedTripPurpose[i] + " is not present Actual - " + tripPurposeOptions.get(i).getText());
                    throw new AssertionError("trip purpose option is not present");
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully verifies the trip purpose options ");

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the options in trip purpose section");
            throw e;
        }
    }

    public void verifyTheCardSectionIsHiddenWhenPayPalIsSelected() {
        // to verify the card section is hidden when we select Pay Pal option
        try {
            actionUtility.selectOption(paymentByPayPal);
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(cardType), "Card type is displayed when paypal is selected");
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(cardNumber), "Card number is displayed when paypal is selected");
            reportLogger.log(LogStatus.INFO, "card payment inputs are hidden when pay pal is selected in payment page");

        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "card payment inputs are displayed when pay pal is selected in payment page");
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if card payment inputs are displayed");
            throw e;
        }

    }

    public void verifyCountryAndPostalCodeInputsWhenDifferentBillingAddressOptionIsSelected() {
        // to select the country drop down under my billing address section
        try {
            try {
                Assert.assertTrue(billingCountry.isDisplayed(), "Billing country drop down is not displayed");
                reportLogger.log(LogStatus.INFO, "Billing Country drop down is displayed when different billing address is selected");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Country drop down is not displayed when different billing address is selected");
                throw e;
            }
            try {
                Assert.assertTrue(billingPostCode.isDisplayed(), "Billing post code is not displayed");
                reportLogger.log(LogStatus.INFO, "Billing post code is displayed when different billing address is selected");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Billing post code is not displayed when different billing address is selected");
                throw e;
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the country and post code in my billing address is different");
            throw e;
        }

    }

    public void verifySaveCardIsDisplayed(boolean flag){
//        this method verifies whether saved is displayed
        if(flag){
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(saveCard),"save card option is not displayed");
                reportLogger.log(LogStatus.INFO,"save card option is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"save card option is not displayed");
                throw e;
            }
        }else{
            try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(saveCard),"save card option is displayed");
                reportLogger.log(LogStatus.INFO,"save card option is not displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"save card option is displayed");
                throw e;
            }
        }
    }





    public void addBaggageFromLastChance() {
        //adds the baggage from last chance section
        try {
            actionUtility.waitForElementVisible(lastChanceHeading, 10);
            actionUtility.click(lastChanceAddBaggage);
            reportLogger.log(LogStatus.INFO, "successfully added the baggage from last chance section");
        } catch (Exception e) {
            reportLogger.log(LogStatus.FAIL, "unable to add the baggage from last chance section");
            throw e;
        }
    }

    public void VerifyLastChanceIfSeatsAndBagsAreNotSelected() {
        //to Verify the Last chance section when user has not selected the Seats and Baggages while booking
        try {
            String LastChanceHeadingExpected = "Last chance!";
            actionUtility.waitForElementVisible(lastChanceHeading, 30);
            try {
                Assert.assertEquals(lastChanceHeading.getText().toLowerCase().trim(), LastChanceHeadingExpected.toLowerCase().trim(), "Last chance Heading is displayed as expected");
                reportLogger.log(LogStatus.INFO, "Last Chance Heading Is Displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Last Chance heading is not matching" + "Expected -" + LastChanceHeadingExpected + " Actual " + lastChanceHeading.getText());
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Last Chance heading is not Displayed");
                throw e;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify last chance section");
            throw e;
        }


    }

    public void verifyBaggagePriceAddedByLastChanceInBasket() {
        // verifies the baggage price in the basket
        try {
            if (actionUtility.verifyIfElementIsDisplayed(lastChanceRemoveBaggage)) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice), "baggage price is not added to the basket");
                    reportLogger.log(LogStatus.INFO, "baggage price is added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price is not added to the basket");
                    throw e;
                }

                try {
                    double expectedPrice = -1.0;
                    double actualPrice = 0.0;
                    expectedPrice = CommonUtility.convertStringToDouble(lastChanceBaggagePrice.getAttribute("textContent"));
                    actualPrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "baggage price in the not matching");
                    reportLogger.log(LogStatus.INFO, "baggage price added by last chance section in the basket is matching");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price added by last chance section in the basket is not matching");
                    throw e;
                }
            }
        } catch (AssertionError e) {
            throw e;

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if baggage price is added to the basket");
            throw e;
        }

    }

    public void verifyRemoveBagsButtonIsDisplayed() {
        //verifies if remove bags button is displayed
        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(lastChanceRemoveBaggage), "remove bags button is displayed after adding the bags in last chance section");
            reportLogger.log(LogStatus.INFO, "remove bags button is displayed after adding the bags in last chance section");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "remove bags button is not displayed after adding the bags in last chance section");
            throw e;

        }

    }






//    ----------------basket-------------------

    public double verifyBasket(double... maxAviosPrice) {
        //verifies the basket and return the total basket price
        verifyBaggagePriceAddedByLastChanceInBasket();
        verifyCardChargesAddedToBasket();
        if(maxAviosPrice.length>0){
            verifyAviosPriceAddedToBasket(maxAviosPrice[0]);
        }
        verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();
        Double totalBasketPrice = captureOverallBasketPrice();
        return totalBasketPrice;
    }

    public double captureAviosPriceFromBasket(){
        // returns avios price from basket
        return CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
    }

    private void verifyTotalBasketPriceWithSummationOfOtherElementsInBasket() {
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;
        double travelInsurancePrice = 0.0;
        double carParkingPrice = 0.0;
        double cardPrice = 0.0;

        double sum = 0.0;
        double overallBasketPrice = 1.0;
        double aviosPrice = 0.0;

        try {
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText()) * 100.0) / 100.0;

            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)) {
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)) {
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)) {
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)) {
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketTravelInsurancePrice)) {
                travelInsurancePrice = Math.round(CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText()) * 100.0) / 100.0;
            }

            cardPrice = Math.round(CommonUtility.convertStringToDouble(basketCardPrice.getText()) * 100.0) / 100.0;

            if (actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice)) {
                carParkingPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice)) {
                aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText()) * 100.0) / 100.0;

            }
            sum = Math.round((outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice + travelInsurancePrice + carParkingPrice + cardPrice - aviosPrice) * 100.0) / 100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            Assert.assertEquals(overallBasketPrice, sum, "total basket price in the payment page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO, "total basket price in the payment page is matching with summation of all the element prices in the basket");

        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "total basket price in the payment page is not matching with summation of all the element prices in the basket Expected - " + sum + " Actual -" + overallBasketPrice);
            throw e;

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify totals prices in the basket in payment page");
            throw e;
        }
    }

    private void verifyAviosPriceAddedToBasket(double maxAviosPrice){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice(maxAviosPrice);

            if (expectedPrice > 0) {

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                    throw e;
                }

                actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

                try {
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }

            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is not added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is not added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is added to basket");
                    throw e;
                }
            }
        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }

    private double calculateAviosPrice(double maxAviosPrice) {
        // to verify the avios price according to selection
        double aviosPriceApplied = 0.0;
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;
        double cardPrice = 0.0;

        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;

        if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
            inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketCardPrice)){
            cardPrice = Math.round(CommonUtility.convertStringToDouble(basketCardPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice + cardPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;

        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }

    public void verifyBasketPriceWithBasketPriceOfPreviousPage(Hashtable<String, Double> previousBasketDetails) {
        //verifies if the total basket price in the extras page is same as previous page

        double expectedPrice = 0.0;
        double actualPrice = -1.0;

        try {
            try {
                expectedPrice = previousBasketDetails.get("overallBasketPrice");
                actionUtility.waitForElementVisible(overallBasketTotal, 10);
                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in payment page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in payment page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in payment page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in payment page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("ticketFlexibilityPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("ticketFlexibilityPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketFlexPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "ticket flexibility price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("basketBaggagePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketBaggagePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "baggage price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("seatPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("seatPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "seat Price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "seat Price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "seat Price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("travelInsurancePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("travelInsurancePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "travel insurance price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "travel insurance price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "travel insurance price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("carHirePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("carHirePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketCarHirePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "car hire price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "car hire price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("basketCarParkingPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketCarParkingPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketCarParkingPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "car hire price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "car parking price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("basketAviosPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketAviosPrice");
                    actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "avios price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify prices in the basket, in the payment page against prices of basket in passenger details/ extras page");
            throw e;
        }
    }

    private void verifyCardChargesAddedToBasket() {
//        verifies if card charges are added to basket if selected
        double expectedPrice = -1.0;
        double actualPrice = 0.0;
        String selectedPaymentCard = null;

        try {

            selectedPaymentCard = new Select(cardType).getFirstSelectedOption().getText();

            if (!selectedPaymentCard.equals("")) {

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCardPrice), "card charges are not added to the basket");
                    reportLogger.log(LogStatus.INFO, "card charges are added to the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "card charges are not added to the basket");
                    throw e;
                }

                expectedPrice = CommonUtility.convertStringToDouble(selectedPaymentCard);
                actualPrice = CommonUtility.convertStringToDouble(basketCardPrice.getText());

                if (expectedPrice == 0) {

                    String expected = "FREE";
                    String actual = basketCardPrice.getText();
                    try {
                        Assert.assertEquals(actual, expected, "card charges are not matching in the basket");
                        reportLogger.log(LogStatus.INFO, "card charges are matching in the basket");

                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "card charges are not matching in the basket Expected -" + expected + " Actual - " + actual);
                        throw e;
                    }
                } else {

                    try {
                        Assert.assertEquals(actualPrice, expectedPrice, "card charges are not matching in the basket");
                        reportLogger.log(LogStatus.INFO, "card charges are matching in the basket");

                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "card charges are not matching in the basket Expected -" + expectedPrice + " Actual - " + actualPrice);
                        throw e;
                    }
                }
            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice));
                    reportLogger.log(LogStatus.INFO, "card charges are not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "card charges are not added to the basket");
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if card charges are added to basket");
            throw e;
        }
    }

    private double captureOverallBasketPrice() {
        //if card charges are added to the basket

        try {
            double totalBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            reportLogger.log(LogStatus.INFO, "successfully captured overall Basket price in payment page");
            return totalBasketPrice;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to capture overall Basket price in payment page");
            throw e;
        }


    }






}



