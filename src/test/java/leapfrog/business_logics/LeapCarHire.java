package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;


public class LeapCarHire {
    ActionUtility actionUtility;
    ExtentTest reportLogger;
    public LeapCarHire() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

//    @FindBy(css = "div#car0 div.rightHandWrapper div#addCarButtonSpan0 a")
//    private WebElement addThisCar;

    @FindBy(id = "newContinueButton")
    private WebElement continueButton;

    @FindBy(css = "div#car0 div.carDescriptionWrapper div div.carHireTermsAndConditionsLink a")
    private WebElement CarHireInformation;

    @FindBy(css = "#carhire_rules_container div div div h2")
    private WebElement carHireInformationHeading;

    @FindBy(css = "#surcharges > h2")
    private WebElement youngDriverHeading;

    @FindBy(css = "#excesses > h2")
    private WebElement carHireHeading;

    @FindBy(xpath = "//div[contains(@id,'removeCarButton') and (@style='' or @style='display: block;')]/preceding-sibling::p")
    private WebElement carHirePrice;

    @FindBy(xpath = "//div[contains(@id,'removeCarParkButton') and (@style='display: block;' or @style='')]/preceding-sibling::p")
    private WebElement carParkingPrice;

    @FindBy(css = "#carHireResults div div div div h1")
    private WebElement carHireSectionHeading;

    @FindBy(css = "#carParkingResults div div div h1")
    private WebElement carParkingSectionHeading;

    //    ----basket outbound & inbound total ------
    @FindBy(id = "basket.sectors.outbound.total")
    private WebElement outboundTotal;

    @FindBy(id = "basket.sectors.return.total")
    private WebElement returnTotal;

    //    -----basket  price----------
    @FindBy(id = "basket.runningTotal.value")
    private WebElement overallBasketTotal;

    @FindBy(id = "basket.travelInsurance.value")
    private WebElement basketTravelInsurancePrice;

    @FindBy(id = "basket.passengerBags.value")
    private WebElement basketBaggagePrice;

    @FindBy(id = "basket.ticketFlexibility.value")
    private WebElement basketFlexPrice;

    @FindBy(id = "basket.passengerSeats.value")
    private WebElement basketSeatPrice;

    @FindBy(id = "basket.carParking.value")
    private WebElement basketCarParkingPrice;

    @FindBy(id = "basket.carHire.payLater.value")
    private WebElement basketCarHirePrice;

//    -------------to add car hire----------------
    @FindBy(xpath = "//div[@vendor='Avis']/descendant::span[contains(text(),'Add this car')]")
    private List<WebElement> avisCarHireButton;

    @FindBy(xpath = "//div[@vendor='Budget']/descendant::span[contains(text(),'Add this car')]")
    private List<WebElement> budgetCarHireButton;

    @FindBy(xpath = "//div[@vendor='Avis']/descendant::p[@class='price']")
    private List<WebElement> avisCarHirePrice;

    @FindBy(xpath = "//div[@vendor='Budget']/descendant::p[@class='price']")
    private List<WebElement> budgetCarHirePrice;

    @FindBy(xpath = "//div[@id='carHireResults']/descendant::div[@class='contentPanelContent']")
    private WebElement carHireSection;

    String pageURL = "cam/initialiseChooseExtras.action";
    String CarHireRuleInfoURL = "terms/rules_Avis.html";

    //    ------------to add car park-------------------------------
    @FindBy(xpath = "//div[@id='carParkingResults']/descendant::div[@class='contentPanelContent']")
    private WebElement carParkingSection;

    @FindBy(xpath = "//span[text()='Add parking']")
    private List<WebElement> addThisCarParking;

    @FindBy(id="carReg")
    private WebElement carParkingRegNo;

    //----------------avios basket-----------

    @FindBy(id = "basket.aviosPartPay.value")
    private WebElement aviosBasketPrice;



    public String addCarHire(String carHireType){
//        this method adds the required car hire to the booking and returns the price of car hire selected
        String carHirePriceCalculated = null;
        try{
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(carHireSection,15);
        }catch (TimeoutException e){
            reportLogger.log(LogStatus.WARNING,"car hire section is not displayed");
            throw e;
        }
        try{
            if(carHireType.toLowerCase().equals("avis")){
                actionUtility.click(avisCarHireButton.get(0));
                reportLogger.log(LogStatus.INFO,"successfully selected "+carHireType+" car");
            }else if(carHireType.toLowerCase().equals("budget")){
                actionUtility.click(budgetCarHireButton.get(0));
                reportLogger.log(LogStatus.INFO,"successfully selected "+carHireType+" car");
            }
            carHirePriceCalculated = String.valueOf(CommonUtility.convertStringToDouble(carHirePrice.getText()));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to choose "+carHireType+" car");
            throw e;
        }

        return carHirePriceCalculated;
    }

    public void isExtrasPageLoaded(){
//      this method verifies whether extras page is loaded and if yes it will be clicked on continue and is navigated to next page
        try {
            actionUtility.waitForPageURL(pageURL, 10);
            continueToPaymentPage();
        }catch (TimeoutException e){
            reportLogger.log(LogStatus.INFO,"Extras Page is not Loaded");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to verify whether Extras Page is Loaded or not");
            throw e;
        }
    }

    public void continueToPaymentPage(){
        try{
            actionUtility.click(continueButton);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to the payment page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable continue to the payment page");
            throw e;
        }
    }

    private void switchToCarHireRuleInfoWindow(){
        //        actionUtility.waitForPageURL(CarHireRuleInfoURL, 10);
        boolean isSwitchSuccess = actionUtility.switchWindow("Car Hire Rules | Flybe");
        if (!isSwitchSuccess) {

            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
            if (isSwitchToCertificateErrorSuccess) {

                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");


            } else {
                reportLogger.log(LogStatus.WARNING,"unable to pass control to Car Hire Rules window");
                throw new RuntimeException("unable to pass control to Car Hire Rules window");
            }

            actionUtility.hardSleep(20000);

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Switching to Car Hire Rule Window is Successful");
        }else{
            reportLogger.log(LogStatus.INFO,"Switching to Car Hire Rule Window is successful");

        }

    }

    public void verifyCarHireBookingPopup() {
        // to verify the headings inside the car hire popup
        String firstHeadingExpected = "1. Car hire information";
        String secondHeadingExpected = "2. Young driver surcharges";
        String thirdHeadingExpected = "3. Car hire excesses";
        try {
            actionUtility.click(CarHireInformation);
            //actionUtility.waitForPageURL(CarHireRuleInfoURL, 10);
            switchToCarHireRuleInfoWindow();
            try {
                Assert.assertEquals(firstHeadingExpected.toLowerCase().trim(), carHireInformationHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Car Hire Information heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Car Hire Information is not matching"+" Expected "+firstHeadingExpected+ " Actual "+ carHireInformationHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Car Hire Information is not displayed"+" Expected "+firstHeadingExpected+ " Actual "+ carHireInformationHeading.getText());
                throw e;
            }
            try {
                Assert.assertEquals(secondHeadingExpected.toLowerCase().trim(), youngDriverHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Young driver surcharges heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Young driver surcharges is not matching"+" Expected "+secondHeadingExpected+" Actual "+ youngDriverHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Young driver surcharges is not displayed"+" Expected "+secondHeadingExpected+" Actual "+ youngDriverHeading.getText());
                throw  e;
            }
            try {
                Assert.assertEquals(thirdHeadingExpected.toLowerCase().trim(), carHireHeading.getText().toLowerCase().trim());
                reportLogger.log(LogStatus.INFO,"Car hire excesses heading is verified");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Car hire excesses is not matching"+" Expected "+thirdHeadingExpected +" Actual "+ carHireHeading.getText());
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Car hire excesses is not displayed"+" Expected "+thirdHeadingExpected +" Actual "+ carHireHeading.getText());
                throw  e;
            }

        }catch (AssertionError e){
            throw e;
        }catch (NoSuchElementException e) {
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Car hire booking page popup windows is not displayed and not verified");
            throw e;
        }

    }

    public Hashtable verifyBasket(double... maxAviosPrice){

        verifyCarHireAndCarParkingAddedToBasket();
        if(maxAviosPrice.length>0){
            verifyAviosPriceAddedToBasket(maxAviosPrice[0]);
        }
        Hashtable<String,Double> basketDetails = verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();
        return  basketDetails;

    }

    private void verifyAviosPriceAddedToBasket(double maxAviosPrice){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice(maxAviosPrice);

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                    throw e;
                }

                actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

                try {
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }

        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }

    private double calculateAviosPrice(double maxAviosPrice) {
        // to verify the avios price according to selection
        double aviosPriceApplied = 0.0;
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;


        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
        inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;

        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;

        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }

    public void verifyBasketPriceWithBasketPriceOfPassengerDetailsPage(Hashtable<String,Double> previousBasketDetails){
        //verifies if the total basket price in the extras page is same as previous page


        double expectedPrice =0.0;
        double actualPrice = -1.0;

        try {
            try {
                expectedPrice = previousBasketDetails.get("overallBasketPrice");
                actionUtility.waitForElementVisible(overallBasketTotal,10);
                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in extras page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in extras page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in extras page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in extras page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("ticketFlexibilityPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("ticketFlexibilityPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketFlexPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "ticket flexibility price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("basketBaggagePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketBaggagePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "baggage price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("seatPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("seatPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "seat Price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "seat Price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "seat Price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("travelInsurancePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("travelInsurancePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "travel insurance price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "travel insurance price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "travel insurance price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("basketAviosPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("basketAviosPrice");
                    actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price in the basket is not matching in extras page");
                    reportLogger.log(LogStatus.INFO, "avios price in the basket is matching in extras page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price in the basket is not matching in extras page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify prices in the basket, in the extras page against prices of basket in passenger details page");
            throw e;
        }
    }

    private void verifyCarHireAndCarParkingAddedToBasket() {
//        verifies if car hire and car parking is added to basket if selected
        double expectedPrice = -1.0;
        double actualPrice = 0.0;

        try {

            if (actionUtility.verifyIfElementIsDisplayed(carParkingPrice)) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice));
                    reportLogger.log(LogStatus.INFO, "car parking price is added to the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is not added to the basket");
                    throw e;
                }

                expectedPrice = Math.round(CommonUtility.convertStringToDouble(carParkingPrice.getText())*100.0)/100.0;
                actualPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText())*100.0)/100.0;

                try {
                    Assert.assertEquals(actualPrice,expectedPrice,"car parking price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "car parking price is matching in the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is not matching in the basket Expected -"+expectedPrice+" Actual - "+actualPrice);
                    throw e;
                }

            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice));
                    reportLogger.log(LogStatus.INFO, "car parking price is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price is added to the basket");
                    throw e;
                }
            }

            if (actionUtility.verifyIfElementIsDisplayed(carHirePrice)) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice));
                    reportLogger.log(LogStatus.INFO, "car hire price is added to the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is not added to the basket");
                    throw e;
                }


                expectedPrice = Math.round(CommonUtility.convertStringToDouble(carHirePrice.getText())*100.0)/100.0;
                actualPrice = Math.round(CommonUtility.convertStringToDouble(basketCarHirePrice.getText())*100.0)/100.0;

                try {
                    Assert.assertEquals(actualPrice,expectedPrice,"car hire price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "car parking hire is matching in the basket");

                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is not matching in the basket Expected -"+expectedPrice+" Actual - "+actualPrice);
                    throw e;
                }

            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice));
                    reportLogger.log(LogStatus.INFO, "car hire price is not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price is added to the basket");
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the car hire and car parking in extras page");
            throw e;
        }
    }

    private Hashtable<String, Double> verifyTotalBasketPriceWithSummationOfOtherElementsInBasket(){
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double carParkingPrice =0.0;
        double carHirePrice = 0.0;
        double sum =0.0;
        double overallBasketPrice =1.0;
        double aviosPrice = 0.0;


        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();


        try{
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);




            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
                basketDetails.put("ticketFlexibilityPrice",ticketFlexibilityPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
                basketDetails.put("basketBaggagePrice",bagPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
                basketDetails.put("seatsPrice",seatsPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketTravelInsurancePrice)){
                travelInsurancePrice = Math.round(CommonUtility.convertStringToDouble(basketTravelInsurancePrice.getText())*100.0)/100.0;
                basketDetails.put("travelInsurancePrice",travelInsurancePrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice)){
                carHirePrice = Math.round(CommonUtility.convertStringToDouble(basketCarHirePrice.getText())*100.0)/100.0;
                basketDetails.put("carHirePrice",carHirePrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice)){
                carParkingPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText())*100.0)/100.0;
                basketDetails.put("carParkingPrice",carParkingPrice);
            }
            if (actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice)) {
                aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText()) * 100.0) / 100.0;
                basketDetails.put("aviosPrice",aviosPrice);

            }

            sum = Math.round((outboundPrice+inboundPrice+ticketFlexibilityPrice+bagPrice+seatsPrice+travelInsurancePrice+carParkingPrice-aviosPrice)*100.0)/100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            basketDetails.put("overallBasketPrice",overallBasketPrice);
            Assert.assertEquals(overallBasketPrice,sum,"total basket price in the extras page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO,"total basket price in the extras page is matching with summation of all the element prices in the basket");
            return basketDetails;
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"total basket price in the extras page is not matching with summation of all the element prices in the basket Expected - "+sum+" Actual -"+overallBasketPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify total prices in the basket, in the extras page");
            throw e;
        }
    }

    public void addCarParking(Hashtable<String,String> testData){
//        this method adds the car parking to the current booking
        try{
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(carParkingSection,10);
        }catch (TimeoutException e){
            reportLogger.log(LogStatus.WARNING,"car parking section is not displayed");
            throw e;
        }
        try{
            actionUtility.click(addThisCarParking.get(0));
            actionUtility.waitForElementVisible(carParkingRegNo,10);
            carParkingRegNo.clear();
            actionUtility.sendKeysByAction(carParkingRegNo,testData.get("carParkingRegNo"));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify car parking");
            throw e;
        }
    }

    public void verifyCarHireIsDisplayedInExtrasPage() {
        // to verify the car hire heading is displayed in extras page
        String carHireHeadingExpected = "Car hire - Book your car now and save!";

        try {
            try{
                actionUtility.waitForElementVisible(carHireSectionHeading,10);
                reportLogger.log(LogStatus.INFO,"Car hire is displayed in extras page");
            }catch (TimeoutException e) {
                reportLogger.log(LogStatus.WARNING,"Car hire is not displayed in extras page");
                throw e;
            }
            try {
                Assert.assertEquals(carHireHeadingExpected.toLowerCase().trim(), carHireSectionHeading.getText().toLowerCase().trim(), "car hire is not displayed in extras page");
                reportLogger.log(LogStatus.INFO,"Car Hire Page is displayed in extras page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"car hire page is not matching"+" Expected - "+carHireHeadingExpected+ " Actual - "+ carHireSectionHeading.getText());
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch (TimeoutException e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify car hire section in extras");
            throw e;
        }
    }

    public void verifyCarParkingIsDisplayedInExtrasPage() {
        // to verify the car parking heading is displayed in extras page
        String carHireHeadingExpected = "Car parking - get the best deal today!";
        try {
              try{
                   actionUtility.waitForElementVisible(carParkingSectionHeading,10);
                    reportLogger.log(LogStatus.INFO,"Car parking is displayed in extras page");
                }catch (TimeoutException e) {
                    reportLogger.log(LogStatus.WARNING,"Car parking is not displayed in extras page");
                    throw e;
                }
            try{
                Assert.assertEquals(carHireHeadingExpected.toLowerCase().trim(), carParkingSectionHeading.getText().toLowerCase().trim(), "car parking heading is not matching in extras page");
                reportLogger.log(LogStatus.INFO,"Car parking heading is matching in extras page");
            }catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "car parking heading is not matching in extras page" + " Expected - " + carHireHeadingExpected + " Actual - " + carParkingSectionHeading.getText());
                throw e;
            }

        }catch (AssertionError e){
            throw e;
        }catch (TimeoutException e){
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify car parking section in extras page");
            throw e;
        }
    }

    public void verifyAviosNotAppliedForCarHireAndCarParkingInBasket(){
        double maxAviosPrice = -1.0;
        double mediumAviosPrice = -1.0;
        double lowAviosPrice = -1.0;

        double appliedAviosPrice = -1.0;
        double outboundPrice=0.0;
        double inboundPrice=0.0;
        double ticketFlexibilityPrice =0.0;
        double bagPrice = 0.0;
        double seatsPrice =0.0;
        double travelInsurancePrice = 0.0;
        double carParkingPrice =0.0;
        double carHirePrice = 0.0;
        double aviosSum =0.0;
        double overallBasketPrice =1.0;
        double aviosPrice = 0.0;
        double totalBasketAfterAvios = 0.0;

        Hashtable<String,Double> basketDetails = new Hashtable<String, Double>();

        try{


            appliedAviosPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;
            basketDetails.put("outboundPrice",outboundPrice);

            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
            aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText())*100.0)/100.0;
            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
                basketDetails.put("inboundPrice",inboundPrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarHirePrice)){
                carHirePrice = Math.round(CommonUtility.convertStringToDouble(basketCarHirePrice.getText())*100.0)/100.0;
                basketDetails.put("carHirePrice",carHirePrice);
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice)) {
                carParkingPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText()) * 100.0) / 100.0;
                basketDetails.put("carParkingPrice", carParkingPrice);
            }
                if (maxAviosPrice > appliedAviosPrice) {

                    try {
                        Assert.assertEquals(appliedAviosPrice, outboundPrice + inboundPrice, "applied avios in basket are not equal to outbound flight");
                        reportLogger.log(LogStatus.INFO, "avios options is not displayed");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "avios options is not displayed");
                        throw e;
                    }
                    aviosSum = Math.round((outboundPrice + inboundPrice + carHirePrice + carParkingPrice - aviosPrice) * 100.0) / 100.0;
                    overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                    basketDetails.put("overallBasketPrice", overallBasketPrice);
                    try {
                        Assert.assertEquals(overallBasketPrice, aviosSum, "applied avios in basket are not equal to outbound flight");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "avios options is not displayed");
                        throw e;
                    }

                } else {
                    reportLogger.log(LogStatus.WARNING, "Unable to verify the applied avios in basket");
                    throw new RuntimeException("Unable to verify the applied avios in basket");
                }


        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify basket");
            throw e;
        }

    }

}

