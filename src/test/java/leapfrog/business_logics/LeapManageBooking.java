package leapfrog.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;
import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;

import java.util.Hashtable;
import java.util.List;

public class LeapManageBooking {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LeapManageBooking() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath ="//div[@class='booking']/descendant::button[text()='Change booking']")
    private WebElement changeBooking;

    @FindBy(xpath ="//button[text()='Check-in']")
    private WebElement checkIn;

    @FindBy(className ="loading-spinner")
    private WebElement changeBookingLoader;

    @FindBy(xpath ="//div[@class='booking']/div[@class='row']/div[@class='left-buttons hidden-xs col-sm-2']//button")
    private List<WebElement> manageButtons;

    //-------------------boxever------------------------------

    @FindBy(xpath = "//div[@class='col-md-3 col-xs-12']")
    private WebElement boxEverElements;

    @FindBy(xpath = "//span[text()='Add baggage']")
    private WebElement addBag;

    @FindBy(xpath = "//span[text()='Add a seat']")
    private WebElement addSeat;

    @FindBy(xpath = "//span[text()='Add car hire']")
    private WebElement addCarHire;

    @FindBy(xpath = "//span[text()='Add a hotel room']")
    private WebElement addHotel;

    @FindBy(xpath = "//span[text()='Add travel insurance']")
    private WebElement addInsurance;

    @FindBy(xpath = "//span[text()='Add airport parking']")
    private WebElement addCarParking;


    String changeBookingLoaderIndicator = "//span[@class='loading-spinner']";

    public void continueToChangeBooking(){
        //click on change booking button
        try {
            actionUtility.waitForElementClickable(changeBooking,20);
            actionUtility.click(changeBooking);
            actionUtility.waitForPageLoad(10);
            try {
                actionUtility.waitForElementVisible(changeBookingLoader, 8);
            }catch (TimeoutException e){}

            actionUtility.waitForElementNotPresent(changeBookingLoaderIndicator, 20);
            reportLogger.log(LogStatus.INFO,"successfully continued to change booking");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to change booking");
            throw e;
        }

    }


    public void verifyChangeBookingIsEnabled(boolean flag){
//        this method verifies whether change flight button is enabled
        try{

            if(flag){
                try{
                    Assert.assertTrue(changeBooking.isEnabled());
                    reportLogger.log(LogStatus.INFO,"change flight button is enabled");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change flight button is disabled");
                    throw e;
                }
            }else{
                try{
                    Assert.assertFalse(changeBooking.isEnabled());
                    reportLogger.log(LogStatus.INFO,"change flight button is disabled");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change flight button is enabled");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify status of change flight button");
            throw e;
        }

    }


    public void waitAndCheckForTicketing(int timeFactor) {
        //checks if check-in button is enabled
        try {
            int count = 0;
            actionUtility.hardSleep(2000);
            while (manageButtons.size()== 0 && count <= timeFactor){
                actionUtility.hardSleep(4000);
                count++;
                try {
                    TestManager.getDriver().navigate().refresh();
                }catch (TimeoutException e){}
                actionUtility.waitForPageLoad(25);
                actionUtility.hardSleep(2000);
                actionUtility.switchBackToWindowFromFrame();
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                try {
                    actionUtility.waitForElementVisible(changeBookingLoader, 15);
                }catch (TimeoutException e){}
                actionUtility.waitForElementNotPresent(changeBookingLoaderIndicator,30);

            }
            if (manageButtons.size()== 0) {
                throw new RuntimeException("booking is not ticketed");
            }
            reportLogger.log(LogStatus.INFO,"booking is ticketed");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"booking is not ticketed");
            throw e;
        }
    }


    public void continueToCheckIn(){
        //click on checking button
        try {
            actionUtility.click(checkIn);
            actionUtility.waitForPageLoad(10);
            try {
                actionUtility.waitForElementVisible(changeBookingLoader, 8);
            }catch (TimeoutException e){}

            actionUtility.waitForElementNotPresent(changeBookingLoaderIndicator, 20);
            reportLogger.log(LogStatus.INFO,"successfully continued to check-in");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to check-in");
            throw e;
        }

    }


    public void verifyURLForPassengerName(Hashtable<String,String> passengerNames){
        try {
            String[] nameForCheckIn = passengerNames.get("passengerName1").split(" ");
            String expected = "forename=" + nameForCheckIn[1] + "&surname=" + nameForCheckIn[2];
            String currentUrl = TestManager.getDriver().getCurrentUrl();

            System.out.println(currentUrl+"****");
            System.out.println(expected+"^^^^^");
            Assert.assertTrue(currentUrl.toLowerCase().contains(expected.toLowerCase()));
            reportLogger.log(LogStatus.INFO,"passenger name  used to retrieve booking is present in the check-in url");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"passenger name used to retrieve booking is not present in the check-in url");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if passenger name used to retrieve booking is present in the check-in url");
            throw e;
        }


    }

    public void verifyCarParkingIsNotDisplayed(){
        try{//verifies that for booking done with car parking, add airport parking will not displayed
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(boxEverElements,20);
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(addCarParking,20),"add car parking should not be displayed");
            reportLogger.log(LogStatus.INFO,"successfully verified that car parking is not displayed in manage my booking when no car parking is available for the selected route");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"car parking is displayed in manage my booking when no car parking is available for the selected route");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if car parking is displayed in manage my booking when no car parking is available for the selected route");
            throw e;
        }
    }

    public void verifyBoxeverForBagAndSeat(){
        //verifies whether Add Bag and Add Seat boxever elements are present
        try{
            actionUtility.waitForElementVisible(boxEverElements,20);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addSeat,10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addBag,10));
            reportLogger.log(LogStatus.INFO,"successfully verified that add baggage and add a seat are displayed in boxever section");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"add baggage and add a seat are not displayed in boxever section");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify if add baggage and add a seat in boxever section");
            throw e;
        }

    }

    public void selectElementFromBoxever(String boxeverElement)
    {//clicks on specified element in boxever
        try{
            actionUtility.waitForElementVisible(boxEverElements,10);
            if(boxeverElement.equalsIgnoreCase("bag")){
                actionUtility.click(addBag);
            }
            else if(boxeverElement.equalsIgnoreCase("seat")){
                actionUtility.click(addSeat);
            }
            else if(boxeverElement.equalsIgnoreCase("insurance")){
                actionUtility.click(addInsurance);
            }
            else if(boxeverElement.equalsIgnoreCase("hotel")){
                actionUtility.click(addHotel);
            }
            else if(boxeverElement.equalsIgnoreCase("carparking")){
                actionUtility.click(addCarParking);
            }
            else if(boxeverElement.equalsIgnoreCase("carhire")){
                actionUtility.click(addCarHire);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select "+boxeverElement+" from boxever");
            throw e;
        }
    }

    public void verifyBoxeverElements() {
        //verifies whether Add Bag and Add Seat boxever elements are present
        try {
            actionUtility.waitForElementVisible(boxEverElements, 20);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addSeat, 10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addBag, 10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addInsurance, 10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addHotel, 10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addCarParking, 10));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(addCarHire, 10));
            reportLogger.log(LogStatus.INFO, "successfully verified that all boxever elements are displayed in boxever section");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "all boxever elements are not displayed in boxever section");
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if all boxever elements in boxever section");
            throw e;
        }
    }





}