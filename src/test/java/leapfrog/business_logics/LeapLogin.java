package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;


public class LeapLogin {
    ActionUtility actionUtility ;
    ExtentTest reportLogger;

    public LeapLogin() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "loginRadio")
    private WebElement loginOption;

    @FindBy(id = "username")
    private WebElement userName;

    @FindBy(id = "passwordInput")
    private WebElement password;

    @FindBy(id = "loginFormSubmit")
    private WebElement continueToLoginRegistering;

    @FindBy(className = "body")
    private WebElement loginContainer;

    @FindBy(id = "registerRadio")
    private WebElement registrationOption;

    @FindBy(id = "email")
    private WebElement registrationPassengerEmail;

    @FindBy(id = "password")
    private WebElement registrationPassengerPassword;

    @FindBy(id = "confirmpassword")
    private WebElement registrationPassengerConfirmPassword;

    @FindBy(name = "title")
    private WebElement registrationPassengerTitle;

    @FindBy(id = "forenames")
    private WebElement registrationPassengerFirstName;

    @FindBy(id = "surname")
    private WebElement registrationPassengerLastName;

    @FindBy(name = "gender")
    private WebElement registrationPassengerGender;

    @FindBy(id = "dateOfBirth")
    private WebElement registrationPassengerDOB;

    @FindBy(id = "country")
    private WebElement registrationPassengerCountry;

    @FindBy(id = "postcode")
    private WebElement registrationPassengerPostCode;

    @FindBy(id = "addressLine1")
    private WebElement registrationAddress1;

    @FindBy(id = "addressLine2")
    private WebElement registrationAddress2;

    @FindBy(id = "addressLine3")
    private WebElement registrationAddress3;

    @FindBy(id = "town")
    private WebElement registrationTown;

    @FindBy(id = "county")
    private WebElement registrationCounty;

    @FindBy(id = "diallingCodeDay")
    private WebElement registrationDiallingCodeLand;

    @FindBy(id = "homephone")
    private WebElement registrationHomePhone;

    @FindBy(id = "diallingCodeMobile")
    private WebElement registrationDiallingCodeMobile;

    @FindBy(id = "mobilephone")
    private WebElement registrationMobilePhone;

    @FindBy(id = "aviosSelectionRegister")
    private WebElement registerWithAvios;

    @FindBy(id = "aviosSelectionOptout")
    private WebElement registerWithoutAvios;

    @FindBy(id = "aviosSelectionLink")
    private WebElement registerWithExistingAvios;

    @FindBy(id = "termsAndConditions")
    private WebElement termsAndConditions;

    @FindBy(css = "input.button")
    private WebElement register;

    @FindBy(css = "div#ca-book p")
    private WebElement registrationSuccessful;

    public void enterRegistrationEmailDetails(Hashtable<String,String> testData){
        //entering registration email details
        try{
            actionUtility.waitForElementVisible(registrationPassengerEmail,10);
            String [] registrationPassengerEmailAddress = testData.get("registrationPassengerEmail").split("@");
            registrationPassengerEmail.sendKeys(registrationPassengerEmailAddress[0]+actionUtility.getRandomNumber()+"@"+registrationPassengerEmailAddress[1]);
            registrationPassengerPassword.sendKeys(testData.get("registrationPassengerPassword"));
            registrationPassengerConfirmPassword.sendKeys(testData.get("registrationPassengerPassword"));
            reportLogger.log(LogStatus.INFO,"successfully entered registration email details");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter registration email details");
            throw e;
        }
    }

    public void enterRegistrationPassengerDetails(Hashtable <String,String> testData){
        //entering registration passenger details
        try {
            String [] registrationPassengerName = testData.get("registrationPassengerName").split(" ");
            actionUtility.dropdownSelect(registrationPassengerTitle, ActionUtility.SelectionType.SELECTBYVALUE,registrationPassengerName[0]);
            registrationPassengerFirstName.sendKeys(registrationPassengerName[1]);
            registrationPassengerLastName.sendKeys(registrationPassengerName[2]);
            if(testData.get("registrationPassengerGender")!= null){
                actionUtility.dropdownSelect(registrationPassengerGender, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("registrationPassengerGender"));
            }
            if(testData.get("registrationPassengerDOB")!=null){
                registrationPassengerDOB.sendKeys(testData.get("registrationPassengerDOB"));
            }
            reportLogger.log(LogStatus.INFO,"successfully entered registration passenger details");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter registration passenger details");
            throw e;
        }

    }

    public void enterRegistrationPassengerAddress(Hashtable <String,String> testData){
        //entering registration passenger address
        try {
            actionUtility.dropdownSelect(registrationPassengerCountry, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("registrationPassengerCountry"));

            if (testData.get("registrationPassengerPostCode")!=null){
                registrationPassengerPostCode.sendKeys(testData.get("registrationPassengerPostCode"));
            }
                registrationAddress1.sendKeys(testData.get("registrationAddress1"));

            if (testData.get("registrationAddress2")!=null){
                registrationAddress2.sendKeys(testData.get("registrationAddress2"));
            }

            if (testData.get("registrationAddress3")!=null){
                registrationAddress3.sendKeys(testData.get("registrationAddress3"));
            }
            registrationTown.sendKeys(testData.get("registrationTown"));
            registrationCounty.sendKeys(testData.get("registrationCounty"));
            reportLogger.log(LogStatus.INFO,"successfully entered registration passenger address");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter registration passenger address");
            throw e;
        }
    }

    public void enterRegistrationPassengerPhoneNumber(Hashtable <String,String> testData){
        //entering registration passenger phone Number
        try {
            if (testData.get("registrationDiallingCodeLand")!=null){
                actionUtility.dropdownSelect(registrationDiallingCodeLand, ActionUtility.SelectionType.SELECTBYVALUE,testData.get("registrationDiallingCodeLand"));

            }

            if (testData.get("registrationHomePhone")!=null){
                registrationHomePhone.sendKeys(testData.get("registrationHomePhone"));
            }

            if (testData.get("registrationDiallingCodeMobile")!=null){
                actionUtility.dropdownSelect(registrationDiallingCodeMobile, ActionUtility.SelectionType.SELECTBYVALUE,testData.get("registrationDiallingCodeMobile"));
            }

            if (testData.get("registrationMobilePhone")!=null){
                registrationMobilePhone.sendKeys(testData.get("registrationMobilePhone"));
            }
            reportLogger.log(LogStatus.INFO,"successfully entered registration passenger phone numbers");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter registration passenger phone numbers");
            throw e;
        }
    }

    public void selectRegistrationAviosOptions(Hashtable <String,String> testData){
        //select registration avios option
        try {
            String aviosOption = testData.get("aviosOption");
            if (aviosOption.toLowerCase().equals("with avios")){
                actionUtility.selectOption(registerWithAvios);
            }else if (aviosOption.toLowerCase().equals("without avios")){
                actionUtility.selectOption(registerWithoutAvios);
            }else if (aviosOption.toLowerCase().equals("existing avios")){
                actionUtility.selectOption(registerWithExistingAvios);
            }
            reportLogger.log(LogStatus.INFO,"successfully selected avios options ");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to selected avios options");
            throw e;
        }
    }

    public void continueToRegistration(){
        //select terms and condition and click register
        try {
            actionUtility.selectOption(termsAndConditions);
            actionUtility.click(register);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully clicked register button ");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to click register button");
            throw e;
        }
    }

    public void selectLoginRegistrationOption(String option){
        //select the option to do login or registration
        try{

            if(option.toLowerCase().equals("login")){
                actionUtility.waitForElementClickable(loginOption,10);
                actionUtility.hardClick(loginOption);

            }else if(option.toLowerCase().equals("registration")){
                actionUtility.waitForElementClickable(registrationOption,10);
                actionUtility.hardClick(registrationOption);
                actionUtility.hardClick(continueToLoginRegistering);
                actionUtility.waitForPageLoad(10);

            }
            reportLogger.log(LogStatus.INFO,"successfully selected the option: "+option);

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the option: "+option);
            throw e;
        }
    }

    public void login(Hashtable<String,String> testData,String... emailId){
        // login with valid credentials
        String userId = null;
        try {
            if(emailId.length>0){
                userName.clear();
                userName.sendKeys(emailId[0]);
                password.clear();
                password.sendKeys(testData.get("contactPassengerPasswordForAccount"));
                userId = emailId[0];
            }
            else {
                userName.clear();
                actionUtility.click(loginContainer);
                String userNameValue = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
                String passwordValue = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
                userName.sendKeys(userNameValue);
                password.clear();
                password.sendKeys(passwordValue);
                userId = userNameValue;
            }
            actionUtility.click(continueToLoginRegistering);
            reportLogger.log(LogStatus.INFO,"successfully tried to login with email id - "+userId);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to login");
            throw e;
        }
    }

    public void verifyRegistration(){
        //verifies the registration successful message
        try {
            Assert.assertTrue(registrationSuccessful.isDisplayed(), "registration unsuccessful");
            reportLogger.log(LogStatus.INFO, "registration successful");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"registration unsuccessful");
            throw e;
        }
        catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify the registration");
            throw e;
        }
    }

}






