package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSChooseExtra {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSChooseExtra() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id ="continueButton")
    private WebElement continueToPaymentButton;

    public void continueToPayment(){
        /*clicks on continue button in your details page  to navigate to the payment page in amend flow.
        this method is just used to click on continue button in your details -legacy page since the button ID is
        different in your details -leapfrog page. Hence instead of having separate method with different contactPassengerAddressListPopupLocator
        in your details, this method is used. However this method is NOT RELATED to extras page.
         */

        try {
            actionUtility.click(continueToPaymentButton);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to payment");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to payment");
            throw e;
        }
    }
}
