package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 5/29/2017.
 */
public class MyAccountMyPaymentCards {

    ActionUtility actionUtility ;
    ExtentTest reportLogger;

    @FindBy(css="form[name='listAccountPaymentMethodsForm'] td:nth-child(3)>a")
    private List<WebElement> savedCards;


    @FindBy(xpath="//a[contains(text(),'Add new payment card')]")
    private WebElement addNewCard;

//    ------------add new card-------------------

    @FindBy(className="heading")
    private WebElement paymentDetailsHeader;

    @FindBy(id="userCardSchemeType")
    private WebElement cardType;

    @FindBy(css="input[styleid='cardNumber']")
    private WebElement cardNumber;

    @FindBy(name="expiryMonth")
    private WebElement cardExpiryMonth;

    @FindBy(name="expiryYear")
    private WebElement cardExpiryYear;

    @FindBy(name="holderName")
    private WebElement cardHolderName;

    @FindBy(name="cvc")
    private WebElement securityNumber;

    @FindBy(id="useMainAddress")
    private WebElement populateContactAddress;

    @FindBy(name="SAVE")
    private WebElement saveCard;

    @FindBy(css="img[src='/img/template/flybe-logo.png']")
    private WebElement flyBeLink;



    public MyAccountMyPaymentCards()
    {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    public void checkAndAddSaveCard(Hashtable<String,String> testData){
        //to check whether the user has saved cards

        try {
            boolean flag=false;
            for (int i = 0; i < savedCards.size(); i++) {
                if(savedCards.get(i).getText().contains(testData.get("savedCardName"))){
                    flag = true;
                }
            }
            if(!flag){

                reportLogger.log(LogStatus.INFO,"the required card is not available in the saved card list");

                addNewCard(testData);


            }else{
                reportLogger.log(LogStatus.INFO," the required is available in the saved card list");

            }


        }
        catch(Exception e)
        {
            reportLogger.log(LogStatus.WARNING,"unable to verify if the card is present in the saved card list");
            //throw e;
        }

    }

    private void addNewCard(Hashtable<String,String> testData)
    {   //to save new card
        try {
            actionUtility.click(addNewCard);
            actionUtility.waitForElementVisible(paymentDetailsHeader,5000);
            actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("addCardType"));
            actionUtility.sendKeys(cardNumber, testData.get("addCardNumber"));
            actionUtility.dropdownSelect(cardExpiryMonth, ActionUtility.SelectionType.SELECTBYINDEX, testData.get("addCardExpiryMonth"));
            actionUtility.dropdownSelect(cardExpiryYear, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("addCardExpiryYear"));
            actionUtility.sendKeys(cardHolderName,testData.get("cardHolderName"));
            actionUtility.sendKeys(securityNumber,testData.get("savedCardCvv"));
            actionUtility.click(populateContactAddress);
            actionUtility.click(saveCard);
            reportLogger.log(LogStatus.INFO,"card details successfully added and  saved for my account");
            //actionUtility.click(flyBeLink);
        }
        catch(Exception e)
        {
            reportLogger.log(LogStatus.WARNING,"unable to add card details and save for my account ");
        }
    }




}
