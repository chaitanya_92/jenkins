package leapfrog.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

public class LeapAddAPI {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LeapAddAPI() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(name = "adult-forename")
    private WebElement apiFirstName;

    @FindBy(name = "adult-middle-name")
    private WebElement apiMiddleName;

    @FindBy(name = "adult-last-name")
    private WebElement apiLastName;

    @FindBy(name = "adult-passport-number")
    private WebElement apiPassportNumber;

    @FindBy(css = "select[name=adult-nationality]+div span")
    private WebElement apiAdultNationalitySelector;

    @FindBy(css = "select[name=adult-nationality]+div input")
    private WebElement apiAdultNationalityInput;

    @FindBy(css = "select[name='adult-nationality']+div ul.chosen-results>li")
    private WebElement apiAdultNationalityOption;

    @FindBy(css = "select[name=adult-country-of-issue]+div span")
    private WebElement apiAdultCountryOfIssueSelector;

    @FindBy(css = "select[name=adult-country-of-issue]+div input")
    private WebElement apiAdultCountryOfIssueInput;

    @FindBy(css = "select[name='adult-country-of-issue']+div ul.chosen-results>li")
    private WebElement apiAdultCountryOfIssueOption;


    @FindBy(id = "adult-male")
    private WebElement apiGenderMale;

    @FindBy(id = "adult-female")
    private WebElement apiGenderFemale;

    @FindBy(css = "label[for=adult-dob]+div select[name='day']")
    private WebElement apiDobDay;

    @FindBy(css = "label[for=adult-dob]+div select[name='month']")
    private WebElement apiDobMonth;

    @FindBy(css = "label[for=adult-dob]+div select[name='year']")
    private WebElement apiDobYear;

    @FindBy(css = "label[for=adult-expiry-date]+div select[name='day']")
    private WebElement apiExpiryDateDay;

    @FindBy(css = "label[for=adult-expiry-date]+div select[name='month']")
    private WebElement apiExpiryDateMonth;

    @FindBy(css = "label[for=adult-expiry-date]+div select[name='year']")
    private WebElement apiExpiryDateYear;

    //    ---------------------------
    @FindBy(name = "infant-forename")
    private WebElement apiInfantFirstName;

    @FindBy(name = "infant-middle-name")
    private WebElement apiInfantMiddleName;

    @FindBy(name = "infant-last-name")
    private WebElement apiInfantLastName;

    @FindBy(name = "infant-passport-number")
    private WebElement apiInfantPassportNumber;

    @FindBy(css = "select[name=infant-nationality]+div span")
    private WebElement apiInfantNationalitySelector;

    @FindBy(css = "select[name=infant-nationality]+div input")
    private WebElement apiInfantNationalityInput;

    @FindBy(css = "select[name='infant-nationality']+div ul.chosen-results>li")
    private WebElement apiInfantNationalityOption;

    @FindBy(css = "select[name=infant-country-of-issue]+div span")
    private WebElement apiInfantCountryOfIssueSelector;

    @FindBy(css = "select[name=infant-country-of-issue]+div input")
    private WebElement apiInfantCountryOfIssueInput;

    @FindBy(css = "select[name='infant-country-of-issue']+div ul.chosen-results>li")
    private WebElement apiInfantCountryOfIssueOption;


    @FindBy(id = "infant-male")
    private WebElement apiInfantGenderMale;

    @FindBy(id = "infant-female")
    private WebElement apiInfantGenderFemale;

    @FindBy(css = "label[for=infant-dob]+div select[name='day']")
    private WebElement apiInfantDobDay;

    @FindBy(css = "label[for=infant-dob]+div select[name='month']")
    private WebElement apiInfantDobMonth;

    @FindBy(css = "label[for=infant-dob]+div select[name='year']")
    private WebElement apiInfantDobYear;

    @FindBy(css = "label[for=infant-expiry-date]+div select[name='day']")
    private WebElement apiInfantExpiryDateDay;

    @FindBy(css = "label[for=infant-expiry-date]+div select[name='month']")
    private WebElement apiInfantExpiryDateMonth;

    @FindBy(css = "label[for=infant-expiry-date]+div select[name='year']")
    private WebElement apiInfantExpiryDateYear;

    @FindBy(xpath = "//button[contains(@class,'btn-large-submit')]")
    private WebElement apiSubmit;

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='male']")
    private List<WebElement> males;

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='female']")
    private List<WebElement> females;


//    private String apiAdultNationalityInput = "//select[@name='adult-nationality']/following-sibling::div/descendant::ul/li[text()='%s']";
//    private String apiAdultCountryOfIssueInput = "//select[@name='adult-country-of-issue']/following-sibling::div/descendant::ul/li[text()='%s']";
//    private String apiInfantNationalityInput = "//select[@name='infant-nationality']/following-sibling::div/descendant::ul/li[text()='%s']";
//    private String apiInfantCountryOfIssueInput = "//select[@name='infant-country-of-issue']/following-sibling::div/descendant::ul/li[text()='%s']";


    public void populateAPIDetails(Hashtable <String,String>testData, Hashtable<String,String>passengerNames, int passengerNo, String passengerType){
        //add api details to the passenger passed
        try{

            enterNamesForAdult(passengerNames.get("passengerName"+passengerNo),testData.get("apiMiddleName"));
            String APIBirthDate=null;
            if(passengerType.toLowerCase().equals("infant")){
                APIBirthDate = getDateOfBirth(testData,"adult");
            }else {
                APIBirthDate = getDateOfBirth(testData,passengerType);
            }
            enterBirthDateForNonInfants(APIBirthDate);
            enterGenderForAdult(testData.get("apiGender"));
            enterPassportDetailsForAdult(testData);
            enterExpiryDateForAdult(testData.get("apiExpiryDate"));
            if (passengerType.toLowerCase().equals("infant")){
                enterNamesForInfant(passengerNames.get("infantName"+passengerNo),testData.get("apiMiddleName"));
                APIBirthDate =getDateOfBirth(testData,"infant");
                enterBirthDateForInfant(APIBirthDate);
                enterGenderForInfant(testData.get("apiGender"));
                enterPassportDetailsForInfant(testData);
                enterExpiryDateForInfant(testData.get("apiExpiryDate"));
            }
            actionUtility.hardClick(apiSubmit);
            actionUtility.waitForElementNotPresent("//button[@type='submit' and text()='Submit']",10);
            reportLogger.log(LogStatus.INFO,"successfully to entered API details");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter API details");
            throw e;
        }
    }

    private String getDateOfBirth(Hashtable<String,String>testData, String PassengerType){
        // fetches the respective api birth date from test data
        if(PassengerType.toLowerCase().equals("adult")){
            return testData.get("apiAdultBirthDate");
        }else if(PassengerType.toLowerCase().equals("teen")){
            return testData.get("apiTeenBirthDate");
        }else if(PassengerType.toLowerCase().equals("child")){
            return testData.get("apiChildBirthDate");
        }else if(PassengerType.toLowerCase().equals("infant")){
            return testData.get("apiInfantBirthDate");
        }

        return PassengerType;
    }

    private void enterNamesForAdult(String passengerName, String middleName){
        // populate passenger name for adult
        apiFirstName.sendKeys(passengerName.split(" ")[1]);
        if (middleName!=null){
            apiMiddleName.sendKeys(middleName);
        }
        apiLastName.sendKeys(passengerName.split(" ")[2]);
    }

    private void enterNamesForInfant(String passengerName, String middleName){
        // populate passenger name for adult
        apiInfantFirstName.sendKeys(passengerName.split(" ")[1]);
        if (middleName!=null){
            apiInfantMiddleName.sendKeys(middleName);
        }
        apiInfantLastName.sendKeys(passengerName.split(" ")[2]);
    }

    private void enterBirthDateForNonInfants(String birthDate){
        // selects the birth date in the dropdown for adult
        String[]birthDates =birthDate.split(" ");
        actionUtility.dropdownSelect(apiDobDay,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[0]);
        actionUtility.dropdownSelect(apiDobMonth,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[1]);
        actionUtility.dropdownSelect(apiDobYear,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[2]);
    }

    private void enterBirthDateForInfant(String birthDate){
        // selects the birth date in the dropdowns for adult
        String[]birthDates =birthDate.split(" ");
        actionUtility.dropdownSelect(apiInfantDobDay,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[0]);
        actionUtility.dropdownSelect(apiInfantDobMonth,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[1]);
        actionUtility.dropdownSelect(apiInfantDobYear,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[2]);
    }

    private void enterGenderForAdult(String gender){
        // selects gender for adult
        if (gender.toLowerCase().toLowerCase().equals("male")){
            actionUtility.hardClick(apiGenderMale);
        }else if (gender.toLowerCase().toLowerCase().equals("female")){
            actionUtility.hardClick(apiGenderFemale);
        }
    }

    private void enterGenderForInfant(String gender){
        // selects gender for infant
        if (gender.toLowerCase().toLowerCase().equals("male")){
            actionUtility.hardClick(apiInfantGenderMale);
        }else if (gender.toLowerCase().toLowerCase().equals("male")){
            actionUtility.hardClick(apiInfantGenderFemale);
        }
    }

    private void enterPassportDetailsForInfant(Hashtable<String,String>testData){
        // populate the passport details for infant

                apiInfantPassportNumber.sendKeys(testData.get("apiPassportNumber"));
        actionUtility.click(apiInfantNationalitySelector);
        apiInfantNationalityInput.sendKeys(testData.get("apiNationality"));
//        actionUtility.hardSleep(2000);
        actionUtility.click(apiInfantNationalityOption);

        actionUtility.click(apiInfantCountryOfIssueSelector);

        apiInfantCountryOfIssueInput.sendKeys(testData.get("apiIssuingCountry"));
        actionUtility.click(apiInfantCountryOfIssueOption);

//        actionUtility.click(apiInfantNationalitySelector);
//        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(apiInfantNationalityInput,testData.get("apiNationality")))));
//        actionUtility.click(apiInfantCountryOfIssueSelector);
//        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(apiInfantCountryOfIssueInput,testData.get("apiIssuingCountry")))));
    }

    private void enterPassportDetailsForAdult(Hashtable<String,String>testData){
        // populate the passport details for adult
        actionUtility.hardSleep(2000);
        apiPassportNumber.sendKeys(testData.get("apiPassportNumber"));
        actionUtility.click(apiAdultNationalitySelector);
        apiAdultNationalityInput.sendKeys(testData.get("apiNationality"));
        actionUtility.click(apiAdultNationalityOption);
//        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(apiAdultNationalityInput,testData.get("apiNationality")))));
//        actionUtility.click(apiAdultCountryOfIssueSelector);
        actionUtility.click(apiAdultCountryOfIssueSelector);
//        actionUtility.waitForElementVisible(apiAdultCountryOfIssueSelector,3);
        apiAdultCountryOfIssueInput.sendKeys(testData.get("apiIssuingCountry"));
        actionUtility.click(apiAdultCountryOfIssueOption);
//        actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(apiAdultCountryOfIssueInput,testData.get("apiIssuingCountry")))),5);
//        actionUtility.click((TestManager.getDriver().findElement(By.xpath(String.format(apiAdultCountryOfIssueInput,testData.get("apiIssuingCountry"))))));

    }

    private void enterExpiryDateForAdult(String expiryDate){
        // selects the expiry date in the dropdowns
        String[]birthDates =expiryDate.split(" ");
        actionUtility.dropdownSelect(apiExpiryDateDay,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[0]);
        actionUtility.dropdownSelect(apiExpiryDateMonth,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[1]);
        actionUtility.dropdownSelect(apiExpiryDateYear,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[2]);
    }

    private void enterExpiryDateForInfant(String expiryDate){
        // selects the expiry date in the dropdowns for infant
        String[]birthDates =expiryDate.split(" ");
        actionUtility.dropdownSelect(apiInfantExpiryDateDay,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[0]);
        actionUtility.dropdownSelect(apiInfantExpiryDateMonth,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[1]);
        actionUtility.dropdownSelect(apiInfantExpiryDateYear,ActionUtility.SelectionType.SELECTBYTEXT,birthDates[2]);
    }

    public void verifyGenderForPassenger(String[] genders){
//        This is not correct we need to correct the WebElements
//        Verifies the gender for the passenger in the check in page
        try{
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
            for(int i=0; i<genders.length; i++){
                if(genders[i].toLowerCase().equals("male")) {
                    try {
                        Assert.assertTrue(males.get(i).isSelected());
                        reportLogger.log(LogStatus.INFO, "successfully verified the gender of the passenger " + genders[i]);
                    }catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "gender of the passenger is not matching: Excepted gender- " + genders[i] + " Actual gender- female");
                        throw e;
                    }catch(Exception e){
                        reportLogger.log(LogStatus.WARNING,"unable to verify the gender");
                        throw e;
                    }
                }
                else if(genders[i].toLowerCase().equals("female")){
                    try{
                        Assert.assertTrue(females.get(i).isSelected());
                        reportLogger.log(LogStatus.INFO,"successfully verified the gender of the passenger "+genders[i]);
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.WARNING, "gender of the passenger is not matching: Excepted gender- " + genders[i] + " Actual gender- male");
                        throw e;
                    }catch(Exception e){
                        reportLogger.log(LogStatus.WARNING,"unable to verify the gender");
                        throw e;
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to switch to frame");
        }
    }
}