package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;
import java.util.logging.Logger;

public class LeapCheapFlight {
    ActionUtility actionUtility;
    ExtentTest reportLogger;


    public LeapCheapFlight() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //    -------------------recent search starts------------
    @FindBy(className = "open-recent-searches-btn")
    private WebElement openRecentSearchTab;

    @FindBy(className = "recent-search-panel")
    private WebElement recentSearchPanel;

    @FindBy(css = ".recent-search-entry .title > span")
    private List<WebElement> recentSearchRoutes;

    @FindBy(css = ".recent-search-entry .title+.dates")
    private List<WebElement> recentSearchDates;

    //    ---------------recent search ends------------------

    @FindBy(xpath = "//div[contains(@class,'teenItaly')]")
    private WebElement unaccompaniedMinorMessage;

    @FindBy(xpath = "//div[@id='flight_from_chosen']/a/div/b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[@id ='flight_from_chosen']//input")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a")
    private WebElement flightToDropDown;

    @FindBy(xpath = "//div[@id='flight_to_chosen']//input")
    private WebElement flightTo;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a[@class='chosen-single']/span")
    private WebElement selectedDestination;

    @FindBy(id = "ui-datepicker-div")
    private WebElement datePicker;

    @FindBy(className = "ui-datepicker-year")
    private WebElement datePickerYear;

    @FindBy(className = "ui-datepicker-month")
    private WebElement datePickerMonth;

    @FindBy(xpath = "//input[contains(@class,'inputAdults')]")
    private WebElement adultNos;

    @FindBy(xpath = "//input[contains(@class,'inputTeens')]")
    private WebElement teenNos;

    @FindBy(xpath = "//input[contains(@class,'inputChildren')]")
    private WebElement childrenNos;

    @FindBy(xpath = "//input[contains(@class,'inputInfants')]")
    private WebElement infantNos;

    @FindBy(xpath = "//button[contains(@class,'adult-up')]")
    private WebElement adultIncrement;

    @FindBy(xpath = "//button[contains(@class,'adult-down')]")
    private WebElement adultDecrement;

    @FindBy(xpath = "//button[contains(@class,'teen-up')]")
    private WebElement teenIncrement;

    @FindBy(xpath = "//button[contains(@class,'teen-down')]")
    private WebElement teenDecrement;

    @FindBy(xpath = "//button[contains(@class,'child-up')]")
    private WebElement childIncrement;

    @FindBy(xpath = "//button[contains(@class,'child-down')]")
    private WebElement childDecrement;

    @FindBy(xpath = "//button[contains(@class,'infant-up')]")
    private WebElement infantIncrement;

    @FindBy(xpath = "//button[contains(@class,'infant-down')]")
    private WebElement infantDecrement;

    @FindBy(xpath = "//h3[contains(text(),'Select passengers')]")
    private WebElement selectPassengerMenu;

    @FindBy(id = "pax-display")
    private WebElement openSelectPassengerMenu;

    @FindBy(xpath = "//div[contains(@class,'pax-close-button')]")
    private WebElement closeSelectPassengerMenu;

    @FindBy(css = "label.js-add-promo.promo-label")
    private WebElement openPromoCode;

    @FindBy(xpath = "//input[contains(@class,'inputPromoCode')]")
    private WebElement promoCode;

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(xpath = "//li[a[text()='My account']]")
    private WebElement myAccount;

    @FindBy(xpath = "//a[contains(text(),'Log in')]")
    private WebElement login;

    @FindBy(xpath = "//li/a[text()='Check-in']")
    private WebElement checkInTab;

    @FindBy(id = "label-booking-ref")
    private WebElement bookingRefForCheckIn;

    @FindBy(id = "label-forename")
    private WebElement foreNameForCheckIn;

    @FindBy(id = "label-surname")
    private WebElement surNameForCheckIn;

    @FindBy(xpath = "//div/button[text()='Check-in']")
    private WebElement checkInButton;

    @FindBy(className ="loading-spinner")
    private WebElement checkInLoader;



    @FindBy(linkText = "Manage booking")
    private WebElement manageBooking;

    @FindBy(linkText = "View booking")
    private WebElement viewBookingTab;

    @FindBy(id = "label-nav-booking")
    private WebElement bookingRefForViewBooking;

    @FindBy(id = "label-nav-forename")
    private WebElement foreNameForViewBooking;

    @FindBy(id = "label-nav-surname")
    private WebElement surNameForViewBooking;

    @FindBy(name = "submit_button")
    private WebElement viewBookingButton;

    @FindBy(className = "popUp")
    private WebElement popUp;

    @FindBy(id = "popHead")
    private WebElement popUpHeader;

    @FindBy(id = "closepopup")
    private WebElement closePopUp;

    @FindBy(xpath = "//div[contains(@class,'error-groupBooking')]")
    private WebElement moreThanEightPassengersMessage;

    @FindBy(id = "herosection")
    private WebElement heroBanner;

    @FindBy(id = "carousel")
    private WebElement carouselSection;

    @FindBy(id = "ctasection")
    private WebElement offerTiles;
//------------PLD----------
    @FindBy(linkText = "Retrieve Price Lock-Down Booking")
    private WebElement retrievePLDTab;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='pnrLocator']")
    private WebElement pldReferenceNumberForSearch;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='forename']")
    private WebElement pldForenameForSearch;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='surname']")
    private WebElement pldSurnameForSearch;

    @FindBy(xpath = "//input[@class='button medium fbred rightButton']")
    private WebElement pldContinueButtonForSearch;

//    @FindBy(xpath = "//span[@class='loading-spinner']")
//    private WebElement checkInLoader;


    String closePopUpLocator = "//*[@id='closepopup']";
    String checkInLoaderIndicator = "//span[text()='Loading...']";

    String datePickerDay = "//div[@id='ui-datepicker-div']/div[contains(@class,'first')]/descendant::a[text()='%s']";


    //----------Footer-in-Home-Page-------------
    @FindBy(css = ".logo a")
    private WebElement flybeLogo;

    @FindBy(css = ".logo +li")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "footer a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    //    @FindBy(linkText = "Website terms and conditions")
    @FindBy(css = "a[href='/flightInfo/website-terms-and-conditions.htm']")
    private WebElement termsAndConditionsLink;

    //    @FindBy(linkText = "Website terms of use")
    @FindBy(css = "a[href='/flightInfo/website-terms-of-use.htm']")
    private WebElement termsOfUseLink;

    //    @FindBy(linkText = "Journey Comparison Times")
    @FindBy(css = "a[href='/proof/']")
    private WebElement JouneryComparisionTimesLink;

    @FindBy(css = "div.container a[href='http://flybe.custhelp.com/app/ask']")
    private WebElement ContactUsLink;

    @FindBy(css = "a[href='/terms/tariff.htm']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = "div.container a[href='/']")
    private WebElement flybeInFooter;

    @FindBy(css = "div.terms p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = "div.terms p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = "div.terms p:nth-child(3)")
    private WebElement footerPara3;


    @FindBy(css = "a[href='/corporate/about-flybe/']")
    private WebElement aboutUsLink;

    @FindBy(css = "a[href='/corporate/investors/']")
    private WebElement investorsLink;

    @FindBy(css = "a[href='/corporate/governance']")
    private WebElement governanceLink;

    @FindBy(css = "a[href='/corporate/media']")
    private WebElement mediaLink;

    @FindBy(css = "a[href='/careers/']")
    private WebElement careersLink;

    @FindBy(css = "a[href='/corporate/sustainability']")
    private WebElement environmentLink;

    @FindBy(css = "a[href='http://www.flybeas.com']")
    private WebElement aviationServicesLink;

    @FindBy(css = "a[href='http://www.flybetraining.com']")
    private WebElement trainingAcademyLink;

    @FindBy(css = "a[href='http://www.travelfusion.com/register?dcsName=Flybe&dcsLogo=http://www.flybe.com/img/template/flybe-logo.png&dcsEmail=travelfusion@flybe.com']")
    private WebElement xmlApiLink;

    @FindBy(css = "a[href='http://www.twitter.com/flybe']")
    private WebElement twitterLink;

    @FindBy(css = "a[href='http://www.facebook.com/flybe']")
    private WebElement fbLink;

    @FindBy(css = "a[href='http://www.youtube.com/user/FlybeOfficial']")
    private WebElement youtubeLink;

    @FindBy(css = "a[href='http://www.linkedin.com/company/flybe']")
    private WebElement linkedinLink;

    @FindBy(css = "a[href='/subscribe']")
    private WebElement subscribeLink;

    @FindBy(xpath = "//strong[contains(text(),'Corporate')]")
    private WebElement corporateHeader;

    @FindBy(xpath = "//strong[contains(text(),'Other Flybe sites')]")
    private WebElement otherFlybeWebsitesHeader;

    @FindBy(xpath = "//strong[contains(text(),'Join the conversation')]")
    private WebElement joinTheConversationHeader;

    @FindBy(xpath = "//li[@class='header']/strong/a[contains(@href,'/assistance/')]")
    private WebElement passengerAssistanceLink;

    @FindBy(xpath = "//button[@data-direction='outbound'and text()='Check-In Now']")
    private List<WebElement> outboundCheckIns;


    @FindBy(linkText = "Flight info & help")
    private WebElement flightInfoAndHelp;

    @FindBy(linkText = "Live arrivals & departures")
    private WebElement liveArrivalsAndDepartures;


    public void enterSourceAndDestination(Hashtable<String,String> testData){
        //enter source and destination
        try {
            handleHomeScreenPopUp();
//            actionUtility.hardSleep(2000);
            actionUtility.click(findFlights);
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightFrom, testData.get("flightFrom") + Keys.RETURN);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightTo, testData.get("flightTo") + Keys.RETURN);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public int selectNoOfPassengers(Hashtable<String,String> testData){
        //select the no.of passengers
        try {
            int noOfPassengers=1;
            actionUtility.click(openSelectPassengerMenu);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;
            if (testData.get("noOfTeen") != null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));

                noOfPassengers += noOfTeen;
                if(noOfTeen == 8)
                {
                    selectPassengers("teens",noOfTeen,Integer.parseInt(testData.get("noOfAdult")));
                }
                else{
                    selectPassengers("teens",noOfTeen);
                }
            }

            if (testData.get("noOfAdult") != null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                selectPassengers("adults", noOfAdult);
                noOfPassengers += (noOfAdult-1);
            }

            if (testData.get("noOfChild") != null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                selectPassengers("children", noOfChild);
                noOfPassengers += noOfChild;
            }
            if (testData.get("noOfInfant") != null) {
                selectPassengers("infants", Integer.parseInt(testData.get("noOfInfant")));
            }
            try{
                actionUtility.click(closeSelectPassengerMenu);
            }catch (Exception e){}
            reportLogger.log(LogStatus.INFO,"successfully selected all types of passengers");
            reportLogger.log(LogStatus.INFO,"Adults - "+noOfAdult+" Teens - "+noOfTeen+" Children - "+noOfChild+" Infant - "+noOfInfant);

            return noOfPassengers;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the number of passengers");
            throw e;
        }
    }

    public void enterPromoCode(Hashtable<String,String> testData){
        // enter the promocode if present in the test data
        try {
            if (testData.get("promoCodeVal") != null) {
                enterPromoCode(testData.get("promoCodeVal"));
                reportLogger.log(LogStatus.INFO,"successfully entered PROMO code");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter PROMO code");
            throw e;
        }
    }

    public void findFlights(){
        // click on  the find flights
        try{
            actionUtility.hardClick(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);

            reportLogger.log(LogStatus.INFO, "Flight searched");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights");
            throw e;
        }
    }

    public void enterTravelDates(Hashtable<String,String> testData){
        // enter the travel dates

        try {
            String depart = testData.get("departDateOffset");
            String promoCodeVal = null;
            String date[] = new String[3];
            int departDateOffset = Integer.parseInt(testData.get("departDateOffset"));
            date = actionUtility.getDate(departDateOffset);
            selectDateFromCalendar(date);

            reportLogger.log(LogStatus.INFO, "Entered the depart date - "+date[0]+"-"+date[1]+"-"+date[2]);

            if (testData.get("returnDateOffset") != null) {
                int returnDateOffset = Integer.parseInt(testData.get("returnDateOffset"));
                date = actionUtility.getDate(returnDateOffset);
                selectDateFromCalendar(date);
                reportLogger.log(LogStatus.INFO, "Entered the return date - "+date[0]+"-"+date[1]+"-"+date[2]);
            }


        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to enter travel Dates");
            throw e;
        }
    }

    private void enterPromoCode(String promoCodeVal){
        // enter the promocode while searching the flight
        actionUtility.hardClick(openPromoCode);
        actionUtility.waitForElementVisible(promoCode,3);
        promoCode.sendKeys(promoCodeVal);

    }

    private void selectDateFromCalendar(String date[]){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,3);
//        String date[] = actionUtility.getDate(offset);
        actionUtility.dropdownSelect(datePickerYear, ActionUtility.SelectionType.SELECTBYTEXT,date[2]);
        actionUtility.dropdownSelect(datePickerMonth, ActionUtility.SelectionType.SELECTBYTEXT,date[1]);
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay,date[0]))));

    }

    private void selectPassengers(String passengerType, int noOfPassenger, int... adultPassenger){
        // selects the no. of passengers
        String passengerNos ;
        boolean flag = true;
        int attempt =0;
        boolean attemptFlag = false;
        while(attempt<8 && attemptFlag == false) {
            try {
                if (!actionUtility.verifyIfElementIsDisplayed(selectPassengerMenu)) {
                    actionUtility.click(openSelectPassengerMenu);

                }

                switch (passengerType) {
                    case ("adults"):
                        while (flag) {

                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = adultNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(adultIncrement);

                            } else if (Integer.parseInt(passengerNos) > noOfPassenger) {
                                actionUtility.click(adultDecrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("teens"):
                        int counter = 0;
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = teenNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(teenIncrement);
                                counter++;
                                if (counter == 7)
                                    if (adultPassenger.length > 0)
                                        if (adultPassenger[0] == 0)
                                            actionUtility.click(adultDecrement);//Decrement adult when there are 8 teens
                            } else {
                                flag = false;
                            }

                        }
                        attemptFlag = true;
                        break;

                    case ("children"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = childrenNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(childIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("infants"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = infantNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(infantIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;
                }
            } catch (Exception e) {

                attempt++;
            }
            System.out.println("from while for passenger - "+attempt);
        }
        if (!attemptFlag){
            throw  new RuntimeException("unable to select the passengers");
        }
    }

    public void openLoginRegistrationPage(){
        // open the login/Registration page from flight search page
        try {
            actionUtility.hardClick(login);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "login page opened");


        }catch (Exception e){

            reportLogger.log(LogStatus.WARNING, "unable to open login page");
            throw e;
        }

    }

    public void navigateToHomepage(){
        //navigate to home page
        try{

            String appUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("appUrl").getAsString();

            try{
                TestManager.getDriver().navigate().to(appUrl);
            }catch (TimeoutException e){}

            actionUtility.waitForPageLoad(25);
            reportLogger.log(LogStatus.INFO, "successfully navigated to home page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the home page");
            throw e;
        }
    }

    public void enterBookingDetailsForCheckIn(String bookingRef, Hashtable<String,String> passengerNames){
        //navigate to the home page and enter the booking ref. number and other details in check-in tab
        try {
            actionUtility.hardSleep(3000);
            boolean flag = true;
            int attempt = 0;
            while (flag && attempt<4) {
                navigateToHomepage();
                handleHomeScreenPopUp();
                actionUtility.hardSleep(2000);
                actionUtility.hardClick(checkInTab);
                bookingRefForCheckIn.sendKeys(bookingRef);
                String[] nameForCheckIn = passengerNames.get("passengerName1").split(" ");
                foreNameForCheckIn.sendKeys(nameForCheckIn[1]);
                surNameForCheckIn.sendKeys(nameForCheckIn[2]);
                actionUtility.hardSleep(1000);
                actionUtility.clickByAction(checkInButton);
                actionUtility.waitForPageLoad(10);

                try{
                    actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                    flag =false;
                    break;
                }catch (TimeoutException e){
                    actionUtility.hardSleep(2000);
                }

                attempt++;
            }
            if(!flag) {
                actionUtility.hardSleep(1000);

                try {
                    actionUtility.waitForElementVisible(checkInLoader, 15);
                }catch (TimeoutException e){}

                int loadChecker=0;
                while (actionUtility.verifyIfElementIsDisplayed(checkInLoader,3) && loadChecker<3) {
                    try {
                        actionUtility.waitForElementNotPresent("//span[@class='loading-spinner']", 30);
                    }catch (TimeoutException e){
                        loadChecker++;
                    }
                }
                reportLogger.log(LogStatus.INFO, "successfully entered the booking details and continued to check-in");
            }else {
                throw new RuntimeException("unable to enter the booking details and continue to check-in");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to enter the booking details and continue to check-in");
            throw e;
        }
    }

    public void enterBookingDetailsForAmending(String bookingRef, Hashtable<String,String> passengerNames, int... paxNoForRetrieve){
        //retrieve the booking to view the changes
        try {
            actionUtility.hardSleep(3000);
            boolean flag = true;
            int attempt = 0;
            while (flag && attempt<3) {
                navigateToHomepage();
                handleHomeScreenPopUp();
                actionUtility.clickByAction(manageBooking);
                actionUtility.clickByAction(viewBookingTab);
                bookingRefForViewBooking.sendKeys(bookingRef);
                String[] nameForCheckIn = passengerNames.get("passengerName1").split(" ");
                if(paxNoForRetrieve.length>0){
                    nameForCheckIn = passengerNames.get("passengerName"+paxNoForRetrieve[0]).split(" ");
                }
                foreNameForViewBooking.sendKeys(nameForCheckIn[1]);
                surNameForViewBooking.sendKeys(nameForCheckIn[2]);
                actionUtility.hardSleep(1000);
                actionUtility.clickByAction(viewBookingButton);
                actionUtility.waitForPageLoad(10);

                try{
                    actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                    flag =false;
                    break;
                }catch (TimeoutException e){
                    actionUtility.hardSleep(5000);
                }

                attempt++;
            }
            if(!flag) {
                actionUtility.hardSleep(2000);

                try {
                    actionUtility.waitForElementVisible(checkInLoader, 15);
                }catch (TimeoutException e){}

                int loadChecker=0;
                while (actionUtility.verifyIfElementIsDisplayed(checkInLoader) && loadChecker<3) {
                    try {
                        actionUtility.waitForElementNotPresent("//span[@class='loading-spinner']", 30);
                    }catch (TimeoutException e){
                        loadChecker++;
                    }
                }
                reportLogger.log(LogStatus.INFO, "successfully retrieved the booking through manage-booking-view-booking");
            }else {
                throw new RuntimeException("unable to retrieve the booking through manage-booking-view-booking");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the booking through manage-booking-view-booking");
            throw e;
        }
    }

    public void handleHomeScreenPopUp(){
//     Handles the home screen popup
        try{
            String popUpMsg = "no pop up";

            try {
                actionUtility.waitForElementVisible(popUp, 2);
                popUpMsg = popUpHeader.getText();
                reportLogger.log(LogStatus.INFO,"home screen pop up is displayed");

            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"home screen pop up is not displayed");

            }
            if(popUpMsg.toLowerCase().equals("be the first to know!")){
//                actionUtility.click(closePopUp);
                actionUtility.waitForElementClickable(closePopUp,3);
                actionUtility.hardClick(closePopUp);
                actionUtility.waitForElementNotPresent(closePopUpLocator,2);
                reportLogger.log(LogStatus.INFO,"successfully handled the home screen pop up");


            }else if(!popUpMsg.toLowerCase().equals("no pop up")){
                throw new RuntimeException("The unknown pop up is being displayed");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to close the home screen popup");
            throw e;
        }
    }

    public void verifyMessageUnaccompaniedMinorForTeens() {
        //verifies the message displayed only teen is traveling from/to Italy

        String messageDisplayed =null;
        String messageTobeDisplayed = null;


        try{

            messageDisplayed = unaccompaniedMinorMessage.getText();
            messageTobeDisplayed = "Teens cannot fly unaccompanied on flights to or from Italy.";

            Assert.assertEquals(messageDisplayed.trim(),messageTobeDisplayed.trim(),"wrong message displayed for unaccompanied teen traveling from/to italy  ");
            reportLogger.log(LogStatus.INFO,"right message is displayed for unaccompanied teen traveling to/from italy");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message is displayed for unaccompanied teen traveling to/from italy "+"Expected: "+messageTobeDisplayed+" Actual: "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message displayed for teen traveling to/from italy");
            throw e;
        }



    }

    public void verifyMessageForMoreThanEightPassengers() {
        //verifies the message displayed only teen is traveling from/to Italy

        String messageDisplayed = null;
        String messageTobeDisplayed = null;
        try{

            messageDisplayed = moreThanEightPassengersMessage.getText();
            messageTobeDisplayed = "Booking for nine or more passengers? Visit our groups page.";

            Assert.assertEquals(messageDisplayed.trim(),messageTobeDisplayed.trim(),"wrong message displayed when more than eight passengers are selected");
            reportLogger.log(LogStatus.INFO,"right message displayed when more than eight passengers are selected");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message displayed when more than eight passengers are selected Expected: "+messageTobeDisplayed+" Actual: "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message displayed when more than eight passengers are selected");
            throw e;
        }

    }

    public void verifyHomePageFooter(){
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on flybe.com are subject to availability, and are inclusive of all taxes, fees and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            handleHomeScreenPopUp();
            actionUtility.hardSleep(2000);

            verifyPreFooter();

            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(termsOfUseLink.isDisplayed(), "Website Terms Of Use Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Website Terms Of Use Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms Of Use Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms Of Use Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(termsAndConditionsLink.isDisplayed(), "Terms And  Conditions Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Website Terms And Conditions Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms And Conditions Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms And Conditions Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(JouneryComparisionTimesLink.isDisplayed(), "Jounery Comparision Times Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Jounery Comparision Times Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Jounery Comparision Times Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Jounery Comparision Times Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(ContactUsLink.isDisplayed(), "Contact Us Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeInFooter.isDisplayed(), "Flybe.com Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Flybe.com Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(ancillaryCharges.isDisplayed(), "Ancillary Charges Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Ancillary Charges Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch(NoSuchElementException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify footer in cheap flight page");
            throw e;
        }
    }

    public void verifyPreFooter(){
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aboutUsLink));
            reportLogger.log(LogStatus.INFO,"About Flybe link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify About Flybe link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(investorsLink));
            reportLogger.log(LogStatus.INFO,"Investors link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Investors link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(governanceLink));
            reportLogger.log(LogStatus.INFO,"Governance link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Governance link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(mediaLink));
            reportLogger.log(LogStatus.INFO,"Media link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Media link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(careersLink));
            reportLogger.log(LogStatus.INFO,"Carrers link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Investors link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(environmentLink));
            reportLogger.log(LogStatus.INFO,"Environment link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Environment link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviationServicesLink));
            reportLogger.log(LogStatus.INFO,"Flybe Aviation Services link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Aviation Services link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(trainingAcademyLink));
            reportLogger.log(LogStatus.INFO,"Flybe Training Academy link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Training Academy link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(xmlApiLink));
            reportLogger.log(LogStatus.INFO,"Flybe XML/API link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe XML/API link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(passengerAssistanceLink));
            reportLogger.log(LogStatus.INFO,"Passenger Assistance link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Passenger Assistance link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(twitterLink));
            reportLogger.log(LogStatus.INFO,"Flybe Twitter link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Twitter link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fbLink));
            reportLogger.log(LogStatus.INFO,"Flybe facebook link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe facebook link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(youtubeLink));
            reportLogger.log(LogStatus.INFO,"Flybe YouTube link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe YouTube link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(linkedinLink));
            reportLogger.log(LogStatus.INFO,"Flybe LinkedIn link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe LinkedIn link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(subscribeLink));
            reportLogger.log(LogStatus.INFO,"Flybe Subscribe link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Subscribe link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(corporateHeader));
            reportLogger.log(LogStatus.INFO,"Corporate header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Corporate header in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(otherFlybeWebsitesHeader));
            reportLogger.log(LogStatus.INFO,"Other Flybe Sites header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Other Flybe Sites header in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(joinTheConversationHeader));
            reportLogger.log(LogStatus.INFO,"Join The Conversation header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Join The Conversation header in the footer");
            throw e;
        }
    }

    public void verifyTheStaticContentsInHomePage(){
        try{
            handleHomeScreenPopUp();
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(heroBanner), "Hero banner is not displayed in Home Page");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carouselSection), "Carousel is not displayed in Home Page");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(offerTiles), "Offer Tiles is not displayed in Home Page");
            actionUtility.click(checkInTab);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(bookingRefForCheckIn),"Booking reference is not displayed in check-in tab");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(foreNameForCheckIn),"Fore name is not displayed in check-in tab");
            verifyHomePageFooter();
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify contents in home page");
            throw e;

        }
    }

    public void navigateToArrivalAndDeparture(){

        try{

            handleHomeScreenPopUp();
            actionUtility.clickByAction(flightInfoAndHelp);
            actionUtility.clickByAction(liveArrivalsAndDepartures);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Successfully navigated to flight in arrival and departure page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to navigate to flight in arrival and departure page");
        }
    }

    public void enterPLDBookingDetailsForAmending(String pldbookingRef, Hashtable<String,String> passengerNames){
        //retrieve the PLDbooking to confirm booking
        try {

            navigateToHomepage();
            handleHomeScreenPopUp();

            actionUtility.clickByAction(manageBooking);
            actionUtility.clickByAction(retrievePLDTab);
            actionUtility.waitForPageLoad(10);
            actionUtility.hardSleep(1000);
            actionUtility.waitForElementVisible(pldReferenceNumberForSearch,30);
            pldReferenceNumberForSearch.sendKeys(pldbookingRef);
            pldForenameForSearch.sendKeys(passengerNames.get("forename"));
            pldSurnameForSearch.sendKeys(passengerNames.get("surname"));
            // actionUtility.hardSleep(1000);
            actionUtility.click(pldContinueButtonForSearch);
            actionUtility.waitForPageLoad(10);

            reportLogger.log(LogStatus.INFO, "successfully retrieved the booking through manage-booking-Retrieve Price Lock-Down Booking");

        } catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the booking through manage-booking-Retrieve Price Lock-Down Booking");
            throw e;
        }
    }

    public void selectRecentFlight(Hashtable<String,String> testData,int indexFlightToSelect){
//        verify the recent search route and clicks on the recent searched flight
        try {
            handleHomeScreenPopUp();
            int dateOffSet = Integer.parseInt(testData.get("departDateOffset"));
            String dateExpected[] = actionUtility.getDate(dateOffSet);
            actionUtility.waitForElementVisible(openRecentSearchTab, 3);
            actionUtility.click(openRecentSearchTab);
            verifyRecentSearchedFlightsDisplayedIsCorrect(testData, dateExpected, indexFlightToSelect);
            actionUtility.click(recentSearchRoutes.get(indexFlightToSelect - 1 + 0));
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"recent search button is not displayed");
            throw e;
        }
    }

    private void verifyRecentSearchedFlightsDisplayedIsCorrect(Hashtable<String,String> testData, String[] dateExpected, int indexFlightToSelect){
//        verify the recent search route
        try{
            actionUtility.waitForElementVisible(recentSearchPanel,3);
            String depAirportExpected = testData.get("recentSearchDepartureAirport");
            String depAirportActual = recentSearchRoutes.get(indexFlightToSelect - 1 + 0).getText().toString().trim();

            try {
                Assert.assertEquals(depAirportExpected,depAirportActual ,"recent search departure airport name is not matching");
                reportLogger.log(LogStatus.INFO,"recent search departure airport is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search departure airport is not matching, Expected- "+ depAirportExpected+" Actual- "+depAirportActual);
                throw e;
            }

            String destAirportExpected = testData.get("recentSearchDestinationAirport");
            String destAirportActual = recentSearchRoutes.get(indexFlightToSelect - 1 + 1).getText().toString().trim();

            try{
                Assert.assertEquals(destAirportExpected, destAirportActual,"recent search destination airport name is not matching");
                reportLogger.log(LogStatus.INFO,"recent search destination airport is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search destination airport is not matching Expected - "+destAirportExpected+" Actual - "+destAirportActual);
                throw e;
            }

            String recentSearchDateExpected = dateExpected[0]+" "+dateExpected[1];
            String recentSearchDateActual = recentSearchDates.get(indexFlightToSelect-1).getText().toString().trim();

            try{
                Assert.assertEquals(recentSearchDateExpected, recentSearchDateActual,"recent search date is not matching");
                reportLogger.log(LogStatus.INFO,"recent search date is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search date is not matching Expected - " + recentSearchDateExpected + "Actual - "+recentSearchDateActual);
                throw e;
            }
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"recent search flight details are not matching");
            throw new RuntimeException("recent search flight details are not matching");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"recent search panel is not displayed");
            throw new RuntimeException("recent search panel is not displayed");
        }
    }

    public void addCurrencyCookie(Hashtable<String,String> testData){
//        this method will add currency cookie which is read from testdata
        try{
            String currencyCookie = testData.get("currencyCookie");
            String currencyCookieValue = testData.get("currencyCookieValue");
            addCookie(currencyCookie,currencyCookieValue);
        }catch (WebDriverException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to read country cookie");
            throw e;
        }
    }

    public void addCountryCookie(Hashtable<String,String> testData){
//        this method will add country cookie which is read from testdata
        try{
            String countryCookie = testData.get("countryCookie");
            String countryCookieValue = testData.get("countryCookieValue");
            addCookie(countryCookie,countryCookieValue);
        }catch (WebDriverException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to read country cookie");
            throw e;
        }
    }

    private void addCookie(String cookieName, String cookieValue){
//        this method will the cookie
        try {
            Cookie newCookieToBeAdded = new Cookie(cookieName, cookieValue);
            TestManager.getDriver().manage().addCookie(newCookieToBeAdded);
            reportLogger.log(LogStatus.INFO,"successfully added the new cookie-"+cookieName+" with value-"+cookieValue);
        }catch (WebDriverException e){
            reportLogger.log(LogStatus.INFO,"unable to add the cookie-"+cookieName+" with value-"+cookieValue);
            throw e;
        }
    }



}
