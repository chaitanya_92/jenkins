package leapfrog.business_logics;
import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;
import org.openqa.selenium.Keys;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSLFPayment {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSLFPayment(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//span[contains(text(),'Yes')]/preceding-sibling::input")
    private WebElement yesFlyBeAccount;

    @FindBy(xpath = "//span[contains(text(),'No')]/preceding-sibling::input")
    private WebElement noFlyBeAccount;

    @FindBy(className = "modal-body")
    private WebElement contactPassengerLoginMenu;

    @FindBy(css=".modal-body input[id^='frc-email']")
    private WebElement contactPassengerEmailForLogin;

    @FindBy(css=".modal-body input[id^='frc-password']")
    private WebElement contactPassengerPassword;

    @FindBy(css=".modal-body button")
    private WebElement contactPassengerLogin;


    @FindBy(xpath="//form[@name='customer-details']")
    private WebElement contactPassengerDetailsLayout;


//    --------------------- contact passenger details---------------------------------

    @FindBy(css = "form[name='customer-details'] select[id^='frc-title']")
    private WebElement contactPassengerSalutation;

    @FindBy(css = "form[name='customer-details'] input[id^='frc-forename']")
    private WebElement contactPassengerFirstName;

    @FindBy(xpath = "//span[text()='Forename is not long enough']")
    private WebElement validationMsgForename;

    @FindBy(css = "form[name='customer-details'] input[id^='frc-surname']")
    private WebElement contactPassengerLastName;

    @FindBy(id = "country")
    private WebElement contactPassengerCountry;

    @FindBy(css = "form[name='customer-details'] input[id^='frc-postcode']")
    private WebElement contactPassengerPostCode;

    @FindBy(css = "label[for='postcode-lookup'] + button")
    private WebElement contactPassengerFindAddress;

    @FindBy(className = "addresses")
    private WebElement contactPassengerAddressListPopup;

    @FindBy(css = ".addresses label>span:nth-of-type(2)")
    private List<WebElement> contactPassengerAddresses;

    @FindBy(css = ".addresses label>input")
    private List<WebElement> contactPassengerAddressSelectors;

    @FindBy(css = ".modal-footer .btn-primary")
    private WebElement contactPassengerAddressContinue;

    @FindBy(id="callingcode")
    private WebElement contactPassengerLandCountryCode ;

    @FindBy(css="form[name='customer-details'] input[id^='frc-phone']")
    private WebElement contactPassengerLandLineNo;

    @FindBy(css="form[name='customer-details'] input[id^='frc-email']")
    private WebElement contactPassengerEmail;


    @FindBy(css="form[name='customer-details'] input[id^='frc-noPostcodeChecked']")
    private WebElement contactPassengerDontHavePostcode;


    @FindBy(css="form[name='customer-details'] input[id^='frc-address1']")
    private WebElement contactPassengerAddressOne;


    @FindBy(css="form[name='customer-details'] input[id^='frc-address2']")
    private WebElement contactPassengerAddressTwo;

    @FindBy(css="form[name='customer-details'] input[id^='frc-address3']")
    private WebElement contactPassengerAddressThree;


    @FindBy(css="form[name='customer-details'] input[id^='frc-town']")
    private WebElement contactPassengerTown;


    @FindBy(css = "input[id^='frc-marketing-']")
    private WebElement pleaseKeepMeUpdated;

    @FindBy(css="form[name='customer-details'] input[id^='frc-county']")
    private WebElement contactPassengerCounty;

    @FindBy(id="saveButton")
    private WebElement savePassengerContact;


//    ------------Payment details-------------------

    @FindBy(id="billingSection")
    private  WebElement paymentSection;



    @FindBy(xpath = "//span[contains(text(),'credit or debit card')]/preceding-sibling::input")
    private WebElement paymentByCard;

    @FindBy(xpath = "//span[text()='Pay using a credit or debit card']/ancestor::label/following-sibling::div")
    private WebElement paymentByCardSection;



    @FindBy(xpath = "//span[contains(text(),'using PayPal')]/preceding-sibling::input")
    private WebElement paymentByPayPal;

    @FindBy(className = "saveCardChangeButton")
    private WebElement differentPaymentCard;

    @FindBy(css="input[value='Add new card']")
    private WebElement addNewCard;

    @FindBy(className = "credit-form")
    private WebElement newPaymentDetails;

    @FindBy(xpath = "//div[contains(@data-reactid,'savedCard')]/span/span[1]")
    private List<WebElement> savedCards;

    @FindBy(id="cvv")
    private WebElement savedCardCvv;

    @FindBy(xpath="//span[contains(@data-reactid,'savedCard')]/span[contains(@data-reactid,'savedCard')][1")
    private WebElement savedCardNames;



    @FindBy(name="card-type")
    private WebElement cardType;


    @FindBy(name="card-number")
    private WebElement cardNumber;


    @FindBy(name="to-month")
    private WebElement expiryDate;


    @FindBy(name="to-year")
    private WebElement expiryDateYear;

    @FindBy(name="cardHolder")
    private WebElement nameOnCard;


    @FindBy(id="cvv")
    private WebElement securityNumber;


    @FindBy(id="saveCard")
    private WebElement saveNewCard;


    @FindBy(css = ".billing-address input[type='radio']")
    private List<WebElement> billingAddressOption;


    @FindBy(id="saveButton")
    private WebElement saveCardDetails;



    @FindBy(xpath = "#cvv + span[class$='-control-feedback']")
    private WebElement securityNumberValidationSymbol;


    @FindBy(xpath = "#cvv + span+span+span[class*='validation-message']")
    private WebElement securityNumberValidationMessage;

//     -------------------------billing address option---------------------

    @FindBy(css = ".payment-details  #country")
    private WebElement billingCountry;


    @FindBy(css = "input[id^='frc-billingPostcode']")
    private WebElement billingPostCode;


    @FindBy(css = ".billing-address button")
    private WebElement findAddress;


    @FindBy(className = "addresses")
    private WebElement billingAddressListPopup;

    @FindBy(className = ".addresses label>span:nth-of-type(2)")
    private List<WebElement> billingAddresses;

    @FindBy(className = ".addresses label>input")
    private List<WebElement> billingAddressSelectors;

    @FindBy(css = ".modal-footer .btn-primary")
    private WebElement billingAddressContinue;



    @FindBy(css = ".billing-address input[id^='frc-noPostcodeChecked']")
    private WebElement billingAddressDontHavePostCode;

    @FindBy(css = "input[id^='frc-billingAddress1']")
    private WebElement billingAddressOne;


    @FindBy(css = "input[id^='frc-billingAddress2']")
    private WebElement billingAddressTwo;

    @FindBy(css = "input[id^='frc-billingAddress3']")
    private WebElement billingAddressThree;


    @FindBy(css = "input[id^='frc-billingTown']")
    private WebElement billingTown;


    @FindBy(css = "input[id^='frc-billingCounty']")
    private WebElement billingCounty;

















//    -----------------PayPal---------------------

    @FindBy(id = "email")
    private WebElement payPalEmail;

    @FindBy(id = "password")
    private WebElement payPalPassword;

    @FindBy(id = "btnLogin")
    private WebElement submitPayPal;

    @FindBy(id = "confirmButtonTop")
    private WebElement payNowPayPal;

    @FindBy(xpath = "//p[@id='spinner-message']")
    private WebElement payPalSpinner;

    String payPalProcessingSpinner = "//p[@id='spinner-message']";


//    ----------------trip purposes------------------------

    @FindBy(id="tripTypesSection")
    private WebElement tripPurposeSection;

    @FindBy(id = "business")
    private WebElement business;

    @FindBy(id = "tripTypeShortBreak")
    private WebElement shortBreak;

    @FindBy(id="tripTypeLongerHoliday")
    private WebElement longHoliday;

    @FindBy(id="tripTypeVisitingFriendsRelatives")
    private WebElement visiting;

    @FindBy(id="tripTypeOther")
    private WebElement others;


    @FindBy(css=" #tripTypesSection #saveButton")
    private WebElement saveTripPurposes;


//    ---------------------terms & condition---------

    @FindBy(id = "termsSection")
    private WebElement termsSection;

    @FindBy(css = "#termsSection input")
    private List<WebElement> termsAndConditions;

    @FindBy(linkText = "Fare rules")
    private WebElement fareRulesInTermsAndCondition;

    @FindBy(xpath =  "//a[span[text()='dangerous goods in baggage']]")
    private WebElement dangerousGoodsInBaggage;

    @FindBy(partialLinkText = "carriage baggage")
    private WebElement generalConditionsOfCarriage;

    @FindBy(className = "modal-dialog")
    private WebElement rulePopup;

    @FindBy(css = "#tabs-1>h1")
    private WebElement generalConditionsOfCarriageHeader;

    @FindBy(css = " #sticky+div h1")
    private WebElement dangerousGoodsInBaggageHeader;

    @FindBy(className = "modal-title")
    private WebElement fareRulesPopupHeading;

    @FindBy(css = "input[name='tripType']")
    private List <WebElement> tripPurposeOptions;






//    -------New Basket Price----------------

    @FindBy(xpath = "//button[span[text()='Continue with new basket price']]")
    private WebElement continueWithNewPrice;

    @FindBy(xpath = "//span[text()='New total']/following-sibling::div[@class='price']/span")
    private WebElement newBasketPrice;

    @FindBy(xpath = "//button[text()='Reselect alternative flights']")
    private WebElement reselectFlight;





//    ----------------validation modal for cards------------------------

    @FindBy(className = "modal-dialog")
    private WebElement incorrectPaymentDetailMessageBox;

    @FindBy(className = "modal-title")
    private WebElement incorrectPaymentDetailValidationHeader;

    @FindBy(css = ".modal-body>h4")
    private WebElement incorrectPaymentDetailValidationMessage;



    //----------Footer-------------

    @FindBy(css = ".footer img[alt='Flybe']")
    private WebElement flybeLogo;

    @FindBy(css = ".footer li:nth-child(1)>span")
    private WebElement flybeCopyRight;

    @FindBy(css = ".footer li:nth-child(2)")
    private WebElement privacyPolicyLink;

    @FindBy(css = ".footer li:nth-child(3)>a")
    private WebElement cookiePolicyLink;

    @FindBy(css = ".footer li:nth-child(4)>a")
    private WebElement contactUsLink;


    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = ".footer p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = ".footer p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = ".footer p:nth-child(3)")
    private WebElement footerPara3;

    @FindBy(css = ".footer p:nth-child(4)")
    private WebElement footerPara4;




    //    -------------------basket--------------------------
    @FindBy(css = ".outbound .price>span:nth-of-type(2)")
    private WebElement outboundTotal;

    @FindBy(css = ".inbound .price>span:nth-of-type(2)")
    private WebElement returnTotal;

    @FindBy(css = ".total .amount .price>span:nth-of-type(2)")
    private WebElement overallBasketTotal;

    @FindBy(css = ".baggageAmount >.price>span:nth-of-type(2)")
    private WebElement basketBaggagePrice;

    @FindBy(css = ".flexAmount>.price>span:nth-of-type(2)")
    private WebElement basketFlexPrice;

    @FindBy(css = ".insuranceAmount>.price>span:nth-of-type(2)")
    private WebElement basketInsurancePrice;

    @FindBy(css = ".seatsAmount>.price>span:nth-of-type(2)")
    private WebElement basketSeatPrice;

    @FindBy(className = "currencySelected-text")
    private WebElement currencySelected;

    @FindBy(css = ".carparkingAmount>div.price>span:nth-of-type(2)")
    private WebElement basketCarParkingPrice;

    @FindBy(css = ".discountAmount>.price>span:nth-of-type(3)")
    private WebElement aviosBasketPrice;

    @FindBy(css = ".carhireAmount>div.price>span:nth-of-type(2)")
    private WebElement basketCarHirePrice;


    @FindBy(css = ".cardAmount>.price>span:nth-of-type(2)")
    private WebElement basketCardPrice;

    @FindBy(id="saveButton")
    private WebElement continueToBooking;

    @FindBy(xpath = "//select[@name='card-type']/option")
    private List<WebElement> cardTypeDropDownList;


    String loadingLocator ="//div[span[@class='loading-spinner-large']][@class='flights-loading']";

    String contactPassengerAddressListPopupLocator ="//div[@class='modal-content']";

    public void selectFlybeAccountOption(boolean option) {
        //Selects option to Do you have a Flybe account?
        try {
            if (option) {
                actionUtility.waitForElementClickable(yesFlyBeAccount,15);
                actionUtility.click(yesFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - Yes, I have a Flybe account");

            } else {
                actionUtility.waitForElementClickable(noFlyBeAccount,15);
                actionUtility.click(noFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - No, I do not have a Flybe account");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING,"unable to select any option in - Do you have a Flybe account?");
            throw e;
        }
    }

//    public void retrievePassengerContactDetailsByLogin(Hashtable<String, String> testData) {
//        // retrieves the passenger contact details by logging in
//        String contactEmail = null, contactPassword = null;
//        try {
//
//            actionUtility.waitForElementVisible(contactPassengerLoginMenu,5);
//
//            contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
//            contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
//            actionUtility.sendKeys(contactPassengerEmailForLogin,contactEmail);
//            actionUtility.sendKeys(contactPassengerPassword,contactPassword);
//            actionUtility.click(contactPassengerLogin);
//            actionUtility.waitForElementVisible(contactPassengerDetailsLayout,10);
//            if(testData.get("pleaseKeepMeUpdated")!=null && testData.get("pleaseKeepMeUpdated").equals("true")){
//                actionUtility.selectOption(pleaseKeepMeUpdated);
//            }
//            reportLogger.log(LogStatus.INFO, "successfully retrieved the passenger contacts by logging in with the email id - "+contactEmail);
//            savePassengerContactDetails();
//        } catch (Exception e) {
//            reportLogger.log(LogStatus.WARNING, "unable to retrieve the passenger contacts by logging in with the email id - "+contactEmail);
//            throw e;
//        }
//    }

    public Hashtable<String, String> retrievePassengerContactDetailsByLogin(Hashtable<String, String> testData) {
        // retrieves the passenger contact details by logging in
        String contactEmail = null, contactPassword = null;
        Hashtable<String,String> passengerName = new Hashtable<String,String>();
        String nameEntered;
        try {

            actionUtility.waitForElementVisible(contactPassengerLoginMenu,5);

            contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
            contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            actionUtility.sendKeys(contactPassengerEmailForLogin,contactEmail);
            actionUtility.sendKeys(contactPassengerPassword,contactPassword);
            actionUtility.click(contactPassengerLogin);
            actionUtility.waitForElementVisible(contactPassengerDetailsLayout,10);
            if(testData.get("pleaseKeepMeUpdated")!=null && testData.get("pleaseKeepMeUpdated").equals("true")){
                actionUtility.selectOption(pleaseKeepMeUpdated);
            }
            reportLogger.log(LogStatus.INFO, "successfully retrieved the passenger contacts by logging in with the email id - "+contactEmail);
            savePassengerContactDetails();
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the passenger contacts by logging in with the email id - "+contactEmail);
            throw e;
        }
        System.out.println(new Select(contactPassengerSalutation).getFirstSelectedOption().getText());
        System.out.println(contactPassengerFirstName.getAttribute("value"));
        System.out.println(contactPassengerLastName.getAttribute("value"));

        nameEntered = new Select(contactPassengerSalutation).getFirstSelectedOption().getText() + " " + contactPassengerFirstName.getAttribute("value") + " " + contactPassengerLastName.getAttribute("value");
        passengerName.put("passengerName1", nameEntered);
        return passengerName;
    }

//    public void enterPassengerContact(Hashtable<String, String> testData) {
//        //enter the passenger names and countries
//        try {
//            String[] contactPassengerName = testData.get("contactPassengerName").split(" ");
//            actionUtility.dropdownSelect(contactPassengerSalutation, ActionUtility.SelectionType.SELECTBYTEXT, contactPassengerName[0]);
//            actionUtility.sendKeys(contactPassengerFirstName,contactPassengerName[1]);
//            actionUtility.sendKeys(contactPassengerLastName,contactPassengerName[2]);
//            actionUtility.dropdownSelect(contactPassengerCountry, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("contactPassengerCountry"));
//
//            if (testData.get("contactPassengerPostCode") != null) {
//                contactPassengerPostCode.sendKeys(testData.get("contactPassengerPostCode"));
//            }
//            reportLogger.log(LogStatus.INFO, "successfully entered passenger contacts");
//        } catch (Exception e) {
//            reportLogger.log(LogStatus.WARNING, "unable to enter passenger contacts");
//            throw e;
//        }
//    }

    public Hashtable<String, String> enterPassengerContact(Hashtable<String, String> testData) {
        //enter the passenger names and countries
        Hashtable<String,String> passengerName = new Hashtable<String,String>();
        String nameEntered;
        try {
            String[] contactPassengerName = testData.get("contactPassengerName").split(" ");
            actionUtility.dropdownSelect(contactPassengerSalutation, ActionUtility.SelectionType.SELECTBYTEXT, contactPassengerName[0]);
            actionUtility.sendKeys(contactPassengerFirstName,contactPassengerName[1]);
            actionUtility.sendKeys(contactPassengerLastName,contactPassengerName[2]);
            actionUtility.dropdownSelect(contactPassengerCountry, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("contactPassengerCountry"));

            if (testData.get("contactPassengerPostCode") != null) {
                contactPassengerPostCode.sendKeys(testData.get("contactPassengerPostCode"));
            }
            reportLogger.log(LogStatus.INFO, "successfully entered passenger contacts");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter passenger contacts");
            throw e;
        }

        nameEntered = new Select(contactPassengerSalutation).getFirstSelectedOption().getText() + " " + contactPassengerFirstName.getAttribute("value") + " " + contactPassengerLastName.getAttribute("value");
        passengerName.put("passengerName1", nameEntered);
        return passengerName;
    }

    public void selectPassengerContactAddress(Hashtable<String, String> testData) {
        //selects the passenger contact address from the list
        try {
            actionUtility.click(contactPassengerFindAddress);
            actionUtility.waitForElementVisible(contactPassengerAddressListPopup, 20);
            for (int i = 0; i < contactPassengerAddresses.size(); i++) {
                if (contactPassengerAddresses.get(i).getText().trim().toLowerCase().equals(testData.get("contactPassengerAddresses").toLowerCase())) {
                    actionUtility.hardSelectOption(contactPassengerAddressSelectors.get(i));
                    break;
                }
            }
            actionUtility.waitForElementClickable(contactPassengerAddressContinue, 10);
            actionUtility.hardClick(contactPassengerAddressContinue);
            actionUtility.waitForElementNotPresent(contactPassengerAddressListPopupLocator, 5);
            reportLogger.log(LogStatus.INFO, "successfully selected the passenger contact address from the list");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the passenger contact address from the list");
            throw e;
        }
    }

    public void enterPassengerContactAddressManually(Hashtable<String, String> testData) {
        // enters the passenger the contact address manually
        try {

            actionUtility.selectOption(contactPassengerDontHavePostcode);

            actionUtility.sendKeys(contactPassengerAddressOne,testData.get("contactPassengerAddressOne"));

            if (testData.get("contactPassengerAddressTwo") != null) {
                actionUtility.sendKeys(contactPassengerAddressTwo, testData.get("contactPassengerAddressTwo"));
            }

            if (testData.get("contactPassengerAddressThree") != null) {
                actionUtility.sendKeys(contactPassengerAddressThree,testData.get("contactPassengerAddressThree"));
            }

            actionUtility.sendKeys(contactPassengerTown,testData.get("contactPassengerTown"));
            actionUtility.sendKeys(contactPassengerCounty,testData.get("contactPassengerCounty"));
            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact address manually");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact address manually");
            throw e;
        }
    }

    public void enterPassengerContactNumbersEmails(Hashtable<String, String> testData) {
        // entering the passenger contact number and emails
        try {


            if (testData.get("contactPassengerCountryCode") != null) {
                actionUtility.dropdownSelect(contactPassengerLandCountryCode, ActionUtility.SelectionType.SELECTBYVALUE, testData.get("contactPassengerCountryCode"));
            }

            if (testData.get("contactPassengerNo") != null) {
                actionUtility.sendKeys(contactPassengerLandLineNo,testData.get("contactPassengerNo"));
            }

            if(actionUtility.verifyIfElementIsDisplayed(contactPassengerEmail)){
                if(testData.get("contactPassengerEmail")!=null) {
                    actionUtility.sendKeys(contactPassengerEmail,testData.get("contactPassengerEmail"));
                }
            }

            if(testData.get("pleaseKeepMeUpdated")!=null && testData.get("pleaseKeepMeUpdated").equals("true")){
                actionUtility.selectOption(pleaseKeepMeUpdated);

            }
            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact number and email");

            savePassengerContactDetails();

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact number and email");
            throw e;
        }
    }

    private void savePassengerContactDetails(){

        //save passenger contact information
        try {
            actionUtility.hardSleep(2000);
            actionUtility.hardClick(savePassengerContact);
            actionUtility.waitForElementVisible(paymentSection, 10);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to save the passenger contact information");
            throw e;
        }
    }



    public void selectPaymentOption(Hashtable<String, String> testData) {
        // selects the payement type and enters the detail
        try {
            if (testData.get("paymentType").toLowerCase().contains("card")) {
                actionUtility.selectOption(paymentByCard);
                actionUtility.waitForElementVisible(paymentByCardSection,5);
                reportLogger.log(LogStatus.INFO, "successfully selected card payment option");

            } else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
                actionUtility.selectOption(paymentByPayPal);
                reportLogger.log(LogStatus.INFO, "successfully selected paypal payment option");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select payment option");
            throw e;
        }
    }

    public boolean selectCardTypeOption(Hashtable<String, String> testData) {
        // select the card type option
        try {
            boolean isDifferentCard = false;

            if (testData.get("savedCardName") != null) {
                return isDifferentCard;
            } else {

                try {
                    isDifferentCard = true;
                    if(!actionUtility.verifyIfElementIsDisplayed(newPaymentDetails,2)) {
                        actionUtility.click(differentPaymentCard);
                        actionUtility.click(addNewCard);
                        actionUtility.waitForElementVisible(newPaymentDetails, 10);
                    }
                } catch (TimeoutException e) {
                }

            }
            reportLogger.log(LogStatus.INFO, "selected card type option");
            return isDifferentCard;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select card type option");
            throw e;
        }
    }

    public void enterCardDetails(Hashtable<String, String> testData) {
        //selects the payment to be made from saved or different card and populate the card details
        try {
            boolean isDifferentCard = selectCardTypeOption(testData);

            if (isDifferentCard) {
                populateCardDetails(testData.get("cardName"));

                if(testData.get("saveNewCard")!=null && testData.get("saveNewCard").equals("true")){
                    actionUtility.selectOption(saveNewCard);
                }
            } else {
                selectSavedCard(testData.get("savedCardName"),testData.get("savedCardCvv"));
            }

            reportLogger.log(LogStatus.INFO, "successfully selected/entered cards details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter cards details");
            throw e;
        }
    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, cardDetails.getAsJsonObject(cardName).get("cardType").getAsString());
        actionUtility.sendKeys(cardNumber,cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryDate, ActionUtility.SelectionType.SELECTBYTEXT, new Integer(expiryDates[0]).toString());
        actionUtility.dropdownSelect(expiryDateYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        actionUtility.sendKeys(nameOnCard,cardDetails.getAsJsonObject(cardName).get("nameOnCard").getAsString());
        actionUtility.sendKeys(securityNumber,cardDetails.getAsJsonObject(cardName).get("securityNumber").getAsString());
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
    }

    private void selectSavedCard(String savedCardName, String cvv) {
        //selects the saved card
        boolean flag = false;

        for (int i = 0; i < savedCards.size(); i++) {

            if (savedCards.get(i).getText().toLowerCase().contains(savedCardName.toLowerCase())) {
                actionUtility.hardClick(savedCards.get(i));
                actionUtility.sendKeys(savedCardCvv,cvv);
                reportLogger.log(LogStatus.INFO, "successfully selected " + savedCardName + " from the list");
                flag = true;
                break;
            }
        }

        if (flag == false) {
            reportLogger.log(LogStatus.WARNING, "unable to select saved cards");
            throw new RuntimeException("unable to select saved cards");
        }

    }

    public void selectBillingAddressOption(Boolean flag) {
        //select the billing address option
        try {
            if (flag) {
                actionUtility.selectOption(billingAddressOption.get(0));
            } else {
                actionUtility.selectOption(billingAddressOption.get(1));
            }
            reportLogger.log(LogStatus.INFO, "successfully to select the billing address option");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the billing address option");
            throw e;
        }
    }

    public void selectTheBillingAddressCountryDropDown(Hashtable<String, String> testData) {
        // to select the country drop down under my billing address section
        try {
            actionUtility.dropdownSelect(billingCountry, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("billingAddressCountry"));
            reportLogger.log(LogStatus.INFO, "successfully selected the country in my billing address");

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the country in my billing address");
            throw e;
        }

    }


    public void selectBillingAddress(Hashtable<String, String> testData) {
        //selects the passenger billing address from the list
        actionUtility.sendKeys(billingPostCode,testData.get("billingPostCode"));
        try {
            actionUtility.click(findAddress);
            actionUtility.waitForElementVisible(billingAddressListPopup, 5);
            for (int i = 0; i < billingAddresses.size(); i++) {

                if (billingAddresses.get(i).getText().toLowerCase().equals(testData.get("billingAddress").toLowerCase())) {
                    actionUtility.selectOption(billingAddressSelectors.get(i));
                    break;
                }
            }
            actionUtility.click(billingAddressContinue);
            reportLogger.log(LogStatus.INFO, "successfully selected the billing address from the list");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the billing contact address from the list");
            throw e;
        }
    }


    public void enterPassengerBillingAddressManually(Hashtable<String, String> testData) {
//         enters the passenger the contact address manually
        try {

            if (new Select(billingCountry).getFirstSelectedOption().getText().trim().equals("UNITED KINGDOM")) {
                actionUtility.selectOption(billingAddressDontHavePostCode);
            }

            if (testData.get("billingPostCode") != null) {
                billingPostCode.clear();
                billingPostCode.sendKeys(testData.get("billingPostCode"));

            }

            billingAddressOne.sendKeys(testData.get("billingAddressOne"));

            if (testData.get("billingAddressTwo") != null) {
                billingAddressTwo.sendKeys(testData.get("billingAddressTwo"));
            }

            if (testData.get("billingAddressThree") != null) {
                billingAddressThree.sendKeys(testData.get("billingAddressThree"));
            }

            billingTown.sendKeys(testData.get("billingTown"));

            billingCounty.sendKeys(testData.get("billingCounty"));

            reportLogger.log(LogStatus.INFO, "successfully entered the passenger contact address manually");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter the passenger contact address manually");
            throw e;
        }
    }

    public void saveCardDetails(){
        //saves the card details
        try {
            actionUtility.click(saveCardDetails);
            actionUtility.waitForElementVisible(tripPurposeSection, 10);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to save the passenger contact information");
        }

    }

    public void enterPayPalCardDetails(Hashtable<String, String> testData) {
//        Selects the paypal login details and enters the username and password and completes the booking
        try {
            System.out.println("*******");
            actionUtility.waitForPageLoad(10);
            try {
                actionUtility.waitForElementVisible(payPalSpinner,3);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 8);
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID, "injectedUl");
            if (actionUtility.verifyIfElementIsDisplayed(payPalEmail)) {
                if ((testData.get("payPalUserName") != null) && (testData.get("payPalPassword")) != null) {
                    payPalEmail.clear();
                    payPalEmail.sendKeys(testData.get("payPalUserName"));
                    payPalPassword.clear();
                    payPalPassword.sendKeys(testData.get("payPalPassword"));
                    actionUtility.click(submitPayPal);
                    actionUtility.switchBackToWindowFromFrame();
                    try {
                        actionUtility.waitForElementVisible(payPalSpinner, 5);
                    }catch (TimeoutException e){}

                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 15);
                    actionUtility.click(payNowPayPal);
                    try {
                        actionUtility.waitForElementVisible(payPalSpinner, 5);
                    }catch (TimeoutException e){}
                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 8);
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully entered paypal details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter paypal details");
            throw e;
        }

    }

    public void verifyDefaultValueOfKeepMeUpdatedCheckBox(String checkBoxes) {
        // to verify the terms and conditions check boxes in payment page

        try {
            Assert.assertFalse(pleaseKeepMeUpdated.isSelected(), "please keep me updated check box is selected");
            reportLogger.log(LogStatus.INFO, "please keep me updated check box is not selected by default");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "please keep me updated check box is  selected by default");
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify default value of  keep me updated check box");
            throw e;
        }

    }

    public void selectTripPurpose(Hashtable<String, String> testData) {
        //selects the trip purpose
        try {
            String tripPurpose = testData.get("tripPurpose").toLowerCase();
            actionUtility.hardSleep(500);
            if (tripPurpose.contains("business")) {
                actionUtility.click(business);
            } else if (tripPurpose.contains("short break")) {
                actionUtility.click(shortBreak);
            } else if (tripPurpose.contains("longer holiday")) {
                actionUtility.click(longHoliday);
            } else if (tripPurpose.contains("visiting")) {
                actionUtility.click(visiting);
            } else if (tripPurpose.contains("other")) {
                actionUtility.click(others);
            }

            actionUtility.click(saveTripPurposes);
            actionUtility.waitForElementVisible(termsSection,10);
            reportLogger.log(LogStatus.INFO, "successfully selected the trip purpose - " + tripPurpose);

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the trip purpose");
            throw e;
        }
    }

    public void acceptTermsAndCondition() {
        //accepts the terms and conditions
        try {
            actionUtility.hardSleep(500);
            if(!actionUtility.verifyIfElementIsDisplayed(termsSection)) {

                if (actionUtility.verifyIfElementIsDisplayed(saveTripPurposes, 1)) {
                    actionUtility.click(saveTripPurposes);
                }
            }

            if (termsAndConditions.size() != 0) {

                for (WebElement condition : termsAndConditions) {
                    actionUtility.selectOption(condition);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the terms and conditions");
            } else {
                reportLogger.log(LogStatus.WARNING, "terms and conditions are not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the terms and conditions");
            throw e;
        }
    }

    public void continueToBooking() {
        //click on continue button for payment and booking
        try {
            actionUtility.click(continueToBooking);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully clicked the option continue to booking");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to booking");
            throw e;
        }
    }

    public double handlePriceChange(String option, double... oldBasketPrice) {
        //handles if there is any price change in between booking process
        try {
            System.out.println("@@@@@@@@@@");
            double updatedBasketPrice = 0.0;
            if (oldBasketPrice.length > 0) {
                updatedBasketPrice = oldBasketPrice[0];
            }

            boolean flag = false;

            try{
                actionUtility.waitForElementNotPresent(loadingLocator, 70);
                actionUtility.waitForElementVisible(continueWithNewPrice, 15);
                flag = true;

            }catch (TimeoutException e){

            }

            try {
                actionUtility.waitForElementVisible(continueWithNewPrice, 15);
                flag = true;
            } catch (TimeoutException e) {

            }

            if (flag) {
                if (option.toLowerCase().equals("continue")) {

                    try {
                        updatedBasketPrice = CommonUtility.convertStringToDouble(newBasketPrice.getText());


                        actionUtility.clickByAction(continueWithNewPrice);
                    } catch (NoSuchElementException e) {
                    }


                } else if (option.toLowerCase().equals("reselect flight")) {

                    try {
                        actionUtility.clickByAction(reselectFlight);
                    } catch (NoSuchElementException e) {
                    }
                }

                reportLogger.log(LogStatus.INFO, "successfully selected " + option + "from price change page");

            }

            return updatedBasketPrice;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to " + option + "from price change page");
            throw e;
        }
    }

    public void verifyValidationMessageForeName(){
        try{
            contactPassengerFirstName.sendKeys("A"+ Keys.SPACE);
            contactPassengerLastName.click();
            actionUtility.hardSleep(1000);
            Assert.assertTrue(validationMsgForename.isDisplayed());
            contactPassengerFirstName.clear();
            contactPassengerFirstName.sendKeys("A"+ Keys.SPACE);
            contactPassengerLastName.click();
            actionUtility.hardSleep(1000);
            Assert.assertTrue(validationMsgForename.isDisplayed());
            reportLogger.log(LogStatus.INFO,"successfully verified validation message for forename field in checkout page");
            }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING,"validation message that Forename is not long enough is not displayed");
            throw e;
            }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the validation message for forename field in checkout page");
            throw e;
            }
    }





    public void verifySecurityNumberValidation() {
        // verifies the security validation message
        try {
            try {
                Assert.assertTrue(securityNumberValidationSymbol.isDisplayed(), "No validation for security number");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "validation symbol for security number is not displayed");
            }

            try {
                Assert.assertTrue(securityNumberValidationMessage.isDisplayed(), "No validation for security number");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "validation message for security number is not displayed");
            }


            try {
                Assert.assertFalse(continueToBooking.isEnabled(), "No validation for security number");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, " \" Next\" button is not disabled for security number validation");
            }
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "validation for security number is not proper");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "validation for security number is not proper");
            throw e;
        }
    }

    public void verifyValidationForIncorrectPaymentDetails() {
        // verifies the security validation message when incorrect cards details are entered

        try {
            actionUtility.waitForElementVisible(incorrectPaymentDetailMessageBox, 50);

            String validationHeader = "Unable to take payment";
            String validationMessage = "There was a problem with your payment details. Please check and try again, or choose another payment method.";

            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(incorrectPaymentDetailMessageBox,15), "incorrect payment detail message box is not displayed");
                reportLogger.log(LogStatus.INFO, "incorrect payment detais message box is displayed");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "incorrect payment details message box is not displayed");
                throw e;
            }

            try {
                Assert.assertEquals(incorrectPaymentDetailValidationHeader.getText().toLowerCase().trim(), validationHeader.toLowerCase().trim(), "validation message header is not matching");
                reportLogger.log(LogStatus.INFO, "incorrect payment details message header is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "incorrect payment details message header is not matching Actual: " + incorrectPaymentDetailValidationHeader.getText().trim() + " Expected: " + validationHeader);
                throw e;
            }

            try {
                Assert.assertEquals(incorrectPaymentDetailValidationMessage.getText().toLowerCase().trim(), validationMessage.toLowerCase().trim(), "validation message is not matching");
                reportLogger.log(LogStatus.INFO, "incorrect payment details message is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "incorrect payment details message is not matching  Actual: " + incorrectPaymentDetailValidationMessage.getText().trim() + " Expected: " + validationMessage);
                throw e;

            }


        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the incorrect payment details message when incorrect card details are entered");
            throw e;
        }
    }

    public void verifyPaymentDetailsFooter() {
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on Flybe.com are subject to availability, inclusive of taxes and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            String fPara4 = "Certain Loganair fares have conditions attached requiring a minimum length of stay.";


            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Copy Right image is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right image is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertTrue(contactUsLink.isDisplayed(), "Contact Us Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in passenger page");
                throw e;
            }



            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in passenger page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


            try {
                Assert.assertEquals(footerPara4.getText().toLowerCase(), fPara4.toLowerCase(), "Footer para is not matching  in passenger page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in passenger page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in passenger page");
                throw e;
            }


        } catch (AssertionError e) {
            throw e;
        } catch (NoSuchElementException e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify footer in passenger page");
            throw e;
        }
    }


    public void verifyListOfEligibleCardsAndAssociatedFee(Hashtable<String, String> testData,Hashtable <String,Double> basketDetails) {
        // to Verify the card type drop down according to the selection
        boolean isDifferentCard = selectCardTypeOption(testData);
        String cardName;
        for (int i = 1; i <= 8; i++) {
            if (isDifferentCard) {

                cardName = testData.get("cardName"+i);
                verifyListOfEligibleCards(cardName);
                verifyCalculationOFCardCharge(cardName, basketDetails.get("overallBasketPrice"));
                verifyCardChargesAddedToBasket();
            } else {
                reportLogger.log(LogStatus.WARNING, "Unable to verify the list of eligible cards since save card option is chosen");
                throw new RuntimeException("Unable to verify the list of eligible cards since save card option is chosen");
            }

        }

    }

    private void verifyListOfEligibleCards(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.waitForElementVisible(cardType, 5);
        try {
            actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, cardDetails.getAsJsonObject(cardName).get("cardType").getAsString());
            reportLogger.log(LogStatus.INFO, cardName + " Card is displayed");
        } catch (NoSuchElementException e) {
            reportLogger.log(LogStatus.WARNING, cardName + " Card is not displayed");
            throw e;
        }

    }

    private void verifyCalculationOFCardCharge(String selectedCard, double basketTotalPrice){
        //verifies the calculation of card charges for all type of cards in pounds

        try {
            String[] chargedCards = {"visa", "masterCard", "americanExpress", "diners"};

            List<String> chargedCardNames = Arrays.asList(chargedCards);
            double cardCharge = 0.0;
            double basketPrice = 0.0;
            double threePercentOfBasketPrice = 0.0;
            double maxPrice = 0.0;
            String selectedCardValue = new Select(cardType).getFirstSelectedOption().getText();
            if (chargedCardNames.contains(selectedCard)) {

                cardCharge = Math.round(CommonUtility.convertStringToDouble(selectedCardValue) * 100.0) / 100.0;
                basketPrice = Math.round(basketTotalPrice * 100.0) / 100.0;
                threePercentOfBasketPrice = Math.round((0.03 * basketPrice) * 100.0) / 100.0;
                maxPrice = Math.max(threePercentOfBasketPrice, 5.00);
                try {
                    Assert.assertEquals(cardCharge, maxPrice);
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "card charges are not matching Expected - "+maxPrice+" Actual - "+cardCharge);
                    throw e;
                }


            } else {
                try {
                    Assert.assertTrue(selectedCardValue.toLowerCase().contains("free"));
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING, "card charges are not free");
                    throw e;
                }

            }

            reportLogger.log(LogStatus.INFO, "card charges are matching for the card - " + selectedCard);
        }catch (AssertionError e){

            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "successfully verified the card charges");
            throw e;
        }





    }


    public void verifyCalculationOFPayPal(double basketTotalPrice){
        //verifies the calculation of card charges for all type of cards in pounds

        try {

            double cardChargeAddedToBasket = 0.0;
            double basketPrice = 0.0;
            double threePercentOfBasketPrice = 0.0;
            double maxPrice = 0.0;
            cardChargeAddedToBasket = Math.round(CommonUtility.convertStringToDouble(basketCardPrice.getText()) * 100.0) / 100.0;
            basketPrice = Math.round(basketTotalPrice * 100.0) / 100.0;
            threePercentOfBasketPrice = Math.round((0.03 * basketPrice) * 100.0) / 100.0;
            maxPrice = Math.max(threePercentOfBasketPrice, 5.00);
            try {
                Assert.assertEquals(cardChargeAddedToBasket, maxPrice);
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING, "paypal charges are not matching Expected - "+maxPrice+" Actual - "+cardChargeAddedToBasket);
                throw e;
            }
            reportLogger.log(LogStatus.INFO, "paypal charges are matching");
        }catch (AssertionError e){

            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "successfully verified the paypal charges");
            throw e;
        }





    }




    public void verifyDefaultValueOfTermsAndConditionsCheckbox(String checkBoxes) {
        // to verify the terms and conditions check boxes in payment page
        try {
            switch (checkBoxes.toLowerCase()) {
                case "i confirm":
                    try {
                        Assert.assertFalse(termsAndConditions.get(0).isSelected(), "i confirm check box is selected");
                        reportLogger.log(LogStatus.INFO, "i confirm check box is not selected by default");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "i confirm check box is selected by default");
                        throw e;
                    }
                    break;
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify terms and conditions check box");
            throw e;
        }

    }



    public void openRulesLinkInTermsAndConditionSectionInPaymentPage(String rulesToOpen) {
        //open the respective rules link from payment page
        try {
            switch (rulesToOpen.toLowerCase()) {

                case "fare rules":
                    actionUtility.click(fareRulesInTermsAndCondition);
                    actionUtility.waitForElementVisible(rulePopup, 3);
                    break;
                case "general conditions of carriage":
                    actionUtility.click(generalConditionsOfCarriage);
                    actionUtility.switchWindow("General Conditions Of Carriage | Terms - Flybe");
                    break;
                case "dangerous goods":
                    actionUtility.click(dangerousGoodsInBaggage);
                    actionUtility.switchWindow("Restricted Baggage | Flybe");
                    break;

            }
            reportLogger.log(LogStatus.INFO, "successfully opened " + rulesToOpen + " in payment page");

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to open " + rulesToOpen + " in payment page");
            throw e;
        }
    }

    public void verifyTheRulesPopupInTermsAndConditionSectionInPaymentPage(String rulesPopup) {
        //To verify the header in rules popup of payment page
        String fareRulesExpected = "Fare rules";
        String generalConditionsOfCarriageExpected = "General conditions of carriage (Passenger and Baggage) - Flybe Limited";
        String dangerousGoodsInBaggageExpected = "restricted luggage";

        try {
            switch (rulesPopup.toLowerCase()) {

                case "fare rules":
                    try {
                        Assert.assertEquals(fareRulesPopupHeading.getText().toLowerCase().trim(), fareRulesExpected.toLowerCase().trim(), "Fare rules popup header is not matching in payment page");
                        reportLogger.log(LogStatus.INFO, "Fare Rule popup header is matching in payment page");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "Fare Rule popup header is not matching in payment page" + " Expected - " + fareRulesExpected + " Actual - " + fareRulesPopupHeading.getText());
                        throw e;
                    }
                    break;

                case "general conditions of carriage":
                    try {


                        switchToConditionsWindow("General Conditions Of Carriage | Terms - Flybe");


                        Assert.assertEquals(generalConditionsOfCarriageHeader.getText().toLowerCase().trim(), generalConditionsOfCarriageExpected.toLowerCase().trim(), "General conditions of carriage is not matching in payment page");

                        reportLogger.log(LogStatus.INFO, "General conditions of carriage popup header is matching in payment page");
                        try{
                            actionUtility.switchWindow("Booking Process | Flybe.com");
                        }catch (Exception e){
                            reportLogger.log(LogStatus.WARNING,"unable to switch back to main window");
                            throw e;
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "General conditions of carriage popup header is not matching in payment page" + " Expected - " + generalConditionsOfCarriageExpected + " Actual - " + generalConditionsOfCarriageHeader.getText());
                        throw e;
                    }
                    break;

                case "dangerous goods":
                    try {

                        switchToConditionsWindow("Restricted Baggage | Flybe");
                        Assert.assertEquals(dangerousGoodsInBaggageHeader.getText().toLowerCase().trim(), dangerousGoodsInBaggageExpected.toLowerCase().trim(), "Dangerous goods in baggage is not matching in payment page");
                        reportLogger.log(LogStatus.INFO, "Dangerous goods in baggage popup header is matching in payment page");
                        try{
                            actionUtility.switchWindow("Booking Process | Flybe.com");
                        }catch (Exception e){
                            reportLogger.log(LogStatus.WARNING,"unable to switch back to main window");
                            throw e;
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "Dangerous goods in baggage popup header is not matching in payment page" + " Expected - " + dangerousGoodsInBaggageExpected + " Actual - " + dangerousGoodsInBaggageHeader.getText());
                        throw e;
                    }
                    break;


            }
        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Unable to verify" + rulesPopup + "popup in payment page");
            throw e;

        }
    }



    private void switchToConditionsWindow(String val){
        // This method is used for switching to baggage terms and conditions
        boolean isSwitchSuccess = actionUtility.switchWindow(val);
        if (!isSwitchSuccess) {

            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
            if (isSwitchToCertificateErrorSuccess) {

                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");


            } else {
                reportLogger.log(LogStatus.WARNING,"unable to pass control to "+val+" window");
                throw new RuntimeException("unable to pass control to "+val+" window");
            }

            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Switching to "+val+" window is Successful");
        }else{
            reportLogger.log(LogStatus.INFO,"Switching to "+val+" window is successful");

        }

    }

    public void verifyTripPurposeOptions() {
        //verify trip purpose check boxes are present
        String expectedTripPurpose[] = {"Business", "Short break", "Longer holiday", "Visiting friends/relatives", "Other"};
        ArrayList<String> tripPurposeValues = new ArrayList<>();
        for (int i = 0; i < tripPurposeOptions.size(); i++) {
            tripPurposeValues.add(tripPurposeOptions.get(i).getAttribute("value"));

        }
        try {
            for (int i = 0; i < expectedTripPurpose.length; i++) {
                if (!(tripPurposeValues.contains(expectedTripPurpose[i]))) {
                    reportLogger.log(LogStatus.WARNING, "trip purpose option Expected - " + expectedTripPurpose[i] + " is not present Actual - " + tripPurposeOptions.get(i).getText());
                    throw new AssertionError("trip purpose option is not present");
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully verifies the trip purpose options ");

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the options in trip purpose section");
            throw e;
        }
    }

    public void verifyTheCardSectionIsHiddenWhenPayPalIsSelected() {
        // to verify the card section is hidden when we select Pay Pal option
        try {
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(paymentByCardSection), "card payment inputs are displayed when paypal is selected");
            reportLogger.log(LogStatus.INFO, "card payment inputs are hidden when pay pal is selected in payment page");

        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "card payment inputs are displayed when pay pal is selected in payment page");
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if card payment inputs are displayed");
            throw e;
        }

    }

    public void verifyCountryAndPostalCodeInputsWhenDifferentBillingAddressOptionIsSelected() {
        // to select the country drop down under my billing address section
        try {
            try {
                Assert.assertTrue(billingCountry.isDisplayed(), "Billing country drop down is not displayed");
                reportLogger.log(LogStatus.INFO, "Billing Country drop down is displayed when different billing address is selected");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Country drop down is not displayed when different billing address is selected");
                throw e;
            }
            try {
                Assert.assertTrue(billingPostCode.isDisplayed(), "Billing post code is not displayed");
                reportLogger.log(LogStatus.INFO, "Billing post code is displayed when different billing address is selected");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Billing post code is not displayed when different billing address is selected");
                throw e;
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify the country and post code in my billing address is different");
            throw e;
        }

    }

    public void verifySaveCardIsDisplayed(boolean flag){
//        this method verifies whether save card option is displayed
        if(flag){
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(saveNewCard),"save card option is not displayed");
                reportLogger.log(LogStatus.INFO,"save card option is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"save card option is not displayed");
                throw e;
            }
        }else{
            try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(saveNewCard),"save card option is displayed");
                reportLogger.log(LogStatus.INFO,"save card option is not displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"save card option is displayed");
                throw e;
            }
        }
    }


    public void verifySavedCardIsDisplayed(boolean flag){
//        this method verifies whether all ready saved card is displayed
        if(flag){
            try{
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(saveNewCard),"saved card is not displayed");
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(savedCardCvv),"saved card is not displayed");

                reportLogger.log(LogStatus.INFO,"saved card is displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"saved card is not displayed");
                throw e;
            }
        }else{
            try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(saveNewCard),"saved card  is displayed");
                reportLogger.log(LogStatus.INFO,"saved card is not displayed");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"saved card is displayed");
                throw e;
            }
        }
    }

    public void verifyBasketPriceWithBasketPriceOfPreviousPage(Hashtable<String, Double> previousBasketDetails) {
        //verifies if the total basket price in the extras page is same as previous page

        double expectedPrice = 0.0;
        double actualPrice = -1.0;

        try {
            try {
                expectedPrice = previousBasketDetails.get("overallBasketPrice");
                actionUtility.waitForElementVisible(overallBasketTotal, 10);
                actualPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "total price in the basket is not matching in payment page");
                reportLogger.log(LogStatus.INFO, "total price in the basket is matching in payment page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "total price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            try {
                expectedPrice = previousBasketDetails.get("outboundPrice");
                actualPrice = CommonUtility.convertStringToDouble(outboundTotal.getText());
                Assert.assertEquals(actualPrice, expectedPrice, "outbound price in the basket is not matching in payment page");
                reportLogger.log(LogStatus.INFO, "outbound price in the basket is matching in payment page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "outbound price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                throw e;

            }

            if (previousBasketDetails.get("inboundPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("inboundPrice");
                    actualPrice = CommonUtility.convertStringToDouble(returnTotal.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "inbound price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "inbound price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "inbound price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("ticketFlexibilityPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("ticketFlexibilityPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketFlexPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "ticket flexibility price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "ticket flexibility price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "ticket flexibility price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("baggagePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("baggagePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketBaggagePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "baggage price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "baggage price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "baggage price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("seatPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("seatPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketSeatPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "seat Price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "seat Price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "seat Price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("travelInsurancePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("travelInsurancePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketInsurancePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "travel insurance price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "travel insurance price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "travel insurance price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("carHirePrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("carHirePrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketCarHirePrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "car hire price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "car hire price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car hire price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("carParkingPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("carParkingPrice");
                    actualPrice = CommonUtility.convertStringToDouble(basketCarParkingPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "car hire price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "car parking price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "car parking price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

            if (previousBasketDetails.get("aviosPrice") != null) {
                try {
                    expectedPrice = previousBasketDetails.get("aviosPrice");
                    actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price in the basket is not matching in payment page");
                    reportLogger.log(LogStatus.INFO, "avios price in the basket is matching in payment page");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price in the basket is not matching in payment page Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify prices in the basket, in the payment page against prices of basket in passenger details/ extras page");
            throw e;
        }
    }

    public double verifyBasket(double... maxAviosPrice) {
        //verifies the basket and return the total basket price
        verifyCardChargesAddedToBasket();
        if(maxAviosPrice.length>0){
            verifyAviosPriceAddedToBasket(maxAviosPrice[0]);
        }
        verifyTotalBasketPriceWithSummationOfOtherElementsInBasket();
        Double totalBasketPrice = captureOverallBasketPrice();
        return totalBasketPrice;
    }


    private void verifyAviosPriceAddedToBasket(double maxAviosPrice){
        //to verify the avios price added in basket
        try {
            double actualPrice = 0.0, expectedPrice = -1.0;
            expectedPrice = calculateAviosPrice(maxAviosPrice);

            if (expectedPrice > 0) {

                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not added to basket");
                    throw e;
                }

                actualPrice = CommonUtility.convertStringToDouble(aviosBasketPrice.getText());

                try {
                    Assert.assertEquals(actualPrice, expectedPrice, "avios price is not matching in the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is matching in the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is not matching in the basket Expected - " + expectedPrice + " Actual -" + actualPrice);
                    throw e;
                }

            }else{
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice), "avios price is not added to the basket");
                    reportLogger.log(LogStatus.INFO, "avios price is not added to basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "avios price is added to basket");
                    throw e;
                }
            }
        }catch(AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the avios price in the basket");
            throw e;
        }
    }

    private void verifyCardChargesAddedToBasket() {
//        verifies if card charges are added to basket if selected
        double expectedPrice = -1.0;
        double actualPrice = 0.0;
        String selectedPaymentCard = null;

        try {

            selectedPaymentCard = new Select(cardType).getFirstSelectedOption().getText();

            if (!selectedPaymentCard.equals("")) {

                expectedPrice = CommonUtility.convertStringToDouble(selectedPaymentCard);


                if (expectedPrice > 0) {
                    actualPrice = CommonUtility.convertStringToDouble(basketCardPrice.getText());


                    try {
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(basketCardPrice), "card charges are not added to the basket");
                        reportLogger.log(LogStatus.INFO, "card charges are added to the basket");

                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "card charges are not added to the basket");
                        throw e;
                    }


                    try {
                        Assert.assertEquals(actualPrice, expectedPrice, "card charges are not matching in the basket");
                        reportLogger.log(LogStatus.INFO, "card charges are matching in the basket");

                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "card charges are not matching in the basket Expected -" + expectedPrice + " Actual - " + actualPrice);
                        throw e;
                    }


                }else{
                    try {
                        Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice));
                        reportLogger.log(LogStatus.INFO, "card charges are not added to the basket");
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.WARNING, "card charges are added to the basket");
                        throw e;
                    }

                }

            } else {

                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice));
                    reportLogger.log(LogStatus.INFO, "card charges are not added to the basket");
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "card charges are added to the basket");
                    throw e;
                }
            }

        } catch (AssertionError e) {
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if card charges are added to basket");
            throw e;
        }
    }

    private double calculateAviosPrice(double maxAviosPrice) {
        // to verify the avios price according to selection
        double aviosPriceApplied = 0.0;
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;
        double cardPrice = 0.0;

        outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText())*100.0)/100.0;

        if (actionUtility.verifyIfElementIsDisplayed(returnTotal)){
            inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)){
            ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)){
            bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)){
            seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText())*100.0)/100.0;
        }
        if (actionUtility.verifyIfElementIsDisplayed(basketCardPrice)){
            cardPrice = Math.round(CommonUtility.convertStringToDouble(basketCardPrice.getText())*100.0)/100.0;
        }

        aviosPriceApplied = outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice + cardPrice;
        aviosPriceApplied = Math.round(aviosPriceApplied*100.0)/100.0;

        if(aviosPriceApplied < maxAviosPrice){
            return aviosPriceApplied;
        }
        return maxAviosPrice;
    }


    public void verifyAddressIsPopulatedAfterPostCodeIsSelected() {
        // verifies if the addresses are popuplated after choosing the post code
        try {
            String address = contactPassengerAddressOne.getAttribute("value");
            String town = contactPassengerTown.getAttribute("value");
            String county = contactPassengerCounty.getAttribute("value");

            try {
                Assert.assertTrue(address.length() > 2, " address is not populated");
                reportLogger.log(LogStatus.INFO, "address is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "address is not populated after selecting the post code");
                throw e;
            }

            try {

                Assert.assertTrue(town.length() > 2, " town is not populated");
                reportLogger.log(LogStatus.INFO, "town is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "town is not populated after selecting the post code");
                throw e;
            }

            try {
                Assert.assertTrue(county.length() > 2, " county is not populated");
                reportLogger.log(LogStatus.INFO, "county is populated after selecting the post code");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "country is not populated after selecting the post code");
                throw e;
            }

        } catch (AssertionError e) {

            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify if address is populated after selecting the post code");
            throw e;
        }
    }


    private void verifyTotalBasketPriceWithSummationOfOtherElementsInBasket() {
        // verifies if the total basket price is equal to sum of the other elements added in the basket
        double outboundPrice = 0.0;
        double inboundPrice = 0.0;
        double ticketFlexibilityPrice = 0.0;
        double bagPrice = 0.0;
        double seatsPrice = 0.0;
        double travelInsurancePrice = 0.0;
        double carParkingPrice = 0.0;
        double cardPrice = 0.0;

        double sum = 0.0;
        double overallBasketPrice = 1.0;
        double aviosPrice = 0.0;

        try {
            outboundPrice = Math.round(CommonUtility.convertStringToDouble(outboundTotal.getText()) * 100.0) / 100.0;

            if (actionUtility.verifyIfElementIsDisplayed(returnTotal)) {
                inboundPrice = Math.round(CommonUtility.convertStringToDouble(returnTotal.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketFlexPrice)) {
                ticketFlexibilityPrice = Math.round(CommonUtility.convertStringToDouble(basketFlexPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketBaggagePrice)) {
                bagPrice = Math.round(CommonUtility.convertStringToDouble(basketBaggagePrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketSeatPrice)) {
                seatsPrice = Math.round(CommonUtility.convertStringToDouble(basketSeatPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(basketInsurancePrice)) {
                travelInsurancePrice = Math.round(CommonUtility.convertStringToDouble(basketInsurancePrice.getText()) * 100.0) / 100.0;
            }

            if(actionUtility.verifyIfElementIsDisplayed(basketCardPrice)){
                cardPrice = Math.round(CommonUtility.convertStringToDouble(basketCardPrice.getText()) * 100.0) / 100.0;
            }


            if (actionUtility.verifyIfElementIsDisplayed(basketCarParkingPrice)) {
                carParkingPrice = Math.round(CommonUtility.convertStringToDouble(basketCarParkingPrice.getText()) * 100.0) / 100.0;
            }

            if (actionUtility.verifyIfElementIsDisplayed(aviosBasketPrice)) {
                aviosPrice = Math.round(CommonUtility.convertStringToDouble(aviosBasketPrice.getText()) * 100.0) / 100.0;

            }
            sum = Math.round((outboundPrice + inboundPrice + ticketFlexibilityPrice + bagPrice + seatsPrice + travelInsurancePrice + carParkingPrice + cardPrice - aviosPrice) * 100.0) / 100.0;
            overallBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            Assert.assertEquals(overallBasketPrice, sum, "total basket price in the payment page is not matching with summation of all the element prices in the basket");
            reportLogger.log(LogStatus.INFO, "total basket price in the payment page is matching with summation of all the element prices in the basket");

        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "total basket price in the payment page is not matching with summation of all the element prices in the basket Expected - " + sum + " Actual -" + overallBasketPrice);
            throw e;

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify totals prices in the basket in payment page");
            throw e;
        }
    }

    private double captureOverallBasketPrice() {
        //if card charges are added to the basket

        try {
            double totalBasketPrice = CommonUtility.convertStringToDouble(overallBasketTotal.getText());
            reportLogger.log(LogStatus.INFO, "successfully captured overall Basket price in payment page");
            return totalBasketPrice;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to capture overall Basket price in payment page");
            throw e;
        }


    }


    public double captureAviosPriceFromBasket(){
        // returns avios price from basket
        return CommonUtility.convertStringToDouble(aviosBasketPrice.getText());
    }

    public void verifyWhetherStringIsPresentInCardTypeDropDown(String expectedString){

        try{
            actionUtility.waitForElementVisible(cardTypeDropDownList,10);
            int size = cardTypeDropDownList.size();
            try {
                for (int i = 1; i < size; i++) {
                    String displayedString = cardTypeDropDownList.get(i).getText();
                    if(!displayedString.contains("Free"))
                        Assert.assertTrue(displayedString.contains(expectedString),"does not contain NOK");
                }
                reportLogger.log(LogStatus.INFO,"successfully verified that card type dropdown has currency "+expectedString+" listed as expected");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,expectedString+" is not displayed in card type dropdown as expected");
                throw e;
            }
        }catch(Exception e) {
            reportLogger.log(LogStatus.WARNING,"unable to verify whether card type dropdown has currency "+expectedString+" listed as expected");
            throw e;
        }
    }

}
