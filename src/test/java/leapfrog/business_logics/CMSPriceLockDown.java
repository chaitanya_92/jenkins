package leapfrog.business_logics;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

/**
 * Created by STejas on 7/3/2017.
 */
public class CMSPriceLockDown {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSPriceLockDown() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "form#tripForm p input")
    private List<WebElement> tripPurposes;


    @FindBy(id = "paymentMethodTypeCard")
    private WebElement paymentByCard;

    @FindBy(id = "paymentMethodTypePayPal")
    private WebElement paymentByPayPal;

    @FindBy(id = "registeredUserN")
    private WebElement noFlyBeAccount;

    @FindBy(id = "registeredUserY")
    private WebElement yesFlyBeAccount;

    @FindBy(id = "authenticationUsername")
    private WebElement contactPassengerEmail;

    @FindBy(id = "authenticationPassword")
    private WebElement contactPassengerAuthenticationPassword;

    @FindBy(css = "a#retrieveDetails>span")
    private WebElement contactPassengerLogin;

    @FindBy(id = "paymentMethodCard")
    private WebElement differentPaymentCard;

    @FindBy(id = "NewCardType")
    private WebElement cardType;

    @FindBy(css = "input[id^='savedCard']:not([type='hidden'])")
    private List<WebElement> savedCards;

    @FindBy(css = "label[for^='savedCard']")
    private List<WebElement> savedCardNames;

    @FindBy(id = "CardNumber")
    private WebElement cardNumber;

    @FindBy(id = "ExpiryDate")
    private WebElement expiryDate;

    @FindBy(id = "ExpiryDateYear")
    private WebElement expiryDateYear;

    @FindBy(id = "CardName")
    private WebElement nameOnCard;

    @FindBy(id = "SaveCardCheck")
    private WebElement saveCard;

    @FindBy(id = "SecurityNumber")
    private WebElement securityNumber;

    @FindBy(id = "email")
    private WebElement payPalEmail;

    @FindBy(id = "password")
    private WebElement payPalPassword;

    @FindBy(id = "btnLogin")
    private WebElement submitPayPal;

    @FindBy(id = "confirmButtonTop")
    private WebElement payNowPayPal;

    @FindBy(xpath = "//p[@id='spinner-message']")
    private WebElement payPalSpinner;

   /* @FindBy(id="general")
    private WebElement termsAndConditions;*/

    @FindBy(css = "li.input input:not([type='hidden'])")
    private List<WebElement> termsAndConditions;

    /* @FindBy(id="newContinueButton")
     private WebElement continueToPriceLockDown;
 */
    @FindBy(id = "newContinueButton")
    private WebElement continueToBooking;

    @FindBy(xpath = "//select[contains(@id,'passengerTitle0_readonly')]")
    private WebElement passengerOneTitleReadOnly;

    @FindBy(xpath = "//input[contains(@id,'FirstName0')]")
    private WebElement passengerOneFirstName;

    @FindBy(xpath = "//input[contains(@id,'LastName0')]")
    private WebElement passengerOneLastName;

    @FindBy(xpath = "//select[contains(@id,'passengerTitle0')]")
    private WebElement passengerOneTitle;


    @FindBy(id = "firstname")
    private WebElement forename;

    @FindBy(id = "lastname")
    private WebElement surname;

    @FindBy(id = "basket.runningTotal.value")
    private WebElement pldTotalAmountInBasket;

    String payPalProcessingSpinner = "//p[@id='spinner-message']";
    String emailCheckInLocator = "//span[text()='Email Check-in']";

    public void selectFlybeAccountOption(boolean option) {
        //Selects option to Do you have a Flybe account?
        try {
            actionUtility.waitForElementNotPresent(emailCheckInLocator, 20);
            if (option) {
                actionUtility.waitForElementClickable(yesFlyBeAccount, 15);
                actionUtility.selectOption(yesFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - Yes, I have a Flybe account");

            } else {
                actionUtility.waitForElementClickable(noFlyBeAccount, 15);
                actionUtility.selectOption(noFlyBeAccount);
                reportLogger.log(LogStatus.INFO, "successfully selected - No, I do not have a Flybe account");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select any option in - Do you have a Flybe account?");
            throw e;
        }
    }

    public void retrievePassengerContactDetailsByLogin(Hashtable<String, String> testData) {
        // retrieves the passenger contact details by logging in
        String contactEmail = null, contactPassword = null;
        try {
            actionUtility.hardSleep(3000);

            contactEmail = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
            contactPassword = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
            actionUtility.waitForElementVisible(contactPassengerEmail, 3);
            contactPassengerEmail.clear();
            contactPassengerEmail.sendKeys(contactEmail);
            actionUtility.hardSleep(2000);
            actionUtility.waitForElementVisible(contactPassengerAuthenticationPassword, 3);
            actionUtility.sendKeys(contactPassengerAuthenticationPassword, contactPassword);

            actionUtility.click(contactPassengerLogin);
            int attempt = 0;
            while (actionUtility.verifyIfElementIsDisplayed(contactPassengerLogin) && (attempt < 3)) {
                try {
                    actionUtility.click(contactPassengerLogin);
                    actionUtility.hardSleep(2000);
                } catch (TimeoutException e) {
                } catch (ElementNotVisibleException e) {
                }
                attempt++;
            }
            if ((attempt == 3) && (actionUtility.verifyIfElementIsDisplayed(contactPassengerLogin))) {
                throw new RuntimeException("unable to retrieve the passenger contacts by logging in with the email id - " + contactEmail);
            }
            reportLogger.log(LogStatus.INFO, "successfully retrieved the passenger contacts by logging in with the email id - " + contactEmail);
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the passenger contacts by logging in with the email id - " + contactEmail);
            throw e;
        }
    }


    public void enterCardDetails(Hashtable<String, String> testData) {
        //selects the payment to be made from saved or different card and populate the card details
        try {
            boolean isDifferentCard = selectCardTypeOption(testData);

            if (isDifferentCard) {
                populateCardDetails(testData.get("cardName"));
            } else {
                selectSavedCard(testData.get("savedCardName"));
            }

            reportLogger.log(LogStatus.INFO, "successfully selected/entered cards details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter cards details");
            throw e;
        }
    }

    public boolean selectCardTypeOption(Hashtable<String, String> testData) {
        // select the card type option
        try {
            boolean isDifferentCard = false;

            if (testData.get("savedCardName") != null) {
                return isDifferentCard;
            } else {

                try {
                    isDifferentCard = true;
                    actionUtility.waitForElementVisible(differentPaymentCard, 3);
                    actionUtility.selectOption(differentPaymentCard);
                    actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, "");

                } catch (TimeoutException e) {
                }

            }
            reportLogger.log(LogStatus.INFO, "selected card type option");
            return isDifferentCard;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select card type option");
            throw e;
        }
    }

    private void selectSavedCard(String savedCardName) {
        //selects the saved card
        boolean flag = false;

        for (int i = 0; i < savedCards.size(); i++) {

            if (savedCardNames.get(i).getText().toLowerCase().contains(savedCardName)) {
                actionUtility.selectOption(savedCards.get(i));
                reportLogger.log(LogStatus.INFO, "successfully selected " + savedCardName + " from the list");
                flag = true;
                break;
            }
        }

        if (flag == false) {
            reportLogger.log(LogStatus.WARNING, "unable to select saved cards");
            throw new RuntimeException("unable to select saved cards");
        }

    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.waitForElementVisible(cardType, 5);
        actionUtility.dropdownSelect(cardType, ActionUtility.SelectionType.SELECTBYVALUE, cardDetails.getAsJsonObject(cardName).get("cardType").getAsString());
        cardNumber.sendKeys(cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryDate, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[0]);
        actionUtility.dropdownSelect(expiryDateYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        nameOnCard.clear();
        nameOnCard.sendKeys(cardDetails.getAsJsonObject(cardName).get("nameOnCard").getAsString());
        securityNumber.sendKeys(cardDetails.getAsJsonObject(cardName).get("securityNumber").getAsString());
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName).get("cardNumber").getAsString());
    }


    public void enterPayPalCardDetails(Hashtable<String, String> testData) {
//        Selects the paypal login details and enters the username and password and completes the booking
        try {
            System.out.println("*******");
            actionUtility.waitForPageLoad(10);
            try {
                actionUtility.waitForElementVisible(payPalSpinner, 5);
            } catch (TimeoutException e) {
            }
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 15);
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID, "injectedUl");
            if (actionUtility.verifyIfElementIsDisplayed(payPalEmail)) {
                if ((testData.get("payPalUserName") != null) && (testData.get("payPalPassword")) != null) {
                    payPalEmail.clear();
                    payPalEmail.sendKeys(testData.get("payPalUserName"));
                    payPalPassword.clear();
                    payPalPassword.sendKeys(testData.get("payPalPassword"));
                    actionUtility.click(submitPayPal);
                    actionUtility.switchBackToWindowFromFrame();
                    actionUtility.waitForElementVisible(payPalSpinner, 20);
                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 40);
                    actionUtility.click(payNowPayPal);
                    actionUtility.waitForElementVisible(payPalSpinner, 20);
                    actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 40);
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully entered paypal details");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter paypal details");
            throw e;
        }

    }

    public void selectPaymentOption(Hashtable<String, String> testData) {
        // selects the payement type and enters the detail
        try {
            if (testData.get("paymentType").toLowerCase().contains("card")) {
                actionUtility.selectOption(paymentByCard);
                reportLogger.log(LogStatus.INFO, "successfully selected card payment option");

            } else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
                actionUtility.selectOption(paymentByPayPal);
                reportLogger.log(LogStatus.INFO, "successfully selected paypal payment option");
            }

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select payment option");
            throw e;
        }
    }

    public void selectTripPurpose(Hashtable<String, String> testData) {
        //selects the trip purpose
        try {
            String tripPurpose = testData.get("tripPurpose").toLowerCase();

            if (tripPurposes.size() != 0) {

                if (tripPurpose.contains("business")) {
                    actionUtility.selectOption(tripPurposes.get(0));
                } else if (tripPurpose.contains("short break")) {
                    actionUtility.selectOption(tripPurposes.get(1));
                } else if (tripPurpose.contains("longer holiday")) {
                    actionUtility.selectOption(tripPurposes.get(2));
                } else if (tripPurpose.contains("visiting")) {
                    actionUtility.selectOption(tripPurposes.get(3));
                } else if (tripPurpose.contains("other")) {
                    actionUtility.selectOption(tripPurposes.get(4));
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the trip purpose - " + tripPurpose);
            } else {
                reportLogger.log(LogStatus.WARNING, tripPurpose + " trip purpose is not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the trip purpose");
            throw e;
        }
    }

    public void acceptTermsAndCondition() {
        //accepts the terms and conditions
        try {

            if (termsAndConditions.size() != 0) {

                for (WebElement condition : termsAndConditions) {
                    actionUtility.selectOption(condition);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected the terms and conditions");
            } else {
                reportLogger.log(LogStatus.WARNING, "terms and conditions are not available to select");
            }
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select the terms and conditions");
            throw e;
        }
    }

    public void continueToPLDBooking() {
        //click on continue button for payment and booking
        try {
            actionUtility.click(continueToBooking);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully clicked the option continue to booking");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to continue to booking");
            throw e;
        }
    }

    private Hashtable<String, Integer> getPassengerCountForEachType(Hashtable<String, String> testData) {
        //returns the passenger Count
        Hashtable<String, Integer> passengerCount = new Hashtable<String, Integer>();
        passengerCount.put("adultCount", 1);
        passengerCount.put("infantCount", 0);
        passengerCount.put("teenCount", 0);
        passengerCount.put("childCount", 0);

        if (testData.get("noOfAdult") != null) {
            passengerCount.put("adultCount", Integer.parseInt(testData.get("noOfAdult")));
        }

        if (testData.get("noOfInfant") != null) {
            passengerCount.put("infantCount", Integer.parseInt(testData.get("noOfInfant")));
        }

        if (testData.get("noOfTeen") != null) {
            passengerCount.put("teenCount", Integer.parseInt(testData.get("noOfTeen")));
        }

        if (testData.get("noOfChild") != null) {
            passengerCount.put("childCount", Integer.parseInt(testData.get("noOfChild")));
        }

        return passengerCount;
    }

    public Hashtable<String,String> getFlybeAccountHolderName(Hashtable<String, String> testData) {
        //get flybe account holder name from UI

        try {

            Hashtable<String,String> accountHolderName = new Hashtable<>();
            accountHolderName.put("forename",forename.getAttribute("value"));
            accountHolderName.put("surname",surname.getAttribute("value"));
            reportLogger.log(LogStatus.INFO, ("successfully retrieved flybe account holder name"));
            return accountHolderName;

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, ("unable to retrieve flybe account holder name"));
            throw e;
        }

    }

    public void verifyPlDTotalInBasket(Hashtable<String, String> testData,int noOfpassengers){

        try {

            int noOfpassengersPlusSector;
            if (testData.get("sector") != null) {
                noOfpassengersPlusSector = noOfpassengers * Integer.parseInt(testData.get("sector"));
            } else {
                noOfpassengersPlusSector = noOfpassengers;
            }
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(pldTotalAmountInBasket, 10);
            String pldTotalAmount = pldTotalAmountInBasket.getText();
            double expectedPldTotalAmount = Double.parseDouble(testData.get("pldPricePerPaxPerSector")) * noOfpassengersPlusSector;
            String ActPldTotalAmount = pldTotalAmount.substring(1);
            double actualPldTotalAmount = Double.parseDouble(ActPldTotalAmount);
            Assert.assertEquals(expectedPldTotalAmount, actualPldTotalAmount);

            reportLogger.log(LogStatus.INFO, "successfully verified Price Lock Down total amount displayed in the basket on payment page");
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, "Price Lock Down total amount displayed in the basket on payment page for " + noOfpassengers + "passenger/s and for " + testData.get("sector") + " sector/s is not correct");
            throw e;
        }
        catch(Exception e)
        {
            reportLogger.log(LogStatus.WARNING,"unable to retrieve and verify Price Lock Down total amount displayed in the basket on payment page");
            throw e;
        }

    }
}
