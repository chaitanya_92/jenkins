package leapfrog.business_logics;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.List;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSChangeBooking {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSChangeBooking() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }


    @FindBy(xpath = "//button[text()='Change flights']")
    private WebElement changeFlight;

    @FindBy(xpath = "//button[contains(text(),'Change seat')]")
    private List<WebElement> changeSeatButtons;

    @FindBy(xpath = "//button[contains(text(),'Change baggage')]")
    private List<WebElement> changeBaggageButtons;

    public void continueToChangeFlights(){
        //clicks on change flight button
        try{
            actionUtility.waitForElementClickable(changeFlight,10);
            actionUtility.clickByAction(changeFlight);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(25);
            reportLogger.log(LogStatus.INFO, "successfully continued to change flights Page");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to continue to change flights page");
            throw e;
        }
    }

    public void continueToChangeSeat(){
        //clicks on change seat or choose seat button
        try{
            System.out.println(changeSeatButtons.size());
            actionUtility.click(changeSeatButtons.get(0));
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully continued to change seats Page");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to continue to change seats page");
            throw e;
        }

    }

    public void continueToChangeBaggage(){
        //clicks on change seat button
        try{
            actionUtility.click(changeBaggageButtons.get(0));
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully continued to change baggage Page");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to continue to change baggage page");
            throw e;
        }

    }

    public void verifyChangeFlightIsEnabled(boolean flag){
//        this method verifies whether change flight button is enabled
        try{
            actionUtility.waitForElementVisible(changeFlight,25);
            if(flag){
                try{
                    Assert.assertTrue(changeFlight.isEnabled());
                    reportLogger.log(LogStatus.INFO,"change flight button is enabled");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change flight button is disabled");
                    throw e;
                }
            }else{
                try{
                    Assert.assertFalse(changeFlight.isEnabled());
                    reportLogger.log(LogStatus.INFO,"change flight button is disabled");
                }catch (AssertionError e){
                    reportLogger.log(LogStatus.WARNING,"change flight button is disabled");
                    throw e;
                }
            }
        }catch (AssertionError e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify status of change flight button");
            throw e;
        }

    }
}
