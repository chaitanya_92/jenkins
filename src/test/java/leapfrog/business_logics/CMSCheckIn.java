package leapfrog.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSCheckIn {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSCheckIn() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css= "div#apisModal div.modal-content")
    private WebElement apiContainer;

    @FindBy(css = "div#checkInNowModal div.modal-content")
    private WebElement checkInContainer;

    @FindBy(xpath = "//table[1][@class='checkin-table']/descendant::button[text()='Add passport details']")
    private List<WebElement> addAPIs;

    @FindBy(xpath = "//table[1][@class='checkin-table']/descendant::button[text()='Add passport details']")
    private WebElement addAPI;

    @FindBy(xpath = "//button[@data-direction='outbound'and text()='Check-in now']")
    private List<WebElement>    outboundCheckIns;

    @FindBy(xpath = "//button[@data-direction='inbound'and text()='Check-in now']")
    private List<WebElement> inboundCheckIns;

    @FindBy(css = "div[data-direction='outbound'] table button")
    private List<WebElement> outboundPrintBoardingPass;

    @FindBy(css = "div[data-direction='return'] table button")
    private List<WebElement> inboundPrintBoardingPass;



    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Booking ref']/following-sibling::div")
    private WebElement adultBoardingPassBookingRefNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Gate closes']/following-sibling::div")
    private WebElement adultBoardingPassGateClose;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Flight']/following-sibling::div")
    private WebElement adultBoardingPassFlightNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Flight departs'][2]/following-sibling::div")
    private WebElement adultBoardingPassFlightDepart;

    @FindBy(xpath = "    //div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[1]")
    private WebElement adultBoardingPassSalutation;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[3]")
    private WebElement adultBoardingPassFirstName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[4]")
    private WebElement adultBoardingPassLastName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Route']/following-sibling::div/span[1]")
    private WebElement adultBoardingPassSource;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Route']/following-sibling::div/span[3]")
    private WebElement adultBoardingPassDestination;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Seat']/following-sibling::div")
    private WebElement adultBoardingPassSeat;


    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Class']/following-sibling::div")
    private WebElement adultBoardingPassClass;


    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::div[text()='Travel date']/following-sibling::div")
    private WebElement adultBoardingPassTravelDate;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][1]/descendant::span[text()='Flight operated by']/following-sibling::span[2]")
    private WebElement adultBoardingPassOperatedBy;




    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Booking ref']/following-sibling::div")
    private WebElement adultTearOffSlipBookingRefNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Travel date']/following-sibling::div")
    private WebElement adultTearOffSlipTravelDate;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Flight']/following-sibling::div")
    private WebElement adultTearOffSlipFlightNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Route']/following-sibling::div/span[1]")
    private WebElement adultTearOffSlipSource;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Route']/following-sibling::div/span[3]")
    private WebElement adultTearOffSlipDestination;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[1]")
    private WebElement adultTearOffSlipSalutation;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[3]")
    private WebElement adultTearOffSlipFirstName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Passenger']/following-sibling::div/span[5]")
    private WebElement adultTearOffSlipLastName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][1]/descendant::div[text()='Seat']/following-sibling::div")
    private WebElement adultTearOffSlipSeat;


    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Booking ref']/following-sibling::div")
    private WebElement infantBoardingPassBookingRefNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Flight']/following-sibling::div")
    private WebElement infantBoardingPassFlightNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Gate closes']/following-sibling::div")
    private WebElement infantBoardingPassGateClose;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Flight departs'][2]/following-sibling::div")
    private WebElement infantBoardingPassFlightDepart;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[1]")
    private WebElement infantBoardingPassSalutation;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[3]")
    private WebElement infantBoardingPassFirstName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[4]")
    private WebElement infantBoardingPassLastName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Travel date']/following-sibling::div")
    private WebElement infantBoardingPassTravelDate;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Route']/following-sibling::div/span[1]")
    private WebElement infantBoardingPassSource;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Route']/following-sibling::div/span[3]")
    private WebElement infantBoardingPassDestination;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::div[text()='Seat']/following-sibling::div")
    private WebElement infantBoardingPassSeat;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='boarding-header'][2]/descendant::span[text()='Flight operated by']/following-sibling::span[2]")
    private WebElement infantBoardingPassOperatedBy;


    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Travel date']/following-sibling::div")
    private WebElement infantTearOffSlipTravelDate;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Flight']/following-sibling::div")
    private WebElement infantTearOffSlipFlightNo;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Route']/following-sibling::div/span[1]")
    private WebElement infantTearOffSlipSource;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Route']/following-sibling::div/span[3]")
    private WebElement infantTearOffSlipDestination;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[1]")
    private WebElement infantTearOffSlipSalutation;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[3]")
    private WebElement infantTearOffSlipFirstName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Passenger']/following-sibling::div/span[5]")
    private WebElement infantTearOffSlipLastName;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Seat']/following-sibling::div")
    private WebElement infantTearOffSlipSeat;

    @FindBy(xpath = "//div[@id='boardingpass-container']/descendant::div[@class='summary-container'][2]/descendant::div[text()='Booking ref']/following-sibling::div")
    private WebElement infantTearOffSlipBookingRefNo;

    @FindBy(xpath = "//span[text()='Hold baggage']/preceding-sibling::span[2]" )
    private WebElement noHoldBaggage;

    @FindBy(xpath = "//span[contains(text(),'Hold baggage')]" )
    private WebElement holdBaggage;

    @FindBy(css = "div[data-direction='outbound'] th.head-checkin-status > span")
    private List<WebElement> checkInValidationOutBound;

    @FindBy(css = "div[data-direction='return'] th.head-checkin-status > span")
    private List<WebElement> checkInValidationInBound;

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='male']")
    private List<WebElement> males;

    @FindBy(css = "div#checkInNowModal li[style='display: list-item;'] input[value='female']")
    private List<WebElement> females;


    @FindBy(xpath = "//span[@class='loading-spinner']" )
    private WebElement checkInLoader;

    @FindBy(css = "td.passenger-action > a")
    private  List<WebElement> checkInLinksForAirFrance;

    @FindBy(css = "td.passenger-action > button")
    private List<WebElement> checkInLink;


    //----------Footer-in-Home-Page-------------
    @FindBy(css = "div.container a[href='#'] img")
    private WebElement flybeLogo;

    @FindBy(css = "div.container nav.horizontal li:nth-child(2)")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    //    @FindBy(linkText = "Journey Comparison Times")
    @FindBy(css = "a[href='/proof/']")
    private WebElement JouneryComparisionTimesLink;

    @FindBy(css = "div.container a[href='http://flybe.custhelp.com/']")
    private WebElement ContactUsLink;

    @FindBy(css = "a[href='/terms/tariff.htm']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide/']")
    private WebElement priceGuide;

    @FindBy(css = "div.container a[href='/']")
    private WebElement flybeInFooter;

    @FindBy(css = "div.terms p:nth-child(1)")
    private WebElement footerPara1;

    @FindBy(css = "div.terms p:nth-child(2)")
    private WebElement footerPara2;

    @FindBy(css = "div.terms p:nth-child(3)")
    private WebElement footerPara3;


    @FindBy(xpath = "//div[@id='app-content']/iframe[contains(@id,'app-frame')]")
    private WebElement frame;

    @FindBy(xpath = "//div[@class='section voucher']")
    private WebElement boardingPassVoucher;


    public void continueAndverifyOtherAirlineWebsiteIsDisplayed(String title,String url){
        //verifies whether code share partner airline website is displayed on clicking check-in button on manage booking
                try{
                actionUtility.switchBackToWindowFromFrame();
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                actionUtility.waitForElementVisible(checkInLink,20);
                actionUtility.click(checkInLink.get(1));
                actionUtility.switchBackToWindowFromFrame();
                actionUtility.hardSleep(20000);
                actionUtility.switchWindow(title);
                String actualURL = TestManager.getDriver().getCurrentUrl();
                Assert.assertTrue(actualURL.contains(url));
                reportLogger.log(LogStatus.INFO,"code share partner website is opened");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"code share partner website is not opened");
                throw e;
            }
        catch(Exception e){
                reportLogger.log(LogStatus.WARNING,"unable to check if code share partner website is opened");
                throw e;
            }
    }



    String checkInLoaderIndicator = "//span[@class='loading-spinner']";

    public void waitAndCheckForCheckIn(int timeFactor) {
        //checks if check-in button is enabled
        try {
            int count = 0;
            actionUtility.hardSleep(2000);
            while (outboundCheckIns.size()== 0 && count <= timeFactor){
                actionUtility.hardSleep(4000);
                count++;
                try {
                    TestManager.getDriver().navigate().refresh();
                }catch (TimeoutException e){}
                actionUtility.waitForPageLoad(25);
                actionUtility.hardSleep(2000);
                actionUtility.switchBackToWindowFromFrame();
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
                try {
                    actionUtility.waitForElementVisible(checkInLoader, 10);
                }catch (TimeoutException e){}
                try {
                    actionUtility.waitForElementNotPresent(checkInLoaderIndicator,30);
                }catch (TimeoutException e){}



            }
            if (outboundCheckIns.size()== 0 && count >timeFactor) {
                throw new RuntimeException("Check-In Not enabled");
            }
            reportLogger.log(LogStatus.INFO,"check-in is enabled");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"check-in is not enabled");
            throw e;
        }
    }

    public void continueToAddAPI(int passengerNo){
        //clicks the add api button
        try {
            actionUtility.switchBackToWindowFromFrame();
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
            actionUtility.waitForElementVisible(addAPI, 10);
            for (int i=0; i<addAPIs.size(); i++){
                if(addAPIs.get(i).getAttribute("data-passenger").equals(String.valueOf(passengerNo-1))){
                    int attempt=0;
                    while(attempt<3){
                        try {
                            actionUtility.click(addAPIs.get(i));
                            actionUtility.waitForElementVisible(apiContainer, 10);
                            break;
                        }catch (TimeoutException e){
                            if (attempt==2){
                                throw e;
                            }
                        }
                        attempt++;

                    }

                }
            }

            reportLogger.log(LogStatus.INFO,"successfully opened add api Popup");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to open add api Popup");
            throw e;
        }

    }

    public void continueToSelectPassengerForCheckIn(String tripType){
        // clicks on on check in button
        try {
            if (tripType.toLowerCase().equals("outbound")) {
                actionUtility.click(outboundCheckIns.get(0));
                actionUtility.waitForElementVisible(checkInContainer,5);
            } else if (tripType.toLowerCase().equals("inbound")) {
                actionUtility.click(inboundCheckIns.get(0));
            }
            reportLogger.log(LogStatus.INFO,"successfully continued to select passenger for check in");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to select passenger for check in");
            throw e;
        }
    }

    public void verifyCheckInValidationMessageForTodayFlight(String tripType){
//        This method verifies check in validation method for Today's flight
        try{
            if(tripType.toLowerCase().equals("outbound")){
                verifyCheckInMessageToday(checkInValidationOutBound, tripType);
            }
            else if(tripType.toLowerCase().equals("inbound")){
                verifyCheckInMessageToday(checkInValidationInBound, tripType);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify check in validation message");
            throw e;
        }
    }

    private void verifyCheckInMessageToday(List<WebElement> checkInValidationToday, String tripType){
        for(int i=0; i<checkInValidationToday.size(); i++){
            try{
                Assert.assertEquals(checkInValidationToday.get(i).getText().toLowerCase(),"check-in\nopen");
                reportLogger.log(LogStatus.INFO,"successfully verified check in validation for today's flight for "+tripType);
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"check in validation message is not displayed correctly");
                throw e;
            }
        }
    }

    public void verifyCheckInValidationMessageForNonTodayFlight(String tripType){
//        This method verifies the check in validation message for non today flight
        try{

            if(tripType.toLowerCase().equals("outbound")){
                verifyCheckInMessageNonToday(checkInValidationOutBound, tripType);
            }
            else if(tripType.toLowerCase().equals("inbound")){
                verifyCheckInMessageNonToday(checkInValidationInBound, tripType);
            }

        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify check in validation message");
            throw e;
        }
    }

    private void verifyCheckInMessageNonToday(List<WebElement> checkInValidationNonToday, String tripType){
        for(int i=0; i<checkInValidationNonToday.size(); i++){
            try{
                Assert.assertNotEquals(checkInValidationNonToday.get(i).getText().toLowerCase(),"check-in\nopen");
                reportLogger.log(LogStatus.INFO,"successfully verified check in validation for non today's flight for "+tripType);
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"check in validation message is not displayed correctly");
                throw e;
            }

        }
    }

    //  Boarding pass methods
    public void printAndVerifyBoardingPass(Hashtable<String,String>testData,Hashtable<String,String>selectedFlightDetails,Hashtable<String,String>passengerNames,String bookingRefNo,Hashtable<String, Hashtable> checkInDetails,boolean baggageEditFlag,int passengerNo,int flightNo,String tripType,Hashtable<String,Hashtable<String,Hashtable>>... seatSelected) {
        //method calls to verify adult and infant boarding pass and tear-off slip
        try {

            List<WebElement> printBoardingPass = null;
            boolean sslFlag = true;
            if (tripType.toLowerCase().equals("outbound")) {
                printBoardingPass = outboundPrintBoardingPass;
            } else if (tripType.toLowerCase().equals("inbound")) {
                printBoardingPass = inboundPrintBoardingPass;
            }

            System.out.println(printBoardingPass.size()+" size of printing");

            int boardingPassCount =0;
            for (int i = 1; i <= flightNo; i++) {
                int count = 0;
                int infantCount = 0;

                if (testData.get("noOfInfant") != null) {
                    infantCount = Integer.parseInt(testData.get("noOfInfant"));
                }

                for (int j = 1; j <= passengerNo; j++) {
                    Boolean flag = (Boolean) checkInDetails.get(("flight" + i)).get(("passenger" + j));

                    if (flag.equals(true)) {
                        try {

                            Assert.assertTrue(printBoardingPass.get(boardingPassCount).getText().toLowerCase().equals("print boarding pass"));
                            reportLogger.log(LogStatus.INFO, "print boarding pass button is displayed");
                        } catch (AssertionError e) {
                            reportLogger.log(LogStatus.WARNING, "print boarding pass button is not displayed");
                            throw e;
                        }
                        actionUtility.hardSleep(2000);

                        int attempt =0;
                        boolean windowOpen = false;

                        while(attempt<3 && windowOpen==false){
                            try {
                                System.out.println(attempt+" to open BP *****");
                                actionUtility.click(printBoardingPass.get(boardingPassCount));
                                actionUtility.hardSleep(9000);
                                actionUtility.waitForPopupAsNewWindow(20);
                                windowOpen = true;
                            }catch (TimeoutException e){
                                attempt++;
                            }

                        }


                        try {
                            handlePrintDialog();
                        }catch (Exception e){}

                        boolean isSwitchSuccess = actionUtility.switchWindow("Check-in Boarding Pass","boarding_pass.PAGE_TITLE");

                        if (!isSwitchSuccess){
                            boolean isSwitchToCertificateErrorSuccess = actionUtility.switchWindow("Certificate Error: Navigation Blocked");
                            if (isSwitchToCertificateErrorSuccess){
                                TestManager.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");
                                handlePrintDialog();
                            }else {
                                throw  new RuntimeException("unable to pass control to boarding pass");
                            }

                        }

                        actionUtility.waitForPageLoad(15);
                        actionUtility.hardSleep(6000);
                        try{
                            actionUtility.waitForElementVisible(checkInLoader,8);
                        }catch (TimeoutException e){}
                        actionUtility.waitForElementNotPresent(checkInLoaderIndicator,20);
//                        handlePrintDialog();

                        if(seatSelected.length>0){


                            verifyAdultBoardingPassDetails(testData, selectedFlightDetails,passengerNames, bookingRefNo, tripType, i, j,seatSelected[0]);
                        }else {
                            verifyAdultBoardingPassDetails(testData, selectedFlightDetails,passengerNames, bookingRefNo, tripType, i, j);
                        }



                        if (count < infantCount) {

                            verifyInfantBoardingPassDetails(testData, selectedFlightDetails, passengerNames, bookingRefNo,tripType, i, j);
                            count++;

                        }
                        verifyBaggage(testData, baggageEditFlag, i, j);
                        actionUtility.switchBackToMainWindow("Flybe.com - Check-In Page");
                        actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");

                    } else {
                        try {
                            Assert.assertFalse(printBoardingPass.get(boardingPassCount).getText().toLowerCase().equals("print boarding pass"));
                            reportLogger.log(LogStatus.INFO, "print boarding pass is not displayed");
                        } catch (AssertionError e) {
                            reportLogger.log(LogStatus.INFO, "print boarding pass is displayed");
                            throw e;
                        }

                    }
                    boardingPassCount++;
                }

            }
            reportLogger.log(LogStatus.INFO, "Successfully to verified the boarding passes");
        }catch(IOException e){}
        catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"sUnable to verify the boarding passe");
            throw e;
        }

    }

    @Deprecated
    public void verifyCheckIn(String tripType) {
        //method calls to verify adult and infant boarding pass and tear-off slip

        List<WebElement> printBoardingPass = null;
        boolean sslFlag = true;
        if (tripType.toLowerCase().equals("outbound")) {
            printBoardingPass = outboundPrintBoardingPass;
        } else if (tripType.toLowerCase().equals("inbound")) {
            printBoardingPass = inboundPrintBoardingPass;
        }

        System.out.println(printBoardingPass.size()+" size of printing");

        int boardingPassCount =0;
        int attempt =0;
        boolean windowOpen = false;

//                        while(attempt<3 && windowOpen==false){
//                            try {
//                                System.out.println(attempt+" to open BP *****");
//                                actionUtility.click(printBoardingPass.get(boardingPassCount));
//                                actionUtility.hardSleep(9000);
//                                actionUtility.waitForPopupAsNewWindow(20);
//                                windowOpen = true;
//                            }catch (TimeoutException e){
//                                attempt++;
//                            }
//
//                            try {
//                                handlePrintDialog();
//                            }catch (Exception e){}
//
//
//                        }
        Assert.assertTrue(printBoardingPass.get(boardingPassCount).getText().toLowerCase().equals("print boarding pass"));


    }


    private void handlePrintDialog() throws IOException {
        //handle print Dialog in boarding pass
        try {
            String printHandlerLocation = ".\\pre_requisites\\printDialogHandler";
            if(DataUtility.readConfig("printDialogHandler.location")!= null){
                printHandlerLocation = DataUtility.readConfig("printDialogHandler.location");
            }

            actionUtility.hardSleep(3000);
            if( TestManager.getDriver() instanceof FirefoxDriver) {

                printHandlerLocation = printHandlerLocation+"\\PrintDialog2.exe";


                Process process = Runtime.getRuntime().exec(printHandlerLocation);
                InputStream is = process.getInputStream();
                int retCode = 0;
                int count =1;
                while(retCode != -1)
                {

                    retCode = is.read();

                    count++;

                }
            }
            else if( TestManager.getDriver() instanceof InternetExplorerDriver ) {
                printHandlerLocation = printHandlerLocation+"\\PrintDialog_ie.exe";
                Process process =Runtime.getRuntime().exec(printHandlerLocation);
                InputStream is = process.getInputStream();
                int retCode = 0;
                while(retCode != -1)
                {
                    retCode = is.read();
                }
            }
            else if( TestManager.getDriver() instanceof ChromeDriver) {
                actionUtility.hardSleep(2000);
                printHandlerLocation = printHandlerLocation+"\\PrintDialog_ie.exe";
                Process process =Runtime.getRuntime().exec(printHandlerLocation);
                InputStream is = process.getInputStream();
                int retCode = 0;

                while(retCode != -1)
                {
                    retCode = is.read();
                }


            }
            reportLogger.log(LogStatus.INFO, "successfully closed the print dialog");
        } catch (IOException e) {
            reportLogger.log(LogStatus.WARNING, "unable to close the print dialog");
            throw e;
        }
    }


//    private void handlePrintDialog() throws IOException {
//        //handle print Dialog in boarding pass
//        try {
//            String printHandlerLocation = ".\\pre_requisites\\printDialogHandler";
//                if(DataUtility.readConfig("printDialogHandler.location")!= null){
//                    DataUtility.readConfig("printDialogHandler.location");
//                }
//
//            actionUtility.hardSleep(3000);
//            if( TestManager.getDriver() instanceof FirefoxDriver) {
//
//                printHandlerLocation = printHandlerLocation+"\\PrintDialog2.exe";
//
//                Process process = Runtime.getRuntime().exec(printHandlerLocation);
//                InputStream is = process.getInputStream();
//                int retCode = 0;
//                int count =1;
//                while(retCode != -1)
//                {
//
//                    retCode = is.read();
//
//                    count++;
//
//                }
//            }
//            else if( TestManager.getDriver() instanceof InternetExplorerDriver ) {
//                printHandlerLocation = printHandlerLocation+"\\PrintDialog_ie.exe";
//                Process process =Runtime.getRuntime().exec(printHandlerLocation);
//                InputStream is = process.getInputStream();
//                int retCode = 0;
//                while(retCode != -1)
//                {
//                    retCode = is.read();
//                }
//            }
//            else if( TestManager.getDriver() instanceof ChromeDriver) {
//                actionUtility.hardSleep(2000);
//                printHandlerLocation = printHandlerLocation+"\\PrintDialog_ie.exe";
//                Process process =Runtime.getRuntime().exec(printHandlerLocation);
//                InputStream is = process.getInputStream();
//                int retCode = 0;
//
//                while(retCode != -1)
//                {
//                    retCode = is.read();
//                }
//
//
//            }
//            reportLogger.log(LogStatus.INFO, "successfully closed the print dialog");
//        } catch (IOException e) {
//            reportLogger.log(LogStatus.WARNING, "unable to close the print dialog");
//            throw e;
//        }
//    }



    private void verifyAdultBoardingPassDetails(Hashtable<String,String>testData,Hashtable<String,String>selectedFlightDetails,Hashtable<String,String>passengerNames,String bookingRefNo,String tripType,int flightNo,int passengerNo,Hashtable<String,Hashtable<String,Hashtable>>... seatSelected){
        //method calls to verify adult boarding pass and tear-off slip
        Hashtable seatSelectedForTrip = null;

//        Assert.assertTrue(false);
        verifyFlightDetailsForAdultInBoardingPass(selectedFlightDetails, flightNo, passengerNo);
        verifyPassengerNameForAdultInBoardingPass(passengerNames, passengerNo, flightNo);
        if(tripType.equals("outbound")) {
            if(seatSelected.length>0) {
                seatSelectedForTrip = seatSelected[0].get("passenger" + passengerNo).get("outbound");
                verifySeatForAdultInBoardingPass(seatSelectedForTrip, passengerNo, flightNo);
                verifySeatForAdultInTearOffSlip(seatSelectedForTrip, passengerNo, flightNo);
            }
        }else if(tripType.equals("inbound")){
            if(seatSelected.length>0) {
                seatSelectedForTrip = seatSelected[0].get("passenger" + passengerNo).get("inbound");
                verifySeatForAdultInBoardingPass(seatSelectedForTrip, passengerNo, flightNo);
                verifySeatForAdultInTearOffSlip(seatSelectedForTrip, passengerNo, flightNo);
            }

        }


        verifyTravelDateForAdultInBoardingPass(testData, flightNo,tripType, passengerNo);
        verifyBookingRefNoForAdultInBoardingPass( bookingRefNo, passengerNo, flightNo);
        verifyFlightDetailsForAdultInTearOffSlip(selectedFlightDetails, flightNo, passengerNo);
        verifyPassengerNameForAdultInTearOffSlip(passengerNames, passengerNo, flightNo);
        verifyTravelDateForAdultInTearOffSlip(testData, passengerNo,tripType,passengerNo);
        verifyBookingRefNoForAdultInTearOffSlip(bookingRefNo, passengerNo, flightNo);


    }

    private void verifyInfantBoardingPassDetails(Hashtable<String,String>testData,Hashtable<String,String>selectedFlightDetails,Hashtable<String,String>passengerNames,String bookingRefNo,String tripType, int flightNo,int passengerNo){
        //method call to verify infant boarding pass and tear-off slip
        try {
            verifyFlightDetailsForInfantInBoardingPass(selectedFlightDetails, flightNo, passengerNo);
            verifyPassengerNameForInfantInBoardingPass(passengerNames, passengerNo, flightNo);
            verifySeatForInfantInBoardingPass(passengerNo, flightNo);
            verifyTravelDateForInfantInBoardingPass(testData, tripType, flightNo, passengerNo);
            verifyBookingRefNoForInfantInBoardingPass(bookingRefNo, passengerNo, flightNo);
            verifyFlightDetailsForInfantInTearOffSlip(selectedFlightDetails, flightNo, passengerNo);
            verifyPassengerNameForInfantInTearOffSlip(passengerNames, passengerNo, flightNo);
            verifySeatForInfantInTearOffSlip(passengerNo, flightNo);
            verifyTravelDateForInfantInTearOffSlip(testData, tripType, passengerNo, flightNo);
            verifyBookingRefNoForInfantInTearOffSlip(bookingRefNo, passengerNo, flightNo);
        }catch(Exception e){
        }


    }


    private void verifyFlightDetailsForAdultInBoardingPass(Hashtable<String,String>selectedFlightDetails,int flightNo,int passengerNo){
        //verifies flight details in adult boarding pass
        try {

            Assert.assertTrue(adultBoardingPassFlightNo.getText().equals(selectedFlightDetails.get(("flightNo" + flightNo))), "flight no. in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight no. in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight no. in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightNo" + flightNo))+" Actual - "+adultBoardingPassFlightNo.getText());
            throw e;
        }
        try {
            Assert.assertTrue(adultBoardingPassFlightDepart.getText().equals(selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5)), "flight depart time in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight depart time in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight depart time in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5)+" Actual - "+adultBoardingPassFlightDepart.getText());
            throw e;
        }

        try {
            Assert.assertEquals(getTimeDifference(selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5), adultBoardingPassGateClose.getText()),"00:30", "Gate close time is incorrect in Boarding Pass");
            reportLogger.log(LogStatus.INFO, flightNo+"-gate close time in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-gate close time time in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+"00:30"+" Actual - "+getTimeDifference(selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5), adultBoardingPassGateClose.getText()));
            throw e;
        }


        try {
            Assert.assertTrue(adultBoardingPassSource.getText().equals(selectedFlightDetails.get(("flightSource" + flightNo))), "flight source in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight source in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight source in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightSource" + flightNo))+" Actual - "+adultBoardingPassSource.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultBoardingPassDestination.getText().equals(selectedFlightDetails.get(("flightDestination" + flightNo))), "flight Destination in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight Destination in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight Destination in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDestination" + flightNo))+" Actual - "+adultBoardingPassDestination.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultBoardingPassOperatedBy.getText().equals(selectedFlightDetails.get(("flightName" + flightNo))), "flight operated by in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight operated by in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight operated by in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightName" + flightNo))+" Actual - "+adultBoardingPassOperatedBy.getText());
            throw e;
        }
    }

    private void verifyPassengerNameForAdultInBoardingPass(Hashtable<String,String>passengerNames,int passengerNo,int flightNo){
        //verifies passenger name in adult boarding pass
        String[] passengerName =  passengerNames.get(("passengerName"+passengerNo)).split(" ");
        try {
            Assert.assertTrue(passengerName[0].contains(adultBoardingPassSalutation.getText()), "salutation of passenger name in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger salutation in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger salutation in boarding pass for flight-" + flightNo + " is not matching. "+adultBoardingPassSalutation.getText()+" is not present in "+passengerName[0]);
            throw e;
        }

        try {
            Assert.assertTrue(adultBoardingPassFirstName.getText().equals(passengerName[1]), "first name of passenger name in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger first name in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger first name in boarding pass for flight-" + flightNo + " is not matching Expected - "+passengerName[1]+" Actual - "+adultBoardingPassFirstName.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultBoardingPassLastName.getText().equals(passengerName[2]), "last name of passenger name in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger last name in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger last name in boarding pass for flight-" + flightNo + " is not matching Expected - "+passengerName[2]+" Actual - "+adultBoardingPassLastName.getText());
            throw e;
        }
    }

    private void verifySeatForAdultInBoardingPass(Hashtable<String,String>seatSelectedForTrip,int passengerNo,int flightNo){
        //verifies seat in adult boarding pass
        String seatNo = null;
        try {
            seatNo= seatSelectedForTrip.get(("flight"+flightNo));
            if (!seatNo.equals("--")) {
                Assert.assertTrue(adultBoardingPassSeat.getText().equals(seatNo), "seat no. for in boarding pass not matching");
                reportLogger.log(LogStatus.INFO, "seat no. for passenger-"+passengerNo+" in boarding pass for flight-" + flightNo + " is matching");
            }else if(seatNo.equals("--")){
                reportLogger.log(LogStatus.INFO, "seats are not selected for passenger-"+passengerNo+"  while booking the ticket for flight-" + flightNo );
            }

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "seat no. for passenger-"+passengerNo+" in boarding pass for flight-" + flightNo + " is not matching Expected - "+seatNo+" Actual - "+adultBoardingPassSeat.getText());
            throw e;
        }
    }


    public void verifyClassInBoardingPass(){
        //verifies class value in adult boarding pass

        String expectedClass = "Y";


        try {
            Assert.assertTrue(adultBoardingPassClass.getText().equals(expectedClass), "Class in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, "class in boarding pass is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "class in boarding pass is not matching Expected - Y"+ " Actual - "+adultBoardingPassClass.getText());
            throw e;
        }

    }


    private void verifyTravelDateForAdultInBoardingPass(Hashtable<String,String>testData,int flightNo,String tripType,int passengerNo){
        //verifies travel date in adult boarding pass

        String expectedDepartureDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("departDateOffset")), "dd MMM yyyy");

        if(tripType.equals("inbound")) {
            expectedDepartureDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("returnDateOffset")), "dd MMM yyyy");
        }

        try {
            Assert.assertTrue(adultBoardingPassTravelDate.getText().equals(expectedDepartureDate), "travel date in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight travel date in boarding pass of passenger:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight travel date in boarding pass of passenger:" + (passengerNo) + " is not matching Expected - "+expectedDepartureDate+" Actual - "+adultBoardingPassTravelDate.getText());
            throw e;
        }

    }

    private void verifyBookingRefNoForAdultInBoardingPass(String bookingRefNo,int passengerNo,int flightNo){
        //verifying the booking ref no. in adult boarding pass

        try {
            Assert.assertTrue(adultBoardingPassBookingRefNo.getText().equals(bookingRefNo), "Booking Ref No. in boarding pass not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in boarding Pass of passenger"+(passengerNo)+" for flight-"+flightNo+" is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in boarding Pass of passenger-"+(passengerNo)+" for flight-"+flightNo+" is not matching Expected - "+bookingRefNo+" Actual - "+adultBoardingPassBookingRefNo.getText());
            throw e;
        }
    }


    private void verifyFlightDetailsForAdultInTearOffSlip(Hashtable<String,String>selectedFlightDetails,int flightNo,int passengerNo){
        //verifies flight details in adult tear-off slip;
        try {
            Assert.assertTrue(adultTearOffSlipFlightNo.getText().equals(selectedFlightDetails.get(("flightNo" + flightNo))), "flight no. in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight no. in tear-off slip of passenger-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight no. in tear-off slip of passenger-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightNo" + flightNo))+" Actual - "+adultTearOffSlipFlightNo.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultTearOffSlipSource.getText().equals(selectedFlightDetails.get(("flightSource" + flightNo))), "flight source in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight source in tear-off slip of passenger-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight source in tear-off slip of passenger-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightSource" + flightNo))+" Actual - "+adultTearOffSlipSource.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultTearOffSlipDestination.getText().equals(selectedFlightDetails.get(("flightDestination" + flightNo))), "flight destination in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight destination in tear-off slip of passenger-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight destination in tear-off slip of passenger-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDestination" + flightNo))+" Actual - "+adultTearOffSlipDestination.getText());
            throw e;
        }

    }

    private void verifyPassengerNameForAdultInTearOffSlip(Hashtable<String,String>passengerNames,int passengerNo,int flightNo){
        //verifies passenger name in adult tear-off slip
        String[] passengerName =  passengerNames.get(("passengerName"+passengerNo)).split(" ");
        try {
            Assert.assertTrue(passengerName[0].contains(adultTearOffSlipSalutation.getText()), "salutation of passenger name in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger salutation in tear-off slip  for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger salutation in tear-off slip  for flight-" + flightNo + " is not matching "+adultTearOffSlipSalutation.getText()+" is not present in "+passengerName[0]);
            throw e;
        }

        try {
            Assert.assertTrue(adultTearOffSlipFirstName.getText().equals(passengerName[1]), "first name of passenger name in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger first name in tear-off slip  for flight-" + (flightNo + " is matching"));
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger first name in tear-off slip  for flight-" + flightNo + " is not matching Expected - "+passengerName[1]+" Actual - "+adultTearOffSlipFirstName.getText());
            throw e;
        }

        try {
            Assert.assertTrue(adultTearOffSlipLastName.getText().equals(passengerName[2]), "first name of passenger name in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-passenger last name in tear-off slip  for flight-" + (flightNo + " is matching"));
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-passenger last name in tear-off slip s for flight-" + flightNo + " is not matching Expected - "+passengerName[2]+" Actual - "+adultTearOffSlipLastName.getText());
            throw e;
        }
    }

    private void verifySeatForAdultInTearOffSlip(Hashtable<String,String>seatSelected,int passengerNo,int flightNo){
        //verifies seat in adult tear-off slip
        String seatNo = null;
        try {
            seatNo = seatSelected.get(("flight"+flightNo));
            Assert.assertTrue(adultTearOffSlipSeat.getText().equals(seatNo), "seat no. for in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, "seat no. for passenger-"+passengerNo+" in tear-off slip  for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "seat no. for passenger-"+passengerNo+" in tear-off slip  for flight-" + flightNo + " is not matching Expected - "+seatNo+" Actual - "+adultTearOffSlipSeat.getText());
            throw e;
        }
    }

    private void verifyTravelDateForAdultInTearOffSlip(Hashtable<String, String>testData,int passengerNo, String tripType, int flightNo){
        //verifies travel date in adult tear-off slip
        String expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("departDateOffset")), "dd MMM yyyy");

        if(tripType.equals("inbound")) {
            expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("returnDateOffset")), "dd MMM yyyy");
        }

        try {
            Assert.assertTrue(adultTearOffSlipTravelDate.getText().equals(expectedTravelDate), "travel date in tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight travel date in tear-off slip of passenger-" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight travel date in tear-off slip of passenger-" + (passengerNo) + " is not matching Expected - "+expectedTravelDate+" Actual - "+adultTearOffSlipTravelDate.getText());
            throw e;
        }

    }

    private void verifyBookingRefNoForAdultInTearOffSlip(String bookingRefNo,int passengerNo, int flightNo){
        //verifies booking ref no. in adult tear-off slip
        try {
            Assert.assertTrue(adultTearOffSlipBookingRefNo.getText().equals(bookingRefNo), "Booking Ref No. in tear-off Slip not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in tear-off slip of passenger-"+(passengerNo)+" for flight-"+flightNo+" is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in tear-off slip of passenger-"+(passengerNo)+" for flight-"+flightNo+" is not matching Expected - "+bookingRefNo+" Actual - "+adultTearOffSlipBookingRefNo.getText());
            throw e;
        }
    }



    private void verifyFlightDetailsForInfantInBoardingPass(Hashtable<String,String>selectedFlightDetails,int flightNo,int passengerNo){
        //verifies flight details in infant boarding pass
        try {
            Assert.assertTrue(infantBoardingPassFlightNo.getText().equals(selectedFlightDetails.get(("flightNo" + flightNo))), "flight no. in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight no. in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight no. in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightNo" + flightNo))+" Actual - "+infantBoardingPassFlightNo.getText());
            throw e;
        }
        try {
            Assert.assertTrue(infantBoardingPassFlightDepart.getText().equals(selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5)), "flight depart time in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight depart time in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight depart time in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDepartTime" + flightNo)).substring(0,5)+" Actual -"+infantBoardingPassFlightDepart.getText());
            throw e;
        }

        try {

            Assert.assertEquals(getTimeDifference(selectedFlightDetails.get(("flightDepartTime"+flightNo)),infantBoardingPassGateClose.getText()),"00:30", "Gate close time is incorrect in infant boarding Pass");
            reportLogger.log(LogStatus.INFO, flightNo+"-gate close time in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-gate close time time in boarding pass of infant:" + (passengerNo) + " is not matching Expected - 00:30"+ " Actual - "+getTimeDifference(selectedFlightDetails.get(("flightDepartTime"+flightNo)),infantBoardingPassGateClose.getText()));
            throw e;
        }


        try {
            Assert.assertTrue(infantBoardingPassSource.getText().equals(selectedFlightDetails.get(("flightSource" + flightNo))), "flight source in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight source in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight source in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightSource" + flightNo))+" Actual - "+infantBoardingPassSource.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantBoardingPassDestination.getText().equals(selectedFlightDetails.get(("flightDestination" + flightNo))), "flight Destination in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight Destination in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight Destination in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDestination" + flightNo))+" Actual - "+infantBoardingPassDestination.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantBoardingPassOperatedBy.getText().equals(selectedFlightDetails.get(("flightName" + flightNo))), "flight operated by in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight operated by in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight operated by in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightName" + flightNo))+" Actual - "+infantBoardingPassOperatedBy.getText());
            throw e;
        }
    }

    private void verifyPassengerNameForInfantInBoardingPass(Hashtable<String,String>passengerNames,int passengerNo,int flightNo){
        //verifies passenger name in infant boarding pass
        String[] passengerName =  passengerNames.get(("infantName"+passengerNo)).split(" ");
        try {
            Assert.assertTrue(passengerName[0].contains(infantBoardingPassSalutation.getText()), "salutation of infant name in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant salutation in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant salutation in boarding pass for flight-" + flightNo + " is not matching "+infantBoardingPassSalutation.getText()+" is not present in "+passengerName[0]);
            throw e;
        }

        try {
            Assert.assertTrue(infantBoardingPassFirstName.getText().equals(passengerName[1]), "first name of infant name in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant first name in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant first name in boarding pass for flight-" + flightNo + " is not matching Expected - "+passengerName[1]+" Actual - "+infantBoardingPassFirstName.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantBoardingPassLastName.getText().equals(passengerName[2]), "last name of infant name in boarding pass not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant last name in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant last name in boarding pass for flight-" + flightNo + " is not matching Expected - "+passengerName[2]+" Actual - "+infantBoardingPassLastName.getText());
            throw e;
        }
    }

    private void verifySeatForInfantInBoardingPass(int passengerNo,int flightNo){
        //verifies seat in infant boarding pass
        String seatNo = null;
        try {
            seatNo = "INF";
            Assert.assertTrue(infantBoardingPassSeat.getText().equals(seatNo), "seat no. for in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, "seat no. for infant-"+passengerNo+" in boarding pass for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "seat no. for infant-"+passengerNo+" in boarding pass for flight-" + flightNo + " is not matching Expected - "+seatNo+" Actual - "+infantBoardingPassSeat.getText());
            throw e;
        }
    }

    private void verifyTravelDateForInfantInBoardingPass(Hashtable<String,String>testData,String tripType, int flightNo,int passengerNo){
        //verifies travel date in infant boarding pass


        String expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("departDateOffset")), "dd MMM yyyy");

        if(tripType.equals("inbound")) {
            expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("returnDateOffset")), "dd MMM yyyy");
        }
        try {
            Assert.assertTrue(infantBoardingPassTravelDate.getText().equals(expectedTravelDate), "travel date in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight travel date in boarding pass of infant:" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight travel date in boarding pass of infant:" + (passengerNo) + " is not matching Expected - "+expectedTravelDate+" Actual - "+infantBoardingPassTravelDate.getText());
            throw e;
        }
    }

    private void verifyBookingRefNoForInfantInBoardingPass(String bookingRefNo,int passengerNo,int flightNo){
        //verifying the booking ref no. in infant boarding pass

        try {
            Assert.assertTrue(infantBoardingPassBookingRefNo.getText().equals(bookingRefNo), "Booking Ref No. in infant boarding pass not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in boarding Pass of infant"+(passengerNo)+" for flight-"+flightNo+" is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in boarding Pass of infant-"+(passengerNo)+" for flight-"+flightNo+" is not matching Expected - "+bookingRefNo+" Actual - "+infantBoardingPassBookingRefNo.getText());
            throw e;
        }
    }


    private void verifyFlightDetailsForInfantInTearOffSlip(Hashtable<String,String>selectedFlightDetails,int flightNo,int passengerNo){
        //verifies flight details in infant tear-off slip
        try {
            Assert.assertTrue(infantTearOffSlipFlightNo.getText().equals(selectedFlightDetails.get(("flightNo" + flightNo))), "flight no. in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight no. in tear-off slip of infant-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight no. in tear-off slip of infant-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightNo" + flightNo))+" Actual - "+infantTearOffSlipFlightNo.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantTearOffSlipSource.getText().equals(selectedFlightDetails.get(("flightSource" + flightNo))), "flight source in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight source in tear-off slip of infant-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight source in tear-off slip of infant-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightSource" + flightNo))+" Actual - "+infantTearOffSlipSource.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantTearOffSlipDestination.getText().equals(selectedFlightDetails.get(("flightDestination" + flightNo))), "flight destination in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight destination in tear-off slip of infant-" + (passengerNo) + " is matching");
        }catch (AssertionError e) {
            reportLogger.log(LogStatus.WARNING, flightNo + "-flight destination in tear-off slip of infant-" + (passengerNo) + " is not matching Expected - "+selectedFlightDetails.get(("flightDestination" + flightNo))+" Actual - "+infantTearOffSlipDestination.getText());
            throw e;
        }

    }

    private void verifyPassengerNameForInfantInTearOffSlip(Hashtable<String,String>passengerNames,int passengerNo,int flightNo){
        //verifies passenger name in infant tear-off slip
        String[] passengerName =  passengerNames.get(("infantName"+passengerNo)).split(" ");
        try {
            Assert.assertTrue(passengerName[0].contains(infantTearOffSlipSalutation.getText()), "salutation of passenger name in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant salutation in tear-off slip  for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant salutation in tear-off slip  for flight-" + flightNo + " is not matching "+infantTearOffSlipSalutation.getText()+" is not present in "+passengerName[0]);
            throw e;
        }

        try {
            Assert.assertTrue(infantTearOffSlipFirstName.getText().equals(passengerName[1]), "first name of passenger name in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant first name in tear-off slip  for flight-" + (flightNo + " is matching"));
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant first name in tear-off slip  for flight-" + flightNo + " is not matching Expected - "+passengerName[1]+" Actual - "+infantTearOffSlipFirstName.getText());
            throw e;
        }

        try {
            Assert.assertTrue(infantTearOffSlipLastName.getText().equals(passengerName[2]), "first name of passenger name in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, passengerNo+"-infant last name in tear-off slip  for flight-" + (flightNo + " is matching"));
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, passengerNo+"-infant last name in tear-off slip s for flight-" + flightNo + " is not matching Expected - "+passengerName[2]+ " Actual - "+infantTearOffSlipLastName.getText());
            throw e;
        }
    }

    private void verifySeatForInfantInTearOffSlip(int passengerNo,int flightNo){
        // verifies seat in infant tear-off slip
        String seatNo = null;
        try {
            seatNo = "INF";
            Assert.assertTrue(infantTearOffSlipSeat.getText().equals(seatNo), "seat no. for in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, "seat no. for infant-"+passengerNo+" in tear-off slip  for flight-" + flightNo + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "seat no. for infant-"+passengerNo+" in tear-off slip  for flight-" + flightNo + " is not matching Expected - "+seatNo+" Actual - "+infantTearOffSlipSeat.getText());
            throw e;
        }
    }

    private void verifyTravelDateForInfantInTearOffSlip(Hashtable<String, String>testData,String tripType,int passengerNo, int flightNo){
        //verifies travel date in infant tear-off slip

        String expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("departDateOffset")), "dd MMM yyyy");

        if(tripType.equals("inbound")) {
            expectedTravelDate = actionUtility.getDateAsString(Integer.parseInt(testData.get("returnDateOffset")), "dd MMM yyyy");
        }
        try {
            Assert.assertTrue(infantTearOffSlipTravelDate.getText().equals(expectedTravelDate), "travel date in infant tear-off slip not matching");
            reportLogger.log(LogStatus.INFO, flightNo+"-flight travel date in tear-off slip of infant-" + (passengerNo) + " is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, flightNo+"-flight travel date in tear-off slip of infant-" + (passengerNo) + " is not matching Expected - "+expectedTravelDate+" Actual - "+infantTearOffSlipTravelDate.getText());
            throw e;
        }

    }

    private void verifyBookingRefNoForInfantInTearOffSlip(String bookingRefNo,int passengerNo, int flightNo){
        //verifies booking ref. in infant tear-off slip
        try {
            Assert.assertTrue(infantTearOffSlipBookingRefNo.getText().equals(bookingRefNo), "Booking Ref No. in infant tear-off Slip not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in tear-off slip of infant-"+(passengerNo)+" for flight-"+flightNo+" is matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in tear-off slip of infant-"+(passengerNo)+" for flight-"+flightNo+" is not matching Expected - "+bookingRefNo+" Actual - "+infantTearOffSlipBookingRefNo.getText());
            throw e;
        }
    }


    private void verifyBaggage(Hashtable<String,String> testData,boolean baggageEditFlag, int flightNo, int passengerNo){
        //verifies the baggage
        String baggage;

        if (baggageEditFlag) {
            baggage = testData.get(("editedPassengerBag" + passengerNo));

        } else {
            baggage = testData.get(("passengerBag" + passengerNo));
        }


        if (baggage != null) {
            try {

                if (baggage.toLowerCase().equals("0kg")) {
                    Assert.assertTrue(noHoldBaggage.getText().toLowerCase().equals("no"), "hold baggage is not matching");
                    reportLogger.log(LogStatus.INFO, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is matching");

                } else {
                    Assert.assertTrue(holdBaggage.getText().toLowerCase().substring(0, 4).equals(baggage.toLowerCase()), "hold baggage is not matching");
                    reportLogger.log(LogStatus.INFO, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is matching");
                }


            } catch (NoSuchElementException e) {

                try {

                    if (baggage.toLowerCase().equals("0kg")) {
                        reportLogger.log(LogStatus.WARNING, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is not matching, Expected - " + baggage.toLowerCase() + " Actual - " + holdBaggage.getText().toLowerCase());

                    } else {
                        reportLogger.log(LogStatus.WARNING, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is not matching, Expected - " + baggage.toLowerCase() + " Actual - " + noHoldBaggage.getText().toLowerCase().substring(0, 4));

                    }
                } catch (NoSuchElementException ne) {
                    reportLogger.log(LogStatus.WARNING, "unable to verify hold baggage");
                    throw ne;
                }
                throw e;

            } catch (AssertionError e) {

                if (baggage.toLowerCase().equals("0kg")) {
                    reportLogger.log(LogStatus.INFO, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is not matching, Expected - " + baggage.toLowerCase() + " Actual - " + noHoldBaggage.getText().toLowerCase());

                } else {
                    reportLogger.log(LogStatus.INFO, "hold baggage for passenger-" + (passengerNo) + " for flight-" + flightNo + " is not matching, Expected - " + baggage.toLowerCase() + " Actual - " + holdBaggage.getText().toLowerCase().substring(0, 4));
                }
                throw e;

            } catch (Exception e) {
                reportLogger.log(LogStatus.INFO, "unable to verify hold baggage");
                throw e;

            }
        } else {
            reportLogger.log(LogStatus.INFO, "hold baggage are not verified");
        }

    }




    private String getTimeDifference(String departureTime, String boardingTime){
        // returns departureTime-boardingTime
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        String timeDiff = null;
        try {
            Date departTime = parser.parse(departureTime);
            Date boardTime = parser.parse(boardingTime);
            long timeDifference  =(departTime.getTime() - boardTime.getTime());
            timeDiff = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeDifference), TimeUnit.MILLISECONDS.toMinutes(timeDifference) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDifference)));

        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException ("unable to subtract the time");
        }
        return timeDiff;
    }

//    End of boarding pass methods

    public void verifyGenderForPassenger(String[] genders){
//        Verifies the gender for the passenger in the check in page
        try{
            actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.NAMEORID,"app-frame");
            for(int i=0; i<genders.length; i++){
                if(genders[i].toLowerCase().equals("male")) {
                    try {
                        Assert.assertTrue(males.get(i).isSelected());
                        reportLogger.log(LogStatus.INFO, "successfully verified the gender of the passenger " + genders[i]);
                    }catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "gender of the passenger is not matching: Excepted gender - " + genders[i] + " Actual gender - female");
                        throw e;
                    }catch(Exception e){
                        reportLogger.log(LogStatus.INFO,"unable to verify the gender");
                        throw e;
                    }
                }
                else if(genders[i].toLowerCase().equals("female")){
                    try{
                        Assert.assertTrue(females.get(i).isSelected());
                        reportLogger.log(LogStatus.INFO,"successfully verified the gender of the passenger "+genders[i]);
                    }catch(AssertionError e){
                        reportLogger.log(LogStatus.INFO, "gender of the passenger is not matching: Excepted gender - " + genders[i] + " Actual gender - male");
                        throw e;
                    }catch(Exception e){
                        reportLogger.log(LogStatus.INFO,"unable to verify the gender");
                        throw e;
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to switch to frame");
        }
    }

    public void verifyAddApiInfoIsDisplayed(boolean flag) {
        // verifies the addApiInfo option is displayed
        try{
            Assert.assertTrue(addAPIs.get(0).isDisplayed(),"add API option is not displayed");
            if (flag) {
                reportLogger.log(LogStatus.INFO, "add API option is displayed");
            }else {
                reportLogger.log(LogStatus.WARNING,"add API option is displayed");
                throw new RuntimeException("API option is displayed");
            }
        }catch (NullPointerException e){
            if(flag) {
                reportLogger.log(LogStatus.WARNING, "add API option is not displayed");
                throw e;
            }else {
                reportLogger.log(LogStatus.INFO,"add API option is not displayed displayed");
            }
        }
    }

    public void verifyCheckInPageFooter(){
        try{
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            String fPara2 = "All fares quoted on Flybe.com are subject to availability, inclusive of taxes and charges.";

            String fPara3 = "For more information on our ancillary charges Click here and our pricing guide Click here.";

            try{
                actionUtility.switchBackToWindowFromFrame();
            }catch (TimeoutException e){
                reportLogger.log(LogStatus.WARNING,"unable to switch to main check in window");
                throw e;
            }

            actionUtility.waitForElementVisible(flybeLogo,20);
            try{
                Assert.assertTrue(flybeLogo.isDisplayed(),"Flybe Logo is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Flybe Logo is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Flybe Logo is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Flybe Logo is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(flybeCopyRight.isDisplayed(),"Copy Right image is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Copy Right image is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Copy Right image is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Copy Right image is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(privacyPolicyLink.isDisplayed(),"Privacy Policy Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Privacy Policy Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Privacy Policy Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Privacy Policy Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(cookiePolicyLink.isDisplayed(),"Cookie Policy Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Cookie Policy Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Cookie Policy Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Cookie Policy Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(JouneryComparisionTimesLink.isDisplayed(),"Jounery Comparision Times Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Jounery Comparision Times Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Jounery Comparision Times Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Jounery Comparision Times Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(ContactUsLink.isDisplayed(),"Contact Us Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Contact Us Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Contact Us Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Contact Us Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(flybeInFooter.isDisplayed(),"Flybe.com Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Flybe.com Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Flybe.com Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Flybe.com Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(ancillaryCharges.isDisplayed(),"Ancillary Charges Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Ancillary Charges Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Ancillary Charges Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Ancillary Charges Link is not displayed in check in page");
                throw e;
            }

            try{
                Assert.assertTrue(priceGuide.isDisplayed(),"Pricing Guide Link is not displayed in check in page");
                reportLogger.log(LogStatus.INFO,"Pricing Guide Link is displayed in check in page");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.WARNING,"Pricing Guide Link is not displayed in check in page");
                throw e;
            }catch (NoSuchElementException e){
                reportLogger.log(LogStatus.WARNING,"Pricing Guide Link is not displayed in check in page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching  in check in page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in check in page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching  in check in page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in check in page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching  in check in page");
                reportLogger.log(LogStatus.INFO, "Footer para is matching  in check in page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching  in check in page");
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch (NoSuchElementException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to verify check in page footer");
            throw e;
        }

    }

    public void verifyTheVoucherSectionForLoganAirFlight(){
        try{

            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(boardingPassVoucher),"vouchers section is displayed in boarding pass for Logan air flight");
            reportLogger.log(LogStatus.INFO,"vouchers section is not displayed in boarding pass for Logan air flight");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Vouchers section is displayed in boarding pass for Logan air flight");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to verify vouchers section in boarding pass for Logan air flight");
            throw e;
        }
    }

    // change the xpath to other locators to all cases
    public void verifyTheCheckInLinkForAirFrance(){
        // to verify the check in link for air france flight in check-in page
        try{
            for (int i = 0; i< checkInLinksForAirFrance.size(); i++){
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(checkInLinksForAirFrance.get(i)),"Check-In link is not displayed for air france flight");
            }


            reportLogger.log(LogStatus.INFO,"Check-In link is displayed for air france flight");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Check-In link is not displayed for air france flight");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to verify if Check-In link is displayed for air france flight");
            throw e;
        }
    }

}
