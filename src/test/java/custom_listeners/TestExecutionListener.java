package custom_listeners;

import com.relevantcodes.extentreports.LogStatus;
import ui_automator.Automator;
import utilities.*;
import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static utilities.ReportUtility.reportLog;

public class TestExecutionListener implements IInvokedMethodListener {
    //
    static String timeTakenForTCExecution;
    static LocalDateTime startTimeofTCExecution;
    static LocalDateTime endTimeofTCExecution;
    String startTimeStamp;
    String endTimeStamp;



    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

        //Setting the driver and report logger in the TestManager
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test"+"- entry");
        if (method.isTestMethod()) {
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test - "+method.getTestMethod().getMethodName());
            String browserName = method.getTestMethod().getXmlTest().getLocalParameters().get("browserName");
            String environment = method.getTestMethod().getXmlTest().getLocalParameters().get("environment");
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test - "+method.getTestMethod().getMethodName()+" browser name:"+browserName+" environment:"+environment);
            WebDriver driver = DriverFactory.getDriver(browserName,environment);

            if (driver!=null) {
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+driver.hashCode());
                TestManager.setWebDriver(driver);
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+driver.hashCode()+" is instigated in test manager");
            }else {
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test - "+method.getTestMethod().getMethodName()+" unable to produce driver");
                throw new RuntimeException("Unable to produce the driver");
            }

            TestManager.setTestEnvironment(environment);
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation for test "+method.getTestMethod().getMethodName()+" - environment:"+environment+" is instigated in test manager");
            if(TestManager.getReportLogger()== null){
                TestManager.setReportLogger(reportLog.startTest(method.getTestMethod().getMethodName()));
            }
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            startTimeStamp = dtf.format(now);
            System.out.println("startTimeStamp "+startTimeStamp);
            startTimeofTCExecution = LocalDateTime.parse(startTimeStamp, dtf);
            System.out.println("startTimeofTCExecution "+startTimeofTCExecution);
        }
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:before invocation - exit");
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

        //log the status of each test and takes the screenShot in case of failure,
        // call the tearDown process after each test

        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" entry");

        if (method.isTestMethod()) {
            try {
                ZonedDateTime currentTime = ZonedDateTime.now();
                String formattedTime = currentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyy_hh_mm_ss"));
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" calculated the log time - "+formattedTime);
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                endTimeStamp = dtf.format(now);
                endTimeofTCExecution = LocalDateTime.parse(endTimeStamp, dtf);
                System.out.println("endTimeofTCExecution "+endTimeofTCExecution);
                timeTakenForTCExecution = ChronoUnit.SECONDS.between(startTimeofTCExecution, endTimeofTCExecution)+"";
                String suiteName[] =  method.getTestMethod().getMethodName().split("_");
                String testEnvironment = TestManager.getTestEnvironment();
                if (testResult.getStatus() == 1) {
                    try {
                        TestManager.getReportLogger().log(LogStatus.PASS, testResult.getName() + "**PASSED***");
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId() + " Test Execution Listener:after invocation for test - " + method.getTestMethod().getMethodName() +" driver:"+TestManager.getDriver().hashCode()+ " logged result as PASS");
                        Automator.incrementProgressBar();
                        if(DataUtility.readConfig("DB.write").equals("yes")) {
                            DBUtility.executeInsertToTestCases(suiteName[0], testEnvironment, "pass");
                        }
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" report tear down after PASS");
                    }catch (Exception e){
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" unable to log result as PASS");
                    }finally {
                        reportLog.endTest(TestManager.getReportLogger());
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" end of the report test log after PASS");
                        reportLog.flush();
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" flushed the logs after PASS");
                        TestManager.tearDownReport();
                        /*Date date=new Date();
                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate=dateFormat.format(date);*/
                        ReportUtility.writeExcel(suiteName[0],testEnvironment,startTimeStamp,"pass",endTimeStamp,timeTakenForTCExecution);
                    }
                } else if (testResult.getStatus() == 2) {
                    boolean isScreenCaptured  = false;
                    try {

                        if (TestManager.getDriver() != null) {
                            ActionUtility.takeScreenShot(testResult.getMethod().getMethodName(), formattedTime);
                            isScreenCaptured = true;
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" screen captured after FAIL");

                        } else {
                            TestManager.getReportLogger().log(LogStatus.INFO, "unable to capture screen shot since driver was not available");
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" driver not available to capture screen after FAIL");
                        }
                        Automator.incrementProgressBar();
                        Automator.incrementFailedCasesCount();
                        if(DataUtility.readConfig("DB.write").equals("yes")) {
                            DBUtility.executeInsertToTestCases(suiteName[0], testEnvironment, "fail");
                        }
                    }catch (Exception e){
                        TestManager.getReportLogger().log(LogStatus.INFO, "unable to capture screen shot");
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" ERROR while capturing the screen after FAIL");
//
                    }finally {
                        TestManager.getReportLogger().log(LogStatus.FAIL, testResult.getName() + "**FAILED***");
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" logged result as FAIL");
                        TestManager.getReportLogger().log(LogStatus.INFO, testResult.getThrowable());
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" logged stack trace after FAIL");
                        if (isScreenCaptured) {
                            TestManager.getReportLogger().log(LogStatus.INFO, "Snapshot below: ", TestManager.getReportLogger().addScreenCapture("screenshots\\" + testResult.getMethod().getMethodName() + "_" + formattedTime + ".png"));
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId() + " Test Execution Listener:after invocation for test - " + method.getTestMethod().getMethodName() +" driver:"+TestManager.getDriver().hashCode()+ " logged screenshot after FAIL");
                        }
                        reportLog.endTest(TestManager.getReportLogger());
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" end of the report test log after FAIL");
                        reportLog.flush();
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" flushed the logs after FAIL");
                        TestManager.tearDownReport();
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" report tear down after FAIL");
                        /*Date date=new Date();
                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate=dateFormat.format(date);*/
                        ReportUtility.writeExcel(suiteName[0],testEnvironment,startTimeStamp,"fail",endTimeStamp,timeTakenForTCExecution);
                    }

                } else if (testResult.getStatus() == 3) {
                    boolean isScreenCaptured  = false;
                    try {
                        if (TestManager.getDriver() != null) {
                            ActionUtility.takeScreenShot(testResult.getMethod().getMethodName(), formattedTime);
                            isScreenCaptured = true;
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" screen captured after SKIP");
                        } else {
                            TestManager.getReportLogger().log(LogStatus.INFO, "unable to capture screen shot since driver was not available");
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" driver not available to capture screen after SKIP");
                        }
                    }catch (Exception e){
                        TestManager.getReportLogger().log(LogStatus.INFO, "unable to capture screen shot");
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" ERROR while capturing the screen after SKIP");
//                        TestManager.getReportLogger().log(LogStatus.INFO, e.getStackTrace().toString());

                    }finally {
                        TestManager.getReportLogger().log(LogStatus.INFO, testResult.getThrowable());
                        if (isScreenCaptured) {
                            TestManager.getReportLogger().log(LogStatus.INFO, "Snapshot below: ", TestManager.getReportLogger().addScreenCapture("screenshots\\" + testResult.getMethod().getMethodName() + "_" + formattedTime + ".png"));
                            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId() + " Test Execution Listener:after invocation for test - " + method.getTestMethod().getMethodName() + " driver:"+TestManager.getDriver().hashCode()+" logged screenshot after SKIP");
                        }
                        reportLog.flush();
                        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" driver:"+TestManager.getDriver().hashCode()+" flushed the logs after SKIP");
                    }

                }
            }finally {

                if ((TestManager.getDriver() != null))  {

                    TestManager.tearDown();
                    ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" - driver tear down FINALLY ");
                }
            }
        }
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Execution Listener:after invocation for test - "+method.getTestMethod().getMethodName()+" exit");
    }

}
