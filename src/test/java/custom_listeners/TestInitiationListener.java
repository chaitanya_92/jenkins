package custom_listeners;


import org.testng.IExecutionListener;
import ui_automator.Automator;
import utilities.DataUtility;
import utilities.ReportUtility;
import utilities.zaco;


public class TestInitiationListener implements IExecutionListener {

    @Override
    public void onExecutionStart() {
        //Calling the methods that deletes old and initiate the new reports at the start of execution
//        ReportUtility.deleteOldReports();
        ReportUtility.initiateReportLog();
        ReportUtility.reportLog.setTestRunnerOutput("Project: "+DataUtility.readConfig("project"));
        ReportUtility.reportLog.setTestRunnerOutput("Release: "+DataUtility.readConfig("project.release"));
        ReportUtility.reportLog.setTestRunnerOutput("flow: "+ DataUtility.readConfig("flow"));
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution start - exit");
        ReportUtility.setXLFilepathAndCreateXL();
    }

    @Override
    public void onExecutionFinish() {
        //Calling the method that writes the result to the HTML file at the end of the execution
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution finish - entry");
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution finish - flushing the report");
        ReportUtility.flushReprotLog();
        if(ReportUtility.reportLog!=null){
            ReportUtility.reportLog=null;
        }
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        zaco.zipAndMail();
//        Automator.afterExcecution();
    }
}
