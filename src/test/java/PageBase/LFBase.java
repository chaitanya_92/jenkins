package PageBase;


import leapfrog.business_logics.*;

public class LFBase extends BasePage{

    public LFFareSelect fareSelect = new LFFareSelect();
    public  LFYourDetails  lfYourDetails = new LFYourDetails();
    public LFPayment lfPayment = new LFPayment();
    public LFCarHire lfCarHire = new LFCarHire();
    public LFCheckIn leapCheckIn = new LFCheckIn();
    public LFSelectPassengerForCheckIn selectPassengerForCheckIn = new LFSelectPassengerForCheckIn();
    public FlightArrivalAndDeparture flightArrivalAndDeparture = new FlightArrivalAndDeparture();
    public MyAccountMyPaymentCards accountMyPaymentCards = new MyAccountMyPaymentCards();







}
