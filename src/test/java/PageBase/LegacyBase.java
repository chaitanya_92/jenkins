package PageBase;

import leapfrog.business_logics.*;

public class LegacyBase extends BasePage{


    public LeapFareSelect fareSelect = new LeapFareSelect();
    public LeapPayment payment = new LeapPayment();
    public LeapYourDetails yourDetails = new LeapYourDetails();
    public LeapCarHire carHire = new LeapCarHire();
    public LFCheckIn leapCheckIn = new LFCheckIn();
    public LFSelectPassengerForCheckIn selectPassengerForCheckIn = new LFSelectPassengerForCheckIn();
    public LeapPriceLockDown leapPLD = new LeapPriceLockDown();


}


