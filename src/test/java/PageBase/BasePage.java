package PageBase;

import leapfrog.business_logics.*;
import legacy.business_logics.*;

public class BasePage {

    public LeapCheapFlight cheapFlight = new LeapCheapFlight();
//    public LeapYourDetails yourDetails = new LeapYourDetails();
    public LeapLogin login = new LeapLogin();
    public MyAccountHome myAccountHome = new MyAccountHome();
//    public LeapPayment payment = new LeapPayment();
    public LeapBookingConfirmation bookingConfirmation = new LeapBookingConfirmation();
    public BoardingPass boardingPass = new BoardingPass();
    public LeapAddAPI addAPI = new LeapAddAPI();
//    public LeapCarHire carHire = new LeapCarHire();
    public ChangeYourFlight changeYourFlight = new ChangeYourFlight();
    public LeapChooseExtras chooseExtras = new LeapChooseExtras();
    public LeapManageBooking manageBooking = new LeapManageBooking();
    public LeapChangeBooking changeBooking = new LeapChangeBooking();
    public SeatOpener seatOpener = new SeatOpener();
    public MiniConfirmation miniConfirmation = new MiniConfirmation();
    public CheckIn checkIn = new CheckIn();
    public Support support = new Support();
    public LeapPLDConfirmation leapPLDConfirmation = new LeapPLDConfirmation();
    public LeapPriceLockDown leapPLD = new LeapPriceLockDown();


}
