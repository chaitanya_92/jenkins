package PageBase;

import leapfrog.business_logics.*;

/**
 * Created by STejas on 6/6/2017.
 */
public class CMSBase {
    public CMSHome cheapFlight = new CMSHome();
    public CMSBookingConfirmation bookingConfirmation = new CMSBookingConfirmation();
    public CMSLogin login = new CMSLogin();
    public CMSMyAccountHome myAccountHome = new CMSMyAccountHome();
    public CMSCheckIn leapCheckIn = new CMSCheckIn();
    public CMSManageBooking manageBooking = new CMSManageBooking();
    public CMSChangeBooking changeBooking = new CMSChangeBooking();
    public CMSChangeYourFlight changeYourFlight = new CMSChangeYourFlight();
    public CMSChooseExtra chooseExtras = new CMSChooseExtra();
    public CMSSelectPassengerForCheckin selectPassengerForCheckIn = new CMSSelectPassengerForCheckin();
    public CMSAddAPI addAPI = new CMSAddAPI();
    public CMSSeatOpener seatOpener = new CMSSeatOpener();
    public CMSMyAccountMyPaymentCards accountMyPaymentCards = new CMSMyAccountMyPaymentCards();
    public CMSArrivalAndDeparture flightArrivalAndDeparture = new CMSArrivalAndDeparture();
    public CMSSupport support = new CMSSupport();
    public CMSPriceLockDown leapPLD = new CMSPriceLockDown();
    public CMSPLDConfirmation leapPLDConfirmation = new CMSPLDConfirmation();
}
