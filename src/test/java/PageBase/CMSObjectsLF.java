package PageBase;

import leapfrog.business_logics.CMSLFExtras;
import leapfrog.business_logics.CMSLFFareSelect;
import leapfrog.business_logics.CMSLFPayment;
import leapfrog.business_logics.CMSLFYourDetails;

/**
 * Created by STejas on 6/6/2017.
 */
public class CMSObjectsLF extends CMSBase{

    //-------------LF flow Page Objects------------------------

    public CMSLFFareSelect fareSelect = new CMSLFFareSelect();
    public CMSLFYourDetails lfYourDetails = new CMSLFYourDetails();
    public CMSLFExtras lfCarHire = new CMSLFExtras();
    public CMSLFPayment lfPayment = new CMSLFPayment();
}
