package PageBase;

import leapfrog.business_logics.*;

/**
 * Created by STejas on 6/5/2017.
 */
public class CMSObjects extends CMSBase {


    //-----------------Cam flow Page Objects--------------------------------------------

    public CMSFareSelect fareSelect = new CMSFareSelect();
    public CMSYourDetails yourDetails = new CMSYourDetails();
    public CMSExtra carHire = new CMSExtra();
    public CMSPayment payment = new CMSPayment();


}
