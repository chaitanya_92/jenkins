package utilities;


import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtility {


    public static double convertStringToDouble(String value){
        // will convertStringToDouble
        try {
            return Double.parseDouble(getDoubleValueFromString(value));
        }catch (NumberFormatException e){
            return 0.0;
        }catch (NullPointerException e) {
            return 0.0;
        }
    }

    private static String getDoubleValueFromString(String value){
        // reads the decimal pattren from string and converts it double and return
        Pattern regex = Pattern.compile("(\\d+(?:\\.\\d+)?)");
        Matcher matcher = regex.matcher(value);
        String doubleVal = null;
        while(matcher.find()) {
            doubleVal = matcher.group(1);
        }
            return doubleVal;
    }

    public static String convertDateFormat(String presentFormat, String requiredFormat, String dateToConvert){
//        this converts the present format to required format
        DateFormat originalFormat = new SimpleDateFormat(presentFormat, Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(requiredFormat);
        Date date = null;
        try {
            date = originalFormat.parse(dateToConvert);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }
}
