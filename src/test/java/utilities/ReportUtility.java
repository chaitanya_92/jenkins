package utilities;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.NetworkMode;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.xpath.operations.Bool;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ReportUtility {
    public static ExtentReports reportLog = null;
    static String formattedTime;
    public static String reportLocation;
    static SXSSFWorkbook  wb ;
    static Sheet sh ;
    static int rowCount=1;

    public static synchronized void initiateReportLog() {
        // creates the report instance
        ZonedDateTime currentTime = ZonedDateTime.now();
        formattedTime = currentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyy_hh_mm_ss"));


        String reportLocation = DataUtility.readConfig("report.location");

//        System.out.println(reportLocation);

        String reportPath ="..execution_reports\\reports";
        if (reportLocation != null){
            reportPath = reportLocation+"\\reports";
        }

        ReportUtility.reportLocation = reportPath+"\\report_for_"+formattedTime;

        String filepath = ReportUtility.reportLocation+"\\flybe_automation_report"+formattedTime+".html";
        Boolean replaceExisting = Boolean.TRUE;
        if (reportLog == null) {
            ReportUtility.reportLog = new ExtentReports(filepath, replaceExisting, DisplayOrder.OLDEST_FIRST, NetworkMode.ONLINE);

            URI configUri = null;
            try {
                configUri = ReportUtility.class.getClassLoader().getResource(DataUtility.readConfig("report.config.name")).toURI();
                ReportUtility.reportLog.loadConfig(configUri.toURL());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            ReportUtility.reportLog.setTestRunnerOutput("Report Utility:initiate report log - report initiated");

        }
        return;
    }

    public static synchronized void flushReprotLog(){
        // writes the test case result to HTML file
        if (ReportUtility.reportLog != null) {
            ReportUtility.reportLog.close();
        }
        return;
    }


    public static synchronized void deleteOldReports(){
        // delete the old reports
        String reportLocation = "src\\reports";
        try {
            FileUtils.forceDelete((new File(reportLocation)));
        } catch (Exception e) {
            System.out.println("--- No Reports folder found,that needs to be deleted ---");
        }
    }

    public static void clearExcel(){
        try {
            File file = new File("");
            FileInputStream inputStream = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheet("Sheet1");
            for(int i=1;i<sheet.getPhysicalNumberOfRows();i++){
                sheet.removeRow(sheet.getRow(i));
            }
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            outputStream.close();
        }catch(FileNotFoundException e){
            System.out.println("file is not present");
        }catch(IOException e){
            System.out.println("cannot find file");
        }
    }


    //Write test report
    public static void writeExcel(String... dataToWrite){

        try{
            String filepath = ReportUtility.reportLocation+"\\flybe_automation_report"+formattedTime+".xlsx";
            System.out.println(filepath);
            File file =    new File(filepath);
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook wb_template = new XSSFWorkbook(inputStream);
            Workbook wkbk = null;
            wkbk = new SXSSFWorkbook(wb_template,100);
            Sheet sheet = (SXSSFSheet) wkbk.getSheet("Sheet1");
            Row row = sheet.createRow(ReportUtility.rowCount);
            for(int cellnum = 0; cellnum <= 7; cellnum++){
                Cell cell = row.createCell(cellnum);
                if (cellnum == 0) {
                    cell.setCellValue(dataToWrite[0]);
                } else if (cellnum == 1) {
                    if(dataToWrite[0].contains("R")) {
                        cell.setCellValue("Regression");
                    }
                    else{cell.setCellValue("Smoke");}
                } else if (cellnum == 2) {
                    cell.setCellValue(dataToWrite[1]);
                } else if (cellnum == 3) {
                    cell.setCellValue(dataToWrite[2]);
                } else if (cellnum == 4) {
                    cell.setCellValue(dataToWrite[3]);
                } else if (cellnum == 5) {
                    cell.setCellValue("");
                }else if (cellnum == 6) {
                    cell.setCellValue(dataToWrite[4]);
                }else if (cellnum == 7) {
                    cell.setCellValue(dataToWrite[5]);
                }
            }
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(file);
            wkbk.write(outputStream);
            outputStream.close();
            ReportUtility.rowCount++;
        }catch(FileNotFoundException e){
            System.out.println("file is not present");
        }catch(IOException e){
            System.out.println("cannot find file");
        }
    }


    public static synchronized void setXLFilepathAndCreateXL(String... dataToWrite){//create excel file
        try {
             String filepath = ReportUtility.reportLocation+"\\flybe_automation_report"+formattedTime+".xlsx";
            wb =  new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
            sh = wb.createSheet("Sheet1");
                    Row row = sh.createRow(0);
                    for (int cellnum = 0; cellnum <= 7; cellnum++) {
                            Cell cell = row.createCell(cellnum);
                            if (cellnum == 0) {
                                cell.setCellValue("ID");
                            } else if (cellnum == 1) {
                                cell.setCellValue("Test_Suite");
                            } else if (cellnum == 2) {
                                cell.setCellValue("Environment");
                            } else if (cellnum == 3) {
                                cell.setCellValue("Date_of_Execution");
                            } else if (cellnum == 4) {
                                cell.setCellValue("Result");
                            } else if (cellnum == 5) {
                                cell.setCellValue("Failure_Reason");
                            } else if (cellnum == 6) {
                                cell.setCellValue("EndTime_Of_Execution");
                            }else if (cellnum == 7) {
                                cell.setCellValue("Time_Duration_Of_Execution");
                            }
                        }
            File f = new File(filepath);
            if(!f.exists()) {
                try{
                    f.createNewFile();
                }catch(IOException e){
                    System.out.println("cannot find filepath");
                }
                FileOutputStream out = new FileOutputStream(f,true);
                wb.write(out);
                out.close();
                // dispose of temporary files backing this workbook on disk
                wb.dispose();
                System.out.println("File is created");
            }
        }catch(FileNotFoundException e){
            System.out.println("cannot find file");
        }
        catch(IOException e){
            System.out.println("cannot find filepath");
        }
    }

}
