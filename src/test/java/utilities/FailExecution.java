package utilities;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.collections.Lists;

import java.util.List;

public class FailExecution implements Runnable{

    public TestNG testNG;

    public void reRunFailedCases(){
//        TestListenerAdapter tla = new TestListenerAdapter();
        testNG = new TestNG();
        List<String> suites = Lists.newArrayList();
        suites.add("test-output\\testng-failed.xml");
        testNG.setTestSuites(suites);
    }

    @Override
    public void run() {
        testNG.run();
    }
}
