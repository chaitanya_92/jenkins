package utilities;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class EmailUtility {

    private static String reportLocation ="..\\execution_report\\reports";;

    public static void getLatestReport() {
//  fetches the latest report

        String reportPath = DataUtility.readConfig("report.location");
        String compressedReportPath ="..\\execution_report\\compressedReports";

        if(reportPath!=null){
            EmailUtility.reportLocation=reportPath+"\\reports";
            compressedReportPath = reportPath+"\\compressedReports";

        }

        File dir = new File(reportLocation);
        File[] files = dir.listFiles();

        File lastModifiedFile = files[0];
        String lastModifiedFileName=lastModifiedFile.getName();

        for (int i = 0; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
                lastModifiedFileName= lastModifiedFile.getName();
            }
        }
        File compressedReportFile = new File(compressedReportPath);
        if(!compressedReportFile.exists()){
            compressedReportFile.mkdirs();
        }

        try {
            String targetPath =new File(compressedReportPath).getCanonicalPath()+"\\"+lastModifiedFileName+".zip";
            String destinationPath = new File(EmailUtility.reportLocation).getCanonicalPath()+"\\"+lastModifiedFileName;
            zaco.OUTPUT_ZIP_FILE = targetPath;
            zaco.SOURCE_FOLDER = destinationPath;
            MailSsl.attachmentLocation = targetPath;
            MailSsl.attachmentName = lastModifiedFileName+".txt";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ;

    }


}






