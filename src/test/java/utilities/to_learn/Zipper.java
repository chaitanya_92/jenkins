package utilities.to_learn;

import utilities.DataUtility;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper
{
//    static  String reportLocation;
//    static  String targetLocation;


    List<String> fileList;
    static  String OUTPUT_ZIP_FILE="D:\\Fly\\automation_5\\reports" ;


    static  String SOURCE_FOLDER ="D:\\Fly\\automation_5\\reports";

//    static String SOURCE_FOLDER ="..\\execution_report\\reports";
//    static String OUTPUT_ZIP_FILE = SOURCE_FOLDER+"\\compressedReports";
    static String fileName;


    Zipper(){
        fileList = new ArrayList<String>();
    }




    public static void main(String[] args) {
//        String latestFileName = EmailUtility.getLatestReport();
//        Zipper.fileName = latestFileName;
        String reportPath = DataUtility.readConfig("report.location");
//        if (reportPath!=null) {
//            Zipper.SOURCE_FOLDER = reportPath + "\\reports";
//            Zipper.OUTPUT_ZIP_FILE = reportPath + "\\compressedReports";
//        }


        System.out.println(Zipper.SOURCE_FOLDER);
        System.out.println(Zipper.OUTPUT_ZIP_FILE);

        File file = new File(OUTPUT_ZIP_FILE);

        if(!file.exists()){
            file.mkdirs();
        }



        Zipper.zipper();

    }


    public static void zipper()
    {


        Zipper appZip = new Zipper();
        appZip.generateFileList(new File(SOURCE_FOLDER+"\\"+fileName));
        appZip.zipIt(OUTPUT_ZIP_FILE+"\\"+fileName+".zip");
    }


    public void zipIt(String zipFile){

        byte[] buffer = new byte[1024];

        try{

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);

            for(String file : this.fileList){

                System.out.println("File Added : " + file);
                ZipEntry ze= new ZipEntry(file);
                zos.putNextEntry(ze);
                String srcFilePath = new File(SOURCE_FOLDER).getCanonicalPath();
                FileInputStream in =
                        new FileInputStream(SOURCE_FOLDER +"\\"+fileName+ File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            //remember close it
            zos.close();

            System.out.println("Done");
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     * @param node file or directory
     */
    public void generateFileList(File node){

        //add file only
        if(node.isFile()){

            try {
                System.out.println(node.getCanonicalPath());
                fileList.add(generateZipEntry(node.getCanonicalPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
                generateFileList(new File(node, filename));
            }
        }

    }

    /**
     * Format the file path for zip
     * @param file file path
     * @return Formatted file path
     */
    private String generateZipEntry(String file){
        System.out.println((SOURCE_FOLDER+"\\"+fileName).length()+1);
        System.out.println((SOURCE_FOLDER+"\\"+fileName));
        System.out.println(file.length());
        System.out.println(file.substring((SOURCE_FOLDER+"\\"+fileName).length()+1, file.length()));
        return file.substring((fileName).length()+1, file.length());
    }
}