package utilities;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.browserlaunchers.locators.InternetExplorerLocator;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ThreadGuard;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.lang.ref.PhantomReference;
import java.util.concurrent.TimeUnit;

public class DriverFactory {
    public static WebDriver getDriver(String browserName,String environment) {
        // creates the driver instance depending on the browserName passed

        WebDriver driver = null;
        String appUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(environment).get("appUrl").getAsString();
        DesiredCapabilities capability;
        String driverPath= ".\\pre_requisites\\drivers";
        String driverLocation = DataUtility.readConfig("driver.path");
        String PROXY = DataUtility.readConfig("network.proxy");

        org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();

        proxy.setHttpProxy(PROXY)
                .setFtpProxy(PROXY)
                .setSslProxy(PROXY);

        if (driverLocation !=null);{
            driverPath = driverLocation;


        }

        String composedDriverPath = null;

        if (browserName.toLowerCase().equals("ie")) {

            try {
                composedDriverPath = new File(driverPath+"\\IE\\32bit\\IEDriverServer.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }


            capability = DesiredCapabilities
                    .internetExplorer();
            capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//            capability.setCapability(
//                    InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
            capability.setCapability(CapabilityType.SUPPORTS_ALERTS, false);
            capability.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                    UnexpectedAlertBehaviour.DISMISS);
            capability.setCapability(
                    InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
            capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
            capability.setCapability(
                    InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
            capability.setCapability(
                    InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
            capability.setCapability("javascriptEnabled", true);
            capability.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, false);
            capability.setCapability("EnableNativeEvents", true);
            capability.setCapability(CapabilityType.BROWSER_NAME, "Internet explorer");
            capability.setCapability(CapabilityType.VERSION, "11");
            capability.setCapability(CapabilityType.PLATFORM, "WINDOWS");
            capability.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
            capability.setCapability("enablePersistentHover", false);
            System.setProperty("webdriver.ie.driver",composedDriverPath);


            if (PROXY!=null){
                capability.setCapability(CapabilityType.PROXY, proxy);
            }
            driver =  new InternetExplorerDriver(capability);
            driver.manage().deleteAllCookies();
//            driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
            driver.manage().window().maximize();

        } else if (browserName.toLowerCase().equals("firefox")) {
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("webdriver.load.strategy", "fast");

            profile.setPreference("startup.homepage_welcome_url.additional",  "about:blank");
            profile.setPreference("browser.private.browsing.autostart",true);

            capability = DesiredCapabilities.firefox();
            capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capability.setCapability(FirefoxDriver.PROFILE, profile);

            if (PROXY!=null){
                capability.setCapability(CapabilityType.PROXY, proxy);
            }

            driver =new FirefoxDriver(capability);
            driver.manage().window().maximize();


        } else if (browserName.toLowerCase().equals("chrome")) {

            try {
                composedDriverPath = new File(driverPath+"\\Chrome\\32bit\\chromedriver.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.setProperty("webdriver.chrome.driver",composedDriverPath);

            ChromeOptions options = new ChromeOptions();
            options.addArguments("incognito");
            options.addArguments("--start-maximized");
            options.addArguments("--dns-prefetch-disable");
            options.addArguments("--disable-print-preview");
            options.addArguments("--disable-extensions");
            capability = DesiredCapabilities.chrome();
            capability.setCapability(ChromeOptions.CAPABILITY, options);
            capability.setCapability("chrome.verbose", false);

            if (PROXY!=null){
                capability.setCapability(CapabilityType.PROXY, proxy);
            }

            driver = new ChromeDriver(capability);

        }else if (browserName.equals("fire")){

            ProfilesIni profile = new ProfilesIni();
            FirefoxProfile myprofile = profile.getProfile("seleniumProfile");
            driver = new FirefoxDriver(myprofile);
            driver.manage().window().maximize();
        }else if(browserName.equals("phantom")){
            capability = new DesiredCapabilities();
            capability.setJavascriptEnabled(true);
//            capability.setCapability("takesScreenShot",true);
            File file = new File("D:\\Fly\\automation_21\\seleniumautomation\\Flybe_Automation_Framework\\pre_requisites\\Drivers\\phantomjs_2_1_1_windows\\bin\\phantomjs.exe");
//            System.setProperty("phantomjs.binary.path", file.getAbsolutePath());

            capability.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,file.getAbsolutePath());
            capability.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

            driver = new PhantomJSDriver(capability);

        }

        try {
            driver.manage().deleteAllCookies();
            driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(110, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
            driver.get(appUrl);
            driver.manage().deleteAllCookies();
            if (driver != null) {
                if (driver.getTitle().toLowerCase().contains("certificate error")) {
                    driver.navigate().to("javascript:document.getElementById('overridelink').click()");
                }
            }
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" properties to the driver is set");
        }catch (Exception e){
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" exception raised while setting the properties to the driver");
            driver.close();
            driver.quit();
            driver=null;
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver cleared");
        }
        Reporter.log("Successfully launched the browser <br><p>");
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" generated");
        return driver;
    }
}

