package legacy.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.List;

public class MiniConfirmation {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public MiniConfirmation() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//tr[3]/td/a[contains(text(),'Check')]")
    private List<WebElement> outboundCheckInButtons;

    @FindBy(xpath = "//tr[4]/td/a[contains(text(),'Check')]")
    private List<WebElement> returnCheckInButtons;

    @FindBy(xpath = "//td/p")
    private WebElement checkInValidationMessage;

    @FindBy(xpath = "//div[@id='test']//table[@class='table'][1]//tr[3]//td[7]/descendant::a[1]")
    private WebElement addAPIButton;

    @FindBy(css = "span.camMiniConfAlign>a")
    private List<WebElement> changeAPIButtons;

    @FindBy(xpath = "//a[contains(text(),'Change flight')]")
    private List<WebElement> changeFlightButtons;

    @FindBy(xpath = "//a[contains(text(),'Reselect')]")
    private List<WebElement> reselectButtons;


    String checkInButtonLocator = "//div[@id='test']/table[%d]/descendant::tr[%d]/td[7]/a";
    String checkInValidationMessageLocator = "//div[@id='test']/table[%d]/descendant::tr[%d]/td[7]/p";


    public void changeFlight(){
        //clicks on change flight button
        try{
            actionUtility.hardClick(changeFlightButtons.get(0));
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to change flights");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to change flights");
            throw e;
        }
    }

    public void changeSeat(){
        //clicks on change flight button
        try{
            actionUtility.click(reselectButtons.get(0));
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully continued to change seats");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to change seats");
            throw e;
        }
    }

    public void verifyCheckInValidationMessageForEachPassenger(int noOfPassenger,int totalFlight,int totalInboundFlight){
//        Yet to be implemented
        int inboundFlightIndex = totalFlight-totalInboundFlight+3;
        String.format(checkInValidationMessageLocator,noOfPassenger,inboundFlightIndex);


    }

    public void waitAndCheckForTicketing(int timeFactor) {
        //checks if change flight button is enabled
        try {
            int count = 0;

            while (changeFlightButtons.size()== 0 && count <= timeFactor){
                actionUtility.hardSleep(30000);
                count++;
                TestManager.getDriver().navigate().refresh();
                actionUtility.waitForPageLoad(10);
            }
            if (changeFlightButtons.size()== 0 && count >timeFactor) {
                throw new RuntimeException("ticketing not done");
            }
            reportLogger.log(LogStatus.INFO,"ticketing is done");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"ticketing is not done");
            throw e;
        }
    }

    public void waitAndCheckForCheckIn(int timeFactor) {
        //checks if check-in button is enabled
        try {
            int count = 0;

            while (outboundCheckInButtons.size()== 0 && count <= timeFactor){
                actionUtility.hardSleep(30000);
                count++;
                TestManager.getDriver().navigate().refresh();
                actionUtility.waitForPageLoad(10);
            }
            if (outboundCheckInButtons.size()== 0 && count >timeFactor) {
                throw new RuntimeException("Check-In Not enabled");
            }
            reportLogger.log(LogStatus.INFO,"check-in is enabled");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"check-in is not enabled");
            throw e;
        }
    }

    public void verifyCheckInValidationMessageDisplayed(){
        //check if the check-In validation message is displayed
        try {
            Assert.assertTrue(checkInValidationMessage.isDisplayed());
            reportLogger.log(LogStatus.INFO, "check-in Validation message is displayed");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"check-in Validation message is not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to verify the check-in Validation message");
            throw e;
        }
    }

    public void addAPI(){
        //click on addAPI button if present
        try{
            try {
                actionUtility.waitForElementClickable(addAPIButton,5);
                actionUtility.hardClick(addAPIButton);
                actionUtility.waitForPageLoad(10);
            }catch (TimeoutException ex){
                reportLogger.log(LogStatus.INFO,"add api button is not present");
            }
            reportLogger.log(LogStatus.INFO,"successfully moved to add api page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to add api");
            throw e;
        }
    }

    public void editAPI(){
        try{
            actionUtility.waitForElementClickable(changeAPIButtons.get(0),5);
            actionUtility.hardClick(changeAPIButtons.get(0));
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully moved to edit api page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to edit api");
            throw e;
        }
    }

    public void continueToCheckIn(String tripType){
        //clicks on check-in button of depending on inbound or outbound
        try {
            if (tripType.toLowerCase().equals("inbound")) {
                actionUtility.waitForElementClickable(returnCheckInButtons.get(0),5);
                actionUtility.hardClick(returnCheckInButtons.get(0));
            } else if (tripType.toLowerCase().equals("outbound")) {
                actionUtility.waitForElementClickable(outboundCheckInButtons.get(0),5);
                actionUtility.hardClick(outboundCheckInButtons.get(0));
            }
            actionUtility.waitForPageLoad(10);
            actionUtility.hardSleep(3000);
            reportLogger.log(LogStatus.INFO,"successfully continued to check in page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to continue to check in page");
            throw e;
        }
    }

}
