package legacy.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class CheckIn {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CheckIn() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//div[@class='section']/div[@class='row'][1]/span/span[@class='columnWideHeading']")
    private List<WebElement> checkInPassengers;

    @FindBy(xpath = "//div[@class='section'][1]/table//tr/td[2][not(span)]")
    private List<WebElement> noOfFlightsForCheckIn;

    @FindBy(css="input[value='Check In']")
    private WebElement checkIn;

    @FindBy(css="input[value='M']")
    private List<WebElement> genderMale;

    @FindBy(css="input[value='F']")
    private List<WebElement> genderFemale;


    String operatedByLocator = "//div[@class='section'][%d]/table/tbody/tr/td[2][not(span)]";
    String checkInOptionLocator = "//div[@class='section'][%d]/table/tbody/tr/td[6][not(span)]//input[not(@type='hidden')]";
    String loadingLocator ="//div[@class='loading pu']";


    public int selectPassengerAndFlightForCheckIn(Hashtable<String,String> testData) {
        //selects the flight for checkIn for each passenger based on the flight operator
        int count=0;
        try {
            for (int i = 0; i < checkInPassengers.size(); i++) {
                String passengerFlightForCheckIn = testData.get("passenger" + (i + 1) + "OutboundCheckIn");

                if (passengerFlightForCheckIn != null) {
                    String[] FlightNamesForCheckIn = passengerFlightForCheckIn.split(",");

                    for (int j = 0; j < FlightNamesForCheckIn.length; j++) {
                        List<WebElement> checkInFlights = TestManager.getDriver().findElements(By.xpath(String.format(operatedByLocator, (i + 1))));

                        for (int k = 0; k < checkInFlights.size(); k++) {

                            if (checkInFlights.get(k).getText().toLowerCase().contains(FlightNamesForCheckIn[j].toLowerCase())) {
                                if (!TestManager.getDriver().findElements(By.xpath(String.format(checkInOptionLocator, (i + 1)))).get(k).isSelected()) {
                                    actionUtility.selectOption(TestManager.getDriver().findElements(By.xpath(String.format(checkInOptionLocator, (i + 1)))).get(k));
                                    count++;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            reportLogger.log(LogStatus.INFO,"Successfully select the passengers and flights for check-in");
            return count;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the passengers and flights for check-in");
            throw e;
        }
    }

//    public Hashtable<String, Hashtable> captureCheckInDetails(int numberOfPassengers, Hashtable<String,String>flightDetails, int flightNos){
//        // returns the checked in details
//        try {
//            Hashtable<String, Hashtable> overAllCheckedInInfo = new Hashtable<String, Hashtable>();
//            for (int i = 0; i < numberOfPassengers; i++) {
//                List<WebElement> checkInLocators = TestManager.getDriver().findElements(By.xpath(String.format(checkInOptionLocator, (i + 1))));
//                overAllCheckedInInfo.put("passenger" + (i + 1), captureCheckInDetailForEachPassenger(checkInLocators, flightDetails, flightNos));
//            }
//            reportLogger.log(LogStatus.INFO,"successfully to captured the checked-in details");
//            return overAllCheckedInInfo;
//        }catch (Exception e){
//            reportLogger.log(LogStatus.WARNING,"unable to capture the checked-in details");
//            throw e;
//        }
//    }

    private Hashtable<String, Boolean> captureCheckInDetailForEachPassenger(List<WebElement> checkInLocators, Hashtable<String,String>flightDetails, int flightNos) {
        //calls the method to capture the check-in for each flight per passenger
        Hashtable<String, Boolean> checkedInInfo = new Hashtable<String, Boolean>();
        ArrayList<Boolean> isFlightsSelected = captureCheckInDetailForEachFlight(checkInLocators,checkedInInfo,flightNos);


        for (int count = 0; count < flightNos; count++) {
            String flightName = flightDetails.get("flightName" + (count + 1));
            if (!flightName.toLowerCase().contains("air france") || flightName.toLowerCase().contains("hop regional")) {
                checkedInInfo.put("flight"+(count+1),isFlightsSelected.get(0));
                isFlightsSelected.remove(0);
            }else{
                checkedInInfo.put("flightName" + (count+1), false);
            }
        }
        return checkedInInfo;
    }

    private ArrayList<Boolean> captureCheckInDetailForEachFlight(List<WebElement> checkInLocators, Hashtable<String, Boolean> checkedInInfo, int flightNos){
        //capture the check-in for each flight
        ArrayList<Boolean> isFlightSelected = new ArrayList<Boolean>();
        for (int j=0;j<checkInLocators.size();j++){
            if (checkInLocators.get(j).isSelected()){
                isFlightSelected.add(true);
            }else {
                isFlightSelected.add(false);
            }
        }
        return isFlightSelected;
    }

    public void checkIn(){
        //clicks on continue button after selecting the passengers and flights
        try{
            actionUtility.click(checkIn);
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementNotPresent(loadingLocator,30);
            reportLogger.log(LogStatus.INFO,"Successfully continued to check-in");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to check-in");
            throw e;
        }
    }

    public void verifyGenderOfPassengers(String... gender){
        try {
            if (gender.length > 0) {
                for (int i = 0; i < gender.length; i++) {
                    if (gender[i].toLowerCase().equals("male")) {
                        Assert.assertTrue(genderMale.get(i).isSelected());
                        Assert.assertFalse(genderFemale.get(i).isSelected());
                    } else if (gender[i].toLowerCase().equals("female")) {
                        Assert.assertFalse(genderMale.get(i).isSelected());
                        Assert.assertTrue(genderFemale.get(i).isSelected());
                    }
                }
                reportLogger.log(LogStatus.INFO,"genders are matching in the API");
            } else {
                reportLogger.log(LogStatus.WARNING,"expected gender is not provided");
                throw new RuntimeException("expected gender is not provided");
            }
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"genders are not matching in the API");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the gender in APIs");
            throw  e;
        }
    }


}
