package legacy.business_logics;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.List;

public class MyAccountHome {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public MyAccountHome() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id="notYouMessage")
    private WebElement loggedInAs;

    @FindBy(linkText = "My account details")
    private WebElement myAccountDetails;

    @FindBy(name = "gender")
    private  WebElement gender;

    @FindBy(name = "postcode")
    private WebElement postCode;

    @FindBy(id="postalLookupLink")
    private WebElement findAddress;

    @FindBy(id="fancybox-content")
    private WebElement addressListPopup;

    @FindBy(name="paf.selectedAddress")
    private List<WebElement> addresses;

    @FindBy(id="submit_button")
    private WebElement addressContinue;

    @FindBy(id="addressLine1")
    private WebElement addressOne;

    @FindBy(name="save")
    private WebElement save;

    @FindBy(xpath="//th[a[text()='Booking date']]")
    private WebElement bookingDateColumnSortOrder;

    @FindBy(xpath="//a[text()='Booking date']")
    private WebElement bookingDateColumnChangeSort;

    @FindBy(xpath="//table[@id='ca-flights-table']/tbody")
    private WebElement flightsTable;


    @FindBy(css="#ca-flights-table tbody tr>td:nth-of-type(2)>a")
    private List<WebElement> bookingRefs;

    @FindBy(xpath = "//a[text()='Next']")
    private WebElement nextPage;

    @FindBy(xpath="//div[@id='ca-menu-content']/span[4]/a")
    private WebElement myPaymentCardsEle;

    //---------PLD Table------------
    @FindBy(css="#ca-flights-table tbody tr>td:nth-of-type(1)>a")
    private List<WebElement> pldBookingRefs;

    @FindBy(xpath = "//div[p[contains(text(),'Price Lock-Down')]]//table")
    private WebElement flightsPLDTable;


    String bookingLocator ="//a[text()='%s']";

    public void sortByBookingDate(){
        try{
            String initialSortOrder = bookingDateColumnSortOrder.getAttribute("class");
            if (!initialSortOrder.equals("sortable sorted order2")){
                actionUtility.click(bookingDateColumnChangeSort);
                actionUtility.waitForPageLoad(10);
            }
            reportLogger.log(LogStatus.INFO,"successfully changed the sort order of booking date");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change the sort order of the booking date");
            throw e;
        }
    }

    public void verifyLogin() {
        // verify if the login is successful
        try {
            Assert.assertTrue(loggedInAs.isDisplayed(), "login unsuccessful");
            reportLogger.log(LogStatus.INFO, "login successful");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING, "login unsuccessful");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify login");
            throw e;
        }
    }

    public void verifyBookingInMyAccount(String bookingRefNo){
        // verifies the booking reference number in the my live flights list in my account page
        try {
            int attempt =0;
            boolean flag = true;
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(flightsTable,20);

            while(bookingRefs.size() <= 0 && attempt<3){
                actionUtility.hardSleep(2000);
                attempt++;
            }

            try {
                TestManager.getDriver().findElement(By.xpath(String.format(bookingLocator, bookingRefNo)));
            }catch (NoSuchElementException e){
                flag = false;
            }

            attempt =0;

            if(!flag){
                while(actionUtility.verifyIfElementIsDisplayed(nextPage)) {
                    actionUtility.click(nextPage);
                    while (bookingRefs.size() <= 0 && attempt<3) {
                        actionUtility.hardSleep(2000);
                        attempt++;
                    }

                    try {
                        TestManager.getDriver().findElement(By.xpath(String.format(bookingLocator, bookingRefNo)));
                        flag =true;
                        break;
                    }catch (NoSuchElementException e){
                        flag = false;
                    }
                    attempt = 0;
                }

            }

            if(flag){
                reportLogger.log(LogStatus.INFO,"booking reference number found in my live flights list in my account page");
            }else{
                reportLogger.log(LogStatus.WARNING,"booking reference number not found in my live flights list in my account page");
                throw new RuntimeException("booking reference number not found in my live flights list in my account page");

            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify booking reference number in my live flights list in my account page");
            throw e;
        }
    }

    public void continueToMyAccountDetails(){
        // opens my account details page
        try {
            actionUtility.click(myAccountDetails);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"successfully navigated to the my accounts details");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to the my accounts details");
            throw e;
        }
    }

    public void verifyEditingAccountDetails(String[]editedDetails){
//        Verifies the edited account details--takes the parameter String array which has come from editAccountDetails method
        try{
            Assert.assertEquals(editedDetails[0],new Select(gender).getFirstSelectedOption().getText());
            Assert.assertEquals(editedDetails[1],postCode.getAttribute("value"));

            Assert.assertEquals(editedDetails[2],addressOne.getAttribute("value"));
            reportLogger.log(LogStatus.INFO,"edited details are matching");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"edited details are not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify edited details");
            throw e;
        }

    }

    public String[] editAccountDetails(){
        // edits the gender, postcode and address
        try {
            String[] editedDetails = new String[3];
            editedDetails[0] = editGender();
            String[] editedAddress = editAddress();
            editedDetails[1] = editedAddress[0];
            editedDetails[2] = editedAddress[1];
            reportLogger.log(LogStatus.INFO,"successfully edited the account details");
            actionUtility.click(save);
            actionUtility.waitForPageLoad(10);
            return editedDetails;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to edit the account details");
            throw e;
        }
    }

    private String editGender(){
        //edits gender
        String  editedGender =null;
        String optionSelected = new Select(gender).getFirstSelectedOption().getText();
        if (optionSelected.toLowerCase().equals("male")){
            actionUtility.dropdownSelect(gender, ActionUtility.SelectionType.SELECTBYTEXT,"Female");
            editedGender = "Female";
        }else {
            actionUtility.dropdownSelect(gender, ActionUtility.SelectionType.SELECTBYTEXT,"Male");
            editedGender = "Male";
        }
        return editedGender;
    }

    private String[] editAddress(){
//        edit post code
        String presentPostCode = postCode.getAttribute("value");
        String address[] = new String[2];
        if(presentPostCode.equals("AL74FG")){
            postCode.clear();
            postCode.sendKeys("NW16XE");
            address[0] = "NW16XE";
            address[1] = selectContactAddress();

        }else {
            postCode.clear();
            postCode.sendKeys("AL74FG");
            address[0]  = "AL74FG";
            address[1] = selectContactAddress();
        }
        return address;
    }

    private String selectContactAddress(){
        //selects the address from the list popup
        actionUtility.click(findAddress);
        actionUtility.waitForElementVisible(addressListPopup, 5);
        actionUtility.selectOption(addresses.get(7));
        actionUtility.click(addressContinue);
        actionUtility.hardSleep(3000);
        String addressSelected = addressOne.getAttribute("value");
        return addressSelected;
    }

    public void navigateToMyPaymentCardsPage()
    {   //clicks Payment Cards link
        try {
            actionUtility.click(myPaymentCardsEle);
            reportLogger.log(LogStatus.INFO,"successfully navigated to my payment cards page");
        }
        catch(Exception e)
        {
            reportLogger.log(LogStatus.WARNING,"unable to navigate to my payment cards page");
            throw e;
        }

        //actionUtility.waitForElementVisible();
    }

    public void verifyPLDBookingInMyAccountANDnavigateToYourDetailsPage(String pldBookingRefNo){
        // verifies the pld booking reference number in the price-lock down held bookings in my account page
        try {
            boolean flag = false;
            WebElement pldRef = pldBookingRefs.get(0);
            actionUtility.waitForPageLoad(10);
            actionUtility.waitForElementVisible(flightsPLDTable,20);
            int pldsize = pldBookingRefs.size();
            try{
                for(int i=0;i<pldsize;i++){
                    if(pldBookingRefs.get(i).getText().contains(pldBookingRefNo)){
                        flag = true;
                        pldRef = pldBookingRefs.get(i);
                        break;
                    }
                }
                if(flag){
                    reportLogger.log(LogStatus.INFO,"PLD booking reference number "+pldBookingRefNo+"found in price-lock down held bookings in my account page");
                }
                Assert.assertTrue(flag,"PLD booking reference no. is not present");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.WARNING,"PLD booking reference number not found in price-lock down held bookings in my account page");
                throw e;
            }
            try {
                actionUtility.click(pldRef);
                actionUtility.waitForPageLoad(10);
            }catch(Exception e){
                reportLogger.log(LogStatus.WARNING,"unable to click on PLD booking reference number in price-lock down held bookings in my account page");
                throw e;
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify PLD booking reference number in price-lock down held bookings in my account page");
            throw e;
        }
    }


}
