package legacy.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class BoardingPass {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public BoardingPass() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/descendant::span[contains(text(),'Record locator')]/following-sibling::span")
    private java.util.List<WebElement> boardingPassBookingReferenceNos;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/fieldset[2]/div[2]/span[1]")
    private java.util.List<WebElement> boardingPassFlightNos;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/fieldset[2]/div[2]/span[3]")
    private java.util.List<WebElement> boardingPassDepartureTimes;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/fieldset[2]/div[2]/span[4]")
    private java.util.List<WebElement> boardingPassSeatNos;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/fieldset[2]/div[2]/span[6]")
    private java.util.List<WebElement> boardingPassBoardingTimes;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/fieldset[1]/div[4]/span[4]")
    private java.util.List<WebElement> boardingPassFlightsOperatedBys;

    @FindBy(xpath = "//div[@class='voucher' and not(@style)]/img[3][contains(@style,'float')]")
    private java.util.List<WebElement> boardingPassQRCodes;

    @FindBy(xpath = "//div[img[@alt='Travel Information']]/descendant::span[contains(text(),'Record locator')]/following-sibling::span")
    private java.util.List<WebElement> travelInformationBookingReferenceNos;

    @FindBy(xpath = "//div[img[@alt='Travel Information']]/fieldset[2]/div[2]/span[2]")
    private java.util.List<WebElement> travelInformationFlightNos;

    @FindBy(xpath = "//div[img[@alt='Travel Information']]/fieldset[2]/div[2]/span[4]")
    private java.util.List<WebElement> travelInformationDepartureTimes;

    @FindBy(xpath = "//div[img[@alt='Travel Information']]/fieldset[2]/div[2]/span[5]")
    private java.util.List<WebElement> travelInformationSeatNos;

    public void handlePrintDialog(){
        //calls the auto it script
        try {
//            new DelayedPressEnterThread("DelayedPressEnterThread",10000);
//            String exePath = "src/test/resources/autoIT/PrintDialog2.exe";
//            Runtime.getRuntime().exec(exePath);
            actionUtility.hardSleep(5000);
            reportLogger.log(LogStatus.INFO,"print dialog closed");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to close the print dialog");
            throw new RuntimeException("unable to close the print dialog");

        }

    }

    public void verifyBoardingPass(Hashtable<String,String> testData, int flightNos,int noOfPassengers, String bookingRefNo, Hashtable<String,String> flightDetails,Hashtable<String,Hashtable>selectedSeats, Hashtable<String,Hashtable>passData){
        //verifies the various details in the boarding pass,like flight no.,flight depart time, seat selected etc.
        try {
            String infants = testData.get("noOfInfant");
            int noOfInfants;
            if(infants != null){
                noOfInfants=Integer.parseInt(infants);
            }else {
                noOfInfants = 0;
            }
            int boardingPassNo=-1;

            for (int i = 0; i < noOfPassengers; i++) {

                for (int j = 0; j < flightNos; j++) {

                    if ((Boolean) passData.get("passenger" + (i + 1)).get("flight" + (j + 1))) {
                        boardingPassNo++;
                        verifyBookingRefNo(boardingPassNo, bookingRefNo);
                        verifyFlightDetailsInBoardingPass(boardingPassNo, (j + 1), flightDetails);
                        verifyFlightDetailsInBoardingPassTravelInformation(boardingPassNo, (j + 1), flightDetails);
                        verifySeatNumber(boardingPassNo, (j + 1), (i + 1), selectedSeats, false);

                        if (noOfInfants >= (i+1)) {
                            boardingPassNo++;
                            verifyBookingRefNo(boardingPassNo, bookingRefNo);
                            verifyFlightDetailsInBoardingPass(boardingPassNo, (j + 1), flightDetails);
                            verifySeatNumber(boardingPassNo, (j + 1), (i + 1), selectedSeats, true);
                        }
                    }
                }
            }
            int actualQRCodes = boardingPassQRCodes.size();
            int expectedQRCodes = boardingPassNo+1;
            Assert.assertEquals(actualQRCodes,expectedQRCodes,"QR codes are not matching");
            reportLogger.log(LogStatus.INFO,"successfully verified boarding passes");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify boarding passes");
            throw e;
        }
    }

    private void verifyBookingRefNo(int boardingPassNo, String bookingRefNo){
        //verifying the booking ref. no. in boarding pass and travel information

        try {
            Assert.assertTrue(boardingPassBookingReferenceNos.get(boardingPassNo).getText().equals(bookingRefNo), "Booking Ref No. in boarding pass not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in Boarding Pass-"+(boardingPassNo+1)+"is matching");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in Boarding Pass-"+(boardingPassNo+1)+"is not matching");
            throw e;
        }

        try {
            Assert.assertTrue(travelInformationBookingReferenceNos.get(boardingPassNo).getText().equals(bookingRefNo), "Booking Ref No. in boarding pass not matching");
            reportLogger.log(LogStatus.INFO,"Booking Ref No. in Boarding Pass-"+(boardingPassNo+1)+"- travel information is matching");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Booking Ref No. in Boarding Pass-"+(boardingPassNo+1)+" - travel information is not matching");
            throw e;
        }

    }

    private void verifyFlightDetailsInBoardingPass(int boardingPassNo, int flightNo, Hashtable<String,String>flightDetails) {
        try {
            String actualFlightNo = boardingPassFlightNos.get(boardingPassNo).getText().trim();
            String expectedFlightNo = flightDetails.get("flightNo" + flightNo).trim();
            Assert.assertEquals(actualFlightNo, expectedFlightNo, "Flight No. is not matching in Boarding Pass-" + (boardingPassNo + 1));

            String actualDepartureTime = boardingPassDepartureTimes.get(boardingPassNo).getText().trim();
            String expectedDepartureTime = flightDetails.get("flightDepartTime" + flightNo).substring(0, 5);
            Assert.assertEquals(actualDepartureTime, expectedDepartureTime, "Flight Depart time is not matching in Boarding Pass-" + (boardingPassNo + 1));

            String actualBoardingTime = boardingPassBoardingTimes.get(boardingPassNo).getText().trim();
            Assert.assertEquals(getTimeDifference(actualDepartureTime, actualBoardingTime),"00:30", "Boarding time is incorrect in Boarding Pass-" + (boardingPassNo + 1));

            String actualOperatedBy = boardingPassFlightsOperatedBys.get(boardingPassNo).getText().trim();
            String expectedOperatedBy = flightDetails.get("flightName" + flightNo).trim();
            Assert.assertEquals(actualOperatedBy, expectedOperatedBy, "Operated By is not matching in Boarding Pass-" + (boardingPassNo + 1));
            reportLogger.log(LogStatus.INFO, "Flight details are matching in Boarding Pass-" + (boardingPassNo + 1));

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "Flight details are not matching in Boarding Pass-" + (boardingPassNo + 1));
            throw e;
        }
    }

    private void verifyFlightDetailsInBoardingPassTravelInformation(int boardingPassNo, int flightNo, Hashtable<String,String>flightDetails){
        try {
            String actualFlightNo = travelInformationFlightNos.get(boardingPassNo).getText().trim();
            String expectedFlightNo = flightDetails.get("flightNo" + flightNo).trim();
            Assert.assertEquals(actualFlightNo, expectedFlightNo, "Flight No. is not matching in Boarding Pass-" + (boardingPassNo+1));

            String actualDepartureTime = travelInformationDepartureTimes.get(boardingPassNo).getText().trim();
            String expectedDepartureTime = flightDetails.get("flightDepartTime" + flightNo).substring(0, 5);
            Assert.assertEquals(actualDepartureTime, expectedDepartureTime, "Flight Depart time is not matching in Boarding Pass-" + (boardingPassNo+1));

            String actualOperatedBy = boardingPassFlightsOperatedBys.get(boardingPassNo).getText().trim();
            String expectedOperatedBy = flightDetails.get("flightName" + flightNo).trim();
            Assert.assertEquals(actualOperatedBy, expectedOperatedBy, "Operated By is not matching in Boarding Pass-" + (boardingPassNo+1));
            reportLogger.log(LogStatus.INFO,"Flight details are matching in Boarding Pass-"+(boardingPassNo+1));

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Flight details are not matching in Boarding Pass-"+(boardingPassNo+1));
            throw e;
        }
    }

    private String getTimeDifference(String departureTime, String boardingTime){
        // returns departureTime-boardingTime
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        String timeDiff = null;
        try {
            Date departTime = parser.parse(departureTime);
            Date boardTime = parser.parse(boardingTime);
            long timeDifference  =(departTime.getTime() - boardTime.getTime());
            timeDiff = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeDifference), TimeUnit.MILLISECONDS.toMinutes(timeDifference) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDifference)));

        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException ("unable to subtract the time");
        }
       return timeDiff;
    }

    private  void verifySeatNumber(int boardingPassNo,int flightNo, int passengerNo, Hashtable<String,Hashtable> selectedSeats, boolean infantFlag){
        // verifies the seat in boarding pass and travel information
        try {
            String actualSelectedSeatInBoardingPass = boardingPassSeatNos.get(boardingPassNo).getText().trim();
            String actualSelectedSeatInBoardingPassTravelInformation = travelInformationSeatNos.get(boardingPassNo).getText().trim();
            String expectedSeat = null;

            if (!infantFlag) {
                expectedSeat = ((String)selectedSeats.get("passenger" + passengerNo).get("flight" + flightNo)).trim();
            } else {
                expectedSeat = "INF";
            }

            if (expectedSeat != null) {

                try {
                    Assert.assertEquals(expectedSeat, actualSelectedSeatInBoardingPass, "seat is not matching in Boarding Pass" + (boardingPassNo + 1));
                    reportLogger.log(LogStatus.INFO,"seat is matching in Boarding Pass-"+(boardingPassNo+1));
                }catch (Exception ex){
                    reportLogger.log(LogStatus.WARNING,"seat is not matching in Boarding Pass-"+(boardingPassNo+1));
                    throw ex;
                }

                try {
                    Assert.assertEquals(expectedSeat, actualSelectedSeatInBoardingPassTravelInformation, "seat is not matching in Boarding Pass Travel Information" + (boardingPassNo+1));
                    reportLogger.log(LogStatus.INFO,"seat is matching in Boarding Pass-"+(boardingPassNo+1)+ " travel information");
                }catch (Exception ex){
                    reportLogger.log(LogStatus.WARNING,"seat is not matching in Boarding Pass-"+(boardingPassNo+1)+" travel information");
                    throw ex;
                }
            }

        }catch (Exception ex){
            reportLogger.log(LogStatus.WARNING,"unable to verify seats in Boarding Pass-"+(boardingPassNo+1+" seat might not be matching"));
            throw ex;

        }
    }

    private void verifyPassengerName(Hashtable<String,String> testData,int boardingPassNo,int noOfPassengers){
        // in progress

        int noOfPassengerVerified=0;
        String expectedName;

        if (testData.get("noOfAdults") != null){
            if (Integer.parseInt(testData.get("noOfAdults")) != 1){
                if (noOfPassengers < Integer.parseInt(testData.get("noOfAdults"))) {
                    if(!testData.get("iAmPassenger").toLowerCase().equals("true")) {
                        expectedName = testData.get("adultName" + (noOfPassengers - noOfPassengerVerified));
                    }else {

                    }
                }
            }else {

            }
        }else {

        }


    }

}
