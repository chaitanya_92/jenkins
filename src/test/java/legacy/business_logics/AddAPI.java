package legacy.business_logics;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Hashtable;
import java.util.List;

public class AddAPI {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public AddAPI() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css ="select[id^='dateOfBirthDay']")
    private List<WebElement> birthDays;

    @FindBy(css ="select[id^='dateOfBirthMonth']")
    private List<WebElement> birthMonths;

    @FindBy(css ="select[id^='dateOfBirthYear']")
    private List<WebElement> birthYears;

    @FindBy(css ="input[id^='passportNumber']")
    private List<WebElement> passportNumbers;

    @FindBy(css ="select[id^='nationality']")
    private List<WebElement> nationalities;

    @FindBy(css ="select[id^='country']")
    private List<WebElement> issuingCountries;

    @FindBy(css ="select[id^='country']")
    private List<WebElement> countries;

    @FindBy(css ="select[id^='expiryDateDay']")
    private List<WebElement> expiryDays;

    @FindBy(css ="select[id^='expiryDateMonth']")
    private List<WebElement> expiryMonths;

    @FindBy(css ="select[id^='expiryDateYear']")
    private List<WebElement> expiryYears;

    @FindBy(css ="a#continueButton")
    private WebElement submitButton;

    @FindBy(css="label[for$='M']+input")
    private List<WebElement> genderMale;

    @FindBy(css="label[for$='F']+input")
    private List<WebElement> genderFemale;

    @FindBy(xpath="//a[text()='Change']")
    private List<WebElement> changeAPIs;

    @FindBy(css="span.account>strong")
    private List<WebElement> editedPassportNumbers;


    String pageTitle="Advance Passenger Information (API)";

    public void editAPIDetails(Hashtable<String,String> testData){
        // edit the passport info in api page
        try {
            actionUtility.hardSleep(3000);
            System.out.println(changeAPIs.size());
            for (int i = 0; i < changeAPIs.size(); i++) {
                actionUtility.click(changeAPIs.get(i));
                {
                    actionUtility.hardSleep(4000);
                    passportNumbers.get(i).clear();
                    passportNumbers.get(i).sendKeys(testData.get("editedApiPassportNumber"));
                }
            }
            enterBirthDate(testData.get("apiBirthDate"));
            enterNationality(testData.get("apiNationality"));
            enterIssuingCountry(testData.get("apiIssuingCountry"));
            enterExpiryDate(testData.get("apiExpiryDate"));
            actionUtility.click(submitButton);
            reportLogger.log(LogStatus.WARNING,"successfully edited the API info");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to edit the API info");
            throw e;
        }
    }

    public void verifyEditedPassportNumber(Hashtable<String,String> testData){
        try{
            for(WebElement editedPassportNumber:editedPassportNumbers){
                Assert.assertTrue(testData.get("editedApiPassportNumber").toLowerCase().contains(editedPassportNumber.getText().toLowerCase()));
//                Assert.assertTrue(editedPassportNumber.getText().toLowerCase().equals(testData.get("editedApiPassportNumber")));
            }
            reportLogger.log(LogStatus.INFO,"API passport number is matching with expected value after editing");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"API passport number is not matching with expected value after editing");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the passport number after editing");
            throw e;
        }
    }

    public void verifyGenderOfPassengers(String... gender){
        try {
            if (gender.length > 0) {
                for (int i = 0; i < gender.length; i++) {
                    if (gender[i].toLowerCase().equals("male")) {
                        Assert.assertTrue(genderMale.get(i).isSelected());
                        Assert.assertFalse(genderFemale.get(i).isSelected());
                    } else if (gender[i].toLowerCase().equals("female")) {
                        Assert.assertFalse(genderMale.get(i).isSelected());
                        Assert.assertTrue(genderFemale.get(i).isSelected());
                    }
                }
                reportLogger.log(LogStatus.INFO,"genders are matching in the API");
            } else {
                reportLogger.log(LogStatus.WARNING,"expected gender is not provided");
                throw new RuntimeException("expected gender is not provided");
            }
        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"genders are not matching in the API");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the gender in APIs");
            throw  e;
        }
    }

    public void addAPIDetails(Hashtable<String,String>testData){
        //enters the API details
        try {
            if (TestManager.getDriver().getTitle().toLowerCase().contains(pageTitle.toLowerCase())) {
                enterBirthDate(testData.get("apiBirthDate"));
                enterPassportNumber(testData.get("apiPassportNumber"));
                enterNationality(testData.get("apiNationality"));
                enterIssuingCountry(testData.get("apiIssuingCountry"));
                enterExpiryDate(testData.get("apiExpiryDate"));
                actionUtility.click(submitButton);
                actionUtility.waitForPageLoad(10);
   }
            reportLogger.log(LogStatus.INFO,"successfully entered API info");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter API info");
            throw e;
        }

    }

    private void enterBirthDate(String birthDate){
        // selects the birth date in the dropdowns
        String[]birthDates =birthDate.split(" ");
        enterBirthDay(birthDates[0]);
        enterBirthMonth(birthDates[1]);
        enterBirthYear(birthDates[2]);
    }

    private void enterBirthDay(String day){
//        selects the birth day in the dropdown
        for(WebElement birthDay: birthDays){
            actionUtility.dropdownSelect(birthDay, ActionUtility.SelectionType.SELECTBYTEXT,day);
        }

    }

    private void enterBirthMonth(String month){
        //        selects the birth month in the dropdown
        for(WebElement birthMonth: birthMonths){
            actionUtility.dropdownSelect(birthMonth, ActionUtility.SelectionType.SELECTBYTEXT,month);
        }

    }

    private void enterBirthYear(String year){
        //selects the birth year in the dropdown
        for(WebElement birthYear: birthYears){
            actionUtility.dropdownSelect(birthYear, ActionUtility.SelectionType.SELECTBYTEXT,year);
        }

    }

    private void enterPassportNumber(String number){
        //enters the passportNUmber
        for(WebElement passportNumber: passportNumbers){
            passportNumber.sendKeys(number);
        }

    }

    private void enterNationality(String nation){
//        selects the Nationality in the dropdown
        for(WebElement nationality: nationalities){
            actionUtility.dropdownSelect(nationality, ActionUtility.SelectionType.SELECTBYTEXT,nation);
        }
    }

    private void enterIssuingCountry(String countryName){
//        selects the issuing country in the dropdown
        for(WebElement country: issuingCountries){
            actionUtility.dropdownSelect(country, ActionUtility.SelectionType.SELECTBYTEXT,countryName);
        }
    }

    private void enterExpiryDate(String expiryDate){
//        selects expiry date in the drop down
        String[] expiryDates = expiryDate.split(" ");
        enterExpiryDay(expiryDates[0]);
        enterExpiryMonth(expiryDates[1]);
        enterExpiryYear(expiryDates[2]);

    }

    private void enterExpiryDay(String day){
//        selects expiry day in the dropdown
        for(WebElement expiryDay: expiryDays){
            actionUtility.dropdownSelect(expiryDay, ActionUtility.SelectionType.SELECTBYTEXT,day);
        }

    }

    private void enterExpiryMonth(String month){
//        selects expiry month in the dropdown
        for(WebElement expiryMonth: expiryMonths){
            actionUtility.dropdownSelect(expiryMonth, ActionUtility.SelectionType.SELECTBYTEXT,month);
        }

    }

    private void enterExpiryYear(String year){
//        selects the expiry year in the dropdown
        for(WebElement expiryYear: expiryYears){
            actionUtility.dropdownSelect(expiryYear, ActionUtility.SelectionType.SELECTBYTEXT,year);
        }

    }


}
